module.exports = {
  
  
  /* Developement/Testing Mode
     gcp_heathcare_info: {
      cloudRegion: 'us-central1',
      projectId: 'semiotic-tracer-279121',
      datasetId: 'curamei-dataset',
      fhirStoreId: 'curamei-fhir-store'
   },

   */
  
  
  /* Production Mode */
    gcp_heathcare_info: {
        cloudRegion: 'us-central1',
        projectId: 'semiotic-tracer-279121',
        datasetId: 'curamei-dataset-prod',
        fhirStoreId: 'curamei-fhir-store'
     },

     gcp_default_data: {
        CodeableConcept : {
          coding:[
                {
                    "system": "http://app.curameitech.com/fhir",
                    "code": "00000000",
                    "display": null
                }
          ],
          text: "default"
        },
        arraySingleObj: [
          {
            text: "default"
          }
        ],
        },
        prescriber: {
          "display": "default",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/T-kmjPGEVPAmnBfmx56HsKgB"
        },
        medicationReference: {
          "display": "default",
          "reference": "https://open-ic.epic.com/FHIR/api/FHIR/DSTU2/Practitioner/T-kmjPGEVPAmnBfmx56HsKgB"
        },
        dosageInstruction: [{
          "text": "default",
          "asNeededBoolean": true,
          "method": {
            "text": "default",
            "coding": [
              {
                "system": "urn:oid:1.2.840.114350.1.13.0.1.7.4.798268.8600",
                "code": "11",
                "display": "default"
              }
            ]
          },
          "timing": {
            "repeat": {
              "frequency": 1,
              "period": 1,
              "periodUnits": "d",
              "boundsPeriod": {
                "start": "2016-11-09T00:00:00Z"
              }
            }
          },
          "doseQuantity": {
            "value": 0,
            "unit": "tablet"
          }
        },
        ],
        dosageInstructionWithAdditional: [{
          "text": "default",
          "asNeededBoolean": true,
          "method": {
            "text": "default",
            "coding": [
              {
                "system": "urn:oid:1.2.840.114350.1.13.0.1.7.4.798268.8600",
                "code": "11",
                "display": "default"
              }
            ]
          },
          "timing": {
            "repeat": {
              "frequency": 1,
              "period": 1,
              "periodUnits": "d",
              "boundsPeriod": {
                "start": "2016-11-09T00:00:00Z"
              }
            }
          },
          "dispenseRequest": {
            "validityPeriod": {
              "start": "2016-11-15T00:00:00Z",
              "end": "2016-11-23T00:00:00Z"
            }
          },
          "additionalInstructions":{
            coding:[
              {
                  "system": "http://app.curameitech.com/fhir",
                  "code": "00000000",
                  "display": null
              }
            ],
            text: "default"
          }
        }
      ]  
}