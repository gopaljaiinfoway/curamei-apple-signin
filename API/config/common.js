//Error type code
var errorTypeEnum = {
    Warning: 1,
    Error: 2,
    Info: 3
}
//Action code for digital health
var actionEnum = {
    MedicalRecords: 10,
    MedicalResourceId: 1,
    ManualUpload:3,
    Fitbit:2,
    Narrative:4
}
//Config key for fitbit
var _secretKey = "fit-bit-config";

//Developement/Testing Mode
 //var bucketFileName = 'JSON-268610-1e41dfef1ada.json';
 // var bucketName = 'curameidatasource';

//Production Mode
var bucketFileName = 'JSON-268610-1e41dfef1ada.json';
var bucketName = 'curameidatasource-prod';

exports.errorTypeEnum = errorTypeEnum;
exports._secretKey = _secretKey;
exports.actionEnum = actionEnum;
exports.bucketFileName = bucketFileName;
exports.bucketName = bucketName;
