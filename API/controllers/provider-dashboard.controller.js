
import axios from "axios";
import { createClient } from '../helper';
import { addErrorLog } from './errorLog.controller';





// count or percentage of  Share resource by user (For Provider Dashboard)
export const getPatientProviderPermission = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    const result = await client.query(`select mu.fullname,string_agg(mr.resource_name::text, ',') resource_name,
    string_agg(mr.resource_id::text, ',') resource_id
    from m_users mu join
    users_providers up on mu.id = up.uid 
    left join patient_provider_resource ppr on mu.id = ppr.pat_id
    left join m_resource mr on ppr.resource_id = mr.resource_id
    where up.pid=$1 and up.usertypeid=$2
     group by mu.fullname order by mu.fullname ASC`, [req.user.pid,2])
    await client.end();
    var medicalRecordsChart = [];
    var digitalRecordsChart = [];
    var uploadRecordsChart = [];
    var resourceId = [];
    if(result.rowCount > 0){
            var toalMedicalPermissionCount =0;
            var toalMedicalNotPermissionCount =0;
            var toalDigitalPermissionCount =0;
            var toalDigitalNotPermissionCount =0;
            var toalUploadPermissionCount =0;
            var toalUploadNotPermissionCount =0;
         for(let obj of  result.rows){
           var resourceIdGet = obj.resource_id;
           if(resourceIdGet && resourceIdGet.length > 0){
            var resourceId = resourceIdGet.split(","); 
            for(let i=0 ; i < resourceId.length; i++){
              if(resourceId[i] =='1' || resourceId[i] == 1 )
              toalMedicalPermissionCount++;
              else
              toalMedicalNotPermissionCount++;
              if(resourceId[i] =='2' || resourceId[i] ==2)
              toalDigitalPermissionCount++;
             else
             toalDigitalNotPermissionCount++;
             if(resourceId[i] =='3' || resourceId[i] ==3)
             toalUploadPermissionCount++;
            else
            toalUploadNotPermissionCount++;
            }
           }
    
         }


         var toalMedicalPermissionCountPer =  (toalMedicalPermissionCount * 100) / (result.rows.length);
         var toalMedicalNotPermissionCountPer =  100 - toalMedicalPermissionCountPer;

         var toalDigitalPermissionCountPer =  (toalDigitalPermissionCount * 100) / (result.rows.length);
         var toalDigitalNotPermissionCountPer =  100 - toalDigitalPermissionCountPer;

         var toalUploadPermissionCountPer =  (toalUploadPermissionCount * 100) / (result.rows.length);
         var toalUploadNotPermissionCountPer =  100 - toalUploadPermissionCountPer;


         medicalRecordsChart.push(
          { "name": "Shared Pecentage","value": parseFloat(toalMedicalPermissionCountPer).toFixed(2)},
          { "name": "Not Shared Pecentage","value":  parseFloat(toalMedicalNotPermissionCountPer).toFixed(2)},
          );
          digitalRecordsChart.push(
            { "name": "Shared Pecentage","value": parseFloat(toalDigitalPermissionCountPer).toFixed(2)},
            { "name": "Not Shared Pecentage","value":  parseFloat(toalDigitalNotPermissionCountPer).toFixed(2)},
            );
            uploadRecordsChart.push(
              { "name": "Shared Pecentage","value": parseFloat(toalUploadPermissionCountPer).toFixed(2)},
              { "name": "Not Shared Pecentage","value":  parseFloat(toalUploadNotPermissionCountPer).toFixed(2)},
              );
              var chartData = {
                  medicalChart : medicalRecordsChart,
                  digitalChart : digitalRecordsChart,
                 uploadChart : uploadRecordsChart
                };
        return res.json({ 'success': true, 'data': result.rows,'chartData': chartData });
        }else{
      return res.json({ 'success': false, 'data': 'Record is not available' });
        }
  }
  catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message })
  }
}

// Get get Support Provider Permission
export const getSupportProviderPermission = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    const result = await client.query(` select  mu.fullname,string_agg(mr.resource_name::text, ',') resource_name from m_users mu join
    users_providers up on mu.id = up.uid 
    left join patient_provider_resource ppr on mu.id = ppr.pat_id
    left join m_resource mr on ppr.resource_id = mr.resource_id
    where up.pid=$1 and up.usertypeid=$2
     group by mu.fullname order by mu.fullname ASC`, [req.user.pid,3])
    await client.end();
     if(result.rowCount > 0){
      return res.json({ 'success': true, 'data': result.rows });
     }else{
      return res.json({ 'success': false, 'data': 'Record is not available' });
     }
  
  }
  catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message })
  }
}








