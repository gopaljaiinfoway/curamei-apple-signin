import { createClient } from '../helper';
const SimpleCrypto = require("simple-crypto-js").default;
import { _secretKey } from '../config/common';
// Add new refresh token each time
var addFitbitRefreshToken = async function (refresh_token) {
  var client = await createClient();
  try {

    client.connect();
    await client.query(`UPDATE fitbit_refresh_token SET is_active=$1 WHERE is_active=$2`, [false, true]);
     //Insert fitbit_refresh_token information
    var sql = 'INSERT INTO fitbit_refresh_token(refresh_token, create_date, is_active) VALUES($1, $2, $3) RETURNING *';
    var params = [refresh_token,new Date(),true];
    await client.query(sql, params);
    await client.end();
    return true;
  } catch (err) {
    console.log('---Error in adding fitbit refresh token---------------')
    await client.end();
    return false;
  }
}

// Get new refresh token
var getFitbitRefreshToken = async function () {
  var client = await createClient();
  try {
   
    client.connect();
    const resultData = await client.query(`SELECT refresh_token from fitbit_refresh_token  WHERE is_active=$1 ORDER BY create_date DESC Limit 1`, [true]);
    await client.end();
          if(resultData.rows.length > 0)
          return resultData.rows[0].refresh_token;
          else
          return null;
  } catch (err) {
    console.log(err.message);
    //await client.end();
    return null;
  }
}






// Get new refresh token
var getFitbitRefreshTokenTest = async function () {
  var client = await createClient();
  try {
   
    client.connect();
    const resultData = await client.query(`SELECT refresh_token from fitbit_refresh_token ORDER BY create_date DESC Limit 1  WHERE is_active=$1`, [true]);
    await client.end();
          if(resultData.rows.length > 0){
            var simpleCrypto = new SimpleCrypto(_secretKey);
            var fitbitObj = simpleCrypto.decryptObject(resultData.rows[0].refresh_token);
            return fitbitObj.refresh_token;
          }
          else
          return null;
  } catch (err) {
    console.log(err.message);
    //await client.end();
    return null;
  }
}

export {addFitbitRefreshToken,getFitbitRefreshToken} ;