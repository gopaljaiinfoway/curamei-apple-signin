
import { createClient } from '../helper';
import { addErrorLog } from './errorLog.controller';
const pg = require("pg");


//User list by usertype
export const ListDataRead = async function (req, res) {
    try {
        var client = await createClient();
        client.connect();
        const text = `select mu.*,mr.name from m_users mu inner join m_user_types mr on mu.usertypeid = mr.id 
                       where mu.usertypeid in(1,2,3) order by mu.usertypeid ASC`;
        const dataStatus = await client.query(text);
        await client.end();
        if (dataStatus.rows) {
            return res.json({ 'success': true, 'data': dataStatus.rows });
        } else {
            return res.json({ 'success': false, 'message': 'Role not found' });
        }
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}
// List by email
export const ListDataByEmailRead = async function (req, res) {
    try {
        var filterData = '';
        if (req.params.id) {
            filterData = req.params.id;
        } else {
            filterData = '';
        }
        var client = await createClient();
        client.connect();
        const text = `select mu.*,mr.name from m_users mu inner join m_user_types mr on mu.usertypeid = mr.id 
                       where mu.usertypeid in(1,2,3) and mu.email like LOWER($1) order by mu.usertypeid ASC`;
        const values = ["%" + filterData + "%"];
        const dataStatus = await client.query(text, values);
        console.log(dataStatus.rows);
        await client.end();
        if (dataStatus.rows) {
            return res.json({ 'success': true, 'data': dataStatus.rows });
        } else {
            return res.json({ 'success': false, 'message': 'Role not found' });
        }
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}
//Get total count records for dashboard display
export const countRecord = async function (req, res) {
    try {
        var client = await createClient();
        client.connect();
        //Get total count records for dashboard display
        const text = `select  
                    (select count(state_id) from m_providers group by state_id) as statetotal,
                     (select count(pid) from m_providers) as provideruserscount,
                    (select count(id) from m_users where usertypeid=2) as patientuserscount,
                    (select count(id) from m_users where usertypeid=3) as supportuserscount,
                    (select count(id) from m_users where usertypeid in(1,2)) as accounttotal,
                    (select count(nodays) from
                    (select current_date - last_login::date as nodays
                    from m_users) AS a
                    where nodays < 7) as activetotal`;
        const dataStatus = await client.query(text);
        await client.end();
        if (dataStatus.rows) {
            return res.json({ 'success': true, 'data': dataStatus.rows });
        } else {
            return res.json({ 'success': false, 'message': 'Records not found' });
        }
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}
//Update enrollment flag
export const EnrollmentUpdate = async function (req, res) {
    try {
        var client = await createClient();
        client.connect();
        //Update enrollment flag
        const text = `update m_users set enrollment_flg =true where id = $1`;
        const value = [req.user.id];
        const dataStatus = await client.query(text, value);
        await client.end();
        if (dataStatus.rows) {
            return res.json({ 'success': true, 'message': 'Enrollment  updated successfully' });
        } else {
            return res.json({ 'success': false, 'message': 'Enrollment not updated successfully' });
        }
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}





