
import axios from "axios";
import { createClient } from '../helper';
import { addErrorLog } from './errorLog.controller';
var format = require('date-format');




// Get single provider from list by id
export const getPatientMedicalRecords = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    const result = await client.query(`select DISTINCT ON (provider_system_id) provider_system_id ,
    provider_system_name,date from usr_oneup_gcp where usr_id=$1`, [req.user.id])
    await client.end();
    if(result.rowCount > 0){
      return res.json({ 'success': true, 'data': result.rows });
     }else{
      return res.json({ 'success': false, 'data': 'Record is not available' });
     }
  }
  catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message })
  }
}

// Get  Patient Manual Count Records
export const getPatientManualCountRecords = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    const result = await client.query(`select tbl.upid,tbl.fullname, count(uai.upid) from (
      select up.uid,up.pid,up.usertypeid,up.upid,
          case when mp.full_name is null then 
                CONCAT (mp.first_name, ' ', mp.last_name)
                else  mp.full_name end as fullname
         from users_providers up
          join m_providers mp on up.pid = mp.pid
          where up.usertypeid=$1 and up.uid=$2
        ) as tbl
        join users_attach_info uai on tbl.upid = uai.upid
        group by tbl.upid,tbl.fullname`, [2,req.user.id])
    await client.end();
     if(result.rowCount > 0){
      return res.json({ 'success': true, 'data': result.rows });
     }else{
      return res.json({ 'success': false, 'data': 'Record is not available' });
     }
  
  }
  catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message })
  }
}


// Get get Patient Provider Access Control
export const getPatientProviderAccessControl = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    const result = await client.query(`Select prov_id,fullname,string_agg(resource_name::text, ','),
    string_agg(created_on::text, ',') created_on
    from(
     select ppr.prov_id, mr.resource_name,ppr.created_on,
      case when mp.full_name is null then 
            CONCAT (mp.first_name, ' ', mp.last_name)
            else  mp.full_name end as fullname
     from m_providers mp
      join patient_provider_resource ppr on ppr.prov_id = mp.pid
      join m_resource mr on mr.resource_id = ppr.resource_id
      where ppr.pat_id=$1
    ) as tbl
    group by prov_id,fullname`, [req.user.id]);
    await client.end();
var resourceDate=[];
    if(result.rowCount > 0){
    for(var obj of result.rows){
      var resourceDate = obj.created_on;
      var finalDate='';
      if(resourceDate && resourceDate.length > 0){
       var dateMultiple = resourceDate.split(","); 
       for(let i=0 ; i < dateMultiple.length; i++){
        var tempDate = format.asString('yyyy-MM-dd', new Date(dateMultiple[i]));
        finalDate += tempDate + ',';
       }
       finalDate = finalDate.substring(0, finalDate.length - 1);
       obj.created_on = finalDate;
      }
    
    }
    return res.json({ 'success': true, 'data': result.rows });
  }else{
    return res.json({ 'success': false, 'data': 'Record is not available' });
   }
  }
  catch (err) {
    console.log(err);
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message })
  }
}




// Get  Digital Heath Configuration (For Patient)
export const getDigitalHeathConfiguration = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    const result = await client.query(`select fitbit_created_on,isfitbit_code_avail from m_users where id=$1 and isfitbit_code_avail=$2`, [req.user.id,true])
    await client.end();
     if(result.rowCount > 0){
      return res.json({ 'success': true, 'data': result.rows });
     }else{
      return res.json({ 'success': false, 'data': 'Record is not available' });
     }
  
  }
  catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message })
  }
}







