
import axios from "axios";
import oneUPData from '../config/one-up';
import { createClient } from '../helper';
import { addErrorLog } from './errorLog.controller';
import { actionEnum,bucketFileName,bucketName } from '../config/common';
var multer = require('multer');
var storage = multer.memoryStorage();
//upload file destination here
var uploadDocumentContent = multer({
  storage: storage,
  limits: { fileSize: 5 * 1000 * 1000 },
  fileFilter: (req, file, cb) => {
    const match = ["text/csv", "application/vnd.ms-excel",'image/png','image/jpg','image/jpeg','application/pdf','application/rtf'];
    if (match.indexOf(file.mimetype) === -1) {
      cb(null, false);
      return cb(new Error("Invalid file type! Only csv,pdf,rtf,png,jpg,jpeg supported"));
    }
    else {
      cb(null, true);
    }

  },
}).array('uploads[]', 500);


//Get NPI records for particular NPI No.
export const getParticularHealthSystem = async (req, res) => {
  try {
    //Get NPI records for particular NPI No.
    var result = await axios.get('https://npiregistry.cms.hhs.gov/api/?version=2.0&number=' + req.params.npi);
    return res.json({ success: true, data: result.data.results });
  }
  catch (err) {
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Get system health list
export const getSystemHealthList = async function (req, res) {
  let debugVal = [];
  try {
    let accessTokenId = req.params.token;
    let filterName = req.params.name;
    //Get system health records by search filter 
    let res1 = await axios.post(`https://system-search.1up.health/api/search?query=${encodeURIComponent(filterName)}`, null,
      {
        headers: {
          'Authorization': 'Bearer ' + accessTokenId
        }
      })

    var objectValue = res1.data;
    var errs = [];
    debugVal = res1.data;
    var dataArr = [];
    var allImages= ['png', 'jpg', 'jpeg', 'gif'];
    for (var i = 0; i < objectValue.length; i++) {
      try {
        if(objectValue[i].value[0].resource.hasOwnProperty('extension')){
          let fileExtension = null;
          if (objectValue[i].value[0].resource.extension[0].valueUri) {
            fileExtension = objectValue[i].value[0].resource.extension[0].valueUri.split('.').pop();
          }
          if(fileExtension && allImages.indexOf(fileExtension) === -1) {
            objectValue[i].value[0].resource.extension[0].valueUri=null;
          }
          objectValue[i].value[0].identifierKey = objectValue[i].key;
          dataArr.push(objectValue[i].value[0]);
        }
      } catch (err) {
        // try-catch to force parsing through returned list but have some feedback on errors
        errs.push({
          'message': err.message,
          'error': err.Error
        });
      }
    }
    return res.json({ 'success': true, 'data': dataArr, 'errors': errs });
  } catch (err) {
    var text;
    if (err.Error) {
      text = err.Error;
    }
    if (err.message) {
      text = err.message;
    }
    console.log(text);

    addErrorLog(err);
    return res.json({ 'success': false, 'message': text, 'debug': debugVal });
  }
}





// Patient access token
export const getPatinetAccessToken = async function (req, res) {
  try {

    // var tokenObj = await PatinetOneUpAccessToken(req);
    var userId = req.user.id;
    var res1;
    var res2;
    var codeNew;
    let params1 = {
      app_user_id: userId,
      client_id: oneUPData.client_id,
      client_secret: oneUPData.client_secret
    }
    var client = await createClient();
    client.connect();
    //oneUpUserId not exists in local variable
    if (!req.body.oneUpUserId || req.body.oneUpUserId == null) {
      //Signup patient in OneUp API
      res1 = await axios.post(oneUPData.signup_oneup, params1);
      //Stored patient oneup_user_id in master table
      await client.query(`UPDATE m_users SET oneup_user_id = $1 WHERE id = $2`,
        [res1.data.oneup_user_id, userId]);
    }
    //oneUpUserId  exists in local variable
    if (req.body.oneUpUserId) {
      //Get new code from OneUp API for get refresh_token and access_token
      res1 = await axios.post(oneUPData.new_signup_code, params1);
    }
    codeNew = (res1.data.code);
    let params2 = {
      client_id: oneUPData.client_id,
      client_secret: oneUPData.client_secret,
      code: codeNew,
      grant_type: "authorization_code"
    }
    //Get refresh_token and access_token
    res2 = await axios.post(oneUPData.new_access_code, params2);
    var responseObj = {
      "refresh_token": res2.data.refresh_token,
      'access_token': res2.data.access_token,
      'oneup_user_id': req.body.oneUpUserId,
    };
    await client.end();
    return res.json({ 'success': true, 'data': responseObj });

  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Add patient provider
export const addPatientProvider = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    //Check NPI No. exists
    const res1 = await client.query(`select * from m_providers where npi=$1`, [req.body.npino]);
    //NPI No. exists
    if (res1.rows.length) {
      var pId = res1.rows[0].pid;
      //Check provider Id and user Id
      const res2 = await client.query(`select * from users_providers where pid = $1 and uid= $2 and is_provider_deleted=$3`, [pId, req.user.id, false]);
      if (res2.rows.length) {
        await client.end();
        return res.json({ 'success': true, 'message': 'Provider already exists' });
      }
      if (!res2.rows.length) {
        //Insert in users_providers with active false
        let text1 = `INSERT into users_providers(uid,pid,usertypeid,active)values($1, $2, $3, $4) RETURNING *`;
        let param1 = [req.user.id, pId, 2, false];
        await client.query(text1, param1);

        // Create notification
      console.log('==============Create notification to update=================');
      var notificationText = 'INSERT INTO s_notifications(user_id, title, description)VALUES($1, $2, $3)';
      var n_values = [req.user.id, 'Provider', req.body.organization_name+ ' provider updated'];
      await client.query(notificationText, n_values);
      await client.end();
      io.emit('notification',{ flag: true });
      return res.json({ 'success': true, 'message': 'Provider Updated successfully' });
      }
    }
    //NPI No. not exists
    if (!res1.rows.length) {
      var firstName = null;
      var lastName = null;
      var fullName = null;
      if (req.body.enumeration_type == "NPI-1") {
        firstName = req.body.f_name;
        lastName = req.body.l_name;
      }
      else {
        fullName = req.body.organization_name;
      }
      let text = 'INSERT into m_providers(npi,address1,address2,city,state_id,zipcode,fax_number,full_name,country,taxonomy,identifiers_desc,identifiers_state, identifiers_no, first_name, last_name )';
      text += 'values($1, $2, $3, $4, $5, $6,$7,$8,$9, $10, $11, $12, $13, $14, $15) RETURNING pid';
      let param = [req.body.npino, req.body.addressone, req.body.addresstwo, req.body.city,req.body.state_name, req.body.postal_code, req.body.postal_code, fullName,
      req.body.country_name, req.body.taxonomy, req.body.medicaid_desc, req.body.medicaid_state, req.body.medicaid_identifier, firstName, lastName];
      let result = await client.query(text, param);
      let pId = result.rows[0].pid;
      let text1 = `INSERT into users_providers(uid,pid,usertypeid,active)values($1, $2, $3, $4) RETURNING *`;
      let param1 = [req.user.id, pId, 2, false];
      await client.query(text1, param1);

      // Create notification
      console.log('\n\n\n==============Create notification to add=================');
      var notificationText = 'INSERT INTO s_notifications(user_id, title, description)VALUES($1, $2, $3)';
      var n_values = [req.user.id, 'Provider', req.body.organization_name+ ' provider added'];
      await client.query(notificationText, n_values);
      await client.end();
      io.emit('notification',{ flag: true });
      return res.json({ 'success': true, 'message': 'Provider added successfully' });
    }
  } catch (err) {
    await client.end();
    addErrorLog(err);
    console.log(err.message);
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Provider list
export const getproviderList = async function (req, res) {
  try {
    var userId;
    if(req.user.id){
      userId = req.user.id;
    }
    if(req.params.user_id){
      userId = req.params.user_id;
    }
    var client = await createClient();
    client.connect(); 
    //Get Provider list by patient add Provider
    const res1 = await client.query(`select up.pid,up.upid,up.usertypeid, mp.*, 
    case when mp.full_name is null then 
    CONCAT  (mp.first_name, ' ', mp.last_name)
    else  mp.full_name end as fullname from users_providers up join m_providers mp on up.pid=mp.pid
    where up.uid=$1 and up.usertypeid=$2 and up.is_provider_deleted=$3 order by mp.pid desc `, [userId, 2, false]);
    await client.end();
    return res.json({ 'success': true, 'data': res1.rows });
  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Get single provider from list by id
export const getParticularProviderFromList = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    const result = await client.query(`select * from m_providers where pid = $1`, [req.params.id])
    await client.end();
    return res.json({ 'success': true, 'data': result.rows });
  }
  catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message })
  }
}

export const getParticularProviderFromListByNpi = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    const res1 = await client.query(`select * from m_providers where npi=$1`, [req.params.npi]);
    //NPI No. exists
    if (res1.rows.length) {
      var pId = res1.rows[0].pid;
      //Check provider Id and user Id
      const res2 = await client.query(`select * from users_providers where pid = $1 and uid= $2 and is_provider_deleted=$3`, [pId, req.user.id, false]);
      if (res2.rows.length) {
        await client.end();
        return res.json({ 'success': true, 'data': res1.rows });
      } else {
        return res.json({ 'success': false, 'message': 'NPI No. not exists' });
      }
    }
    if (!res1.rows.length) {
      return res.json({ 'success': false, 'message': 'NPI No. not exists' });
    }
  }
  catch (err) {
    console.log(err.message);
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message })
  }
}

// Update single provider by id
export const updateParticularProvider = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    var pgQuery = 'UPDATE m_providers set full_name=$1, state_id=$2, city=$3, zipcode=$4, country=$5, address1=$6, address2=$7, taxonomy=$8, first_name=$9, last_name=$10 where pid=$11';
    var value = [req.body.full_name, req.body.state_name, req.body.city, req.body.postal_code, req.body.country_name, req.body.addressone, req.body.addresstwo, req.body.taxonomy, req.body.f_name, req.body.l_name, req.params.id];
    const result = await client.query(pgQuery, value);
    await client.end();
    return res.json({ 'success': true, message: 'Provider updated successfully' });
  }
  catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message })
  }
}
// Get single provider from list by state
export const filterProviderList = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    var pgQuery = 'SELECT * from m_providers where state_id =$1';
    var value = [req.params.id];
    const result = await client.query(pgQuery, value);
    await client.end();
    return res.json({ 'success': true, 'data': result.rows });
  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'meaagae': err.message })

  }
}
// Individual patient file list
export const getPatientProviderDocument = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    //get individual patient attachment file list
    const res1 = await client.query(`select * from users_attach_info where upid = $1 and isArchived=$2 order by upiid desc `, [req.params.id, false]);
    await client.end();
    return res.json({ 'success': true, 'data': res1.rows });
  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Patient file upload
export const patientFilesUpload = async function (req, res) {
  var client = await createClient();
  try {
    client.connect();
    var result = null;
    await uploadDocumentContent(req, res, async function (err) {
      try {
        if (err) {
          return res.json({ success: false, message: err.message })
        }
        const { Storage } = require('@google-cloud/storage');
        const storage = new Storage({ keyFilename: bucketFileName });
        const bucketname = bucketName;
        const bucket = storage.bucket(bucketname);
        let promises = [];
        for (var requestfile of req.files) {
          const promise = new Promise((resolve, reject) => {
            const gcsFileName = `${Date.now()}-${requestfile.originalname}`;
            const displayFileName = requestfile.originalname;
            const uploadFileName = `${req.user.id}/${gcsFileName}`;
            const file = bucket.file(uploadFileName);
            const stream = file.createWriteStream({
              metadata: {
                contentType: requestfile.mimetype,
              },
            });

            stream.on('error', (err) => {
              console.log(err)
              requestfile.cloudStorageError = err;
              reject(err)
            });

            stream.on('finish', async () => {
              try {
                var create_date = new Date();
                //Insert file records in table
    
                var text = 'INSERT INTO users_attach_info(upid, file_displayname,file_fullname,create_on)VALUES($1, $2, $3,$4)';
                var values = [req.body.upId, displayFileName, gcsFileName, create_date];
                await client.query(text, values);
                // Create notification
                var notificationText = 'INSERT INTO s_notifications(user_id, title, description)VALUES($1, $2, $3)';
                var n_values = [req.user.id, 'Manual upload', 'New '+requestfile.originalname+ ' uploaded'];
                await client.query(notificationText, n_values);
                resolve();
                io.emit('notification',{ flag: true });
              }
              catch (ex) {
                if (file)
                  await file.delete();
                reject(ex)
              }
            });

            stream.end(requestfile.buffer);
          })
          promises.push(promise);
        }
        Promise.all(promises)
          .then(_ => {
            client.end();
            return res.json({ success: true, message: "Files Uploaded successfully" });
          })
          .catch((err) => {
            client.end();
            return res.json({ success: false, message: err.message })
          });
      }

      catch (e) {
        console.log(e);
        return res.json({ success: false, message: e.message })
      }
    });
  }
  catch (e) {
    console.log(e);
    return res.json({ success: false, message: e.message })
  }


}
// Delete patient attachment file
export const deletePatientProviderDocument = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    //Delete  patient attachment file
    const text = 'DELETE FROM  users_attach_info WHERE upiid =$1 RETURNING *';
    const values = [req.params.id];
    const dataStatus = await client.query(text, values);
    const { Storage } = require('@google-cloud/storage');
    const storage = new Storage({ keyFilename: bucketFileName });
    const bucketname = bucketName;
    const bucket = storage.bucket(bucketname);
    var stream = bucket.file(`${req.user.id}/${dataStatus.rows[0].file_fullname}`);
    await stream.delete();
    await client.end();
    if (dataStatus) {
      return res.json({ 'success': true, 'message': 'File  has been deleted' });
    } else {
      return res.json({ 'success': false, 'message': 'File has not been deleted' });
    }
  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ success: false, message: err.message });
  }
}

//dowload file from cloude server

export const downloadPatientDocument = async function (req, res) {
  var client = await createClient();
  try { 
      if (!req.params.id)
          return res.json({ success: false, message: "INVALID_REQUEST" });
      client.connect();
      var result = await client.query(`SELECT file_fullname,upiid from users_attach_info where upiid=$1`,[req.params.id]);
      client.end();
    var user_Id;
    user_Id = req.user.id;
    if(req.params.userId != 'undefined')
     user_Id= req.params.userId;
      if (result.rows.length === 0)
          return res.json({ success: false, message: "INVALID_REQUEST" });
      var url = result.rows[0].file_fullname;
       
      const { Storage } = require('@google-cloud/storage');
      const storage = new Storage({ keyFilename: bucketFileName });
      const bucketname = bucketName;
      const bucket = storage.bucket(bucketname);
      var stream = bucket.file(`${user_Id}/${url}`).createReadStream();
      console.log(`${user_Id}/${url}`);
     res.writeHead(200);
      stream.on('data', function (data) {
          res.write(data);
      });

      stream.on('error', function (err) {
         // console.log('error reading stream', err);
      });

      stream.on('end', function () {
          res.end();
      });
  }
  catch (err) {
    await client.end();
    addErrorLog(err.message);
    return res.json({ success: false, message: err.message });
  }
}




//Update patient attachment file status
export const archivePatientProviderDocument = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    //Update patient attachment file status
    const text = 'UPDATE users_attach_info SET isArchived=$1 WHERE upiid =$2';
    const values = [true, req.params.id];
    const dataStatus = await client.query(text, values);
    await client.end();
    if (dataStatus) {
      return res.json({ 'success': true, 'message': 'File  has been archived' });
    } else {
      return res.json({ 'success': false, 'message': 'File has not been archived' });
    }
  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ success: false, message: err.message });
  }
}
// Get Provider list for patient provider
export const getproviderWithFilesList = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    //Get provider list for patient provider add with file attachment
    const res1 = await client.query(`select * from m_providers mp join
    (select ua.upid ,up.pid from users_providers up join users_attach_info ua on ua.upid=up.upid where up.uid=$1 and up.usertypeid=$2 and up.is_provider_deleted=false
    group by ua.upid,up.pid)abc on mp.pid=abc.pid`, [req.user.id, 2]);
    await client.end();
    return res.json({ 'success': true, 'data': res1.rows });
  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Not in use
export const deleteProviderData = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    let cur_date = new Date();
    const text = 'update users_providers set active=$1,is_provider_deleted=$2,deleted_on=$3 where uid=$4 and pid=$5 and usertypeid=$6';
    const values = [false, true, cur_date, req.user.id, req.params.id, 2];
    const dataStatus = await client.query(text, values);
    await client.end();
    return res.json({ 'success': true, 'message': 'Provider has been deleted' });
  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ success: false, message: err.message });
  }
}
// Get notification
export const getNotification = async (req, res) => {
  try{
    var client = await createClient();
    client.connect();
    const text = `select * from s_notifications where user_id=$1 and status=$2`;
    let param = [req.user.id, false];
    const result = await client.query(text,param);
    await client.end();
    result.rows.forEach(element => {
      var datetime = new Date();
      var hours = Math.floor(Math.abs(element.date - datetime) / 36e5);
      element.hours = hours;
    });
    return res.json({ success: true, notification: result.rows });
  }catch(err){
    await client.end();
    addErrorLog(err);
    return res.json({ success: false, message: err.message });
  }
}
// Make unread notification
export const makeUnreadNotification = async (req, res) => {
  try{
    if(req.body.length > 0){
      var ids = '(';
      req.body.forEach(element => {
        ids = ids + element.id + ','
      });
      ids = ids.slice(0, -1);
      ids = ids + ')';

      var client = await createClient();
      client.connect();
      const text = `update s_notifications set status=true where user_id=${req.user.id} and id IN${ids}`;
      const result = await client.query(text);
      await client.end();
      return res.json({'success': true})
    }
  }catch(err){
    await client.end();
    addErrorLog(err);
    return res.json({'success': false})

  }
}
// Get all patient for by resource
export const getAllPatient = async function(req, res) {
  try{
    var resource_type;
    if(req.params.type == 'Medical_record'){
      resource_type = actionEnum.MedicalResourceId;
    }
    if(req.params.type == 'Fitbit'){
      resource_type = actionEnum.Fitbit;
    }
    if(req.params.type == 'Narrative'){
      resource_type = actionEnum.Narrative;
    }
    if(req.params.type == 'Manual_upload'){
      resource_type = actionEnum.ManualUpload;
    }
    var client = await createClient();
    client.connect();
    const text = `select mu.fullname, mu.id from m_users mu 
    inner join patient_provider_resource pr
    on mu.id = pr.pat_id where pr.prov_id = $1 and mu.usertypeid = $2
    and  pr.resource_id=$3`;
    let param = [req.user.pid, 2, resource_type];
    const result = await client.query(text,param);
   
    await client.end();
    return res.json({ success: true, users: result.rows });

  }catch(err){
    await client.end();
    addErrorLog(err);
    return res.json({ success: false, message: err.message });
  }
}



// Get patient portal connection list by user
export const getPatientPortalConnection = async function(req, res) {
  var client = await createClient();
  try{
    client.connect();
    const text = `select provider_system_name,provider_system_id,provider_system_address from usr_oneup_gcp where usr_id=$1
    group by provider_system_id,provider_system_name,provider_system_address`;
    let param = [req.user.id];
    const result = await client.query(text,param);
    await client.end();
    
    console.log("=============Dispaly======================");
    console.log( result.rows);
    
    if(result.rowCount > 0)
     return res.json({ success: true, data: result.rows });
     else
     return res.json({ success: true, message: 'Data is not found' });



  }catch(err){
    await client.end();
    addErrorLog(err);
    return res.json({ success: false, message: err.message });
  }
}


// Document file name update by user
export const DocumentFileNameUpdate = async function(req, res) {
  try {
    var client = await createClient();
    client.connect();
    //Update patient attachment file rename
    const sql = 'UPDATE users_attach_info SET file_displayname=$1 WHERE upiid =$2';
    const params = [req.body.name,req.body.upiid];
    const response = await client.query(sql, params);
    await client.end();
    if (response) {
      return res.json({ 'success': true, 'message': 'File  has been renamed' });
    } else {
      return res.json({ 'success': false, 'message': 'File has not been renamed' });
    }
  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ success: false, message: err.message });
  }
}





// Patient access token
async function PatinetOneUpAccessToken(req) {
  var client = await createClient();
  try {
    var userId = req.user.id;
    var res1;
    var res2;
    var codeNew;
    let params1 = {
      app_user_id: userId,
      client_id: oneUPData.client_id,
      client_secret: oneUPData.client_secret
    }
    client.connect();
    //oneUpUserId not exists in local variable
    if (!req.user.oneup_user_id || req.user.oneup_user_id == null || req.user.oneup_user_id==undefined) {
      //Signup patient in OneUp API
      res1 = await axios.post(oneUPData.signup_oneup, params1);
      //Stored patient oneup_user_id in master table
       if(res1.data.oneup_user_id && res1.data.oneup_user_id!=undefined){
        await client.query(`UPDATE m_users SET oneup_user_id = $1 WHERE id = $2`,
        [res1.data.oneup_user_id, userId]);
        await client.end();
       }
     
    }
    //oneUpUserId  exists in Token variable
    if (req.user.oneup_user_id) {
      //Get new code from OneUp API for get refresh_token and access_token
      res1 = await axios.post(oneUPData.new_signup_code, params1);
    }
    codeNew = (res1.data.code);
    let params2 = {
      client_id: oneUPData.client_id,
      client_secret: oneUPData.client_secret,
      code: codeNew,
      grant_type: "authorization_code"
    }
    //Get refresh_token and access_token
    res2 = await axios.post(oneUPData.new_access_code, params2);
    var responseObj = {
      "refresh_token": res2.data.refresh_token,
      'access_token': res2.data.access_token,
      'oneup_user_id': req.user.oneup_user_id,
    };
    return responseObj;
  } catch (err) {
    console.log(err);
    await client.end();
    addErrorLog(err);
    return false;
  }
}

exports.PatinetOneUpAccessToken = PatinetOneUpAccessToken;










