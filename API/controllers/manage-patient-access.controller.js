
import { createClient } from '../helper';
import { addErrorLog } from './errorLog.controller';
// In Provider show all list of users
export const allUserListByType = async function (req, res) {
    try {

        console.log("==========================allUserListByType=========================");
        var client = await createClient();
        client.connect();
        var sql;
        if (req.params.typeId == 2) {
            sql = `select m.first_name,m.last_name,m.email,m.phone_number,m.fullname,m.dob,
                    up.upid,up.uid,up.active from m_users m inner join
                    users_providers up on m.id = up.uid 
                    where up.pid =$1 and up.usertypeid= $2 and 
                    up.uid IN (SELECT pat_id FROM patient_provider_resource)`;
        } else {
            sql = `select m.first_name,m.last_name,m.email,m.phone_number,m.fullname,m.dob,
                    up.upid,up.uid,up.active from m_users m inner join
                    users_providers up on m.id = up.uid 
                    where up.pid =$1 and up.usertypeid= $2`;
        }
        let params = [req.user.pid,req.params.typeId]
        const dataRes = await client.query(sql,params);
        await client.end();
        if (dataRes.rowCount) 
            return res.json({ 'success': true, 'data': dataRes.rows });
            else 
            return res.json({ 'success': false, 'message': 'Patient not found' });
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}





// In Provider show all list of users with search filter
export const allUserListByTypeWithSearch = async function (req, res) {
    try {
        var client = await createClient();
        client.connect();
        var sql;
        if(req.params.searchFilter=='email' && req.params.typeId == 2){
            sql = `select m.first_name,m.last_name,m.email,m.phone_number,m.fullname,m.dob,
                up.upid,up.uid,up.active from m_users m inner join
                users_providers up on m.id = up.uid 
                where m.email like $1 and up.pid =$2 and up.usertypeid= $3 and 
                up.uid IN (SELECT pat_id FROM patient_provider_resource)`;
        } else if (req.params.searchFilter=='email') {
            sql = `select m.first_name,m.last_name,m.email,m.phone_number,m.fullname,m.dob,
                up.upid,up.uid,up.active from m_users m inner join
                users_providers up on m.id = up.uid 
                where m.email like $1 and up.pid =$2 and up.usertypeid= $3`;
        } else if (req.params.typeId == 2) {
            sql = `select m.first_name,m.last_name,m.email,m.phone_number,m.fullname,m.dob,
                up.upid,up.uid,up.active from m_users m inner join
                users_providers up on m.id = up.uid 
                where  m.first_name like $1 and up.pid =$2 and up.usertypeid= $3 and 
                up.uid IN (SELECT pat_id FROM patient_provider_resource)`;
        }else{
            sql = `select m.first_name,m.last_name,m.email,m.phone_number,m.fullname,m.dob,
                up.upid,up.uid,up.active from m_users m inner join
                users_providers up on m.id = up.uid 
                where  m.first_name like $1 and up.pid =$2 and up.usertypeid= $3`;
        }
        let params = ["%"+req.params.searchDataValue+"%",req.user.pid,req.params.typeId]
        const dataRes = await client.query(sql,params);
        await client.end();
        if (dataRes.rowCount) 
            return res.json({ 'success': true, 'data': dataRes.rows });
            else 
            return res.json({ 'success': false, 'message': 'Patient not found' });
    } catch (err) {
        console.log(err.message);
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}



// Provider give access of user
export const providerUserAccessControl = async function (req, res) {
    try {
        var client = await createClient();
        client.connect();
        //Update User flag of active
        if(req.body.status == 'clear'){
            let sql = `DELETE FROM patient_provider_resource ppr 
                WHERE ppr.pat_id IN (SELECT uid FROM users_providers up WHERE upid=$1)
                and ppr.prov_id IN (SELECT uid FROM users_providers up WHERE upid=$1)`;
            let params = [req.body.upid];
            const dataStatus = await client.query(sql, params);
            await client.end();
            if (dataStatus.rows) {
                io.emit('globalProviderMenu',{ flag: true });
                return res.json({ 'success': true, 'message': 'Patient access removed successfully' });
            } else {
                return res.json({ 'success': false, 'message': ' Patient access not removed' });
            }
        }else{
            let flag = !(req.body.status);
            console.log(flag)
            let sql = `update users_providers set active =$1 where upid = $2`;
            let params = [flag,req.body.upid];
            const dataStatus = await client.query(sql, params);
           // 
            await client.end();
            if (dataStatus.rows) {
                io.emit('globalProviderMenu',{ flag: true });
                return res.json({ 'success': true, 'message': 'Patient access updated successfully' });
            } else {
                return res.json({ 'success': false, 'message': ' Patient access not updated successfully' });
            }
        }
        
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}





