const crypto = require('crypto');
var jwt = require('jsonwebtoken');
import axios from "axios";
import { cat } from "shelljs";
import { smtpTransport, fromEmail } from '../config/mail-config';
import oneUPData from '../config/one-up';
import { createClient } from '../helper';
import { addErrorLog } from './errorLog.controller';
var mailOptions, host, link;
const pg = require("pg");
var path = require('path');
const fs = require('fs');
var multer = require('multer');
var storage = multer.memoryStorage();

// Upload ID Proof
var uploadIDProofDocument = multer({
    storage: storage,
    limits: { fileSize: 5 * 1000 * 1000 },
    fileFilter: (req, file, cb) => {
      const match = ['image/png','image/jpg','image/jpeg','application/pdf'];
      if (match.indexOf(file.mimetype) === -1) {
        cb(null, false);
        return cb(new Error("Invalid file type! Only pdf,png,jpg,jpeg supported"));
      }
      else {
        cb(null, true);
      }
  
    },
  }).single('ID_proof');

//Get NPI records
export const getHealthSystem = async (req, res) => {
    try {
        //get NPI records
        var result = await axios.get('https://npiregistry.cms.hhs.gov/api/?version=2.0&enumeration_type=' + req.params.npi + '&organization_name=' + req.params.orgname + '*');
        return res.json({ success: true, data: result.data.results });

    }
    catch (err) {
        addErrorLog(err);
        return res.json({
            'success': false, 'message': err.message
        });
    }
}

export const getHealthSystemIndividualFirstName = async (req, res) => {
    try {
        //get NPI records
        var result = await axios.get('https://npiregistry.cms.hhs.gov/api/?version=2.0&enumeration_type=' + req.params.npi + '&first_name=' + req.params.firstname + '*');
        return res.json({ success: true, data: result.data.results });

    }
    catch (err) {
        addErrorLog(err);
        return res.json({
            'success': false, 'message': err.message
        });
    }
}

export const getHealthSystemIndividualLastName = async (req, res) => {
    try {
        //get NPI records
        var result = await axios.get('https://npiregistry.cms.hhs.gov/api/?version=2.0&enumeration_type=' + req.params.npi + '&last_name=' + req.params.lastname + '*');
        return res.json({ success: true, data: result.data.results });

    }
    catch (err) {
        addErrorLog(err);
        return res.json({
            'success': false, 'message': err.message
        });
    }
}

export const getHealthSystemIndividualBothName = async (req, res) => {
    try {
        //get NPI records
        var result = await axios.get('https://npiregistry.cms.hhs.gov/api/?version=2.0&enumeration_type=' + req.params.npi + '&first_name=' + req.params.firstname + '&last_name=' + req.params.lastname + '*');
        return res.json({ success: true, data: result.data.results });
    }
    catch (err) {
        addErrorLog(err);
        return res.json({
            'success': false, 'message': err.message
        });
    }
}
//Get NPI record by NPI No.
export const getNPI = async (req, res) => {
    try {
        //get NPI record by NPI No.
        var result = await axios.get('https://npiregistry.cms.hhs.gov/api/?version=2.1&number=' + req.params.id);
        return res.json({ success: true, data: result.data.results });
    }
    catch (err) {
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}
//Check user e-mail exists or not with user type_id
export const checkUsers = async (req, res) => {
    try {
        var reqEmail = req.body.pemail;
        var client = await createClient();
        client.connect();
        //Check E-mail already exists in table
        var result = await client.query(`select * FROM m_users WHERE LOWER(email) = LOWER($1) and usertypeid=1`, [reqEmail]);
        await client.end();
        if (result.rowCount > 0) {
            return res.json({ 'success': true, 'message': 'Email Already Exist' })
        } else {
            return res.json({ 'success': false, 'message': 'Email does not Exist' });
        }

    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}
//Check user e-mail exists or not
export const emailCheckUser = async (req, res) => {
    try {
        var reqEmail = req.body.email;
        var client = await createClient();
        client.connect();
        //Check E-mail already exists in table
        var result = await client.query(`select * FROM m_users WHERE LOWER(email) = LOWER($1)`, [reqEmail]);
        await client.end();
        if (result.rowCount > 0) {
            return res.json({ 'success': true, 'message': 'Email Already Exist' })
        } else {
            return res.json({ 'success': false, 'message': 'Email does not Exist' });
        }

    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}
//Check NPI already exists in table
export const npiChecker = async (req, res) => {
    var client = await createClient();
    try {
        var reqNpi = req.body.npi;
        client.connect();
 //check NPI No. exits
 let checkNPINoExists = await client.query(`SELECT * from m_providers WHERE npi = $1`, [reqNpi]);
 //NPI No. found then,
 if (checkNPINoExists.rows.length) {
     lpid = checkNPINoExists.rows[0].pid;
     //check uid exist of this pid
     let checkUidExists = await client.query(`SELECT * from  m_users WHERE pid = $1 and usertypeid=$2`, [lpid, 1]);
     await client.end();
     if (checkUidExists.rows.length > 0) {
        res.status(200).send({ 'success': true, 'message': 'NPI Already Exist'});
        }else{
            res.status(200).send({'success': false, 'message': 'NPI is ok'});
         }
 }else{
    await client.end();
    res.status(200).send({'success': false, 'message': 'NPI is ok'});
 }

    }
    catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}
//Check Mobile already exists in table
export const contactCheckUser = async (req, res) => {
    try {
        var reqMobile = req.body.mobile;
        var client = await createClient();
        client.connect();
        //Check Mobile already exists in table
        var result = await client.query(`select COUNT(*) FROM m_users WHERE mobile = $1`, [reqMobile]);
        await client.end();
        if (result.rows[0].count > 0) {
            res.status(200).send({
                'success': true,
                'message': 'Contact Already Exist'
            });
        } else {
            res.status(200).send({
                'success': false,
                'message': 'Contact is ok'
            });
        }


    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}


//Get user profile list


export const getUserProfileList = async (req, res) => {
    try {
        var userTypeId = req.params.userTypeId;
        var client = await createClient();
        client.connect();
        var result;
        if (userTypeId == 1) {
            result = await client.query(`select m.id,m.username,m.email,m.first_name,m.last_name,m.phone_number,m.dob,
            mp.npi,mp.address1,mp.address2,mp.city,mp.state_id,mp.zipcode,mp.fax_number
           from m_users m left join m_providers mp on m.pid=mp.pid where id =$1`, [req.user.id]);
        }
        else if (userTypeId == 3) {
            result = await client.query(`select m.id,m.username,m.email,m.first_name,m.last_name,m.phone_number, 
            (select email from m_users where pid= (select mp.pid from m_providers mp inner join users_providers up
            on mp.pid = up.pid where uid=$1 and usertypeid=3) and 
            usertypeid = 1)as pemail from m_users m where m.id=$1`, [req.user.id]);
        }
        else {
            result = await client.query(`select id,username,email,first_name,last_name,phone_number,dob,address1,address2,city,zipcode,state from m_users where id =$1`, [req.user.id]);
        }
        await client.end();
        if (result.rowCount > 0)
            res.status(200).send({ 'success': true, 'data': result.rows[0] });
        else
            res.status(200).send({ 'success': false, 'message': 'data is not found' });

    } catch (err) {
        console.log(err.message);
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}

//Update user profile



export const UserProfileUpdate = async (req, res) => {
    try {
        var client = await createClient();
        client.connect();
        var result = await client.query(`update m_users set first_name=$1,last_name=$2,email=$3,phone_number=$4,dob=$5, address1=$6,address2=$7,city=$8,state=$9,zipcode=$10 where id=$11`,
            [req.body.first_name, req.body.last_name, req.body.email, req.body.phone_number, req.body.dob, req.body.address1,req.body.address2, req.body.city, req.body.state_id, req.body.zipcode,req.user.id]);
        await client.end();
        return res.json({ 'success': true, 'message': 'Profile has been updated' });

    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}


//Update provider profile

export const providerProfileUpdate = async (req, res) => {
    try {
        var client = await createClient();
        client.connect();
        var result = await client.query(`update m_users set first_name=$1,last_name=$2,email=$3,phone_number=$4, dob=$5, address1=$6,address2=$7,city=$8,state=$9,zipcode=$10 where id=$11 RETURNING pid`,
            [req.body.first_name, req.body.last_name, req.body.email, req.body.phone_number, req.body.dob, req.body.address1,req.body.address2, req.body.city, req.body.state_id, req.body.zipcode, req.user.id]);
        var pid = result.rows[0].pid;
        if (result.rows[0].pid) {
            await client.query(`update m_providers set address1=$1,address2=$2,city=$3,state_id=$4,zipcode=$5,fax_number=$6 where pid=$7`,
                [req.body.address1, req.body.address2, req.body.city,req.body.state_id, req.body.zipcode, req.body.fax_number, pid]);
        }

        await client.end();
        return res.json({ 'success': true, 'message': 'Profile has been updated' });

    } catch (err) {
        console.log(err.message);
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}

//Upload user profile photo

export const uploadProfileImage = async (req, res) => {
    var client = await createClient();
    try {

        client.connect();
        const file = req.file
        if (!file) {
            const error = new Error('Please upload a file')
            error.httpStatusCode = 400
            return res.json({ 'success': false });
        } else {
            // res.send(file)
            var text = `Update m_users set image_url=$1 where id = $2`
            var value = [req.file.originalname, req.user.id]
            var response = await client.query(text, value);
            await client.end();
            return res.json({ 'success': true, message: 'Image Uploaded' });

        }
    } catch (err) {
        console.log(err.message)
        await client.end();
    }
}


//User profile Image read

export const UserPhotoRead = async function (req, res) {
    var client = await createClient();
    try {
        client.connect();
        let text = 'SELECT image_url from m_users where id= $1';
        const values = [req.params.usr_id];
        var dataStatus = await client.query(text, values);
        var pathFile = (path.join(__dirname, '../../uploaded/photos/Men-Profile-Image.png'));;
        await client.end();
        if (dataStatus.rowCount) {
            var dir = (path.join(__dirname, '../../uploaded/photos/'));
            if (dataStatus.rows[0].image_url)
                pathFile = dir + dataStatus.rows[0].image_url;
            if (fs.existsSync(pathFile))
                return res.sendFile(pathFile);
            else
                return res.sendFile(pathFile);
        } else
            return res.sendFile(pathFile);
    } catch (err) {
        console.log(err.message)
        await client.end();
        return res.sendFile(pathFile);
    }
}

// Register new provider user or add new user
export const uploadIDProof = async (req, res) => {
    var client = await createClient();
    try {
      client.connect();
      var result = null;
      await uploadIDProofDocument(req, res, async function (err) {
        try {
          if (err) {
            return res.json({ success: false, message: err.message })
          }
          var npi_no = JSON.parse(req.body.npi_no)
          const ext = req.file.mimetype.split('/')[1];

          const { Storage } = require('@google-cloud/storage');
          const storage = new Storage({ keyFilename: 'JSON-268610-1e41dfef1ada.json' });
          const bucketname = `curameidatasource`;
          const bucket = storage.bucket(bucketname);
          const gcsFileName = `${npi_no}.${ext}`;
          const uploadFileName = `ID-proof/${gcsFileName}`;
          const file = bucket.file(uploadFileName);

          const stream = file.createWriteStream({
            metadata: {
                contentType: req.file.mimetype,
            },
          });

        stream.on('error', (err) => {
            req.file.cloudStorageError = err;
            return res.json({ success: false, message: err.message })
        });
        stream.on('finish', async () => {
            try {
                return res.json({ 'success': true, 'message': 'File has been uploaded', 'savedFileName':gcsFileName });
            // var email = JSON.parse(req.body.user_email)
            // var text = 'update m_users set idproof = $1 where email = $2;'
            // var values = [gcsFileName, email.toLowerCase()];
            // var dataStatus = await client.query(text, values);
            // await client.end();
            //     if (dataStatus) {
            //     return res.json({ 'success': true, 'message': 'File has been uploaded' });
            //     } else {
            //     return res.json({ 'success': false, 'message': 'File has not been uploaded' });
            //     }
            }
            catch (ex) {
                if (file)
                await file.delete();
                return res.json({ success: false, message: ex.message })
            }
          });
          stream.end(req.file.buffer);
        }
        catch (e) {
          console.log(e);
          return res.json({ success: false, message: e.message })
        }
      });
    }
    catch (e) {
      console.log(e);
      return res.json({ success: false, message: e.message })
    }
}


//get provider idProof

export const getprovideridProof = async (req, res) => {
    var client = await createClient();
    try {
        if (!req.params.filename)
        return res.json({ success: false, message: "INVALID_REQUEST" });
      client.connect();
      var url = req.params.filename;
      const { Storage } = require('@google-cloud/storage');
      const storage = new Storage({ keyFilename: 'JSON-268610-1e41dfef1ada.json' });
      const bucketname = `curameidatasource`;
      const bucket = storage.bucket(bucketname);
      var stream = bucket.file(`ID-proof/${url}`).createReadStream();
      console.log("===Image Url Path Start=======");
      console.log(`ID-proof/${url}`);
     
      console.log("===Image Url Path End=======");
     res.writeHead(200);
      stream.on('data', function (data) {
        console.log("==========Stresm data===========");
        console.log(data);
        console.log("==========Stresm data===========");
          res.write(data);
      });

      stream.on('error', function (err) {
          console.log('error reading stream', err);
      });

      stream.on('end', function () {
          res.end();
      });





     
    }
    catch (e) {
      console.log(e);
      return res.json({ success: false, message: e.message })
    }
}





// Register new user or add new user
export const addUser = async (req, res) => {
    var client = await createClient();
    try {
        client.connect();
        console.log(req.body)
        var user_email = req.body.emailid.toLowerCase();
        var cur_date = new Date();
        var reqProviderEmail = req.body.pemail;
        if (!req.body.fname)
            return res.json({ 'success': false, 'message': 'First Name Not Found.' });

        if (!req.body.lname)
            return res.json({
                'success': false, 'message': 'Last Name Not Found.'
            });

        if (!req.body.emailid)
            return res.json({
                'success': false,
                'message': 'E-mail Not Found.'
            });

        if (!req.body.termsofservices)
            return res.json({
                'success': false,
                'message': 'Terms of Service Not Found.'
            });

        if (!req.body.phonenumber)
            return res.json({
                'success': false,
                'message': 'Phone No. Not Found.'
            });
        // if (!req.body.phoneone || !req.body.phonetwo || !req.body.phonethree)
        //     return res.json({
        //         'success': false,
        //         'message': 'Phone No. Not Found.'
        //     });


        if (!req.body.pass)
            return res.json({
                'success': false,
                'message': 'Password Name Not Found.'
            });

        if (!req.body.cpass)
            return res.json({
                'success': false,
                'message': 'canfirm Password  Not Found.'
            });

        var token;
        function rand(length, current) {
            current = current ? current : '';
            return length ? rand(--length, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz".charAt(Math.floor(Math.random() * 60)) + current) : current;
        }
        token = rand(25);
        var name = req.body.fname + " " + req.body.lname;
        // var phone_number = req.body.phoneone + req.body.phonetwo + req.body.phonethree;
        var phone_number = req.body.phonenumber;
        var fax_number = req.body.faxone + req.body.faxtwo + req.body.faxthree;
        var dob = req.body.dob;

        if(dob != ''){
            dob = dob.split('T')[0];
        }else{
            dob = null;
        }

        host = req.get('host');
        //Link for  Signup Verify Page
        link = "https://app.curameitech.com/signup-verify?token=" + token;
        mailOptions = {
            from: 'Curamei <' + fromEmail + '>',
            to: user_email,
            subject: "Verify Your Account",
            html: `
            <table align="center" border="0" cellpadding="0" cellspacing="0" style="text-align: left; margin: 0 auto; padding: 16px;" width="568">
            <tbody>
                <tr>
                    <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;">
                        <a href="https://curameitech.com/"><img height="50px" src="https://storage.googleapis.com/www.curameitech.com/images/WhiteBorder-Curamei-IconOnly.png" width="50px"></a>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;">
                        <p><span style="font-family: 'Open Sans',Arial,sans-serif; font-weight: 400; font-size: 16px; color: #0e1318; text-align: center; margin: 0;"><strong>Just one more step!</strong></span></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="text-align: left; margin: 0 auto; padding: 0px 16px;" width="568">
            <tbody>
                <tr>
                    <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;">
                        <p><span style="font-family: 'Open Sans',Arial,sans-serif; font-weight: 400; font-size: 16px; line-height: 24px; color: #0e1318; text-align: left; margin: 0;">Hello <strong>${name}</strong>, thank you for registering with Curamei.</span></p>
                        <p><span style="font-family: 'Open Sans',Arial,sans-serif; font-weight: 400; font-size: 16px; line-height: 24px; color: #0e1318; text-align: left; margin: 0;">To ensure the security of our platform, we require you to activate your account using the following link. You will be redirected to the platform registration link, and will be notified within seconds that your account is activated and ready for use.</span></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: separate; line-height: 100%; width: 292px; margin-top: 16px; height: 34px;">
            <tbody>
                <tr>
                    <td align="center" bgcolor="#008080" role="presentation" style="color: #ffffff; height: 52px; width: 258px; border: none; border-radius: 20px;" valign="middle">
                        <a href="${link}" style="text-decoration: none!important;"><p style="background-color: #008080; color: #ffffff; font-family: 'Open Sans',Arial,sans-serif; font-size: 16px; font-weight: 600; line-height: 24px; text-decoration: none; text-transform: none; border-radius: 4px; margin: 0;">Verify your account</p></a>
                    </td>
                </tr>
            </tbody>
        </table>
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="text-align: left; margin-top: 30px; padding: 0;" width="568">
            <tbody>
                <tr>
                    <td style="text-align:center;"><span><a href="https://twitter.com/CurameiT" style="margin-right: 10px;"><svg height="35px" viewbox="0 0 512 512" width="35px" xmlns="http://www.w3.org/2000/svg">
                    <path d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm116.886719 199.601562c.113281 2.519532.167969 5.050782.167969 7.59375 0 77.644532-59.101563 167.179688-167.183594 167.183594h.003906-.003906c-33.183594 0-64.0625-9.726562-90.066406-26.394531 4.597656.542969 9.277343.8125 14.015624.8125 27.53125 0 52.867188-9.390625 72.980469-25.152344-25.722656-.476562-47.410156-17.464843-54.894531-40.8125 3.582031.6875 7.265625 1.0625 11.042969 1.0625 5.363281 0 10.558593-.722656 15.496093-2.070312-26.886718-5.382813-47.140624-29.144531-47.140624-57.597657 0-.265624 0-.503906.007812-.75 7.917969 4.402344 16.972656 7.050782 26.613281 7.347657-15.777343-10.527344-26.148437-28.523438-26.148437-48.910157 0-10.765624 2.910156-20.851562 7.957031-29.535156 28.976563 35.554688 72.28125 58.9375 121.117187 61.394532-1.007812-4.304688-1.527343-8.789063-1.527343-13.398438 0-32.4375 26.316406-58.753906 58.765625-58.753906 16.902344 0 32.167968 7.144531 42.890625 18.566406 13.386719-2.640625 25.957031-7.53125 37.3125-14.261719-4.394531 13.714844-13.707031 25.222657-25.839844 32.5 11.886719-1.421875 23.214844-4.574219 33.742187-9.253906-7.863281 11.785156-17.835937 22.136719-29.308593 30.429687zm0" fill=" #008080"></path></svg></a> <a href="https://linkedin.com/company/curamei-technologies" style="margin-left: 10px;"><svg height="35px" viewbox="0 0 24 24" width="35px" xmlns="http://www.w3.org/2000/svg">
                    <path d="M19 0h-14c-2.761 0-5 2.239-5 5v14c0 2.761 2.239 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-11 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.396-2.586 7-2.777 7 2.476v6.759z" fill=" #008080"></path></svg></a></span></td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;font-family: 'Open Sans',Arial,sans-serif; font-size: 12px;">
                        <a href="https://curameitech.com/help" style="text-decoration: none; padding: 10px;">Help Center</a> <a href="https://curameitech.com/privacy-policy" style="text-decoration: none; padding: 10px;">Privacy Policy</a> <a href="https://curameitech.com/terms-of-service" style="text-decoration: none; padding: 10px;">Terms of Service</a>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;font-family: 'Open Sans',Arial,sans-serif; font-size: 12px;">Curamei Technologies © 2020, All Rights Reserved<br>
                    Ponte Vedra, FL</td>
                </tr>
            </tbody>
        </table>
            `
        }

        // Strong Password encryption and store in salt and hash variable
        var salt = crypto.randomBytes(16).toString('hex');
        var hash = crypto.pbkdf2Sync(req.body.pass, salt, 1000, 64, 'sha512').toString('hex');
        await client.query("BEGIN");
        var cdate = new Date();

        var text;
        var value;
        var lpid = null;
        var uid;
        let sType = parseInt(req.body.sType);
        // For Provider signup type id
        if (sType === 1) {
            //check NPI No. exits
            let checkNPINoExists = await client.query(`SELECT * from m_providers WHERE npi = $1`, [req.body.npi]);
            //NPI No. found then,
            if (checkNPINoExists.rows.length) {
                lpid = checkNPINoExists.rows[0].pid;
                //check uid exist of this pid
                let checkUidExists = await client.query(`SELECT * from  m_users WHERE pid = $1 and usertypeid=$2`, [lpid, 1]);
                if (checkUidExists.rows.length > 0) {
                    throw new Error('Provider already exist');
                }
            }
            //NPI No. not found then,
            if (!checkNPINoExists.rows.length) {
                var sql = `INSERT into m_providers(npi,address1,address2,city,state_id,zipcode,fax_number,first_name,last_name)values($1, $2, $3, $4, $5, $6,$7,$8,$9) RETURNING *`;
                var param = [req.body.npi, req.body.addressone, req.body.addresstwo, req.body.city, req.body.stateid, req.body.zipcode, fax_number, req.body.fname, req.body.lname];
                let result = await client.query(sql, param);
                var provider = result.rows;
                lpid = provider[0].pid;
            }
        }
        // For Provider Support signup type id
        else if (sType === 3) {
            var result = await client.query(`select pid FROM m_users WHERE email =$1 and active=true and usertypeid=1`, [reqProviderEmail]);
            if (result.rowCount) {
                lpid = result.rows[0].pid;
            }
            if (!result.rowCount) {
                throw new Error('Provider does not exist.')
            }
        }
        //Master table all type of signup comman data insert here
        var roleId;
        if (sType == 1 || sType == 2) {
            roleId = 1; //For Provider or Patient
        } else if (sType == 3) {
            roleId = 2; //For Support
        } else {
            roleId = 1; //For Admin 
        }
        text = `INSERT into m_users
        (usertypeid, username, fullname, dob, idproof, address1, address2, email, phone_number, first_name, last_name, hash, salt, signup_token,active,
         createdate,termsofservices,pid,role_id,city,state,zipcode)
         values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22) RETURNING *`;
    value = [req.body.sType, user_email, name, dob, req.body.idproof, req.body.addressone, req.body.addresstwo, user_email, phone_number, req.body.fname,
    req.body.lname, hash, salt, token, false, cdate, req.body.termsofservices, lpid, roleId,req.body.city,req.body.stateid,req.body.zipcode];
    var responseData = await client.query(text, value);

        uid = responseData.rows[0].id;
        //If signup by provide(active=true) or provider support (active=false)
        let active = (sType === 1) ? true : false;
        if (sType === 1 || sType === 3) {
            var sql_uprovider = `INSERT into users_providers(uid,pid,usertypeid,active)values($1, $2, $3, $4) RETURNING *`;
            var param = [uid, lpid, req.body.sType, active];
            await client.query(sql_uprovider, param);
        }
        var mailResponse = await smtpTransport.sendMail(mailOptions);
        await client.query("COMMIT");
        await client.end();
        return res.json({ 'success': true, 'message': 'User Created', 'user_email':user_email });
    } catch (err) {
        console.log(err.message)
        await client.query("ROLLBACK");
        addErrorLog(err);
        await client.end();
        return res.json({ 'success': false, 'message': err.message });
    }
}
// User verification with token
export const userVerifyWithToken = async (req, res) => {
    try {
        var reqToken = req.body.e_token;
        var client = await createClient();
        var Newtoken;
        function rand(length, current) {
            current = current ? current : '';
            return length ? rand(--length, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz".charAt(Math.floor(Math.random() * 60)) + current) : current;
        }
        Newtoken = rand(25);
        client.connect();
        // Check signup token exits
        var result = await client.query(`select * FROM m_users WHERE signup_token =$1`, [reqToken]);
        if (result.rowCount) {
            var pId = result.rows[0].pid;
            var userId = result.rows[0].id;
            var userType = result.rows[0].usertypeid;
            var fullname = result.rows[0].fullname;
            // Update master table status
            var response = await client.query('update m_users set active = true, signup_token = $1 where signup_token = $2', [Newtoken, reqToken]);
            link = "https://app.curameitech.com/signin";
            // If signup is provider support then send email to provider
            if (userType == 3) {
                var response1 = await client.query(`select email,usertypeid,pid from m_users where usertypeid=1 and pid=$1`, [pId]);
                var getEmail = response1.rows[0].email;
                mailOptions = {
                    from: 'Curamei <' + fromEmail + '>',
                    to: getEmail,
                    subject: "Curamei - A new request approval",
                    html: `<p><a class="navbar-brand" href="http://intelli-books.com">
                                <table style="text-align: left; margin: 0 auto; padding: 16px;" border="0" width="568" cellspacing="0" cellpadding="0" align="center">
                                <tbody>
                                <tr>
                                <td style="width: 100%; display: block; max-width: 600px !important; text-align: center;" align="left"><span style="font-family: 'Open Sans',Arial,sans-serif; font-weight: 400; font-size: 16px; line-height: 24px; color: #0e1318; text-align: left; margin: 0;">Hello <strong>${fullname} ,</strong><br />A new request has been submitted for your approval. Please log into your account to approve the Support request.</span></td>
                                </tr>
                                </tbody>
                                </table>
                                <table style="border-collapse: separate; line-height: 100%; width: 292px; padding: 0px 16px; height: 34px;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tbody>
                                <tr>
                                <td style="border-radius: 4px; color: #ffffff; height: 52px; width: 258px; border: none;" role="presentation" align="center" valign="middle" bgcolor="#00c4cc">
                                <a href="${link}" style="text-decoration: none!important;"> <p style="background-color: #00c4cc; color: #ffffff; font-family: 'Open Sans',Arial,sans-serif; font-size: 16px; font-weight: 600; line-height: 24px; text-decoration: none; text-transform: none; border-radius: 4px; margin: 0; padding: 16px 0;">Click here to SignIn</p>
                                </a></td>
                                </tr>
                                </tbody>
                                </table>
                                <p>&nbsp;</p>
                                <hr />`
                }
                var mailResponse = await smtpTransport.sendMail(mailOptions);
            }
            //Signup is patient
            else if (userType == 2) {
                try {
                    let params = {
                        app_user_id: userId,
                        client_id: oneUPData.client_id,
                        client_secret: oneUPData.client_secret,
                    }
                    //Register patient in OneUp api
                    let res1 = await axios.post(oneUPData.signup_oneup, params);
                    //Store in master table oneup_user_id
                    await client.query(`UPDATE m_users SET oneup_user_id = $1 WHERE id = $2`,
                        [res1.data.oneup_user_id, userId]);

                }
                catch (err) {
                    addErrorLog(err);
                    return res.json({ 'success': false, 'message': err.message });
                }
            }

            await client.end();
            res.status(200).send({
                'success': true,
                'message': 'Account has been verified'
            });
        } else {
            await client.end();
            res.send({
                'success': false,
                'message': 'Invalid Token'
            });
        }
    } catch (err) {
        console.log(err.message);
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}
// Validate admin user
export const validateAdmin = async function (req, res) {
    try {
        var username = req.body.username;
        var password = req.body.password;
        if (!req.body.username)
            return res.json({
                'success': false,
                'message': 'Username  Not Found.'
            });
        if (!req.body.password)
            return res.json({
                'success': false,
                'message': 'Password Not Found.'
            });
        var client = await createClient();
        client.connect();
        //Usename check
        var result = await client.query(`SELECT * FROM m_users WHERE LOWER(username) = LOWER($1)`, [username]);
        if (result.rows.length === 0) {
            await client.end();
            return res.json({ 'success': false, 'message': 'Username or Password Invalid' });
        }
        var user = result.rows[0];
        //Function call for validate password
        var passwordResult = validPassword(password, user.salt, user.hash);
        if (!passwordResult) {
            await client.end();
            return res.json({ 'success': false, 'message': 'Username or Password Invalid' });
        }
        if (!user.active) {
            await client.end();
            return res.json({ 'success': false, 'message': 'Verify email to activate account' });
        }
        let userToken = generateJwt(user);
        var responseObj = {
            "token": userToken,
            'fullName': user.fullname,
            'userTypeId': user.usertypeid,
            'last_login': user.last_login,
            'enrollment_flg': user.enrollment_flg,
            'oneup_user_id': user.oneup_user_id,
            'role_id': user.role_id
        };
        var CurDate = new Date();
        var phone = user.phone_number;
        var mfa_required = user.mfa
        //Update last login date status
        await client.query('update m_users set last_login = $1 where id = $2', [CurDate, user.id]);
        await client.end();
        console.log("=============");
        console.log(mfa_required);
        return res.json({
            'success': true,
            'data': responseObj,
            'phone': phone,
            'mfa_required': mfa_required
        });

    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });

    }

}
// Forgot Password
export const forgotPassword = async function (req, res) {
    try{
        console.log(req.body.username)
        var client = await createClient();
        client.connect();
		var forget_password_token = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        var text = 'UPDATE m_users set forget_password_token = $1, forget_password_status = $2 where username = $3;'
        var values = [forget_password_token, true, req.body.username.toLowerCase()];
        var dataStatus = await client.query(text, values);
        if (dataStatus) {
            let result = await client.query(`SELECT * from m_users WHERE LOWER(username) = LOWER($1)`, [req.body.username]);
            var user = result.rows[0];
            // console.log(user)
            // console.log(user.first_name);
            // console.log(user.email);
            // console.log(user.forget_password_token)
            await client.end();
            var getEmail = user.email;
            link = "https://app.curameitech.com/reset-password?key=" + user.forget_password_token;
            mailOptions = {
                from: 'Curamei <' + fromEmail + '>',
                to: getEmail,
                subject: "Reset Your Password",
                html: `
                <table align="center" border="0" cellpadding="0" cellspacing="0" style="text-align: left; margin: 0 auto; padding: 16px;" width="568">
                        <tbody>
                            <tr>
                                <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;">
                                    <a href="https://curameitech.com/"><img height="50px" src="https://storage.googleapis.com/www.curameitech.com/images/WhiteBorder-Curamei-IconOnly.png" width="50px"></a>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;">
                                    <p><span style="font-family: 'Open Sans',Arial,sans-serif; font-weight: 400; font-size: 16px; color: #0e1318; text-align: center; margin: 0;"><strong>Password Recovery</strong></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="text-align: left; margin: 0 auto; padding: 0px 16px;" width="568">
                        <tbody>
                            <tr>
                                <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;">
                                    <p><span style="font-family: 'Open Sans',Arial,sans-serif; font-weight: 400; font-size: 16px; line-height: 24px; color: #0e1318; text-align: left; margin: 0;">Hey <strong>${user.first_name}</strong>, here is the link to reset your forgotten password.</span></p>
                                    <p><span style="font-family: 'Open Sans',Arial,sans-serif; font-weight: 400; font-size: 16px; line-height: 24px; color: #0e1318; text-align: left; margin: 0;">If you did not want to change your password or didn't request this, please ignore and delete this message. Feel free to reach out if you have concerns about your account security to <a href="mailto:contact@curameitech.com">contact@curameitech.com</a></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: separate; line-height: 100%; width: 292px; margin-top: 16px; height: 34px;">
                        <tbody>
                            <tr>
                                <td align="center" bgcolor="#008080" role="presentation" style="color: #ffffff; height: 52px; width: 258px; border: none; border-radius: 20px;" valign="middle">
                                    <a href="${link}" style="text-decoration: none!important;"><p style="background-color: #008080; color: #ffffff; font-family: 'Open Sans',Arial,sans-serif; font-size: 16px; font-weight: 600; line-height: 24px; text-decoration: none; text-transform: none; border-radius: 4px; margin: 0;">Reset your password</p></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="text-align: left; margin-top: 30px; padding: 0;" width="568">
                        <tbody>
                            <tr>
                                <td style="text-align:center;"><span><a href="https://twitter.com/CurameiT" style="margin-right: 10px;"><svg height="35px" viewbox="0 0 512 512" width="35px" xmlns="http://www.w3.org/2000/svg">
                                <path d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm116.886719 199.601562c.113281 2.519532.167969 5.050782.167969 7.59375 0 77.644532-59.101563 167.179688-167.183594 167.183594h.003906-.003906c-33.183594 0-64.0625-9.726562-90.066406-26.394531 4.597656.542969 9.277343.8125 14.015624.8125 27.53125 0 52.867188-9.390625 72.980469-25.152344-25.722656-.476562-47.410156-17.464843-54.894531-40.8125 3.582031.6875 7.265625 1.0625 11.042969 1.0625 5.363281 0 10.558593-.722656 15.496093-2.070312-26.886718-5.382813-47.140624-29.144531-47.140624-57.597657 0-.265624 0-.503906.007812-.75 7.917969 4.402344 16.972656 7.050782 26.613281 7.347657-15.777343-10.527344-26.148437-28.523438-26.148437-48.910157 0-10.765624 2.910156-20.851562 7.957031-29.535156 28.976563 35.554688 72.28125 58.9375 121.117187 61.394532-1.007812-4.304688-1.527343-8.789063-1.527343-13.398438 0-32.4375 26.316406-58.753906 58.765625-58.753906 16.902344 0 32.167968 7.144531 42.890625 18.566406 13.386719-2.640625 25.957031-7.53125 37.3125-14.261719-4.394531 13.714844-13.707031 25.222657-25.839844 32.5 11.886719-1.421875 23.214844-4.574219 33.742187-9.253906-7.863281 11.785156-17.835937 22.136719-29.308593 30.429687zm0" fill=" #008080"></path></svg></a> <a href="https://linkedin.com/company/curamei-technologies" style="margin-left: 10px;"><svg height="35px" viewbox="0 0 24 24" width="35px" xmlns="http://www.w3.org/2000/svg">
                                <path d="M19 0h-14c-2.761 0-5 2.239-5 5v14c0 2.761 2.239 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-11 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.396-2.586 7-2.777 7 2.476v6.759z" fill=" #008080"></path></svg></a></span></td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;font-family: 'Open Sans',Arial,sans-serif; font-size: 12px;">
                                    <a href="https://curameitech.com/help" style="text-decoration: none; padding: 10px;">Help Center</a> <a href="https://curameitech.com/privacy-policy" style="text-decoration: none; padding: 10px;">Privacy Policy</a> <a href="https://curameitech.com/terms-of-service" style="text-decoration: none; padding: 10px;">Terms of Service</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 100%; display: block; max-width: 600px !important; text-align: center;font-family: 'Open Sans',Arial,sans-serif; font-size: 12px;">Curamei Technologies © 2020, All Rights Reserved<br>
                                Ponte Vedra, FL</td>
                            </tr>
                        </tbody>
                    </table>
                `
            }
            var mailResponse = await smtpTransport.sendMail(mailOptions);
            console.log('======================================')
            console.log(mailResponse)
            console.log('======================================')

            return res.json({ 'success': true});
        } else {
        return res.json({ 'success': false});
        }

    }catch(err){
        console.log(err.message)
        return res.json({'success': false, 'message': err.message})
    }
}
// Validate reset password key
export const validateResetPasswordKey = async function (req, res) {
   try{
    var client = await createClient();
    client.connect();
    let result = await client.query(`SELECT * from m_users WHERE forget_password_token = $1 and forget_password_status = $2`, [req.params.key, true]);
    await client.end();
    if(result.rows[0]){
        return res.json({'success': true});
    }else{
        return res.json({'success': false});

    }
   }catch(err){
       return res.json({'success': false, 'message': err.message});
   }
}
// Reset password
export const resetPassword = async function (req, res){
    try{
        var client = await createClient();
        client.connect();
        if(req.body.password.toLowerCase() != req.body.repassword.toLowerCase())
        return res.json({'success': false, 'message': 'Password must be same'});
        // Strong Password encryption and store in salt and hash variable
        var salt = crypto.randomBytes(16).toString('hex');
        var hash = crypto.pbkdf2Sync(req.body.password, salt, 1000, 64, 'sha512').toString('hex');
        var text = 'UPDATE m_users set salt = $1, hash = $2, forget_password_status = $3 where forget_password_token = $4;'
        var values = [salt, hash, false, req.params.key];
        var dataStatus = await client.query(text, values);
        if(dataStatus){
            return res.json({'success': true})
        }else{
            return res.json({'success': false}) 
        }
    }catch(err){
        return res.json({'success': false, 'message': err.message});
    }
}
// Soacial Login
export const socialLogin = async function (req, res) {
    try{
        // var sType = 2
        var client = await createClient();
        client.connect();
        // let result = await client.query(`SELECT * from m_users WHERE LOWER(username) = LOWER($1) and usertypeid = $2`, [req.body.email, sType]);
        let result = await client.query(`SELECT * from m_users WHERE LOWER(username) = LOWER($1)`, [req.body.email]);

        if (result.rows.length === 0) {
            //   Add new user
            AddUserViaSocial(req, res)
        }else{
            var user = result.rows[0];
            if (!user.active) {
                await client.end();
                return res.json({ 'success': false, 'message': 'Verify email to activate account' });
            }
            let userToken = generateJwt(user);
            var responseObj = {
                "token": userToken,
                'fullName': user.fullname,
                'userTypeId': user.usertypeid,
                'last_login': user.last_login,
                'enrollment_flg': user.enrollment_flg,
                'oneup_user_id': user.oneup_user_id,
                'role_id': user.role_id
            };
            var CurDate = new Date();
            //Update last login date status
            await client.query('update m_users set last_login = $1 where id = $2', [CurDate, user.id]);
            await client.end();
            return res.json({
                'success': true,
                'data': responseObj
            });
        }
    }catch(err){
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}

async function AddUserViaSocial(req, res){
    try{
        console.log('====Hello===')
        console.log(req.body)
        // var client = await createClient();
        // client.connect();
        return res.json({'success': true, 'requiredProfile':true, 'data': req.body});
    }catch(err){
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}

export const loginRequired = (action) => {
    return async function (req, res, next) {
        try {
            if (!req.user)
                throw new Error('Access Denied');
            if (action) {
                var client = await createClient();
                client.connect();
                try {
                    let sqlPermission = await client.query(`select u.role_id,mrp.role_id
                          from m_users u
                          inner join m_role_permission mrp
                           on u.role_id = mrp.role_id
                          where u.id=$1 and mrp.action_id=$2`, [req.user.id, action]);
                    await client.end();
                    if (sqlPermission.rowCount > 0)
                        return next();
                    throw new Error('Access Denied');
                } catch (err) {
                    //client.end();
                    console.log(err.message);
                    return res.status(401).json({ message: err.message });
                }
            }
            return next()
        } catch (err) {
            console.log(err.message);
            return res.status(401).json({ message: err.message });
        }


    }
}

// Admin login required

export const adminLoginRequired = (action) => {
    return async function (req, res, next) {
        try {
            if (!req.user)
                throw new Error('Access Denied');
            if (action) {
                var client = await createClient();
                client.connect();
                try {
                    let sqlPermission = await client.query(`select u.role_id,mrp.role_id
                          from m_users u
                          inner join m_role_permission mrp
                           on u.role_id = mrp.role_id
                          where u.id=$1 and mrp.action_id=$2 and u.usertypeid=4`, [req.user.id, action]);
                    await client.end();
                    if (sqlPermission.rowCount > 0)
                        return next();
                    throw new Error('Access Denied');
                } catch (err) {
                    //client.end();
                    console.log(err.message);
                    return res.status(401).json({ message: err.message });
                }
            }
            return next()
        } catch (err) {
            console.log(err.message);
            return res.status(401).json({ message: err.message });
        }


    }
}

//Validate password
var validPassword = function (password, salt, hash) {
    var new_hash = crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString(`hex`);
    return hash === new_hash;
};
//Store information on JWT for security perpose
var generateJwt = function (user) {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
    return jwt.sign({
        id: user.id,
        username: user.username,
        fullName: user.fullname,
        userTypeId: user.usertypeid,
        oneup_user_id: user.oneup_user_id,
        enrollment_flg: user.enrollment_flg,
        role_id: user.role_id,
        pid: user.pid,
        exp: parseInt(expiry.getTime() / 1000),
    }, 'llp');
};

// ================== Operation by Admin =================
// Get all users
export const getAllUsers = async function(req, res){
    try{
        var client = await createClient();
        client.connect();     
        let text = `SELECT
        mu.id,
        mu.fullname,
        mu.username,
        mu.email,
        mu.active,
        mu.phone_number,
        mu.oneup_user_id,
        mu.dob,
        mu.idproof,
        mu.last_login,
        mu.mfa,
        ut.name
      from
        m_users mu
        INNER JOIN m_user_types ut ON mu.usertypeid = ut.id
      ORDER BY
        mu.fullname;`    
        var response = await client.query(text);   
        await client.end();
        if(response.rows.length){
            return res.json({'success': true, 'data':response.rows});
        }else{
            return res.json({'success': false, 'message': 'No record found'});
        }
    }catch(err){
        await client.end();
        addErrorLog(err);
        return res.json({'success': false, 'message': 'Error: '+err.message})
    }
}
// Apply MFA
export const applyMFA = async function(req, res) {
    try{
        var client = await createClient();
        client.connect();  
        let text = `UPDATE m_users set mfa=$1 where id=$2;`
        const values = [req.body.mfa_status, req.params.id];
        var dataStatus = await client.query(text, values);
        await client.end();
        if(dataStatus){
            return res.json({'success': true});
        }else{
            return res.json({'success': false, 'message': 'Error applying MFA'});
        }
    }catch(err){
        await client.end();
        addErrorLog(err);
        return res.json({'success': false, 'message': 'Error: '+err.message})
    }
}

