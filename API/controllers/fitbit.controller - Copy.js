
import axios from "axios";
import { createClient } from '../helper';
import { addErrorLog } from './errorLog.controller';
import { _secretKey } from '../config/common';
const SimpleCrypto = require("simple-crypto-js").default;
var format = require('date-format');
import fitbitConfigData from '../config/fitbit';




//Delete fitbit configuration by user
 

export const deleteUserFitbitConfig = async function (req, res) {
  try {
    var client = await createClient();
    client.connect();
    //Delete  patient bitbit configuration
    const sql = 'update m_users set isfitbit_code_avail=$1,fitbit_config=$2 where id=$3 ';
    const params = [false,null,req.user.id];
    const dataStatus = await client.query(sql, params);
    await client.end();
    if (dataStatus) {
      return res.json({ 'success': true, 'message': 'FitBit configuration has been removed' });
    } else {
      return res.json({ 'success': false, 'message': 'FitBit configuration has not been removed' });
    }
  } catch (err) {
    await client.end();
    addErrorLog(err);
    return res.json({ success: false, message: err.message });
  }
}



//Check Fitbit demo account permission
export const chechFitbitDemoPermission = async (req, res) => {
  var client = await createClient();
  try {
    client.connect();
    var resultData = await client.query(`SELECT is_fitbit_demo_enable from m_users WHERE is_fitbit_demo_enable=$1 and id=$2`, [true,req.user.id]);
    await client.end();
    if (resultData.rows.length) {
      return res.json({ 'success': true, 'message': 'Access' });
      }
      if (!resultData.rows.length){
        return res.json({ 'success': false, 'message': 'Access Denied' });
      }

    }catch (err) {
    addErrorLog(err);
    console.log(err.messsage);
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}

//Check & update fitbit data by access_token
export const checkUserFitbitConfig = async (req, res) => {
  var client = await createClient();
  try {

    client.connect();
    //Get Fitbit data
    var resultData = await client.query(`SELECT fitbit_config,isfitbit_code_avail from m_users WHERE isfitbit_code_avail=$1 AND id=$2`, [true, req.user.id]);
    var simpleCrypto = new SimpleCrypto(_secretKey);
    if (resultData.rows.length) {
    
      //Decript fitbit json data
      var fitbitObj = simpleCrypto.decryptObject(resultData.rows[0].fitbit_config);
      let dataValue = fitbitConfigData.client_id + ":" + fitbitConfigData.client_secret;
      let binaryData = Buffer.from(dataValue, "utf8");
      var AuthCode = binaryData.toString("base64");
      var encrypted;
      var result;
      //If  Fitbit refresh_token exists then get new access_token

      try{

          console.log('-------Object fitbitConfig-------------');
          console.log(fitbitObj);
          console.log('-------Object fitbitConfig-------------');

        if (fitbitObj.refresh_token) {
          result = await axios.post(`https://api.fitbit.com/oauth2/token?grant_type=refresh_token&refresh_token=${fitbitObj.refresh_token}`, null,
            {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic ' + AuthCode
              }
            })
          fitbitObj.refresh_token = result.data.refresh_token;
          //Encript fitbit json data
  
          encrypted = simpleCrypto.encrypt(fitbitObj);
          //Update fitbit data
          await client.query(`UPDATE m_users SET fitbit_config=$1 WHERE id=$2`, [encrypted, req.user.id]);
          await client.end();
          return res.json({ 'success': true, 'data': result.data.access_token });
        }
      }catch(err){
            console.log("+++ If  Fitbit refresh_token exists then get new access_token +++");
            console.log(err);
            return res.json({ 'success': false, 'message': err.message });
      }
   
      //If  Fitbit refresh_token not exists then get new access_token (pass fitbit code)
      if (!fitbitObj.refresh_token) {
        result = await axios.post(`https://api.fitbit.com/oauth2/token?code=${fitbitObj.code}&grant_type=authorization_code&redirect_uri=${fitbitConfigData.callback_url}`, null,
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': 'Basic ' + AuthCode
            }
          })

        fitbitObj.refresh_token = result.data.refresh_token;
        //Encript fitbit json data
        encrypted = simpleCrypto.encrypt(fitbitObj);
        //Update fitbit data
        await client.query(`UPDATE m_users SET fitbit_config=$1 WHERE id=$2`, [encrypted, req.user.id]);
        await client.end();
        return res.json({ 'success': true, 'data': result.data.access_token });
      }

    }
    await client.end();
    if (!resultData.rows.length){ 
   return res.json({ 'success': 'redirect', 'url': `https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=${fitbitConfigData.client_id}&redirect_uri=${fitbitConfigData.callback_url}&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight` });

    }
       
  }
  catch (err) {
    addErrorLog(err);
    console.log("-------------------------------------------checkUserFitbitConfig--------------------------------");
     console.log(err);
   // console.log(err.message);
    console.log("-------------------------------------------checkUserFitbitConfig--------------------------------");
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Check fitbit config by user
export const checkUserFitbitConfigByUser = async (req, res) => {
  var client = await createClient();
  try {
    client.connect();
    //Get Fitbit data
    var resultData = await client.query(`SELECT fitbit_config,isfitbit_code_avail from m_users WHERE isfitbit_code_avail=$1 AND id=$2`, [true, req.params.user_id]);
    var simpleCrypto = new SimpleCrypto(_secretKey);
    if (resultData.rows.length) {
      //Decript fitbit json data
      var fitbitObj = simpleCrypto.decryptObject(resultData.rows[0].fitbit_config);
      let dataValue = fitbitConfigData.client_id + ":" + fitbitConfigData.client_secret;
      let binaryData = Buffer.from(dataValue, "utf8");
      var AuthCode = binaryData.toString("base64");
      var encrypted;
      var result;
      //If  Fitbit refresh_token exists then get new access_token
      if (fitbitObj.refresh_token) {
        result = await axios.post(`https://api.fitbit.com/oauth2/token?grant_type=refresh_token&refresh_token=${fitbitObj.refresh_token}`, null,
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': 'Basic ' + AuthCode
            }
          })
        fitbitObj.refresh_token = result.data.refresh_token;
        //Encript fitbit json data

        encrypted = simpleCrypto.encrypt(fitbitObj);
        //Update fitbit data
        await client.query(`UPDATE m_users SET fitbit_config=$1 WHERE id=$2`, [encrypted, req.params.user_id]);
        await client.end();
        return res.json({ 'success': true, 'data': result.data.access_token });
      }
      //If  Fitbit refresh_token not exists then get new access_token (pass fitbit code)
      if (!fitbitObj.refresh_token) {
        result = await axios.post(`https://api.fitbit.com/oauth2/token?code=${fitbitObj.code}&grant_type=authorization_code&redirect_uri=${fitbitConfigData.callback_url}`, null,
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': 'Basic ' + AuthCode
            }
          })

        fitbitObj.refresh_token = result.data.refresh_token;
        //Encript fitbit json data
        encrypted = simpleCrypto.encrypt(fitbitObj);
        //Update fitbit data
        await client.query(`UPDATE m_users SET fitbit_config=$1 WHERE id=$2`, [encrypted, req.params.user_id]);
        await client.end();
        return res.json({ 'success': true, 'data': result.data.access_token });
      }

    }
    await client.end();
    if (!resultData.rows.length){ 
   return res.json({ 'success': 'redirect', 'url': `https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=${fitbitConfigData.client_id}&redirect_uri=${fitbitConfigData.callback_url}&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight` });

    }
       
  }
  catch (err) {
    addErrorLog(err);
    console.log(err.messsage);
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}
//Update fitbit data
export const updateFitbitCode = async (req, res) => {
  try {
    var client = await createClient();
    client.connect();
    var simpleCrypto = new SimpleCrypto(_secretKey);
    let fitbitObj = {
      code: req.body.code
    }
    let encrypted = simpleCrypto.encrypt(fitbitObj);
      //Update fitbit data
      await client.query(`UPDATE m_users SET fitbit_config=$1 WHERE id=$2`, [encrypted, req.user.id]);
      let dataValue = fitbitConfigData.client_id + ":" + fitbitConfigData.client_secret;
      let binaryData = Buffer.from(dataValue, "utf8");
      var AuthCode = binaryData.toString("base64");
      //If  Fitbit refresh_token not exists then get new access_token (pass fitbit code)
      var result = await axios.post(`https://api.fitbit.com/oauth2/token?code=${fitbitObj.code}&grant_type=authorization_code&redirect_uri=${fitbitConfigData.callback_url}`, null,
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + AuthCode
          }
        })
      fitbitObj.refresh_token = result.data.refresh_token;
      //Encript fitbit json data
      encrypted = simpleCrypto.encrypt(fitbitObj);
      //Update fitbit data
      var curDate = new Date();
      await client.query(`UPDATE m_users SET fitbit_config=$1,isfitbit_code_avail=$2,fitbit_created_on=$3 WHERE id=$4`, [encrypted, true, curDate, req.user.id]);
      await client.end();
      return res.json({ 'success': true, 'data': result.data.access_token });
  }
  catch (err) {
    addErrorLog(err);
    console.log(err.message);
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}

// Get today activity
export const getTodayActivity = async (req, res) => {
  try {
    var client = await createClient();
    client.connect();
    var activityObj;
    var flagData =JSON.parse(req.params.flag);
    if(flagData) {
      var result = await axios.get(`https://api.fitbit.com/1/user/-/activities/date/today.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })
        activityObj = {
        steps: result.data.summary.steps,
        goalCaloriesOut: result.data.goals.caloriesOut,
        caloriesOut: result.data.summary.caloriesOut
      }
    }

    if(!flagData) {
      let resulTSet = await client.query(`SELECT calories_out,steps FROM fitbit_records_dummy ORDER BY frd_id DESC LIMIT 1`);
      activityObj = {
        steps: resulTSet.rows[0].steps,
        goalCaloriesOut: 0,
        caloriesOut: resulTSet.rows[0].calories_out
      }
    }
    await client.end();
    return res.json({ 'success': true, 'activityObj': activityObj });
  }
  catch (err) {
    addErrorLog(err);
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Get last week activity
export const getlastTwoWeekActivity = async (req, res) => {
  try {
    var client = await createClient();
    client.connect();
    let dateObj = new Date();
    var currentDate = format.asString('yyyy-MM-dd', new Date());
    var ourDate = new Date();
    //Change it so that it is 14 days in the past.
    var pastDate = ourDate.getDate() - 13;
    ourDate.setDate(pastDate);
    ourDate = format.asString('yyyy-MM-dd', ourDate);

    var lastTwoWeekTotalSteps = 0;
    var lastTwoWeekAVgSteps = 0;
    var weeksGraph = [];
    var oneGraphActivity = [];
    var activityObj;
    var flagData =JSON.parse(req.params.flag);
    if (flagData) {
      var result = await axios.get(`https://api.fitbit.com/1/user/-/activities/steps/date/${ourDate}/${currentDate}.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })


        console.log("====================================ACTGIVITY===============================");
        console.log(result.data);
        console.log(req.params.token);
     
        console.log("====================================ACTGIVITY===============================");

      var dataObj = result.data[Object.keys(result.data)];
      for (let i = 0; i < dataObj.length; i++) {
        lastTwoWeekTotalSteps += parseInt(dataObj[i].value);
       // weeksGraph.push({ "name": i + 1, "value": dataObj[i].value });
        weeksGraph.push({ "name":  dataObj[i].dateTime, "value": dataObj[i].value });
      }
    }

    if (!flagData) {
      var resultSet = await client.query(`SELECT steps FROM fitbit_records_dummy ORDER BY frd_id DESC LIMIT 14`);
        
      
      for (let i = 0; i < resultSet.rows.length; i++) {
           lastTwoWeekTotalSteps += parseInt(resultSet.rows[i].steps);
           weeksGraph.push({ "name": i + 1, "value": resultSet.rows[i].steps});
            }
    }
      oneGraphActivity.push({ "name": "Germany", "value": lastTwoWeekTotalSteps });
      lastTwoWeekAVgSteps = parseInt(lastTwoWeekTotalSteps / 14);
       activityObj = {
        lastTwoWeekAVgSteps: lastTwoWeekAVgSteps,
        lastTwoWeekTotalSteps: lastTwoWeekTotalSteps,
        weeksGraph: weeksGraph,
        oneGraphActivity: oneGraphActivity
      }
      await client.end();
      return res.json({ 'success': true, 'activityObj': activityObj });
    
  }
  catch (err) {
    addErrorLog(err);
    console.log(err.message);
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Get last two week calories
export const getlastTwoWeekCalories = async (req, res) => {
  try {
    var client = await createClient();
    client.connect();
    let dateObj = new Date();
    var currentDate = format.asString('yyyy-MM-dd', new Date());
    var ourDate = new Date();
    //Change it so that it is 14 days in the past.
    var pastDate = ourDate.getDate() - 13;
    ourDate.setDate(pastDate);
    ourDate = format.asString('yyyy-MM-dd', ourDate);
    var lastTwoWeekTotalCaloriesOut = 0;
    var lastTwoWeekAVgCaloriesOut = 0;
    var weeksGraph = [];
    var oneGraphCalories = [];
    var caloriesObj;
    var flagData =JSON.parse(req.params.flag);
     
    if (flagData) {
      var result = await axios.get(`https://api.fitbit.com/1/user/-/activities/tracker/calories/date/${ourDate}/${currentDate}.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })
      var dataObj = result.data[Object.keys(result.data)];
      for (let i = 0; i < dataObj.length; i++) {
        lastTwoWeekTotalCaloriesOut += parseInt(dataObj[i].value);
       // weeksGraph.push({ "name": i + 1, "value": dataObj[i].value });
       weeksGraph.push({ "name": dataObj[i].dateTime, "value": dataObj[i].value });
      
      }

    }
    if (!flagData) {
      var resultSet = await client.query(`SELECT calories_out FROM fitbit_records_dummy ORDER BY frd_id DESC LIMIT 14`);
      for (let i = 0; i < resultSet.rows.length; i++) {
        lastTwoWeekTotalCaloriesOut += parseInt(resultSet.rows[i].calories_out);
           weeksGraph.push({ "name": i + 1, "value": resultSet.rows[i].calories_out});
            }
    }


      lastTwoWeekAVgCaloriesOut = parseInt(lastTwoWeekTotalCaloriesOut / 14);
      oneGraphCalories.push(
        { "name": "Calories Burnt", "value": lastTwoWeekTotalCaloriesOut });

      caloriesObj = {
        lastTwoWeekAVgCaloriesOut: lastTwoWeekAVgCaloriesOut,
        lastTwoWeekTotalCaloriesOut: lastTwoWeekTotalCaloriesOut,
        weeksGraph: weeksGraph,
        oneGraphCalories: oneGraphCalories
      }
      await client.end();
      return res.json({ 'success': true, 'caloriesObj': caloriesObj });
    
  }
  catch (err) {
    addErrorLog(err);
    console.log(err.message);
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Get last two week calories In
export const getlastTwoWeekInCalories = async (req, res) => {
  try {
    var client = await createClient();
    client.connect();
    var currentDate = format.asString('yyyy-MM-dd', new Date());
    var ourDate = new Date();
    //Change it so that it is 14 days in the past.
    var pastDate = ourDate.getDate() - 13;
    ourDate.setDate(pastDate);
    ourDate = format.asString('yyyy-MM-dd', ourDate);
    var lastTwoWeekTotalCaloriesIn = 0;
    var weeksGraph = [];
    var oneWeekGraph = [];
    var oneGraphCaloriesIn = [];
    var todayCaloriesIn = 0;
    var caloriesInObj;
    var flagData =JSON.parse(req.params.flag);
    if (flagData) {
      var result = await axios.get(`https://api.fitbit.com/1/user/-/foods/log/caloriesIn/date/${ourDate}/${currentDate}.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })

        var result1 = await axios.get(`https://api.fitbit.com//1/user/-/foods/log/caloriesIn/date/${currentDate}/1w.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })
  
      var dataObj = result.data[Object.keys(result.data)];
      var dataObj1 = result1.data[Object.keys(result1.data)];
       for (let i = 0; i < dataObj1.length; i++) {
        oneWeekGraph.push({ "name":  dataObj1[i].dateTime, "value": dataObj1[i].value });
      }
      for (let i = 0; i < dataObj.length; i++) {
        lastTwoWeekTotalCaloriesIn += parseInt(dataObj[i].value);
        weeksGraph.push({ "name": i + 1, "value": dataObj[i].value });
          if(i==13)
          todayCaloriesIn = dataObj[i].value;
      }

    }

    
    if (!flagData) {
      var resultSet = await client.query(`SELECT calories_in,createdon FROM fitbit_records_dummy ORDER BY frd_id DESC LIMIT 14`);
      for (let i = 0; i < resultSet.rows.length; i++) {
        lastTwoWeekTotalCaloriesIn += parseInt(resultSet.rows[i].calories_in);
           weeksGraph.push({ "name": i + 1, "value": resultSet.rows[i].calories_in});
               if(i<=6)
                oneWeekGraph.push({ "name":  resultSet.rows[i].createdon, "value":resultSet.rows[i].calories_in});
               if(i==13)
               todayCaloriesIn = resultSet.rows[i].calories_in;

            }
    }
      oneGraphCaloriesIn.push({ "name": "Calories In", "value": lastTwoWeekTotalCaloriesIn });
      caloriesInObj = {
        lastTwoWeekTotalCaloriesIn: lastTwoWeekTotalCaloriesIn,
        weeksGraph: weeksGraph,
        oneGraphCaloriesIn: oneGraphCaloriesIn,
        todayCaloriesIn: todayCaloriesIn,
        oneWeekGraph:oneWeekGraph
      }
      await client.end();
      return res.json({ 'success': true, 'caloriesInObj': caloriesInObj });
   
  }
  catch (err) {
    addErrorLog(err);
    console.log(err.message);
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Get last two week weight
export const getlastTwoWeekWeight = async (req, res) => {
  try {
    var client = await createClient();
    client.connect();
    var currentDate = format.asString('yyyy-MM-dd', new Date());
    var ourDate = new Date();
    //Change it so that it is 14 days in the past.
    var pastDate = ourDate.getDate() - 13;
    ourDate.setDate(pastDate);
    ourDate = format.asString('yyyy-MM-dd', ourDate);
    var lastTwoweeksGraph=[];
    var todayWeightGraph = [];
     var todayBmi = 0;
     var todayWeight = 0;
     var todayFat = 0;
     var weightObj;
     var flagData =JSON.parse(req.params.flag);
    if (flagData) {
      var result = await axios.get(`https://api.fitbit.com/1/user/-/body/weight/date/${ourDate}/${currentDate}.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })
        
        var recentBmi = await axios.get(`https://api.fitbit.com/1/user/-/body/bmi/date/${currentDate}/1d.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })



        var recentWeight = await axios.get(`https://api.fitbit.com/1/user/-/body/weight/date/${currentDate}/1d.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })



        var recentFat = await axios.get(`https://api.fitbit.com/1/user/-/body/fat/date/${currentDate}/1d.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })

      var lastTwoweeksData = result.data[Object.keys(result.data)];
     var recentBmiData = recentBmi.data[Object.keys(recentBmi.data)];
     var recentWeightData = recentWeight.data[Object.keys(recentWeight.data)];
     var recentFatData = recentFat.data[Object.keys(recentFat.data)];
 
      todayBmi = recentBmiData[0].value;
     todayWeight = recentWeightData[0].value;
     todayFat = recentFatData[0].value;
      for (let i = 0; i < lastTwoweeksData.length; i++) {
       // lastTwoweeksGraph.push({ "name": i + 1, "value": lastTwoweeksData[i].value });
        lastTwoweeksGraph.push({ "name": lastTwoweeksData[i].dateTime, "value":  lastTwoweeksData[i].value });
      }
    }  

   if (!flagData) {
      var resultSet = await client.query(`SELECT bmi,fat,weight FROM fitbit_records_dummy ORDER BY frd_id DESC LIMIT 14`);
      for (let i = 0; i < resultSet.rows.length; i++) {
       // sleepWeeksTotal += parseInt(resultSet.rows[i].sleep);
        lastTwoweeksGraph.push({ "name": i + 1, "value": resultSet.rows[i].weight});
               if(i==13){
                todayBmi = resultSet.rows[i].bmi;
                todayWeight = resultSet.rows[i].weight;
                todayFat = resultSet.rows[i].fat;
               }
            }
    }
      todayWeightGraph.push({ "name": "Weight", "value": todayWeight });
      weightObj = {
        lastTwoweeksGraph: lastTwoweeksGraph,
        todayBmi: todayBmi,
        todayWeight: todayWeight,
        todayFat:todayFat,
        todayWeightGraph:todayWeightGraph
      }
      await client.end();
      return res.json({ 'success': true, 'weightObj': weightObj });
  }
  catch (err) {
    addErrorLog(err);
    console.log(err.message);
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}
// Get last two week sleep
export const getlastTwoWeekSleep = async (req, res) => {
  try {
    var client = await createClient();
    client.connect();
    var currentDate = format.asString('yyyy-MM-dd', new Date());
    var ourDate = new Date();
    //Change it so that it is 14 days in the past.
    var pastDate = ourDate.getDate() - 13;
    ourDate.setDate(pastDate);
    ourDate = format.asString('yyyy-MM-dd', ourDate);
    var sleepWeeksGraph = [];
    var sleepWeeksTotal = 0;
    var weeksHours =0;
    var weeksMinutes = 0;
    var todayHours =0;
    var todayMinutes =0;
    var weeksAvgSleepHours =0;
    var oneGraphWeeksSleep = [];
    var sleepToday;
    var sleepObj;
    var flagData =JSON.parse(req.params.flag);
    if (flagData) {
      var sleepWeeksObj = await axios.get(`https://api.fitbit.com/1.2/user/-/sleep/date/${ourDate}/${currentDate}.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })

        var sleepTodayObj = await axios.get(`https://api.fitbit.com/1.2/user/-/sleep/date/${currentDate}.json`,
        {
          headers: {
            'Authorization': 'Bearer ' + req.params.token
          }
        })


                  sleepToday  = (sleepTodayObj.data.summary.totalMinutesAsleep==0) ? 0 : sleepTodayObj.data.summary.totalMinutesAsleep;
                 for (let i = 0; i < sleepWeeksObj.data.sleep.length; i++) {
                  sleepWeeksGraph.push({ "name": sleepWeeksObj.data.sleep[i].dateOfSleep, "value":  sleepWeeksObj.data.sleep[i].minutesAsleep });
                  
                  sleepWeeksTotal += parseInt(sleepWeeksObj.data.sleep[i].minutesAsleep);
                }

     }


 if (!flagData) {
      var resultSet = await client.query(`SELECT sleep FROM fitbit_records_dummy ORDER BY frd_id DESC LIMIT 14`);
      for (let i = 0; i < resultSet.rows.length; i++) {
        sleepWeeksTotal += parseInt(resultSet.rows[i].sleep);
        sleepWeeksGraph.push({ "name": i + 1, "value": resultSet.rows[i].sleep});
               if(i==13)
               sleepToday = resultSet.rows[i].sleep;

            }
    }

                    if(sleepWeeksTotal!=0){
                      let avgSleep = parseInt(sleepWeeksTotal / 14);
                      weeksAvgSleepHours = Math.floor(avgSleep / 60);
                       weeksHours = Math.floor(sleepWeeksTotal / 60);  
                       weeksMinutes = sleepWeeksTotal % 60;
                    }else{
                      weeksHours=0;
                      weeksMinutes=0;
                      weeksAvgSleepHours=0;
                    }
                    if(sleepToday!=0){
                       todayHours = Math.floor(sleepToday / 60);  
                      todayMinutes = sleepToday % 60;
                    }else{
                      todayHours = 0;
                      todayMinutes =0;
                    }
                    oneGraphWeeksSleep.push({ "name": "Sleep", "value": sleepWeeksTotal });
      sleepObj = {
        sleepWeeksTotal: sleepWeeksTotal,
        sleepWeeksGraph: sleepWeeksGraph,
        weeksAvgSleepHours: weeksAvgSleepHours,
        weeksHours: weeksHours,
        weeksMinutes: weeksMinutes,
        todayHours: todayHours,
        todayMinutes: todayMinutes,
        oneGraphWeeksSleep:oneGraphWeeksSleep
      }

      await client.end();
      return res.json({ 'success': true, 'sleepObj': sleepObj });
  }
  catch (err) {
    addErrorLog(err);
    await client.end();
    console.log(err.message);
    return res.json({ 'success': false, 'message': err.message });
  }
}

//Carories in vs out
export const getlastWeekInOutCalories = async (req, res) => {
  try {
     var client = await createClient();
    client.connect();
    var currentDate = format.asString('yyyy-MM-dd', new Date());
    var ourDate = new Date();
    //Change it so that it is 14 days in the past.
    var pastDate = ourDate.getDate() - 13;
    ourDate.setDate(pastDate);
    ourDate = format.asString('yyyy-MM-dd', ourDate);
    var lastWeekTotalCaloriesIn = 0;
    var lastWeekTotalCaloriesOut = 0;
    var lastWeekTotalDiff=0;
    var oneWeekGraph = [];
    var flagData =JSON.parse(req.params.flag);
    var caloriesInOutObj;
    var weeksGraph = [];
    // if (flagData) {
    //   var result = await axios.get(`https://api.fitbit.com/1/user/-/foods/log/caloriesIn/date/${ourDate}/${currentDate}.json`,
    //     {
    //       headers: {
    //         'Authorization': 'Bearer ' + req.params.token
    //       }
    //     })

    //     var result1 = await axios.get(`https://api.fitbit.com//1/user/-/foods/log/caloriesIn/date/${currentDate}/1w.json`,
    //     {
    //       headers: {
    //         'Authorization': 'Bearer ' + req.params.token
    //       }
    //     })
  
    //   var dataObj = result.data[Object.keys(result.data)];
    //   var dataObj1 = result1.data[Object.keys(result1.data)];
    //    for (let i = 0; i < dataObj1.length; i++) {
    //     oneWeekGraph.push({ "name":  dataObj1[i].dateTime, "value": dataObj1[i].value });
    //   }
    //   for (let i = 0; i < dataObj.length; i++) {
    //     lastTwoWeekTotalCaloriesIn += parseInt(dataObj[i].value);
    //     weeksGraph.push({ "name": i + 1, "value": dataObj[i].value });
    //       if(i==13)
    //       todayCaloriesIn = dataObj[i].value;
    //   }

    // }

    
    if (!flagData) {
      var resultSet = await client.query(`SELECT calories_in,createdon,calories_out FROM fitbit_records_dummy ORDER BY frd_id DESC LIMIT 7`);
      for (let i = 0; i < resultSet.rows.length; i++) {
        lastWeekTotalCaloriesIn += parseInt(resultSet.rows[i].calories_in);
        lastWeekTotalCaloriesOut += parseInt(resultSet.rows[i].calories_out);
        var series = [];
        var days = "Day " + (i+1);
        series.push({"name": "Calorie In","value": resultSet.rows[i].calories_in},{"name": "Calorie Out", "value": resultSet.rows[i].calories_out })
        weeksGraph.push({days, series});
            }
            lastWeekTotalDiff = lastWeekTotalCaloriesIn - lastWeekTotalCaloriesOut;
            lastWeekTotalDiff = Math.abs(lastWeekTotalDiff); 
            oneWeekGraph.push( {"name": " Calories In", "value": lastWeekTotalCaloriesIn }, { "name": "Calories Out","value": lastWeekTotalCaloriesOut })
    }


        weeksGraph.forEach(element => {
                      console.log(element.series)
             });



      caloriesInOutObj = {
        lastWeekTotalCaloriesIn: lastWeekTotalCaloriesIn,
        lastWeekTotalCaloriesOut: lastWeekTotalCaloriesOut,
        lastWeekTotalDiff: lastWeekTotalDiff,
        weeksGraph :weeksGraph,
        oneWeekGraph:oneWeekGraph
      }
      await client.end();
      return res.json({ 'success': true, 'caloriesInOutObj': caloriesInOutObj });
   
  }
  catch (err) {
    addErrorLog(err);
    console.log(err.message);
    await client.end();
    return res.json({ 'success': false, 'message': err.message });
  }
}



