
import { createClient } from '../helper';
import { addErrorLog } from './errorLog.controller';
import { actionEnum } from '../config/common';


//Get all resources data
export const getResourceList = async function (req, res) {
    try {
        var client = await createClient();
        client.connect();
        const text = `select * from m_resource`;
        const dataStatus = await client.query(text);
        await client.end();
        if (dataStatus.rows) {
            return res.json({ 'success': true, 'data': dataStatus.rows });
        } else {
            return res.json({ 'success': false, 'message': 'Resource not found' });
        }
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}

//Patient sharing resoure to provider
export const patientResourceSharingSave = async function (req, res) {
    try {
        console.log(req.body)
        var client = await createClient();
        client.connect();
        const checkExits = await client.query(`SELECT id from m_users where pid=$1 and usertypeid=$2`,[req.body.providerName,1]); 
        if(checkExits.rowCount > 0){
        var providers = [];
        providers.push(req.body.providerName);
        var resources = req.body.ResourceName;
        for(var i=0; i<providers.length; i++){
            for(var j=0; j<resources.length; j++){
                const checkExits = await client.query(`select * from patient_provider_resource where pat_id=$1 and prov_id=$2 and resource_id=$3`,[req.user.id, providers[i], resources[j]]); 
                console.log(checkExits.rowCount)
                if(checkExits.rowCount == 0){
                    console.log('not matched')
                    const sqlPatientResorce = `INSERT INTO patient_provider_resource(pat_id,prov_id,resource_id)VALUES($1,$2,$3)`;
                    const params = [req.user.id, providers[i], resources[j]];
                    const dataStatus = await client.query(sqlPatientResorce,params);
                }else{
                }
            }
        }
        return res.json({ 'success': true, 'message':'Patient resource sharing added successfully' });
        }else{
            await client.end();
            return res.json({ 'success': false, 'message':'Provider is not registered with Curamei. Please ask Provider to Signup.' });
        }

    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}

//Get Patient sharing resoure for provider
export const getPatientResourceSharing = async function (req, res) {
    try {
        var client = await createClient();
        client.connect();
        const sqlPatientResorce = `SELECT r.resource_name,ppr.pat_prov_id,
        case when mp.full_name is null then 
        CONCAT  (mp.first_name, ' ', mp.last_name)
        else  mp.full_name end as fullname
        FROM patient_provider_resource ppr
                INNER JOIN m_resource r  using(resource_id)
                INNER JOIN m_providers mp on mp.pid =ppr.prov_id   
                WHERE pat_id=$1`;
          const params = [req.user.id];
        const dataSet = await client.query(sqlPatientResorce,params);
        await client.end();
        if (dataSet.rowCount > 0) {
            return res.json({ 'success': true, 'data':dataSet.rows});
        } else {
            return res.json({ 'success': false, 'message':'Record is not available'});
        }
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }

}


//Delete Patient sharing resoure for provider
export const patientResourceSharingDelete = async function (req, res) {
    try {
      var client = await createClient();
      client.connect();
      let cur_date = new Date();
      const sqlDelete = 'DELETE FROM patient_provider_resource WHERE pat_prov_id=$1';
      const params = [req.params.patProvId];
      const dataStatus = await client.query(sqlDelete, params);
      await client.end();
      return res.json({ 'success': true, 'message': 'Provider Sharing been deleted' });
    } catch (err) {
      await client.end();
      addErrorLog(err);
      return res.json({ success: false, message: err.message });
    }
  }

  
//Get all Roles data
  export const getRole = async function (req, res) {
    try {
        var client = await createClient();
        client.connect();
        const sqlRole = `SELECT * FROM m_role`;
        const dataRole = await client.query(sqlRole);
        await client.end();
        if (dataRole.rowCount) {
            return res.json({ 'success': true, 'data':dataRole.rows});
        } else {
            return res.json({ 'success': false, 'message': 'Role is not available' });
        }
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}



//Get all applicaton permission
export const getAppicationPermission = async function (req, res) {
    try {
        var client = await createClient();
        client.connect();
        const sqlAppPermtn = `SELECT *,
        CASE WHEN action.action_id IS NULL THEN false ELSE true END as selected
        FROM m_action
        LEFT OUTER JOIN 
        (SELECT * FROM m_role_permission WHERE role_id = $1)as action
        ON m_action.aid = action.action_id`;
        const params = [req.params.roleId];
        const dataAppPermtn = await client.query(sqlAppPermtn,params);
        await client.end();
        if (dataAppPermtn.rowCount) {
            return res.json({ 'success': true, 'data':dataAppPermtn.rows});
        } else {
            return res.json({ 'success': false, 'message': 'Application permission is not available' });
        }
    } catch (err) {
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}



//ADD  applicaton permission
export const AppicationPermissionSave = async function (req, res) {
    try {

      
        var data = req.body.data;
         var client = await createClient();
    client.connect();
    console.log(data);
    await client.query(`DELETE FROM m_role_permission WHERE role_id=$1`,[req.body.roleId]);
    for(var i=0 ; i < data.length;i++){
       
                 if(data[i].selected){
                await client.query(`INSERT INTO m_role_permission(role_id,action_id)Values($1,$2)`,[req.body.roleId,data[i].aid]);
                 }
         }
         await client.end();
            return res.json({ 'success': true, 'message':'Application Permission  added successfully' }); 
    } catch (err) {
        console.log(err);
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}

//Check medical records access permission
export const chechMedicalRecordAccessPermission = async function (req, res) {
    try {
   var client = await createClient();
    client.connect();
    let sqlPermission = await client.query(`select u.role_id,mrp.role_id
    from m_users u
    inner join m_role_permission mrp
     on u.role_id = mrp.role_id
    where u.id=$1 and mrp.action_id=$2`,[req.user.id,actionEnum.MedicalRecords]);
    await client.end();
      if(sqlPermission.rowCount > 0)
      return res.json({ 'success': true, 'message':'Accessed' }); 
      else
      return res.json({ 'success': false, 'message':'Access Denied' }); 
    } catch (err) {
        console.log(err);
        await client.end();
        addErrorLog(err);
        return res.json({ 'success': false, 'message': err.message });
    }
}




