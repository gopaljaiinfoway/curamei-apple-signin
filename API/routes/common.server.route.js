import express from 'express';
var multer = require('multer')
var path = require('path');
const fs = require('fs');
import * as AdminDashboardController from '../controllers/admin-dashboard.controller';
import * as FititControler from '../controllers/fitbit.controller';
import * as PatientController from '../controllers/patient.controller';
import * as POCTestController from '../controllers/poc-test.controller';
import * as UserController from '../controllers/user.controller';
import * as SharingResouceController from '../controllers/sharing-resouce.controller'
import * as ManagePatientAccessController from '../controllers/manage-patient-access.controller';
import * as PatientDashboardController from '../controllers/patient-dashboard.controller';
import * as ProviderDahboardController from '../controllers/provider-dashboard.controller';
import { actionEnum } from '../config/common';

const router = express.Router();
//===============User signup  router here start ================
router.route('/user-logined').get(UserController.loginRequired(null),UserController.checkUserLogined);
router.route('/login').post(UserController.validateAdmin);
router.route('/signup').post(UserController.addUser);
router.route('/uploadIDProof').post(UserController.uploadIDProof);
router.route('/emailCheck').post(UserController.emailCheckUser);
router.route('/contactCheck').post(UserController.contactCheckUser);
router.route('/npiCheck').post(UserController.npiChecker);
router.route('/checkUsers').post(UserController.checkUsers);
router.route('/user-verify').post(UserController.userVerifyWithToken);
router.route('/socialLogin').post(UserController.socialLogin);
router.route('/forgot-password').post(UserController.forgotPassword);
router.route('/validate-resetpassword/:key').get(UserController.validateResetPasswordKey);
router.route('/reset-password/:key').post(UserController.resetPassword);

//===============NPI related operation ================
router.route('/npi/:id').get(UserController.getNPI);
router.route('/healthsystem/:orgname/:npi').get(UserController.getHealthSystem);
router.route('/healthsystemIndividualFirstName/:firstname/:npi').get(UserController.getHealthSystemIndividualFirstName);
router.route('/healthsystemIndividualLastName/:lastname/:npi').get(UserController.getHealthSystemIndividualLastName);
router.route('/healthsystemIndividualBothName/:firstname/:lastname/:npi').get(UserController.getHealthSystemIndividualBothName);

//===============Admin  router here start ================
router.route('/admin-user-list').get(UserController.loginRequired(null),AdminDashboardController.ListDataRead);
router.route('/admin-user-count').get(UserController.loginRequired(null),AdminDashboardController.countRecord);
router.route('/admin-user-get-by-email/:id').get(UserController.loginRequired(null),AdminDashboardController.ListDataByEmailRead);
router.route('/enrollmen-update').put(UserController.loginRequired(null),AdminDashboardController.EnrollmentUpdate);



//patient router work
router.route('/patient-access-token').post(UserController.loginRequired(null),PatientController.getPatinetAccessToken);
router.route('/system-health-list/:token/:name').get(UserController.loginRequired(null),PatientController.getSystemHealthList);
router.route('/patient-upload-files').post(UserController.loginRequired(null),PatientController.patientFilesUpload);
router.route('/patient-provider').post(UserController.loginRequired(null),PatientController.addPatientProvider);
router.route('/patient-provider').get(UserController.loginRequired(null),PatientController.getproviderList);
router.route('/patient-provider/:user_id').get(UserController.loginRequired(null),PatientController.getproviderList);
router.route('/patient-provider/:id').delete(UserController.loginRequired(null),PatientController.deleteProviderData);
router.route('/patient-provider-document/:id').get(UserController.loginRequired(null),PatientController.getPatientProviderDocument);
router.route('/patient-provider-with-files').get(UserController.loginRequired(null),PatientController.getproviderWithFilesList);
router.route('/patient-provider-document/:id').delete(UserController.loginRequired(null),PatientController.deletePatientProviderDocument);
router.route('/patient-provider-document-archive/:id').delete(UserController.loginRequired(null),PatientController.archivePatientProviderDocument);
router.route('/singlehealthsystem/:npi').get(UserController.loginRequired(null),PatientController.getParticularHealthSystem);
router.route('/get-provider/:id').get(UserController.loginRequired(null),PatientController.getParticularProviderFromList);
router.route('/get-provider-npi/:npi').get(UserController.loginRequired(null),PatientController.getParticularProviderFromListByNpi);
router.route('/update-provider/:id').put(UserController.loginRequired(null),PatientController.updateParticularProvider);
router.route('/filter-provider/:id').get(UserController.loginRequired(null),PatientController.filterProviderList);
// by manish start
router.route('/notification').get(UserController.loginRequired(null),PatientController.getNotification);
router.route('/notification').post(UserController.loginRequired(null),PatientController.makeUnreadNotification)
 

router.route('/all-patient/:type').get(UserController.loginRequired(null),PatientController.getAllPatient);
// by manish end
//fitbit api call here
//router.route('/fitbit-config').post(UserController.loginRequired(null),FititControler.fitbitConfig);
router.route('/user-fitbit-code').post(UserController.loginRequired(null),FititControler.updateFitbitCode);
router.route('/check-fitbit-config').get(UserController.loginRequired(null),FititControler.checkUserFitbitConfig);
router.route('/check-fitbit-config').delete(UserController.loginRequired(null),FititControler.deleteUserFitbitConfig);
router.route('/check-fitbit-config-by-user/:user_id').get(UserController.loginRequired(null),FititControler.checkUserFitbitConfigByUser);
router.route('/today-activity/:token/:flag').get(UserController.loginRequired(null),FititControler.getTodayActivity);
router.route('/last-two-week-activity/:token/:flag').get(UserController.loginRequired(null),FititControler.getlastTwoWeekActivity);
router.route('/last-two-week-calories/:token/:flag').get(UserController.loginRequired(null),FititControler.getlastTwoWeekCalories);
router.route('/last-two-week-in-calories/:token/:flag').get(UserController.loginRequired(null),FititControler.getlastTwoWeekInCalories);
router.route('/last-two-week-weight/:token/:flag').get(UserController.loginRequired(null),FititControler.getlastTwoWeekWeight);
router.route('/last-two-week-sleep/:token/:flag').get(UserController.loginRequired(null),FititControler.getlastTwoWeekSleep);
router.route('/last-week-in-out-calories/:token/:flag').get(UserController.loginRequired(null),FititControler.getlastWeekInOutCalories);
router.route('/fitbit-demo').get(UserController.loginRequired(null),FititControler.chechFitbitDemoPermission);

// ==================================Small POC testing by MANISH=====================================================
router.route('/local-db-patient-check/:type').get(UserController.loginRequired(null),POCTestController.getPatientIfExist);

router.route('/createFhirResource/:resource_Type').post(UserController.loginRequired(null),POCTestController.createFhirResourcesTest);
router.route('/getFhirResources/:resource_Type/:resource_Id').get(UserController.loginRequired(null),POCTestController.getFhirResources);
router.route('/searchFhirResourcesByResource/:resource_Type').get(UserController.loginRequired(null),POCTestController.searchFhirResourcesByResource);
router.route('/getFhirResourceEverything/:resource_Type/:resource_Id').get(UserController.loginRequired(null),POCTestController.getFhirResourceEverything);
router.route('/deleteFhirResource/:resource_Type/:resource_Id').get(UserController.loginRequired(null),POCTestController.deleteFhirResource);
router.route('/executeFhirBundleTest').get(UserController.loginRequired(null),POCTestController.executeFhirBundleTest);
router.route('/importOneUpToGCPTest').get(UserController.loginRequired(null),POCTestController.importOneUpToGCPTest);
router.route('/getOneUpData').get(UserController.loginRequired(null),POCTestController.getOneUpData);
router.route('/poc-test').post(POCTestController.postDataTest);
//======================================================end testing====================================

 
 // ===============================OneUp & HCP Healthcare============================================

router.route('/gcp-health-data').get(UserController.loginRequired(null),POCTestController.postDataTest);
router.route('/all-oneup-user-id/:systemHealthId/:systemHealthName/:systemHealthAddress').get(UserController.loginRequired(null),POCTestController.getpatientOneupUserId);

router.route('/health-data/:type/:user/:resourceType').get(UserController.loginRequired(null), POCTestController.getHealthData);

router.route('/all-oneup-user-id').get(UserController.loginRequired(null),POCTestController.getpatientOneupUserId);
router.route('/oneUp-health-resource/:resourceType').get(UserController.loginRequired(actionEnum.MedicalRecords),POCTestController.getOneupResourceByResource);
router.route('/gcp-health-resource/:resourceType/:type?/:patient_id?').get(UserController.loginRequired(actionEnum.MedicalRecords),POCTestController.getHealthResourceByResourceType);
router.route('/gcp-health-resource/:resourceType').put(UserController.loginRequired(actionEnum.MedicalRecords),POCTestController.updateHealthResourceData);

router.route('/gcp-health-patient/:resourceType/:patient_id?').get(UserController.loginRequired(actionEnum.MedicalRecords),POCTestController.getFhirPatient);

// ===================================================================================================================================================================

//Patient resource sharing for provider
router.route('/new-resource').post(UserController.loginRequired(actionEnum.MedicalRecords),POCTestController.createNewResource);
router.route('/sharing-resource').get(UserController.loginRequired(null),SharingResouceController.getResourceList);
router.route('/patient-resource-sharing').get(UserController.loginRequired(null),SharingResouceController.getPatientResourceSharing);
router.route('/patient-resource-sharing').post(UserController.loginRequired(null),SharingResouceController.patientResourceSharingSave);
router.route('/patient-resource-sharing/:patProvId').delete(UserController.loginRequired(null),SharingResouceController.patientResourceSharingDelete);

//===============Roles  router here end ================
router.route('/role').get(UserController.loginRequired(null),SharingResouceController.getRole);
router.route('/appication-permission/:roleId').get(UserController.loginRequired(null),SharingResouceController.getAppicationPermission);
router.route('/appication-permission').post(UserController.loginRequired(null),SharingResouceController.AppicationPermissionSave);
router.route('/medical-record-aceess').get(UserController.loginRequired(null),SharingResouceController.chechMedicalRecordAccessPermission);


//User profile list
router.route('/user-profile-list/:userTypeId').get(UserController.getUserProfileList);
router.route('/update-user-profile').post(UserController.UserProfileUpdate);
router.route('/update-provider-profile').post(UserController.providerProfileUpdate);
router.route('/profile-image/:usr_id').get(UserController.UserPhotoRead);

//Patient for Provider document files
router.route('/document-download/:id/:userId').get(UserController.loginRequired(null),PatientController.downloadPatientDocument);
router.route('/document-filename-update').post(UserController.loginRequired(null),PatientController.DocumentFileNameUpdate);
router.route('/provider-manage-access/:typeId').get(UserController.loginRequired(null),ManagePatientAccessController.allUserListByType);
router.route('/provider-manage-access').post(UserController.loginRequired(null),ManagePatientAccessController.providerUserAccessControl);
router.route('/provider-manage-access/:searchFilter/:typeId/:searchDataValue').get(UserController.loginRequired(null),ManagePatientAccessController.allUserListByTypeWithSearch);
const uploadSingleFile = {
    storage: multer.diskStorage({
          //Setup where the user's file will go
          destination: function (req, file, next) {
                //Directory where file upload
                var dir = (path.join(__dirname, '../../uploaded/photos'));
                if (!fs.existsSync(dir)) {
                      fs.mkdirSync(dir, { recursive: true })
                }
                next(null, dir);
          },
          filename: function (req, file, next) {
                const newName = file.originalname;
                console.log(newName);
                next(null, newName);
          }

    }),
    fileFilter: function (req, file, next) {
          if (!file) {
                next();
          }
          const image = file.mimetype.startsWith('image/');
          if (image) {
                next(null, true);
          } else {
                console.log("file not supported");
                //TODO: A better message response to user on failure.
                return next();
          }
    }
};

router.route('/upload-profile-image')
      .post(multer(uploadSingleFile).single('upload'), UserController.uploadProfileImage);




     //Patinent dashboard  manage 
      router.route('/patient-medical-records').get(UserController.loginRequired(null),PatientDashboardController.getPatientMedicalRecords);
      router.route('/patient-manual-count-records').get(UserController.loginRequired(null),PatientDashboardController.getPatientManualCountRecords);
      router.route('/patient-provider-access-control').get(UserController.loginRequired(null),PatientDashboardController.getPatientProviderAccessControl);
      router.route('/patient-digital-health-configuration').get(UserController.loginRequired(null),PatientDashboardController.getDigitalHeathConfiguration);

       //Provider dashboard manage

       router.route('/patient-provider-permission').get(UserController.loginRequired(null), ProviderDahboardController.getPatientProviderPermission);
       router.route('/support-provider-permission').get(UserController.loginRequired(null), ProviderDahboardController.getSupportProviderPermission);

       router.route('/patient-portal-connection-list').get(UserController.loginRequired(null), PatientController.getPatientPortalConnection);
       
//Operation by Admin
router.route('/all-users').get(UserController.adminLoginRequired(null), UserController.getAllUsers);
router.route('/apply-mfa/:id').post(UserController.adminLoginRequired(null), UserController.applyMFA);
router.route('/show-idProof/:filename').get(UserController.getprovideridProof);

export default router;
