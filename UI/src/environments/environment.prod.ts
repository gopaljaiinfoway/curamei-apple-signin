export const environment = {
  //For server API 
 // apiBaseUrl : "/api/",
  // SOCKET_ENDPOINT: '/',
  production: true,

  //1Up qa
 // client_id:"a85f5840eeb54fc98bf06ec6d56dcd90",
  //client_secret:"pXkU4BiOJzjxYFBO4Sju0mSY3Wswor8C",
 
  //Firebase setup qa
  // firebase: {  
  //   apiKey: "AIzaSyCgPoFa-D4OPGLOBkokWhON_B_5naWhlOI",
  //   authDomain: "dummy-daa5c.firebaseapp.com",
  //   databaseURL: "https://dummy-daa5c.firebaseio.com",
  //   projectId: "dummy-daa5c",
  //   storageBucket: "dummy-daa5c.appspot.com",
  //   messagingSenderId: "405591563617",
  //   appId: "1:405591563617:web:972029181f21ae0678b920",
  //   measurementId: "G-XL6MWG1XFY"
  // },

  //For Android set up qa
 // apiBaseUrl : "https://test.curameitech.com/api/",
  //SOCKET_ENDPOINT: 'https://test.curameitech.com',

 //1Up production
 client_id:"3856e646e3ea3440a7b322e5b5e8da41",
 client_secret:"2e68696d709ed1bc414ae3aa78d1954c",

 //Firebase setup production web+android app
 firebase: {  
  apiKey: "AIzaSyDNF9s6jI_qOxEEedyntZq2ppvnKR880Hc",
  authDomain: "curamei-authentication-service.firebaseapp.com",
  projectId: "curamei-authentication-service",
  storageBucket: "curamei-authentication-service.appspot.com",
  messagingSenderId: "997928388629",
  appId: "1:997928388629:web:4c10cae3787d0ceb38261c"
},

//For Android set up Production
  apiBaseUrl : "https://app.curameitech.com/api/",
  SOCKET_ENDPOINT: 'https://app.curameitech.com',
};
