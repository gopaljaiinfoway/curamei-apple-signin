// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  //setup qa
    apiBaseUrl : 'http://localhost:2222/api/',
    SOCKET_ENDPOINT: 'http://localhost:2222',
    //setup production
     //apiBaseUrl : 'https://app.curameitech.com/api/',
    // SOCKET_ENDPOINT: 'https://app.curameitech.com',
   production: false,
   //Local oneup demo
   client_id:"2a15987bdfea40f5bc7305323932ccdb",
   client_secret:"7n33zGsxmdajTavYgC6PHi7wt2wulr1h",
//Firebase setup qa
   firebase: {  
      apiKey: "AIzaSyCgPoFa-D4OPGLOBkokWhON_B_5naWhlOI",
      authDomain: "dummy-daa5c.firebaseapp.com",
      databaseURL: "https://dummy-daa5c.firebaseio.com",
      projectId: "dummy-daa5c",
      storageBucket: "dummy-daa5c.appspot.com",
      messagingSenderId: "405591563617",
      appId: "1:405591563617:web:972029181f21ae0678b920",
      measurementId: "G-XL6MWG1XFY"
    }

     //Firebase setup production web+android app
//  firebase: {  
//   apiKey: "AIzaSyDNF9s6jI_qOxEEedyntZq2ppvnKR880Hc",
//   authDomain: "curamei-authentication-service.firebaseapp.com",
//   projectId: "curamei-authentication-service",
//   storageBucket: "curamei-authentication-service.appspot.com",
//   messagingSenderId: "997928388629",
//   appId: "1:997928388629:web:4c10cae3787d0ceb38261c"
// },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
