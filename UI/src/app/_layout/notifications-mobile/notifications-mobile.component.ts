import { Component, Inject, OnInit } from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
import { CommmanService } from '../../_services/commman.service';

@Component({
  selector: 'notifications-mobile',
  templateUrl: './notifications-mobile.component.html',
  styleUrls: ['./notifications-mobile.component.css']
})
export class NotificationsMobileComponent implements OnInit {
    notificationArr = [];
    isEmpty = false;

    constructor (public dialogRef: MatBottomSheetRef<NotificationsMobileComponent>,
        private commonService: CommmanService) 
    { }

    ngOnInit(): void {
        this.getNotification();
    }

    getNotification(){
        this.commonService.getNotification()
        .subscribe((result: any) => {
            if(result.success){
                this.notificationArr = result.notification;
                if (this.notificationArr.length == 0) this.isEmpty = true;
            }
        })
    }

    close() {
        this.dialogRef.dismiss();
    }
}
