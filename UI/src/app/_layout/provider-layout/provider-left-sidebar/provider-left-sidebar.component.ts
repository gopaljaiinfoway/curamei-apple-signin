import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-provider-left-sidebar',
  templateUrl: './provider-left-sidebar.component.html',
  styleUrls: ['./provider-left-sidebar.component.css']
})
export class ProviderLeftSidebarComponent implements OnInit {
  constructor( private router: Router) { }

  ngOnInit(): void {
  }
  goToDashboard(){
    this.router.navigateByUrl('/provider/dashboard');
  }
}
