import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { CookieService } from 'ngx-cookie-service'
declare var $: any
@Component({
  selector: 'app-provider-header',
  templateUrl: './provider-header.component.html',
  styleUrls: ['./provider-header.component.css'],
})
export class ProviderHeaderComponent implements OnInit {
  userobj: any
  userName: any
  constructor(private router: Router, private cookieService: CookieService) {}

  ngOnInit(): void {
    this.userobj = JSON.parse(localStorage.getItem('user'))
    this.userName = this.userobj.fullName
  }
  sidebarCollapse() {
    $('#sidebar').toggleClass('active')
    $('#body').toggleClass('active')
  }
  logout() {
    localStorage.removeItem('user')
    localStorage.clear()
    this.cookieService.delete('token')
    this.cookieService.deleteAll()
    this.router.navigateByUrl('')
  }
}
