import { MediaMatcher } from '@angular/cdk/layout'
import { ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core'
import { MatSidenav } from '@angular/material/sidenav'
import { NavigationEnd, Router } from '@angular/router'
import { CookieService } from 'ngx-cookie-service'
import { BehaviorSubject } from 'rxjs'
import jwt_decode from 'jwt-decode';
import { animateText, onMainContentChange, onSideNavChange } from '../animation';
import { CommmanService } from '../../_services/commman.service';
import { environment } from '../../../environments/environment';
import { globalProviderSelect } from '../../_services/globalProviderSelect.service'
import { NarrativeComponent } from '../../narrative/narrative.component'
import { ProviderListComponent } from '../../patient/provider-list/provider-list.component'
import { MedicalRecordsComponent } from '../../patient/medical-records/medical-records.component'
import { FitbitDashboardComponent } from '../../dashboard/fitbit-dashboard/fitbit-dashboard.component'
import { SocketioService } from 'src/app/socketIO.service'
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { PatientSelectMobileComponent } from '../patient-select-mobile/patient-select-mobile.component'
import { NotificationsMobileComponent } from '../notifications-mobile/notifications-mobile.component'
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any

interface Page {
  link: string
  name: string
  icon: string
}

@Component({
  selector: 'app-provider-layout',
  templateUrl: './provider-layout.component.html',
  styleUrls: ['./provider-layout.component.css'],
  animations: [onSideNavChange, animateText, onMainContentChange],
  providers: [globalProviderSelect]
})
export class ProviderLayoutComponent implements OnInit, OnDestroy {

  mobileScreenGroup = 1;
  email : string;
  first_name: string;
  last_name: string;
  npi: string;
  notificationArr: [];
  notificationLength: Number;

  mobileQuery: MediaQueryList
  events: string[] = []
  panelOpenState: boolean
  onSideNavChange: any
  private _mobileQueryListener: () => void
  public sideNavState: boolean = false
  public linkText: boolean = false
  flag = true
  visible = 'visible'
  hidden = 'hidden'
  userobj: any
  userName: any
  screenWidth: number;
  userTypeId:any;
  usr_id: any;
  basePath: any;
  medicalRecordsShow :boolean=false;

  selectablePatients = [];
  selectedPatientId: number;
  showPatientDropdown = false;
  profileImage:any;
  private screenWidth$ = new BehaviorSubject<number>(window.innerWidth);
  @ViewChild('sidenav') sidenav: MatSidenav;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth$.next(event.target.innerWidth);
  }
  constructor ( 
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private cookieService: CookieService,
    private commanService: CommmanService,
    private _globalProviderSelect: globalProviderSelect,
    private socketService: SocketioService,
    private dialog: MatBottomSheet,
    private sanitizer : DomSanitizer
  ) {
    let tokenGet = this.cookieService.get('token');
    if (tokenGet && tokenGet != null) {
      let decoded = jwt_decode(tokenGet, 'llp');
      this.basePath = environment.apiBaseUrl;
      this.userTypeId = decoded.userTypeId;
      this.usr_id = decoded.id;
    }

    this.mobileQuery = media.matchMedia('(max-width: 600px)')
    this._mobileQueryListener = () => changeDetectorRef.detectChanges()
    this.mobileQuery.addListener(this._mobileQueryListener);
    
    // this._globalProviderSelect.getRefresh().subscribe(n => {
    //   this.getSelectablePatients();
    //   this._globalProviderSelect.resetRefresh();
    // });
  } 

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener)
  }
  ngOnInit(): void {
    // this.sidebarCollapse();
    this.getProfileImageLink(this.usr_id);
    this.medicalRecordsEnable();
    this.flag = true
    this.userobj = JSON.parse(localStorage.getItem('user'))
    this.userName = this.userobj.fullName;
    this.getSelectablePatients();
    this.screenWidth$.subscribe(width => {
      this.screenWidth = width;
    });

    if (this.screenWidth <= 640) {
      this.initMobileScreenGroup();
      this.router.events.subscribe((val) => {
        this.initMobileScreenGroup();
      });
      this.getUserData();
    }

    this.getNotification();
    this.socketService.getData()
      .subscribe((result: any) =>{
        if(result.flag){
          this.getNotification();
        }
      });

    this.socketService.getDropdownMenuUserList()
    .subscribe((result: any) =>{
      if(result.flag){
        this.getSelectablePatients()
      }
    } );
    this.socketService.getlatestProfilePic()
    .subscribe((result: any) =>{
      if(result.flag){
        this.getProfileImageLink(this.usr_id);
      }
    } );

  }

  getSelectablePatients() {
    this.commanService.getPatientListByType(2)
      .subscribe((result: any) => {
        if(result.success){
          this.selectablePatients = [];
          if(result.data.length > 0){
            result.data.forEach(element => {
              if(element.active){
               this.selectablePatients.push(element)
              }
            });
          }
        }
      })
  }

  selectedUser(user_id) {
    this.selectedPatientId = user_id;
    this._globalProviderSelect.setId(this.selectedPatientId);
  }

  getUserData() {
    this.commanService.getUserProfileList(1).subscribe(
      (res: any) => {
        console.log(res.data);
        if (res.success) {
          this.email = res.data.email;
          this.first_name = res.data.first_name;
          this.last_name = res.data.last_name;
          this.npi = res.data.npi;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  onOutletLoad(component) {
    if (component instanceof NarrativeComponent || component instanceof FitbitDashboardComponent || 
      component instanceof MedicalRecordsComponent || component instanceof ProviderListComponent) {
        this.showPatientDropdown = true;
    } else {
      this.showPatientDropdown = false;
    }
  }

  onSinenavToggle() {
    this.sideNavState = !this.sideNavState

    setTimeout(() => {
      this.linkText = this.sideNavState
    }, 200)
    // this._sidenavService.sideNavState$.next(this.sideNavState)
  }

  close() {
    this.sideNavState = false;

  }

  toggleSide() {
    this.sideNavState = !this.sideNavState;
  }

  logout() {
    localStorage.removeItem('user')
    localStorage.clear()
    this.cookieService.delete('token')
    this.cookieService.deleteAll()
    this.router.navigateByUrl('')
  }

  //   sidebarCollapse() {
  //     $("#sidebar").toggleClass('active');
  //     $('#body').toggleClass('active');
  // }


  medicalRecordsEnable() {
    this.commanService.checkmedicalRecordsAccess().subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.medicalRecordsShow =true;
        }else{
          this.medicalRecordsShow =false;
        } 
      },
      (err) => {
        console.log(err);
      }
    );
  }

  mobLogout() {
    this.mobileScreenGroup = 1;
    this.toggleSide();
    this.logout();
  }
  
  // get initial value for the mobileScreenGroup
  initMobileScreenGroup() {
    if (this.router.url === '/provider/narrative' || this.router.url === '/provider/medical-records'
      || this.router.url === '/provider/provider-list' || this.router.url === '/provider/fitbit-dashboard')
    {
      this.mobileScreenGroup = 2;
    }
    else if (this.router.url === '/provider/manage-patient-access') this.mobileScreenGroup = 3;
  }

  // show home group of screens on mobile
  showHome() {
    this.mobileScreenGroup = 1;
    this.router.navigateByUrl('/provider/dashboard');
  }

  // show records group of screens on mobile
  showRecords() {
    this.mobileScreenGroup = 2;
    this.router.navigateByUrl('/provider/medical-records');
  }

  // show providers group of screens on mobile
  showPatients() {
    this.mobileScreenGroup = 3;
    this.router.navigateByUrl('/provider/manage-patient-access');
  }

  showProfile() {
    this.router.navigateByUrl('/provider/profile');
  }

  mobShowProfile() {
    this.mobileScreenGroup = 1;
    this.toggleSide();
    this.showProfile();
  }

  mobileOpenPatientSelect(): void {
    const dialogRef =this.dialog.open(PatientSelectMobileComponent,{ data: [this.selectablePatients, this.selectedPatientId] });

    dialogRef.afterDismissed().subscribe((result:any) => {
      this.selectedUser(result);
    });
  }

  getNotification(){
    this.commanService.getNotification()
    .subscribe((result: any) => {
      if(result.success){
        this.notificationArr = result.notification;
        this.notificationLength = result.notification.length;
      }
    })
  }
  makeUnreadNotification(){
    this.commanService.unreadNotification(this.notificationArr)
    .subscribe((result: any) => {
      this.notificationLength = 0;
    })
  }

  mobileOpenNotifications(): void {
    this.makeUnreadNotification();
    const dialogRef =this.dialog.open(NotificationsMobileComponent,{});

    dialogRef.afterDismissed().subscribe((result:any) => {
      this.getNotification();
    });
  }

  goToDashboard() {
    this.router.navigateByUrl('/provider/dashboard')
  }
  profilePage(){
    this.router.navigateByUrl('/provider/profile') 
  }
  getProfileImageLink(userId){ 
    this.commanService.getProfileImageLink(userId).subscribe(
      (blob: any) => {
        let objectURL = URL.createObjectURL(blob);
        this.profileImage = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      (err) => {
      }
    );
  }
}
