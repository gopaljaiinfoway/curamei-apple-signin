import { animate, state, style, transition, trigger } from '@angular/animations';


export const onSideNavChange = trigger('onSideNavChange', [
  state('close',
    style({
      'min-width': '250px'
    })
  ),
  state('open',
    style({
      'min-width': '50px'
    })
  ),
  transition('close => open', animate('250ms ease-in')),
  transition('open => close', animate('250ms ease-in')),
]);


export const onMainContentChange = trigger('onMainContentChange', [
  state('close',
    style({
      'margin-left': '0'
    })
  ),
  state('open',
    style({
      'margin-left': '50px'
    })
  ),
  transition('close => open', animate('250ms ease-in')),
  transition('open => close', animate('250ms ease-in')),
]);

//  added sidenav state change to open close animateText  
export const animateText = trigger('animateText', [
  state('close',
    style({
      'display': 'none',
      opacity: 0,
    })
  ),
  state('open',
    style({
      'display': 'block',
      opacity: 1,
    })
  ),
  transition('close => open', animate('350ms ease-in')),
  transition('open => close', animate('200ms ease-out')),
]);

