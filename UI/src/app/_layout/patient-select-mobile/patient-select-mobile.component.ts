import { Component, Inject, OnInit } from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
import { CommmanService } from '../../_services/commman.service';
import { globalProviderSelect } from '../../_services/globalProviderSelect.service'

@Component({
  selector: 'patient-select-mobile',
  templateUrl: './patient-select-mobile.component.html',
  styleUrls: ['./patient-select-mobile.component.css']
})
export class PatientSelectMobileComponent implements OnInit {
    selectablePatients = [];
    selectedPatientId: number;
    loadingNames = true;
    isEmpty = false;

    constructor (public dialogRef: MatBottomSheetRef<PatientSelectMobileComponent>,
        private commonService: CommmanService, private _globalProviderSelect: globalProviderSelect,
        @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) 
    { }

    ngOnInit(): void {
        this.selectablePatients = this.data[0];
        this.selectedPatientId = this.data[1];
        if (this.selectablePatients.length == 0) this.isEmpty = true;
        this.loadingNames = false;
    }

    selectedUser(uid) {
        this.selectedPatientId = uid;
        this.close();
    }

    close() {
        this.dialogRef.dismiss(this.selectedPatientId);
    }
}
