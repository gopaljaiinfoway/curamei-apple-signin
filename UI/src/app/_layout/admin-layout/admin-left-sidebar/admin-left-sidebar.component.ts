import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-admin-left-sidebar',
  templateUrl: './admin-left-sidebar.component.html',
  styleUrls: ['./admin-left-sidebar.component.css']
})
export class AdminLeftSidebarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  goToDashboard(){
    this.router.navigateByUrl('/admin/dashboard');
  }
}
