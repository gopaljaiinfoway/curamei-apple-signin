import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-patient-left-sidebar',
  templateUrl: './patient-left-sidebar.component.html',
  styleUrls: ['./patient-left-sidebar.component.css']
})
export class PatientLeftSidebarComponent implements OnInit {
  constructor( private router: Router) { }

  ngOnInit(): void {
  }


  goToDashboard(){
    this.router.navigateByUrl('/patient/dashboard');
  }
 
}
