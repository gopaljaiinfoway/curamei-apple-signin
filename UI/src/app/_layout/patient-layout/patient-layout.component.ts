import { MediaMatcher } from '@angular/cdk/layout'
import { ChangeDetectorRef, Component, HostListener, OnInit, ViewChild } from '@angular/core'
import { MatSidenav } from '@angular/material/sidenav'
import { Router } from '@angular/router'
import { CookieService } from 'ngx-cookie-service'
import { BehaviorSubject } from 'rxjs';
import * as jwtDecode from 'jwt-decode';
import { environment } from '../../../environments/environment';
import { animateText, onMainContentChange, onSideNavChange } from '../animation'
import { CommmanService } from '../../_services/commman.service';
import { SocketioService } from 'src/app/socketIO.service';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { NotificationsMobileComponent } from '../notifications-mobile/notifications-mobile.component';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any
@Component({
  selector: 'app-patient-layout',
  templateUrl: './patient-layout.component.html',
  styleUrls: ['./patient-layout.component.css'],
  animations: [onSideNavChange, animateText, onMainContentChange],
})
export class PatientLayoutComponent implements OnInit {

  socket: SocketIOClient.Socket;

  mobileScreenGroup = 1;
  email : string;
  first_name: string;
  last_name: string;

  notificationArr = [];
  notificationLength = 0;
  mobileQuery: MediaQueryList
  events: string[] = []
  panelOpenState: boolean
  onSideNavChange: any
  private _mobileQueryListener: () => void
  public sideNavState: boolean = false
  public linkText: boolean = false
  flag = true
  visible = 'visible'
  hidden = 'hidden'
  userobj: any
  userName: any
  screenWidth: number;
  usr_id: any;
  basePath: any;
  fitbitConfigurationFlag: boolean=false;
  profileImage:any;
  private screenWidth$ = new BehaviorSubject<number>(window.innerWidth);
  @ViewChild('sidenav') sidenav: MatSidenav;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth$.next(event.target.innerWidth);
  }

  constructor (
    private socketService: SocketioService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private cookieService: CookieService,
    private commonService: CommmanService,
    public dialog: MatBottomSheet,
    private sanitizer : DomSanitizer
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)')
    this._mobileQueryListener = () => changeDetectorRef.detectChanges()
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.basePath = environment.apiBaseUrl;
    var decoded = jwtDecode(this.cookieService.get('token'));
    this.usr_id = decoded.id;
  }

  ngOnDestroy(): void {
    this.getNotification()
    this.mobileQuery.removeListener(this._mobileQueryListener)
  }
  ngOnInit(): void {
    this.getProfileImageLink(this.usr_id);
    // this.backdropValue = false;
     this.checkUserConfigured();
    this.flag = true
    this.userobj = JSON.parse(localStorage.getItem('user'))
    this.userName = this.userobj.fullName
    this.screenWidth$.subscribe(width => {
      this.screenWidth = width;
      this.getUserData();
    });

    // if mobile, initialize the screen group variable
    if (this.screenWidth <= 640) {
      this.initMobileScreenGroup();
      this.router.events.subscribe((val) => {
        this.initMobileScreenGroup();
      })
    }
    
    this.getNotification()
    this.socketService.getData()
    .subscribe((result: any) =>{
      if(result.flag){
        this.getNotification()
      }
    } );


    this.socketService.getlatestProfilePic()
    .subscribe((result: any) =>{
      if(result.flag){
        this.getProfileImageLink(this.usr_id);
      }
    } );

  }
  getNotification(){
    this.commonService.getNotification()
    .subscribe((result: any) => {
      if(result.success){
        this.notificationArr = result.notification;
        this.notificationLength = result.notification.length;
      }
    })
  }
  makeUnreadNotification(){
    this.commonService.unreadNotification(this.notificationArr)
    .subscribe((result: any) => {
      this.notificationLength = 0;
      
    })


  }

  toggleSide() {
    this.sideNavState = !this.sideNavState;
  }

  onSinenavToggle() {
    this.sideNavState = !this.sideNavState
    // this.backdropValue = true;
    setTimeout(() => {

      this.linkText = this.sideNavState
    }, 200)
    // this._sidenavService.sideNavState$.next(this.sideNavState)
  }

  // get email and name
  getUserData() {
    this.commonService.getUserProfileList(2).subscribe(
      (res: any) => {
        console.log(res.data);
        if (res.success) {
          this.email = res.data.email;
          this.first_name = res.data.first_name;
          this.last_name = res.data.last_name;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  logout() {
    localStorage.removeItem('user')
    localStorage.clear()
    this.cookieService.delete('token')
    this.cookieService.deleteAll()
    this.router.navigateByUrl('')
  }

  mobLogout() {
    this.mobileScreenGroup = 1;
    this.toggleSide();
    this.logout();
  }
  
  // get initial value for the mobileScreenGroup
  initMobileScreenGroup() {
    if (this.router.url === '/patient/datasource' || this.router.url === '/patient/medical-records'
      || this.router.url === '/patient/provider-list' || this.router.url === '/patient/digital-health'
      || this.router.url === '/patient/narrative' || this.router.url === '/patient/fitbit-dashboard') {
        this.mobileScreenGroup = 2;
      }
    else if (this.router.url === '/patient/access-controls') this.mobileScreenGroup = 3;
  }

  // show home group of screens on mobile
  showHome() {
    this.mobileScreenGroup = 1;
    this.router.navigateByUrl('/patient/dashboard');
  }

  // show records group of screens on mobile
  showRecords() {
    this.mobileScreenGroup = 2;
    this.router.navigateByUrl('/patient/datasource');
  }

  // show providers group of screens on mobile
  showProviders() {
    this.mobileScreenGroup = 3;
    this.router.navigateByUrl('/patient/access-controls');
  }

  showProfile() {
    this.router.navigateByUrl('/patient/patient-profile');
  }

  mobShowProfile() {
    this.mobileScreenGroup = 1;
    this.toggleSide();
    this.showProfile();
  }

  goToDashboard() {
    this.router.navigateByUrl('/patient/dashboard')
  }  
  profilePage(){
    this.router.navigateByUrl('/patient/patient-profile') 
  }

  mobileOpenNotifications(): void {
    this.makeUnreadNotification();
    const dialogRef =this.dialog.open(NotificationsMobileComponent,{});

    dialogRef.afterDismissed().subscribe((result:any) => {
      this.getNotification();
    });
  }

  checkUserConfigured() {
    this.commonService.checkFitbitConfig().subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.fitbitConfigurationFlag=true;
          localStorage.setItem('fitbit_refresh_token', resmsg.data);
        }else{
          this.fitbitConfigurationFlag=false;
        }
        
      },
      (err) => {
        this.fitbitConfigurationFlag=false;
      }
    );
  }


  getProfileImageLink(userId){ 
    this.commonService.getProfileImageLink(userId).subscribe(
      (blob: any) => {
        let objectURL = URL.createObjectURL(blob);
        this.profileImage = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      (err) => {
      }
    );
  }

}
