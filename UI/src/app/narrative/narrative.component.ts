import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { Router } from '@angular/router';
import html2canvas from 'html2canvas';
import { CommmanService } from 'src/app/_services/commman.service';
import { globalProviderSelect } from 'src/app/_services/globalProviderSelect.service'
import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
import { MatSnackBar } from '@angular/material/snack-bar'
const { Filesystem } = Plugins;

export interface PatientPeriodicElement {
  name: string;
  gender: string;
  m_status: string;
  dob: string;
  contact: string;
  address: string;
}
export interface familyPeriodicElement{
  condition: string;
  relationship: string;
}
export interface observationPeriodicElement {
  observation: string;
  value: string;
  r_range: string;
  date: string;
}
const ELEMENT_DATA: PatientPeriodicElement[] = [{name:'ABC', gender:'male', m_status:'married', dob:'04/03/1996', contact:'78547854', address: 'Ranchi'}];
const FAMILY_ELEMENT_DATA: familyPeriodicElement[] = [{condition:'abc', relationship: 'mother' }, {condition:'abc', relationship: 'grandmother' }];
const OBSERVATION_ELEMENT_DATA: observationPeriodicElement[] = [
  {observation: 'Glucose [Moles/volume] in Blood', value: '6.3 mmol/L',r_range: '3.1 mmol/L - 6.2 mmol/L', date: '04/02/2013 - 04/05/2013'}
]
@Component({
  selector: 'app-narrative',
  templateUrl: './narrative.component.html',
  styleUrls: ['./narrative.component.css']
})
export class NarrativeComponent implements OnInit {
  action = null;
  throughProviderFlag: boolean = false;
  view: boolean = true;

  users: any = [];
  selectedPatientId: any;

  showFitbit = false;
  tabFlag = true;
  fitbitData = [];

  displayedColumnsPatient: string[] = ['name', 'gender', 'm_status', 'dob', 'contact', 'address'];
  displayedColumnsFamily: string[] = ['condition','relationship'];
  displayedColumnsObservation: string[] = ['observation','value','r_range','date'];
  dataSourcePatient = [];
  dataSourceFamily = [];
  dataSourceObservation = [];

  patient = null;
  conditions = [];
  allergies = [];
  immunizations = [];
  carePlans = [];
  procedures = [];
  medicationOrders = [];
  familyMemberHistory = [];
  observations = [];
  diagnosticReports = [];
  prescriptions = [];
  goals = [];

  patientText = 'Loading...';
  conditionText = 'Loading...';
  allergyText = 'Loading...';
  immunizationText = 'Loading...';
  carePlanText = 'Loading...';
  procedureText = 'Loading...';
  medicationOrderText = 'Loading...';
  familyMemberHistoryText = 'Loading...';
  observationText = 'Loading...';
  diagnosticReportText = 'Loading...';
  prescriptionText = 'Loading...';
  goalText = 'Loading...';
  noRecordFond = 'Loading...';

  constructor(
    private commanService: CommmanService,
    private router: Router,
    private elementRef: ElementRef,
    private _snackBar: MatSnackBar,
    private _globalProviderSelect: globalProviderSelect
  ) {  }

  ngOnInit(): void {
    if(this.router.url == '/provider/narrative'){
      this.throughProviderFlag = true;
      this.view = false;

      this._globalProviderSelect.getId().subscribe(n => {
        this.selectedPatientId = n;
        this.selectedUser(this.selectedPatientId);
      
      });

      this.selectedUser(this._globalProviderSelect.getId());

      // this.getAllPatient('Narrative');
    }else if(this.router.url == '/patient/narrative'){
      this.throughProviderFlag = false;
      this.view = true;
      this.checkUserConfigured();
      this.getFHIRPatient('Patient', null);
    }
  }

  copyToImage(fileName){
    // First we get our section to save from dom
    let docElem = document.getElementById('mainContainer');
    html2canvas(docElem).then((canvas) =>  {
      var link = document.createElement('a');
      link.href = canvas.toDataURL();
      link.download = fileName;
      document.body.appendChild(link);
      link.click();
    })
  }




  copyToImageInAndroid(fileName){
    // First we get our section to save from dom
    let docElem = document.getElementById('mainContainer1');

     document.getElementById("mainContainer1").setAttribute("content", "width=1200px");
  

     html2canvas(docElem).then((canvas) =>  {
       var getCap =canvas.toDataURL();
       document.body.appendChild(canvas);
      Filesystem.writeFile({
        path: fileName,
        data: getCap,
        directory: FilesystemDirectory.Documents,
       // encoding: FilesystemEncoding.UTF8
      });

      this._snackBar.open('capture has downloaded', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['green-snackbar'],
      });
    })
     




  }






 
  checkUserConfigured(){
    this.fitbitData=[];
    this.commanService.checkFitbitConfig().subscribe(
      (resmsg: any) => {
        console.log(resmsg)
        if(resmsg.success){
          localStorage.setItem('fitbit_refresh_token', resmsg.data);
          this.getSteps(resmsg.data, this.tabFlag);
          this.getSleep(resmsg.data, this.tabFlag);
          this.showFitbit = true;
        }else{
          this.fitbitData.push('No connected digital health data to show.');
          this.showFitbit = true;
        }
      }, (err) => {
        console.log(err)
      })
  }

  providerCheckUserConfigured(patient_id) {
    this.fitbitData=[];
    this.commanService.checkFitbitConfigurationThroughProvider(patient_id).subscribe(
      (resmsg: any) => {
        console.log(resmsg)
        if(resmsg.success){
          if (this.fitbitData[0] == "No connected digital health data to show.") {
            this.fitbitData = [];
          }
          localStorage.setItem('fitbit_refresh_token', resmsg.data);
          this.getSteps(resmsg.data, this.tabFlag);
          this.getSleep(resmsg.data, this.tabFlag);
          this.showFitbit = true;
        }else{
          this.fitbitData.push('No connected digital health data to show.');
          this.showFitbit = true;
        }
      }, (err) => {
        console.log(err)
      })
  }

  getSteps(access_token, tabFlag) {
    this.commanService.getlastTwoWeekActivity(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
         
          if (resmsg.activityObj.weeksGraph.length != 0) {
            if (this.fitbitData[0] == "No connected digital health data to show.") {
              this.fitbitData = [];
            }
            var min = Number.MAX_SAFE_INTEGER;
            var max = Number.MIN_SAFE_INTEGER;
            resmsg.activityObj.weeksGraph.forEach((element) => {
              if (element.value > max) {
                max = parseInt(element.value);
              }
              if (element.value < min ) {
                min = parseInt(element.value);
              }
            })
            this.fitbitData.push("Daily Step Count:")
            this.fitbitData.push("Range (2 weeks): " + min + " - " + max + " steps");
            this.fitbitData.push("Mean (2 weeks): " + resmsg.activityObj.lastTwoWeekAVgSteps);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getSleep(access_token, tabFlag) {
    this.commanService.getlastTwoWeekSleep(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          if (resmsg.sleepObj.sleepWeeksGraph.length != 0) {
            var min = Number.MAX_SAFE_INTEGER;
            var max = Number.MIN_SAFE_INTEGER;
            resmsg.sleepObj.sleepWeeksGraph.forEach((element) => {
              if (element.value > max) {
                max = element.value/60.0;
              }
              if (element.value < min ) {
                min = element.value/60.0;
              }
            })
            this.fitbitData.push("Nightly Sleep Length:")
            this.fitbitData.push("Range (2 weeks): " + min.toFixed(1) + " - " + max.toFixed(1) + " hrs");
            this.fitbitData.push("Mean (2 weeks): " + resmsg.sleepObj.weeksAvgSleepHours);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }
 
  getFHIRPatient(type, patient_id){
    this.dataSourcePatient=[];
    this.clearDataSourceObject();
    this.patient=null;
    this.commanService.fhirPatient(type, patient_id)
    .subscribe((result: any) => {
      console.log(result)
      if(result.success){
        this.patient = result.data;
        var contact = 'NA';
        if(result.data.contact){
          contact = result.data.contact[0].telecom[0].value;
        }
        var obj = {
          name: result.data.name[0].family[0], 
          gender: result.data.gender, 
          m_status: result.data.maritalStatus.coding[0].code, 
          dob: result.data.birthDate, 
          contact: contact,
          address: result.data.address[0].text
        }


        this.dataSourcePatient.push(obj)
        this.getAllResources(patient_id);
      }else{
        this.patientText = result.message;
        var errorText = 'This data is not available';
        this.conditionText = errorText;
        this.allergyText = errorText;
        this.immunizationText = errorText;
        this.carePlanText = errorText;
        this.procedureText = errorText;
        this.medicationOrderText = errorText;
        this.familyMemberHistoryText = errorText;
        this.observationText = errorText;
        this.diagnosticReportText = errorText;
        this.prescriptionText = errorText;
        this.goalText = errorText;
      }
    },(res: any) => {
      this.patientText = 'Patient data is unavailable';
    })
  }

   getAllResources(patient_id){
    this.clearDataSourceObject();
    console.log("==========patient_id=============",patient_id);
    this.fetchGCPHealthcareResource('Condition', patient_id);
    this.fetchGCPHealthcareResource('AllergyIntolerance', patient_id);
    this.fetchGCPHealthcareResource('Immunization', patient_id);
    this.fetchGCPHealthcareResource('Observation', patient_id);
    // this.fetchGCPHealthcareResource('MedicationStatement', patient_id);
    this.fetchGCPHealthcareResource('FamilyMemberHistory', patient_id);
    this.fetchGCPHealthcareResource('Procedure', patient_id);
    this.fetchGCPHealthcareResource('MedicationOrder', patient_id);
    this.fetchGCPHealthcareResource('Goal', patient_id);
    this.fetchGCPHealthcareResource('CarePlan', patient_id);
    this.fetchGCPHealthcareResource('DiagnosticReport', patient_id);
    // this.fetchGCPHealthcareResource('Encounter', patient_id);
    
  }


clearDataSourceObject(){
 // this.patient = null;
 //this.dataSourcePatient = [];
 this.dataSourceFamily = [];
 this.dataSourceObservation = [];
  this.conditions = [];
  this.allergies = [];
  this.immunizations = [];
  this.carePlans = [];
  this.procedures = [];
  this.medicationOrders = [];
  this.familyMemberHistory = [];
  this.observations = [];
  this.diagnosticReports = [];
  this.prescriptions = [];
  this.goals = [];
}


  fetchGCPHealthcareResource(resourType, patient_id) {

    this.commanService.fetchGCPHealthcareResource(resourType, 'narrative_type', patient_id)
      .subscribe((result: any) => {
        // console.log('============================================')
        // console.log(result)
        if(result.success){
          for(var i=0; i<3; i++){
            if(result.data[i].resourceType == 'Condition'){
              this.conditions.push(result.data[i])
            }
            if(result.data[i].resourceType == 'AllergyIntolerance'){
              this.allergies.push(result.data[i])
            }
            if(result.data[i].resourceType == 'FamilyMemberHistory'){
              this.familyMemberHistory.push(result.data[i])
              var relationship = 'NA';
              var condition ='NA';

              if(result.data[i].relationship){
                if(result.data[i].relationship.coding){
                  if(result.data[i].relationship.coding[0].display){
                    relationship = result.data[i].relationship.coding[0].display;
                  }else{
                    relationship = 'NA'
                  }
                }else{
                  relationship = 'NA'
                }
              }else{
                relationship = 'NA'
              }
              if(result.data[i].condition){
                if(result.data[i].condition.coding){
                  if(result.data[i].condition.coding[0].display){
                    condition = result.data[i].condition.coding[0].display;
                  }else{
                    condition = 'NA'
                  }
                }else{
                  condition = 'NA'
                }
              }else{
                condition = 'NA'
              }
              var obj = {
                condition: condition,
                relationship: relationship
              }
              this.dataSourceFamily.push(obj)
            }
            if(result.data[i].resourceType == 'Immunization'){
              this.immunizations.push(result.data[i])
            }
            if(result.data[i].resourceType == 'CarePlan'){
              this.carePlans.push(result.data[i])
            }
            if(result.data[i].resourceType == 'Procedure'){
              this.procedures.push(result.data[i])
            }
            if(result.data[i].resourceType == 'MedicationOrder'){
              this.medicationOrders.push(result.data[i])
            }
            if(result.data[i].resourceType == 'Observation'){
              this.observations.push(result.data[i])
              var observation = 'NA';
              var value = 'NA';
              var r_range_low = 'NA';
              var r_range_high = 'NA';
              var date = 'NA';
              // observation display
              if(result.data[i].code){
                if(result.data[i].code.coding){
                  if(result.data[i].code.coding[0].display){
                    observation = result.data[i].code.coding[0].display;
                  }
                }
              }
              // Value
              if(result.data[i].valueQuantity){
                if(result.data[i].valueQuantity.value){
                  value = result.data[i].valueQuantity.value;
                }
              }
              // r_range low
              if(result.data[i].referenceRange){
                if(result.data[i].referenceRange[0].low){
                  if(result.data[i].referenceRange[0].low.value){
                    if(result.data[i].referenceRange[0].low.unit){
                      r_range_low = result.data[i].referenceRange[0].low.value + ' ' + result.data[i].referenceRange[0].low.unit;
                    }else{
                      r_range_low = result.data[i].referenceRange[0].low.value;
                    }
                  }
                }
              }
              // r_range high
              if(result.data[i].referenceRange){
                if(result.data[i].referenceRange[0].high){
                  if(result.data[i].referenceRange[0].high.value){
                    if(result.data[i].referenceRange[0].high.unit){
                      r_range_high = result.data[i].referenceRange[0].high.value + ' ' + result.data[i].referenceRange[0].high.unit;

                    }else{
                      r_range_high = result.data[i].referenceRange[0].high.value;
                    }
                  }
                }
              }
              // issued last updated
              if(result.data[i].issued){
                date = result.data[i].issued;
              }
              var obobj = {
                observation: observation,
                value: value,
                r_range: r_range_low +' - '+r_range_high,
                date: date,
              }
              this.dataSourceObservation.push(obobj)
            }
            if(result.data[i].resourceType == 'DiagnosticReport'){
              this.diagnosticReports.push(result.data[i])
            }
            if(result.data[i].resourceType == 'Goal'){
              this.goals.push(result.data[i])
            }
          }
          console.log('++++++++++++++++++++++++++++++++++++++++++');
          console.log(this.observations)
          console.log(this.dataSourceObservation)
        }else{
          this.setErrorText(resourType, result.message);
        }
      }, (error: any) => {
        var errorText = 'Getting error, result not found';
        this.setErrorText(resourType, errorText);
      })
  }
  setErrorText(resourType, msgText){
    if(resourType == 'Condition'){
      this.conditionText = msgText;
    }
    if(resourType == 'AllergyIntolerance'){
      this.allergyText = msgText;
    }
    if(resourType == 'FamilyMemberHistory'){
      this.familyMemberHistoryText = msgText;
    }
    if(resourType == 'Immunization'){
      this.immunizationText = msgText;
    }
    if(resourType == 'CarePlan'){
      this.carePlanText = msgText;
    }
    if(resourType == 'Procedure'){
      this.procedureText = msgText;
    }
    if(resourType == 'MedicationOrder'){
      this.medicationOrderText = msgText;
    }
    if(resourType == 'Observation'){
      this.observationText = msgText;
    }
    if(resourType == 'DiagnosticReport'){
      this.diagnosticReportText = msgText;
    }
    if(resourType == 'Goal'){
      this.goalText = msgText;
    }
  }
  getAllPatient(type){
    this.commanService.getAllPatient(type)
    .subscribe((result: any) => {
      if(result.success){
        this.noRecordFond = 'No patient user selected.';
        this.users = result.users;
      }
    })
  }
  selectedUser(user_id){
    if(user_id == undefined){
      this.view = false;
      this.reset();
    }else{
      this.view = true;
      console.log(user_id);
      this.getFHIRPatient('Patient', user_id);
      this.providerCheckUserConfigured(user_id);
    }
  }
  reset(){
    this.noRecordFond = 'No patient user selected.';
    
    this.patientText = 'Loading...';
    this.conditionText = 'Loading...';
    this.allergyText = 'Loading...';
    this.immunizationText = 'Loading...';
    this.carePlanText = 'Loading...';
    this.procedureText = 'Loading...';
    this.medicationOrderText = 'Loading...';
    this.familyMemberHistoryText = 'Loading...';
    this.observationText = 'Loading...';
    this.diagnosticReportText = 'Loading...';
    this.prescriptionText = 'Loading...';
    this.goalText = 'Loading...';
  }

}
