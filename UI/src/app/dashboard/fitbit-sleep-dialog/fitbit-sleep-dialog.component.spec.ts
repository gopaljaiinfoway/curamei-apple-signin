import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitbitSleepDialogComponent } from './fitbit-sleep-dialog.component';

describe('FitbitSleepDialogComponent', () => {
  let component: FitbitSleepDialogComponent;
  let fixture: ComponentFixture<FitbitSleepDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitbitSleepDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitbitSleepDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
