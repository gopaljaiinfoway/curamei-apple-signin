export var weight = [
  {
    "name": "01",
    "value": 80
  },
  {
    "name": "02",
    "value": 79.2
  },
  {
    "name": "03",
    "value": 79.3
  },
  {
    "name": "04",
    "value": 79.4
  },
  {
    "name": "05",
    "value": 79.5
  },
  {
    "name": "06",
    "value": 79.6
  },
  {
    "name": "07",
    "value": 78.7
  },
  {
    "name": "08",
    "value": 78.8
  },
  {
    "name": "09",
    "value": 78.9
  },
  {
    "name": "10",
    "value": 77
  },
  {
    "name": "11",
    "value": 77.8
  },
  {
    "name": "12",
    "value": 77.7
  },
  {
    "name": "13",
    "value": 76.5
  },
  {
    "name": "14",
    "value": 76
  },

];