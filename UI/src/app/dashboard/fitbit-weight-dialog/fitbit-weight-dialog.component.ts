import { Component, OnInit, Inject,Output, EventEmitter  } from '@angular/core';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { weight } from './weight';
@Component({
  selector: 'app-fitbit-weight-dialog',
  templateUrl: './fitbit-weight-dialog.component.html',
  styleUrls: ['./fitbit-weight-dialog.component.css']
})
export class FitbitWeightDialogComponent implements OnInit {
  weight: any[]
  view: any[]
 
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  todayBmi = 0;
  todayWeight = 0;
  todayFat = 0;


  colorSchemeWeight = {
    domain: ['#184d47', '#96bb7c']
  };

  constructor (public dialogRef: MatDialogRef<FitbitWeightDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    Object.assign(this, { weight })
  }

  ngOnInit(): void {
    Object.assign(this, { weight : this.data.lastTwoweeksGraph })
    this.todayBmi = this.data.todayBmi;
    this.todayWeight = this.data.todayWeight;
    this.todayFat = this.data.todayFat;
  }

  close() {
    this.dialogRef.close();
  }

}
