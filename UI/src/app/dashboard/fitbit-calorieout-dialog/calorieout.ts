export var calorieout = [
    {
      "name": "Day 1",
      "series": [
        {
          "name": "Calorie In",
          "value": 73
        },
        {
          "name": "Calorie Out",
          "value": 89
        }
      ]
    },
  
    {
      "name": "Day 2",
      "series": [
        {
            "name": "Calorie In",
            "value": 53
          },
          {
            "name": "Calorie Out",
            "value": 67
          }
      ]
    },

  
    {
        "name": "Day 3",
        "series": [
          {
              "name": "Calorie In",
              "value": 73
            },
            {
              "name": "Calorie Out",
              "value": 89
            }
        ]
      },


  
    {
        "name": "Day 4",
        "series": [
          {
              "name": "Calorie In",
              "value": 98
            },
            {
              "name": "Calorie Out",
              "value": 140
            }
        ]
      },


  
    {
        "name": "Day 5",
        "series": [
          {
              "name": "Calorie In",
              "value": 67
            },
            {
              "name": "Calorie Out",
              "value": 87
            }
        ]
      },


  
    {
        "name": "Day 6",
        "series": [
          {
              "name": "Calorie In",
              "value": 45
            },
            {
              "name": "Calorie Out",
              "value": 76
            }
        ]
    },


  
    {
        "name": "Day 7",
        "series": [
          {
              "name": "Calorie In",
              "value": 87
            },
            {
              "name": "Calorie Out",
              "value": 98
            }
        ]
      },


  
    // {
    //     "name": "Day 8",
    //     "series": [
    //       {
    //           "name": "Calorie In",
    //           "value": 730
    //         },
    //         {
    //           "name": "Calorie Out",
    //           "value": 8940
    //         }
    //     ]
    //   },


  
    // {
    //     "name": "Day 9",
    //     "series": [
    //       {
    //           "name": "Calorie In",
    //           "value": 730
    //         },
    //         {
    //           "name": "Calorie Out",
    //           "value": 8940
    //         }
    //     ]
    //   },


  
    // {
    //     "name": "Day 10",
    //     "series": [
    //       {
    //           "name": "Calorie In",
    //           "value": 730
    //         },
    //         {
    //           "name": "Calorie Out",
    //           "value": 8940
    //         }
    //     ]
    //   },



  
    // {
    //     "name": "Day 11",
    //     "series": [
    //       {
    //           "name": "Calorie In",
    //           "value": 730
    //         },
    //         {
    //           "name": "Calorie Out",
    //           "value": 8940
    //         }
    //     ]
    //   },

    //   {
    //     "name": "Day 12",
    //     "series": [
    //       {
    //           "name": "Calorie In",
    //           "value": 730
    //         },
    //         {
    //           "name": "Calorie Out",
    //           "value": 8940
    //         }
    //     ]
    //   },

    //   {
    //     "name": "Day 13",
    //     "series": [
    //       {
    //           "name": "Calorie In",
    //           "value": 730
    //         },
    //         {
    //           "name": "Calorie Out",
    //           "value": 8940
    //         }
    //     ]
    //   },

    //   {
    //     "name": "Day 14",
    //     "series": [
    //       {
    //           "name": "Calorie In",
    //           "value": 730
    //         },
    //         {
    //           "name": "Calorie Out",
    //           "value": 8940
    //         }
    //     ]
    //   }


    
]