export var single = [
  {
    "name": "01",
    "value": 10000
  },
  {
    "name": "02",
    "value": 7000
  },
  {
    "name": "03",
    "value": 8000
  },
  {
    "name": "04",
    "value": 5000
  },
  {
    "name": "05",
    "value": 9500
  },
  {
    "name": "06",
    "value": 8000
  },
  {
    "name": "07",
    "value": 8200
  },
  {
    "name": "08",
    "value": 2500
  },
  {
    "name": "09",
    "value": 6000
  },
  {
    "name": "10",
    "value": 2500
  },
  {
    "name": "11",
    "value": 8000
  },
  {
    "name": "12",
    "value": 7500
  },
  {
    "name": "13",
    "value": 7000
  },
  {
    "name": "14",
    "value": 1000
  },
  {
    "name": "15",
    "value": 6500
  },
  {
    "name": "16",
    "value": 8500
  },
  {
    "name": "17",
    "value": 2500
  },
  {
    "name": "18",
    "value": 4000
  },
  {
    "name": "19",
    "value": 5500
  },
  {
    "name": "20",
    "value": 6200
  },
  {
    "name": "21",
    "value": 7800
  },
  {
    "name": "22",
    "value": 9000
  },
  {
    "name": "23",
    "value": 8000
  },
  {
    "name": "24",
    "value": 250
  },
  {
    "name": "25",
    "value": 2500
  },
  {
    "name": "26",
    "value": 6300
  },
  {
    "name": "27",
    "value": 4800
  },
  {
    "name": "28",
    "value": 8200
  },
  {
    "name": "29",
    "value": 7400
  },
  {
    "name": "30",
    "value": 6000
  },
  {
    "name": "31",
    "value": 5700
  }
];