import { Component, OnInit, Input, ɵConsole } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { CommmanService } from '../../_services/commman.service';
import { globalProviderSelect } from '../../_services/globalProviderSelect.service'
import { FitbitActivityDialogComponent } from '../fitbit-activity-dialog/fitbit-activity-dialog.component';
import { FitbitCalorieIntakeDialogComponent } from '../fitbit-calorie-intake-dialog/fitbit-calorie-intake-dialog.component';
import { FitbitCalorieoutDialogComponent } from '../fitbit-calorieout-dialog/fitbit-calorieout-dialog.component';
import { FitbitCaloriesDialogComponent } from '../fitbit-calories-dialog/fitbit-calories-dialog.component';
import { FitbitSettingsComponent } from '../fitbit-settings/fitbit-settings.component';
import { FitbitSleepDialogComponent } from '../fitbit-sleep-dialog/fitbit-sleep-dialog.component';
import { FitbitWeightDialogComponent } from '../fitbit-weight-dialog/fitbit-weight-dialog.component';
import { single } from './today-activity';
import { MatSnackBar } from '@angular/material/snack-bar'
@Component({
  selector: 'app-fitbit-dashboard',
  templateUrl: './fitbit-dashboard.component.html',
  styleUrls: ['./fitbit-dashboard.component.css']
})
export class FitbitDashboardComponent implements OnInit {


  @Input() user_id: any = null;

  selectedPatientId: any;
  users: any = [];
  noRecordFond = 'Loading...';

  single: any[];
  steps: any[];
  weight: any[];
  calories: any[];
  view: any[];
  fitbitPopup: boolean = true;
  params: any;
  action=null;
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';
  tabFlag: boolean = false;
  liveFlagTab: boolean = false;
  demoFlagTab: boolean = true;
  //activity keys
  lastTwoWeekTotalSteps = 0;
  lastTwoWeekAVgSteps = 0;
  activityPopupModel: any;
  todayStep = 0;
  lastTwoWeekTotalStepsFlag: boolean = false;
  todayStepFlag: boolean = false;

  //Caloriesout keys
  todayCaloriesOut = 0;
  lastTwoWeekAVgCaloriesOut = 0;
  lastTwoWeekTotalCaloriesOut = 0;
  caloriesPopupModel: any;
  lastTwoWeekTotalCaloriesOutFlag: boolean = false;
  todayCaloriesOutFlag: boolean = false;

  //CaloriesIn keys
  lastTwoWeekTotalCaloriesIn = 0;
  caloriesInPopupModel: any;
  todayCaloriesIn = 0;
  lastTwoWeekTotalCaloriesInFlag: boolean = false;
  todayCaloriesInFlag: boolean = false;
  caloriesInOut;


  //weight keys
  todayWeight = 0;
  weightPopupModel: any;
  todayWeightFlag: boolean = false;
  todayWeightGraph = 0;
  //seep keys
  sleepWeeksTotal = 0;
  todayHours = 0;
  todayMinutes = 0;
  weeksHours = 0;
  weeksMinutes = 0;
  sleepPopupModel: any;
  weeksHoursFlag: boolean = false;
  todayHoursFlag: boolean = false;

  //Carories in/out key
  caloriesInOutPopupModel: any;
  lastWeekTotalDiff = 0;
  weekCaloriesInOutFlag: boolean = false;
  colorScheme = {
    domain: ['#7209B7', '#3A0CA3', '#4361EE']
  };

  colorSchemeCalories = {
    domain: ['#FC6400', '#FAC000']
  };

  colorSchemeWeight = {
    domain: ['#184d47', '#96bb7c']
  };

  colorSchemeTakeIn = {
    domain: ['#D19477', '#E5B5A1']
  };

  colorSchemeSleep = {
    domain: ['#0D1821', '#344966']
  };

  colorSchemeSCalorieOut = {
    domain: ['#810000', '#ff6464']
  };

  //Demo fitbit flag
flagDemoFitbitShow: boolean = false;
patientFitbitFlag: Boolean = false;
  byProviderTextFlag: Boolean = false;
  byProviderShowUser: boolean = false;

  userRemoveFitBitConfigurationFlag: Boolean = false;
  fitbitConfigAvailableStatusMsg='spinner';

  constructor (public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private commanService: CommmanService,
    private spinner: NgxSpinnerService,private _snackBar: MatSnackBar, private _globalProviderSelect: globalProviderSelect) {
    Object.assign(this, { single });
    // Object.assign(this, { steps })
    // Object.assign(this, { calories })
    // Object.assign(this, { weight })
    this.params = this.route.snapshot.queryParams["code"];
    console.log("this.params");
    this.spinner.show();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  ngOnInit(): void {
    if(this.params && this.router.url == '/patient/fitbit-dashboard'+'?code='+this.params+'#_=_'){
      console.log("===============CALLING============================ ");
      this.patientFitbitFlag = true;
      this.updateCode();
    }
    else if(this.router.url == '/patient/fitbit-dashboard'){
      this.patientFitbitFlag = true;
      this.DemofitbitEnable();
    }else if(this.router.url == '/provider/fitbit-dashboard'){
      this.getAllPatient('Fitbit');
      this.byProviderTextFlag = true;  
      this.byProviderShowUser = true;  
      this._globalProviderSelect.getId().subscribe(n => {
        this.selectedPatientId = n;
        this.selectedUser(this.selectedPatientId);
      });
      this.selectedUser(this._globalProviderSelect.getId());
    }else{
      this.patientFitbitFlag = true;
      this.DemofitbitEnable();
    }
   
  }

  onSelect(event) {
    console.log(event);
  }
  getAllPatient(type){
    this.commanService.getAllPatient(type)
    .subscribe((result: any) => {
      if(result.success){
        this.noRecordFond = 'No any user selected, please select user to view';
        this.users = result.users;
      }
    })
  }
  selectedUser(user_id){
    if(user_id == undefined){
      this.byProviderTextFlag = true;
     this.userRemoveFitBitConfigurationFlag=false;
    }else{
      //this.liveData()
      this.getLiveDataForProvider(user_id);
      this.byProviderTextFlag = false;
    }
  }
  demoData() {
    this.liveFlagTab = false;
    this.demoFlagTab = true;
    this.tabFlag = false;

    this.lastTwoWeekTotalStepsFlag = false;
    this.todayStepFlag = false;
    this.lastTwoWeekTotalCaloriesOutFlag = false;
    this.todayCaloriesOutFlag = false;
    this.lastTwoWeekTotalCaloriesInFlag = false;
    this.todayCaloriesInFlag = false;
    this.todayWeightFlag = false;
    this.weeksHoursFlag = false;
    this.todayHoursFlag = false;
    this.weekCaloriesInOutFlag = false;

    this.getTodayActivities(null, this.tabFlag);
    this.getlastTwoWeekActivity(null, this.tabFlag);
    this.getlastTwoWeekCalories(null, this.tabFlag);
    this.getlastTwoWeekInCalories(null, this.tabFlag);
    this.getlastTwoWeekWeight(null, this.tabFlag);
    this.getlastTwoWeekSleep(null, this.tabFlag);
    this.getlastWeekInOutCalories(null, this.tabFlag);


  }



getLiveDataForProvider(user_id){
  this.liveFlagTab = true;
  this.demoFlagTab = false;
  this.tabFlag = true;

  this.lastTwoWeekTotalStepsFlag = false;
  this.todayStepFlag = false;
  this.lastTwoWeekTotalCaloriesOutFlag = false;
  this.todayCaloriesOutFlag = false;
  this.lastTwoWeekTotalCaloriesInFlag = false;
  this.todayCaloriesInFlag = false;
  this.todayWeightFlag = false;
  this.weeksHoursFlag = false;
  this.todayHoursFlag = false;
  
  this.checkConfigurationThroughProvider(user_id);     
}

checkConfigurationThroughProvider(user_id){
  this.commanService.checkFitbitConfigurationThroughProvider(user_id).subscribe(
    (resmsg: any) => {
      this.redirectFitbit(resmsg);
    },
    (err) => {
      console.log(err);
    }
  );
}




  liveData() {
    this.liveFlagTab = true;
    this.demoFlagTab = false;
    this.tabFlag = true;

    this.lastTwoWeekTotalStepsFlag = false;
    this.todayStepFlag = false;
    this.lastTwoWeekTotalCaloriesOutFlag = false;
    this.todayCaloriesOutFlag = false;
    this.lastTwoWeekTotalCaloriesInFlag = false;
    this.todayCaloriesInFlag = false;
    this.todayWeightFlag = false;
    this.weeksHoursFlag = false;
    this.todayHoursFlag = false;
    
    this.checkUserConfigured();
  }

  openFitBitSleepDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.sleepPopupModel;
    const dialogRef = this.dialog.open(FitbitSleepDialogComponent, dialogConfig);
  }


  openWeightDialog() {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.data = this.particularProviderRecord;
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.weightPopupModel;
    const dialogRef = this.dialog.open(FitbitWeightDialogComponent, dialogConfig);
  }


  openActivityDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.activityPopupModel;
    const dialogRef = this.dialog.open(FitbitActivityDialogComponent, dialogConfig);
  }

  openCaloriesDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.caloriesPopupModel;
    const dialogRef = this.dialog.open(FitbitCaloriesDialogComponent, dialogConfig);
  }

  openCaloriesIntakeDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.caloriesInPopupModel;
    const dialogRef = this.dialog.open(FitbitCalorieIntakeDialogComponent, dialogConfig);
  }

  openFitBitSettingDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    const dialogRef = this.dialog.open(FitbitSettingsComponent, dialogConfig);
  }
  // viewFitbitByUser(userId){
  //   this.commanService.checkFitbitConfigByUser(userId)
  //   .subscribe(
  //     (resmsg: any) => {
  //       console.log(resmsg)
  //       if(resmsg.success){
  //         this.byProviderTextFlag = false;
  //         this.userRemoveFitBitConfigurationFlag =true;
  //         localStorage.setItem('fitbit_refresh_token', resmsg.data);
  //         this.getTodayActivities(resmsg.data, this.tabFlag);
  //         this.getlastTwoWeekActivity(resmsg.data, this.tabFlag);
  //         this.getlastTwoWeekCalories(resmsg.data, this.tabFlag);
  //         this.getlastTwoWeekInCalories(resmsg.data, this.tabFlag);
  //         this.getlastTwoWeekWeight(resmsg.data, this.tabFlag);
  //         this.getlastTwoWeekSleep(resmsg.data, this.tabFlag);
  //       }else{
  //         this.userRemoveFitBitConfigurationFlag =false;
  //         this.fitbitConfigAvailableStatusMsg = 'Your FitBit configuration has been removed.';
  //       }
  //     }, (err) => {
  //       console.log(err)
  //     })
  // }
  checkUserConfigured() {
    this.commanService.checkFitbitConfig().subscribe(
      (resmsg: any) => {
        this.redirectFitbit(resmsg);
      },
      (err) => {
        console.log(err);
      }
    );
  }

redirectFitbit(resmsg){
  if (resmsg.success == true) {
    this.fitbitPopup = false;
    this.userRemoveFitBitConfigurationFlag =true;
   // localStorage.setItem('fitbit_refresh_token', resmsg.data);
    this.getTodayActivities(resmsg.data, this.tabFlag);
    this.getlastTwoWeekActivity(resmsg.data, this.tabFlag);
    this.getlastTwoWeekCalories(resmsg.data, this.tabFlag);
    this.getlastTwoWeekInCalories(resmsg.data, this.tabFlag);
    this.getlastTwoWeekWeight(resmsg.data, this.tabFlag);
    this.getlastTwoWeekSleep(resmsg.data, this.tabFlag);
  }else if(resmsg.success=='redirect'){
   // localStorage.setItem('fitbit_refresh_token', '');
       this.userRemoveFitBitConfigurationFlag =false;
       this.fitbitConfigAvailableStatusMsg = 'Your FitBit authorization has been removed or is unavailable.';
  }
  else {
   // localStorage.setItem('fitbit_refresh_token', '');
    this.userRemoveFitBitConfigurationFlag =false;
    this.fitbitConfigAvailableStatusMsg = 'Your FitBit authorization has been removed or is unavailable.';
   
  }
}




  removeFitbitConfig(){
    if (confirm("Are you sure you want to revoke Curamei access to Fitbit data?")) {
    this.commanService.removeFitbitConfigByUser().subscribe((resmsg: any) => {
      this.checkUserConfigured();
        if (resmsg.success == true) {
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-green'],
          });
        }else{
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
        
      },
      (err) => {
        console.log(err);
      }
    );
    }
  }







  openFitBitCalorieOutDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "40%";
    dialogConfig.panelClass = "mob-fitbit-dialog";
    dialogConfig.data = this.caloriesInOutPopupModel;
    const dialogRef = this.dialog.open(FitbitCalorieoutDialogComponent, dialogConfig);
  }


  updateCode() {
    if (this.params) {
      this.commanService.updateUserFitbitCode({ 'code': this.params }).subscribe((result: any) => {
        if (result.success == true) {
          this.params=null;
         // localStorage.setItem('fitbit_refresh_token', result.data);
           this.liveData();
        } else{
          this.liveData();
        }
      });
    }
  }

 

  getTodayActivities(access_token, tabFlag) {
    this.commanService.getTodayActivities(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {

          this.todayStepFlag = true;
          this.todayCaloriesOutFlag = true;
          this.todayStep = Math.round(resmsg.activityObj.steps);
          this.todayCaloriesOut = Math.round(resmsg.activityObj.caloriesOut);

        } else {
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      },
      (err) => {
        this._snackBar.open(err.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    );
  }

  getlastTwoWeekActivity(access_token, tabFlag) {
    this.commanService.getlastTwoWeekActivity(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.lastTwoWeekTotalStepsFlag = true;
          console.log(resmsg.activityObj.oneGraphActivity);
          this.activityPopupModel = resmsg.activityObj;
          this.lastTwoWeekTotalSteps = Math.round(resmsg.activityObj.lastTwoWeekTotalSteps);
          this.lastTwoWeekAVgSteps = resmsg.activityObj.lastTwoWeekAVgSteps;
          Object.assign(this, { steps: resmsg.activityObj.oneGraphActivity });
        } else {
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      },
      (err) => {
        this._snackBar.open(err.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    );
  }

  getlastTwoWeekCalories(access_token, tabFlag) {
    this.commanService.getlastTwoWeekCalories(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.lastTwoWeekTotalCaloriesOutFlag = true;
          Object.assign(this, { calories: resmsg.caloriesObj.oneGraphCalories });
          this.lastTwoWeekAVgCaloriesOut = resmsg.caloriesObj.lastTwoWeekAVgCaloriesOut;
          this.lastTwoWeekTotalCaloriesOut = resmsg.caloriesObj.lastTwoWeekTotalCaloriesOut;
          this.caloriesPopupModel = resmsg.caloriesObj;
        } else {
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      },
      (err) => {
        this._snackBar.open(err.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    );
  }

  getlastTwoWeekInCalories(access_token, tabFlag) {
    this.commanService.getlastTwoWeekInCalories(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.lastTwoWeekTotalCaloriesInFlag = true;
          this.todayCaloriesInFlag = true;
          Object.assign(this, { weight: resmsg.caloriesInObj.oneGraphCaloriesIn });
          this.lastTwoWeekTotalCaloriesIn = Math.round(resmsg.caloriesInObj.lastTwoWeekTotalCaloriesIn);
          this.caloriesInPopupModel = resmsg.caloriesInObj;
          this.todayCaloriesIn = Math.round(resmsg.caloriesInObj.todayCaloriesIn);
        } else {
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      },
      (err) => {
        this._snackBar.open(err.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    );
  }

  getlastTwoWeekWeight(access_token, tabFlag) {
    this.commanService.getlastTwoWeekWeight(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.todayWeightFlag = true;
          this.todayWeight = resmsg.weightObj.todayWeight;
          this.weightPopupModel = resmsg.weightObj;
          Object.assign(this, { todayWeightGraph: resmsg.weightObj.todayWeightGraph });

        } else {
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      },
      (err) => {
        this._snackBar.open(err.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    );
  }

  getlastTwoWeekSleep(access_token, tabFlag) {
    this.commanService.getlastTwoWeekSleep(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {

          Object.assign(this, { sleepWeeksTotal: resmsg.sleepObj.oneGraphWeeksSleep });
          this.sleepPopupModel = resmsg.sleepObj;
          // this.sleepWeeksTotal=resmsg.sleepObj.sleepWeeksTotal;
          this.todayHours = resmsg.sleepObj.todayHours;
          this.todayMinutes = resmsg.sleepObj.todayMinutes;
          this.weeksHours = resmsg.sleepObj.weeksHours;
          this.weeksMinutes = resmsg.sleepObj.weeksMinutes;
          this.weeksHoursFlag = true;
          this.todayHoursFlag = true;

        } else {
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      },
      (err) => {
        this._snackBar.open(err.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    );
  }


  getlastWeekInOutCalories(access_token, tabFlag) {
    this.commanService.getlastWeekInOutCalories(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.caloriesInOutPopupModel = resmsg.caloriesInOutObj;
          this.lastWeekTotalDiff = resmsg.caloriesInOutObj.lastWeekTotalDiff;
          Object.assign(this, { caloriesInOut: resmsg.caloriesInOutObj.oneWeekGraph });

          this.weekCaloriesInOutFlag = true;
        } else {
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      },
      (err) => {
        this._snackBar.open(err.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    );
  }

  DemofitbitEnable() {
    this.commanService.checkDemofitbitEnable().subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.flagDemoFitbitShow =true;
          this.demoData();
        }else{
          this.flagDemoFitbitShow =false;
          this.liveData();
        } 
      },
      (err) => {
        this._snackBar.open(err.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    );
  }


}
