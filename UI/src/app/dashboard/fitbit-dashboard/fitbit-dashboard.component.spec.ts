import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitbitDashboardComponent } from './fitbit-dashboard.component';

describe('FitbitDashboardComponent', () => {
  let component: FitbitDashboardComponent;
  let fixture: ComponentFixture<FitbitDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitbitDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitbitDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
