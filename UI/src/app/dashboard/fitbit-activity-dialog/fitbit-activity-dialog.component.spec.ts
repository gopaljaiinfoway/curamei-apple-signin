import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitbitActivityDialogComponent } from './fitbit-activity-dialog.component';

describe('FitbitActivityDialogComponent', () => {
  let component: FitbitActivityDialogComponent;
  let fixture: ComponentFixture<FitbitActivityDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitbitActivityDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitbitActivityDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
