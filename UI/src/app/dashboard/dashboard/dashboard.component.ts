import { state, style, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { CommmanService} from '../../_services/commman.service';
import { NotifierService } from 'angular-notifier';
declare var $: any
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      // transition(
      //   'expanded <=> collapsed',
      //   animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      // ),
    ]),
  ],
})
export class DashboardComponent implements OnInit {
  private notifier: NotifierService;
  displayedColumns: string[] = ['type', 'name', 'email', 'created', 'details']
  panelOpenState = false
  expandedElement:any;
  public icon = 'add'
  searchData:string;

  totalStates:any=0;
  totalPatients:any=0;
  totalProvider:any=0;
  totalSupport:any=0;
  totalAccounts:any=0;
  totalActive:any=0;
  totalSource:any=0;
  totalIntegration:any=0;

  dataSource : MatTableDataSource<any>;
  email = new FormControl('', [Validators.email])
  constructor(public notifierService: NotifierService,private commanService: CommmanService) {
    this.notifier = notifierService;
    
  }

 

  public toggleIcon() {
    if (this.icon === 'add') {
      this.icon = 'remove'
    } else {
      this.icon = 'add'
    }
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value'
    }

    return this.email.hasError('email') ? 'Not a valid email' : ''
  }



  ngOnInit(): void {
    this.sidebarCollapse();
    this.ListGet();
    this.adminDashboardTotalCount();
  }
  sidebarCollapse() {
    $('#sidebar').toggleClass('active')
    $('#body').toggleClass('active')
  }


  ListGet(){
    this.commanService.adminDashboardList().subscribe((res: any) => { 
      if(res.success && res.data.length > 0){
        console.log(res.data);
        this.dataSource = new MatTableDataSource(res.data); 
           }else{
            this.dataSource = new MatTableDataSource([]);
           }
    },
    err => {
     console.log(err);
   });

}



adminDashboardTotalCount(){
  this.commanService.adminDashboardTotalCount().subscribe((res: any) => { 
    if(res.success && res.data.length > 0){
      console.log(res.data);
      this.totalStates = res.data[0].statetotal;
     this.totalPatients = res.data[0].patientuserscount;
      this.totalProvider = res.data[0].provideruserscount;
      this.totalSupport = res.data[0].supportuserscount;
      this.totalAccounts = res.data[0].accounttotal;
     this.totalActive =res.data[0].activetotal;
         }
  },
  err => {
   console.log(err);
 });

}






searchDataClear(){
  this.searchData='';
 // this.applyFilter();
}


applyFilter(data){
  if(data=='' || data==undefined){
      this.ListGet();
  }else{
 //this.dataSource.filter = this.searchData.trim().toLocaleLowerCase();
 this.commanService.adminUserGetInfoByEmail(data).subscribe((res: any) => { 
  if(res.success && res.data.length > 0){
    console.log(res.data);
    this.dataSource = new MatTableDataSource(res.data); 
       }else{
        this.dataSource = new MatTableDataSource([]);
       }
},
err => {
 console.log(err);
});
  }
 
  
}



}


