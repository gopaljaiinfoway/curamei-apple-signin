import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RolesComponent } from './roles/roles.component';
import { FitbitDashboardComponent } from './fitbit-dashboard/fitbit-dashboard.component';
import { FitbitActivityDialogComponent } from './fitbit-activity-dialog/fitbit-activity-dialog.component';
import { FitbitWeightDialogComponent } from './fitbit-weight-dialog/fitbit-weight-dialog.component';
import { FitbitCaloriesDialogComponent } from './fitbit-calories-dialog/fitbit-calories-dialog.component';
import { FitbitCalorieIntakeDialogComponent } from './fitbit-calorie-intake-dialog/fitbit-calorie-intake-dialog.component';
import { FitbitSettingsComponent } from './fitbit-settings/fitbit-settings.component';
import { FitbitSleepDialogComponent } from './fitbit-sleep-dialog/fitbit-sleep-dialog.component';
import { FitbitCalorieoutDialogComponent } from './fitbit-calorieout-dialog/fitbit-calorieout-dialog.component';


@NgModule({
  declarations: [DashboardComponent, RolesComponent, FitbitDashboardComponent, FitbitActivityDialogComponent, FitbitWeightDialogComponent, FitbitCaloriesDialogComponent, FitbitCalorieIntakeDialogComponent, FitbitSettingsComponent, FitbitSleepDialogComponent, FitbitCalorieoutDialogComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
