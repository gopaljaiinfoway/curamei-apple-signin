import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { CustomFormsModule } from 'ng2-validation';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { FitbitActivityDialogComponent } from './dashboard/fitbit-activity-dialog/fitbit-activity-dialog.component';
import { FitbitCalorieIntakeDialogComponent } from './dashboard/fitbit-calorie-intake-dialog/fitbit-calorie-intake-dialog.component';
import { FitbitCalorieoutDialogComponent } from './dashboard/fitbit-calorieout-dialog/fitbit-calorieout-dialog.component';
import { FitbitCaloriesDialogComponent } from './dashboard/fitbit-calories-dialog/fitbit-calories-dialog.component';
import { FitbitDashboardComponent } from './dashboard/fitbit-dashboard/fitbit-dashboard.component';
import { FitbitSettingsComponent } from './dashboard/fitbit-settings/fitbit-settings.component';
import { FitbitSleepDialogComponent } from './dashboard/fitbit-sleep-dialog/fitbit-sleep-dialog.component';
import { FitbitWeightDialogComponent } from './dashboard/fitbit-weight-dialog/fitbit-weight-dialog.component';
import { AngularMaterialModule } from './material-module';
import { MaterialModule } from './material/material.module';
import { DataSourcesComponent } from './patient/data-sources/data-sources.component';
import { EditProviderDialogComponent } from './patient/edit-provider-dialog/edit-provider-dialog.component';
import { EncountersComponent } from './patient/encounters/encounters.component';
import { EnrollmentComponent } from './patient/enrollment/enrollment.component';
import { Enrollment1Component } from './patient/enrollment1/enrollment1.component';
import { Enrollment2Component } from './patient/enrollment2/enrollment2.component';
import { Enrollment3Component } from './patient/enrollment3/enrollment3.component';
import { Enrollment4Component } from './patient/enrollment4/enrollment4.component';
import { ManualMedicalRecordsComponent } from './patient/manual-medical-records/manual-medical-records.component';
import { MedicalRecordsComponent } from './patient/medical-records/medical-records.component';
import { PatientProviderComponent } from './patient/patient-provider/patient-provider.component';
import { PdashboardComponent } from './patient/pdashboard/pdashboard.component';
import { PractitionersComponent } from './patient/practitioners/practitioners.component';
import { ProviderConnectComponent } from './patient/provider-connect/provider-connect.component';
import { ProviderListComponent } from './patient/provider-list/provider-list.component';
import { RequestHealthSystemComponent } from './patient/request-health-system/request-health-system.component';
import { SelectDataSourceComponent } from './patient/select-data-source/select-data-source.component';
import { ProviderDashboardComponent } from './provider/provider-dashboard/provider-dashboard.component';
import { LoginComponent } from './session/login/login.component';
import { RegistrationComponent } from './session/registration/registration.component';
import { SignupSuccessComponent } from './session/signup-success/signup-success.component';
import { signupVerifyComponent } from './session/signup-verify/signup-verify.component';
import { ActivateGuard, AdminAuthGuard } from './_guard/activate.guard';
import { ActivatechildGuard } from './_guard/activatechild.guard';
import { adminHeaderComponent } from './_layout/admin-layout/admin-header/admin-header.component';
import { AdminLayoutComponent } from './_layout/admin-layout/admin-layout.component';
import { AdminLeftSidebarComponent } from './_layout/admin-layout/admin-left-sidebar/admin-left-sidebar.component';
import { PatientHeaderComponent } from './_layout/patient-layout/patient-header/patient-header.component';
import { PatientLayoutComponent } from './_layout/patient-layout/patient-layout.component';
import { PatientLeftSidebarComponent } from './_layout/patient-layout/patient-left-sidebar/patient-left-sidebar.component';
import { ProviderHeaderComponent } from './_layout/provider-layout/provider-header/provider-header.component';
import { ProviderLayoutComponent } from './_layout/provider-layout/provider-layout.component';
import { ProviderLeftSidebarComponent } from './_layout/provider-layout/provider-left-sidebar/provider-left-sidebar.component';
import { CommmanService } from './_services/commman.service';
import { globalProviderSelect } from './_services/globalProviderSelect.service'
import { UserAuthenticationCheckService } from './_services/user-authentication-check.service';
import { PatientMedicalViewComponent } from './provider/patient-medical-view/patient-medical-view.component';
import { SharePatientResourceComponent } from './patient/share-patient-resource/share-patient-resource.component';
import { ManageApplicationPermissionComponent } from './provider/manage-application-permission/manage-application-permission.component';
import { ProviderFitbitDashboardComponent } from './provider/provider-fitbit-dashboard/provider-fitbit-dashboard.component';
import { SupportLayoutComponent } from './_layout/support-layout/support-layout.component';
import { SupportDashboardComponent } from './support-dashboard/support-dashboard.component';
import {UserProfileComponent } from './session/user-profile/user-profile.component';

import { ProviderProfileComponent } from './provider/provider-profile/provider-profile.component';
import { ManagePatientAccessComponent } from './provider/manage-patient-access/manage-patient-access.component';
import { MedicalRecordsDetailsComponent } from './patient/medical-records/medical-records-details/medical-records-details.component'
import { NarrativeComponent } from './narrative/narrative.component'
import { DigitalHealthComponent } from './patient/digital-health/digital-health.component';
import { DigitalHealthDetailsComponent } from './patient/digital-health-details/digital-health-details.component';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { SocketioService } from './socketIO.service';
import { ResetPasswordComponent } from './session/reset-password/reset-password.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ForgotPasswordComponent } from './session/forgot-password/forgot-password.component';
import {NgxMatIntlTelInputModule} from 'ngx-mat-intl-tel-input';
import { UsersComponent } from './admin/users/users.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { AdminService } from './_services/admin.service';
import { DialogBoxComponent } from './admin/dialog-box/dialog-box.component';
import {ConnectionServiceModule} from 'ng-connection-service';  

import { NotificationsMobileComponent } from './_layout/notifications-mobile/notifications-mobile.component';
import { PatientSelectMobileComponent } from "./_layout/patient-select-mobile/patient-select-mobile.component"
import { ProviderListMobileComponent } from './patient/provider-list-mobile/provider-list-mobile.component';
import { SharePatientResourceMobile} from './patient/share-patient-resource-mobile/share-patient-resource-mobile.component';
import {ManagePatientAccessMobile } from './provider/manage-patient-access-mobile/manage-patient-access-mobile.component';
import {MedicalRecordsDetalisMobile } from './patient/medical-records/medical-records-detalis-mobile/medical-records-detalis-mobile.component';
import {ManualMedicalRecordsMobile } from './patient/manual-medical-records-mobile/manual-medical-records-mobile.component';
import {UserProfileMobile } from './session/user-profile-mobile/user-profile-mobile.component';
import { ProviderProfileMobile} from './provider/provider-profile-mobile/provider-profile-mobile.component';
import {ProviderIdproofMobile } from './session/provider-idproof-mobile/provider-idproof-mobile.component';
import {ProviderListDocumentUploadMobile } from './patient/provider-list-document-upload-mobile/provider-list-document-upload-mobile.component'
import { UserLoginedService } from './_services/user-logined.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { HomeComponent } from './home/home.component';
import { ErrorService} from './_services/error.service';
// Notifier
const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'left',
      distance: 12,
    },
    vertical: {
      position: 'bottom',
      distance: 12,
      gap: 10,
    },
  },
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4,
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease',
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50,
    },
    shift: {
      speed: 300,
      easing: 'ease',
    },
    overlap: 150,
  },
};
//end notifier
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    RegistrationComponent,
    SignupSuccessComponent,
    signupVerifyComponent,
    adminHeaderComponent,
    AdminLayoutComponent,
    AdminLeftSidebarComponent,
    ProviderHeaderComponent,
    ProviderLayoutComponent,
    ProviderLeftSidebarComponent,
    PatientLayoutComponent,
    PatientHeaderComponent,
    PatientLeftSidebarComponent,
    PdashboardComponent,
    ProviderDashboardComponent,
    EnrollmentComponent,
    Enrollment1Component,
    Enrollment2Component,
    Enrollment3Component,
    Enrollment4Component,
    DataSourcesComponent,
    ProviderConnectComponent,
    PatientProviderComponent,
    ProviderListComponent,
    EditProviderDialogComponent,
    MedicalRecordsComponent,
    PractitionersComponent,
    EncountersComponent,
    FitbitDashboardComponent,
    FitbitActivityDialogComponent,
    FitbitWeightDialogComponent,
    FitbitCaloriesDialogComponent,
    FitbitCalorieIntakeDialogComponent,
    FitbitSettingsComponent,
    FitbitSleepDialogComponent,
    FitbitCalorieoutDialogComponent,
    RequestHealthSystemComponent,
    SelectDataSourceComponent,
    ManualMedicalRecordsComponent,
    PatientMedicalViewComponent,
    SharePatientResourceComponent,
    ManageApplicationPermissionComponent,
    ProviderFitbitDashboardComponent,
    SupportLayoutComponent,
    SupportDashboardComponent,
    UserProfileComponent,
    ProviderProfileComponent,
    ManagePatientAccessComponent,
    MedicalRecordsDetailsComponent,
    NarrativeComponent,
    DigitalHealthComponent,
    DigitalHealthDetailsComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    UsersComponent,
    AdminDashboardComponent,
    AdminLoginComponent,
    DialogBoxComponent,
    NotificationsMobileComponent,
    PatientSelectMobileComponent,
    ProviderListMobileComponent,
    SharePatientResourceMobile,
    ManagePatientAccessMobile,
    MedicalRecordsDetalisMobile,
    ManualMedicalRecordsMobile,
    UserProfileMobile,
    ProviderProfileMobile,
    ProviderIdproofMobile,
    ProviderListDocumentUploadMobile,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    CommonModule,
    HttpClientModule,
    MaterialModule,
    NotifierModule.withConfig(customNotifierOptions),
    RouterModule.forRoot(AppRoutes),
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    NgxSpinnerModule,
    HttpClientJsonpModule,
    AngularMaterialModule,
    PaginationModule.forRoot(),
    NgxPaginationModule,
    NgxChartsModule,
    AngularFireModule.initializeApp(environment.firebase),
    NgxCaptchaModule,
    NgxMatIntlTelInputModule,
    ConnectionServiceModule
  ],
  exports: [
    AngularFireModule, AngularFireAuthModule
  ],
  providers: [
    SocketioService,
    ActivatechildGuard,
    ActivateGuard,
    AdminAuthGuard,
    CommmanService,
    AdminService,
    UserLoginedService,
    UserAuthenticationCheckService,
    ErrorService,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    globalProviderSelect,
    AndroidPermissions,
    Camera,
    Chooser,
    ImagePicker
    
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    EditProviderDialogComponent,
    FitbitActivityDialogComponent, 
    FitbitWeightDialogComponent, 
    FitbitCaloriesDialogComponent, 
    FitbitCalorieIntakeDialogComponent,
    MedicalRecordsDetailsComponent,
    DigitalHealthDetailsComponent,
    DialogBoxComponent,
    ProviderListMobileComponent,
    SharePatientResourceMobile,
    ManagePatientAccessMobile,
    MedicalRecordsDetalisMobile,
    ManualMedicalRecordsMobile,
    UserProfileMobile,
    ProviderProfileMobile,
    ProviderIdproofMobile,
    ProviderListDocumentUploadMobile
  ],
})
export class AppModule { }
