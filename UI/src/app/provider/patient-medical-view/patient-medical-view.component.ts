import { Component, OnInit } from '@angular/core';
import { CommmanService } from 'src/app/_services/commman.service';

@Component({
  selector: 'app-patient-medical-view',
  templateUrl: './patient-medical-view.component.html',
  styleUrls: ['./patient-medical-view.component.css']
})
export class PatientMedicalViewComponent implements OnInit {

  users: any = [];
  user_id: any;
  user_type: any;
  constructor(
    private commanService: CommmanService,
  ) { }

  ngOnInit(): void {
    this.user_type = 'GCP';
    this.getAllPatient('Medical_record');
  }
    // All Patient
    getAllPatient(type){
      this.commanService.getAllPatient(type)
      .subscribe((result: any) => {
        if(result.success){
          this.users = result.users;
        }
      })
    }
    selectForRecord(type){
      if(type == 0){
        this.user_type = 'GCP';
      }
      if(type == 1){
        this.user_type = 'Manual';
      }
    }
    selectedUser(id){
      if(id != undefined){
        this.user_id = id;
      }else{
        this.user_id = null;
      }
    }

}
