import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientMedicalViewComponent } from './patient-medical-view.component';

describe('PatientMedicalViewComponent', () => {
  let component: PatientMedicalViewComponent;
  let fixture: ComponentFixture<PatientMedicalViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientMedicalViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientMedicalViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
