import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePatientAccessMobile } from './manage-patient-access-mobile.component';

describe('ManagePatientAccessMobile', () => {
  let component: ManagePatientAccessMobile;
  let fixture: ComponentFixture<ManagePatientAccessMobile>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePatientAccessMobile ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePatientAccessMobile);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
