
import { Component, OnInit , Inject} from '@angular/core'
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
@Component({
  selector: 'app-manage-patient-access-mobile',
  templateUrl: './manage-patient-access-mobile.component.html',
  styleUrls: ['./manage-patient-access-mobile.component.css'],
})

export class ManagePatientAccessMobile implements OnInit {
 dataObj:any;
 currentPatientsFlag: boolean = false;
 requestsFlag: boolean = false;
  constructor(public dialogRef: MatBottomSheetRef<ManagePatientAccessMobile> , @Inject(MAT_BOTTOM_SHEET_DATA) public data:any) { }
  ngOnInit(): void {
   this.dataObj = this.data.objData;
     if(this.data.sectionType=='Requests')
      this.requestsFlag=true;
      if(this.data.sectionType=='currentPatients')
      this.currentPatientsFlag =true;
  } 


  closedPopupBottom(type,upid, status){
    this.dialogRef.dismiss({type:type,upid:upid,status:status});
  }

  closePopUp(){
    this.dialogRef.dismiss(null);
  }

}
