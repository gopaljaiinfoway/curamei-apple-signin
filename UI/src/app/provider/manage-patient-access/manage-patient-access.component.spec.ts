import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePatientAccessComponent } from './manage-patient-access.component';

describe('ManagePatientAccessComponent', () => {
  let component: ManagePatientAccessComponent;
  let fixture: ComponentFixture<ManagePatientAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePatientAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePatientAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
