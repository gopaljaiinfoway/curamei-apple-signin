import { Component, OnInit } from '@angular/core';
import { CommmanService } from 'src/app/_services/commman.service'
declare var $: any;
@Component({
  selector: 'app-provider-dashboard',
  templateUrl: './provider-dashboard.component.html',
  styleUrls: ['./provider-dashboard.component.css']
})
export class ProviderDashboardComponent implements OnInit {
  view: any[] = [150, 150];
  medicalRecordChart: any[];
  digitalHealthChart: any[];
  manualUploadChart: any[];
  p: number = 1;

  colorScheme = {
    domain: ['#227f77', '#d9d9d9']
  };

  constructor(private commanService: CommmanService) {
    this.medicalRecordChart = [
      {
        "name": "Sharing",
        "value": 0
      },
      {
        "name": "Not Sharing",
        "value": 100
      },
    ];
    this.digitalHealthChart = [
      {
        "name": "Sharing",
        "value": 0
      },
      {
        "name": "Not Sharing",
        "value": 100
      },
    ];
    this.manualUploadChart = [
      {
        "name": "Sharing",
        "value": 0
      },
      {
        "name": "Not Sharing",
        "value": 100
      },
    ];
    Object.assign(this, { medicalRecordChart:  this.medicalRecordChart });
    Object.assign(this, { digitalHealthChart:  this.digitalHealthChart });
    Object.assign(this, { manualUploadChart:  this.manualUploadChart });
  }
  patientProviderPerMsg='Loading...';
  patientProviderPerData=[];
  supportProviderPerMsg='Loading...';
  supportProviderPerData=[];
  ngOnInit(): void {
    this.sidebarCollapse();
    this.patientProviderPermission();
  }
  sidebarCollapse() {
    $('#sidebar').toggleClass('active');
    $('#body').toggleClass('active');
}

patientProviderPermission(){
  this.patientProviderPerMsg='Loading...';
  this.commanService.getPatientProviderPermission().subscribe(
    (res: any) => {
      console.log(res);
      if (res.success && res.data.length > 0) {
        res.data.forEach(element => {
          if (element.resource_name != null) {
            let perms = element.resource_name.split(',');
            this.patientProviderPerData.push({
            fullname: element.fullname,
            perms: perms
          });
          }
        });
        Object.assign(this, { medicalRecordChart:  res.chartData.medicalChart });
        Object.assign(this, { digitalHealthChart:   res.chartData.digitalChart });
        Object.assign(this, { manualUploadChart:   res.chartData.uploadChart });
      } else {
        this.patientProviderPerMsg='Data is not available.';
        this.patientProviderPerData = [];
      }
    },
    (err) => {
      this.patientProviderPerData = [];
      console.log(err);
      this.patientProviderPerMsg='Data is not available.';
    }
  );
}


supportProviderPermission(){
  this.patientProviderPerMsg='Loading...';
  this.commanService.getSupportProviderPermission().subscribe(
    (res: any) => {
      if (res.success && res.data.length > 0) {
          this.supportProviderPerData = res.data;
          console.log(res.data);
      } else {
        this.supportProviderPerMsg='Data is not available.';
        this.supportProviderPerData = [];
      }
    },
    (err) => {
      this.supportProviderPerData = [];
      console.log(err);
      this.supportProviderPerMsg='Data is not available.';
    }
  );
}



}
