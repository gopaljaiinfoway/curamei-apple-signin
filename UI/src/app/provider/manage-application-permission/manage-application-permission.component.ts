import { Component, OnInit } from '@angular/core';
import { CommmanService } from 'src/app/_services/commman.service';
import {formatDate} from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import jwt_decode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service'

@Component({
  selector: 'app-manage-application-permission',
  templateUrl: './manage-application-permission.component.html',
  styleUrls: ['./manage-application-permission.component.css']
})
export class ManageApplicationPermissionComponent implements OnInit {
  noRecordFond:any;
  buttonTextPermission="Save";
  action=null;
  noRecordFondResourceSharing:any;
  public patientAppPermission: FormGroup;
  roleData = [];
  loading:any;
  applicationPermissionData:MatTableDataSource<any>;
  displayedColumnsAppPer: string[] = ['action', 'checkbox']
  roleId:any;
 // dataSourceAppPer: MatTableDataSource<any>;
  constructor(
    private commanService: CommmanService,  private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar, private cookieService: CookieService,
  ) {
    let tokenGet = this.cookieService.get('token')
    if (tokenGet && tokenGet != null) {
      let decoded = jwt_decode(tokenGet, 'llp');
      this.roleId = decoded.role_id;
    }

    this.patientAppPermission = this.formBuilder.group({
      role: [this.roleId, Validators.required],
      roleActionSelected: [null, Validators.required],
      
    })

  }

  ngOnInit(): void {
   this.role();
   this.applicationPermission();
  }

  onSubmitPermission(){
    this.buttonTextPermission = 'Sending...';
   var  roleId = this.patientAppPermission.value.role;
    this.commanService.applicationPermissionAdd(this.applicationPermissionData.filteredData,roleId).subscribe(
      (resmsg: any) => {
             if(resmsg.success){
              this.applicationPermission();
              this.buttonTextPermission = 'Save'
             // this.patientAppPermission.reset();
              this._snackBar.open(resmsg.message, this.action, {
                duration: 3000,
                verticalPosition: 'top', // 'top' | 'bottom'
                panelClass: ['green-snackbar'],
              });
             }else {
              this._snackBar.open(resmsg.message, this.action, {
                duration: 3000,
                verticalPosition: 'top', // 'top' | 'bottom'
                panelClass: ['red-snackbar'],
              });
              this.buttonTextPermission = 'Save';
            }
      },
      (err) => {
        this.buttonTextPermission = 'Save';
      }
    )
  }

  role() {
    this.commanService.getRole().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
          this.roleData = (res.data);
        } else {
          this.roleData = [];
        }
      },
      (err) => {
        this.roleData = [];
        console.log(err);
      }
    );
  }


  applicationPermission() {
    this.loading="Loading...";
    this.commanService.getApplicationPermission(this.roleId).subscribe(
      (res: any) => {
         console.log(res.data);
        if (res.success && res.data.length > 0) {
          this.applicationPermissionData =  new MatTableDataSource (res.data);
        } else {
          this.applicationPermissionData =  new MatTableDataSource([]);
        }
      },
      (err) => {
        this.applicationPermissionData =  new MatTableDataSource([]);
        console.log(err);
      }
    );
  }

  selectChangeHandler (event: any) {
    this.roleId = event.value;
    this.applicationPermission();
  }

  demo(){
    console.log("-------9999999999--------------");
    console.log(this.applicationPermissionData.filteredData);
    console.log("-------9999999999--------------");
  }

}
