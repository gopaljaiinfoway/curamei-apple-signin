import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageApplicationPermissionComponent } from './manage-application-permission.component';

describe('ManageApplicationPermissionComponent', () => {
  let component: ManageApplicationPermissionComponent;
  let fixture: ComponentFixture<ManageApplicationPermissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageApplicationPermissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageApplicationPermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
