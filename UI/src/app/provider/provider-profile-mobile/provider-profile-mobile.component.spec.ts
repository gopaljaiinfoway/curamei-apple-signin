import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderProfileMobile } from './provider-profile-mobile.component';

describe('ProviderProfileMobile', () => {
  let component: ProviderProfileMobile;
  let fixture: ComponentFixture<ProviderProfileMobile>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderProfileMobile ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderProfileMobile);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
