
import { Component, OnInit , Inject,  ChangeDetectorRef,ChangeDetectionStrategy} from '@angular/core'
import {MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Chooser, ChooserResult } from '@ionic-native/chooser/ngx';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-provider-profile-mobile',
  templateUrl: './provider-profile-mobile.component.html',
  styleUrls: ['./provider-profile-mobile.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  
})

export class ProviderProfileMobile implements OnInit {
 dataObj:any;
 displayFileName:any;
 safeImg: any='';
 userImage;
 fileObj:any;
 imageShowFlag:boolean=false;
 cameraOptions: CameraOptions = {
  quality: 50,
  targetWidth: 800,
  targetHeight: 600,
  destinationType: this.camera.DestinationType.DATA_URL,
  encodingType: this.camera.EncodingType.JPEG,
  mediaType: this.camera.MediaType.PICTURE,
  allowEdit: false
 }

 gelleryOptions: CameraOptions = {
  quality: 50,
targetWidth: 800,
targetHeight: 600,
  sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  destinationType: this.camera.DestinationType.DATA_URL,
  allowEdit: false
  }




  constructor(public dialogRef: MatBottomSheetRef<ProviderProfileMobile> , @Inject(MAT_BOTTOM_SHEET_DATA) public data:any,
  private camera : Camera,private sanitizer: DomSanitizer,private changeDetectorRef: ChangeDetectorRef,
  private chooser: Chooser) {

   }
  ngOnInit(): void {


  }


takePhoto() {
  this.camera.getPicture(this.cameraOptions).then((imgData) => {
this.onArticlePictureCreated(imgData);
    }, (err) => {
    console.log(err);
    })
   }


   openGallery() {
    this.camera.getPicture(this.gelleryOptions).then((imgData) => {
    this.onArticlePictureCreated(imgData);
  
     }, (err) => {
     console.log(err);
     })
    }


    openFiles(){
      this.chooser.getFile("image/png,image/jpeg")
      .then((file:ChooserResult) => {
      this.fileObj =file.dataURI; 
       this.imageShowFlag=true;
       this.userImage = this.fileObj;
       this.changeDetectorRef.detectChanges();
      })
   .catch((error: any) => console.error(error));
 
 
     }
 
  
    onArticlePictureCreated(imageData: string){
      let base64Img = 'data:image/jpeg;base64,' + imageData;
      this.imageShowFlag=true;
      this.userImage = base64Img;
      this.changeDetectorRef.detectChanges();
      this.safeImg = this.sanitizer.bypassSecurityTrustUrl(base64Img);
    }

    savePhoto(){
      this.dialogRef.dismiss({type:'image',userImgDisplay:this.safeImg,userImageRead:this.userImage});
    }

}
