import { Component, OnInit } from '@angular/core';
import { CommmanService } from 'src/app/_services/commman.service';

@Component({
  selector: 'app-provider-fitbit-dashboard',
  templateUrl: './provider-fitbit-dashboard.component.html',
  styleUrls: ['./provider-fitbit-dashboard.component.css']
})
export class ProviderFitbitDashboardComponent implements OnInit {

 
  users: any = [];
  user_id: any;
  constructor(
    private commanService: CommmanService,
  ) { }

  ngOnInit(): void {
    this.getAllPatient('Fitbit');
  }
    // All Patient
    getAllPatient(type){
      this.commanService.getAllPatient(type)
      .subscribe((result: any) => {
        if(result.success){
          this.users = result.users;
        }
      })
    }
    selectedUser(id){
      if(id != undefined){
        this.user_id = id;
      }else{
        this.user_id = null;
      }
    }

}
