import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminService } from 'src/app/_services/admin.service';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  action = null
  account_status_color = 'primary';
  account_status_disabled = true;

  userRecords = null;
  noRecordFound = 'Loading...';
  filterValue: string;
  
  displayedColumnsUsers: string[] = ['index', 'name', 'fullname', 'username', 'email', 'phone_number', 'idproof', 'MFA', 'active'];
  dataSourceUsers;
  userdata = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  
  constructor(
    private adminService: AdminService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,

  ) { }

  ngOnInit(): void {
    this.getUsers()
  }
  applyFilter(event: Event){
    this.filterValue = (event.target as HTMLInputElement).value;
    this.dataSourceUsers.filter = this.filterValue.trim().toLowerCase();
  }
  getUsers(){
    this.adminService.getAllUsers().subscribe((result: any) => {
      if(result.success){
        this.userdata = result.data;
        this.dataSourceUsers = new MatTableDataSource(result.data);
        this.dataSourceUsers.paginator = this.paginator;
        this.dataSourceUsers.sort = this.sort;
      }else{
        this.noRecordFound = result.message;
      }
    })
  } 
  viewIDProof(file){
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      minHeight: 'calc(25vh - 25px)',
      height : 'auto',
      width: '550px',
      data: {filename: file}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onChangeMFA(id, e) {
    this.adminService.applyMFA(id, e.checked)
    .subscribe((result: any) => {
      if(result.success){
        this.getUsers();
      }else{
        this._snackBar.open(result.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    })
  }
}


