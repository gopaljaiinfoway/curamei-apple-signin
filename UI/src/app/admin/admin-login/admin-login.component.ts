import { AfterViewInit, Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import jwt_decode from 'jwt-decode'
import { CookieService } from 'ngx-cookie-service'
import { CommmanService } from '../../_services/commman.service'
import * as CryptoJS from 'crypto-js';
import { MatSnackBar } from '@angular/material/snack-bar'
import { NgxSpinnerService } from 'ngx-spinner'

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

 
  hide = true
  userData: any
  serverErrorMessages: any
  public loginForm: FormGroup

  capchaFlag = false;
  errorStatus = false
  errorMessage = ''

  signinFlag: boolean = true;
  OTPsigninFlag: boolean = false;
  action = null

  appVerifier: any = '';
  loginResult: any;

  goFlag : boolean = false;
  constructor (
    private formBuilder: FormBuilder,
    private commanService: CommmanService,
    private cookieService: CookieService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private spinner: NgxSpinnerService,

  ) {

  }

  ngOnInit(): void {

    this.loginForm = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    })
  }

  onSubmit() {
    this.commanService.login(this.loginForm.value).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          // Handle login response
          this.handleLoginResponse(resmsg);
        } else {
          this.errorStatus = true
          this.errorMessage = resmsg.message
        }
      },
      (err) => {
        this.serverErrorMessages = err.error.message
      }
    )
  }
  
  handleLoginResponse(resmsg){
    if (resmsg.data.userTypeId === 4) {
      var expire = new Date()
      var time = Date.now() + 3600 * 1000 * 6 // current time + 6 hours ///
      expire.setTime(time)
      this.cookieService.set('admintoken', resmsg.data.token, expire)
      this.router.navigateByUrl('/admin/dashboard')
    }else{
      this._snackBar.open('Login Error', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
    }
  }
  
  forgotPassword(){
    this.router.navigate(['forgot-password'])
  }

}
