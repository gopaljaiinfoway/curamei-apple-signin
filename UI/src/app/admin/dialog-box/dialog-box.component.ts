import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminAuthGuard } from 'src/app/_guard/activate.guard';
import { AdminService } from 'src/app/_services/admin.service';
import { FileSaver } from 'file-saver';
import { saveAs } from 'file-saver';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.css']
})
export class DialogBoxComponent implements OnInit {

  viewflag: boolean = false;
  basePath=null;
  //src = 'https://storage.cloud.google.com/curameidatasource/ID-proof/';
    
  src: any;
  filename:any;
  constructor(
    private adminService: AdminService,
    public dialogRef: MatDialogRef<DialogBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.basePath = environment.apiBaseUrl;
     }

  ngOnInit(): void {
    this.filename = this.data.filename;
    this.viewFlagFunction();
 
  }
  viewFlagFunction(){
    setTimeout(function(){  this.viewflag = true}, 2000);
  }

 

  onNoClick(): void {
    this.dialogRef.close();
  }
  // viewIDProof(filename){
  //   this.src = this.src +filename+'?authuser=1';
  //   this.viewflag = true
  // }
  downloadFile(){
    console.log(this.src)
    // saveAs(this.src, "image.jpg");
  }

}
