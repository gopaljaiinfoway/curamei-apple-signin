import { AfterViewInit, Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import jwt_decode from 'jwt-decode'
import { CookieService } from 'ngx-cookie-service'
import { CommmanService } from '../../_services/commman.service';
import { UserLoginedService} from '../../_services/user-logined.service';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from "firebase/app";
// Add the Firebase services that you want to use
import "firebase/auth";
import "firebase/firestore";
import { environment } from '../../../environments/environment';
import * as CryptoJS from 'crypto-js';
import { MatSnackBar } from '@angular/material/snack-bar'
import { NgxSpinnerService } from 'ngx-spinner'
import { globalProviderSelect } from 'src/app/_services/globalProviderSelect.service';
import { Plugins } from '@capacitor/core';

const { Device, SignInWithApple } = Plugins;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, AfterViewInit {
  
  hide = true
  buttonText = 'Sign In'
  userData: any
  serverErrorMessages: any
  public loginForm: FormGroup
  public forgotForm: FormGroup
  public OTPForm: FormGroup

  capchaFlag = false;
  errorStatus = false
  errorMessage = ''

  signinFlag: boolean = true;
  OTPsigninFlag: boolean = false;
  action = null

  appVerifier: any = '';
  loginResult: any;

  goFlag : boolean = false;
  checked: boolean = false;
  loadingFlag:boolean = false;
  loggedSeviceCallFlag;
  showAppleSignIn = true;
  constructor (
    private formBuilder: FormBuilder,
    private commanService: CommmanService,
    private cookieService: CookieService,
    private router: Router,
    public afAuth: AngularFireAuth,
    private _snackBar: MatSnackBar,
    private spinner: NgxSpinnerService,
    public userLoginedService:UserLoginedService,
    private _globalProviderSelect: globalProviderSelect

  ) {
    this._globalProviderSelect.getLoggedFlag().subscribe(n => {
      this.loggedSeviceCallFlag = n;    
    });
  }

  async ngOnInit() {
    const device = await Device.getInfo();
   // this.showAppleSignIn = device.platform === 'ios';
    if (!firebase.apps.length) {
      firebase.initializeApp(environment.firebase);
   }else {
      // firebase.app(); // if already initialized,
   }

    this.loginForm = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
      remeberMe:[false]
    })

    this.OTPForm = this.formBuilder.group({
      OTP: [null, Validators.required],
    })
  }
  ngAfterViewInit(){
    
    this.appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'normal',
      'callback':(response)=> {
        if(response){
          this.goFlag = true;
        }
      }
    });
    this.appVerifier.render();
    if(!this.loggedSeviceCallFlag)
       this.router.navigateByUrl('/home')
  }
  onSubmit() {
    this.buttonText = 'Sending...'
    this.commanService.login(this.loginForm.value).subscribe(
      (resmsg: any) => {
        this.buttonText = 'Sign In'
        if (resmsg.success == true) {
          // Handle login response
          if(!resmsg.mfa_required){
          this.handleLoginResponse(resmsg);
          }else{
            this.OTPsigninFlag = true;
            this.signinFlag = false;
            this.loginResult = resmsg;
            this.sendOTP(this.loginResult.phone);
          }
        } else {
          this.errorStatus = true
          this.errorMessage = resmsg.message
          this.buttonText = 'Sign In'
        }
      },
      (err) => {
        this.buttonText = 'Sign In'
        this.serverErrorMessages = err.error.message
      }
    )
  }
  sendOTP(phone){    
    firebase.auth().signInWithPhoneNumber(phone, this.appVerifier).then((confirmationResult) => {
      this.appVerifier.confirmationResult = confirmationResult;
    }).catch(err => {
      console.log('============Send OTP Error=========');
      console.log(err.message)
      this._snackBar.open(err.message, this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
    });
  }
  resendOTP(){
    this.sendOTP(this.loginResult.phone);
    // this.spinner.show();
    // firebase.auth().signInWithPhoneNumber(this.loginResult.phone, this.appVerifier).then((confirmationResult) => {
    //   this.appVerifier.confirmationResult = confirmationResult;
    //   this.spinner.hide();
    // }).catch(err => {
    // this.spinner.hide();
    // this._snackBar.open(err.message, this.action, {
    //   duration: 3000,
    //   verticalPosition: 'top', // 'top' | 'bottom'
    //   panelClass: ['red-snackbar'],
    // });
    // });
  }
  verifyOTP(){
    try{
      var OTP  = this.OTPForm.value.OTP;
    this.appVerifier.confirmationResult.confirm(OTP)
    .then((userCredential)=> {
      if(this.loginResult.phone == userCredential.user.phoneNumber){
        this.handleLoginResponse(this.loginResult);
      }else{
        this._snackBar.open('Login Error', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    }).catch((err)=>{
      this._snackBar.open(err.message, this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
    })
    }catch(err){
      this._snackBar.open(err.message, this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
    }
  }
  handleLoginResponse(resmsg){
    var expire = new Date()
    var time = Date.now() + 3600 * 1000 * 84 // current time + 7 days ///
    expire.setTime(time)
    
    if(resmsg.data.userTypeId === 4){
      this.cookieService.set('admintoken', resmsg.data.token, expire);
    }else{
      this.cookieService.set('token', resmsg.data.token, expire);
     // const remeberMe =  this.loginForm.controls['remeberMe'].value;
      this.userData = {
        fullName: resmsg.data.fullName,
        last_login: resmsg.data.last_login,
        oneup_user_id: resmsg.data.oneup_user_id,
        remeberMe:this.checked
      }
      localStorage.setItem('user', JSON.stringify(this.userData));
    }
    if (resmsg.data.userTypeId === 1) {
      this.router.navigateByUrl('/provider/dashboard')
    } else if (resmsg.data.userTypeId === 2) {
      this.router.navigateByUrl('/patient/dashboard')
      // if (resmsg.data.enrollment_flg) {
      //   this.router.navigateByUrl('/patient/dashboard')
      // } else {
      //   this.router.navigateByUrl('/patient/enrollment')
      // }
    } else if (resmsg.data.userTypeId === 3) {
      this.router.navigateByUrl('/support/dashboard')
    } 
    else if (resmsg.data.userTypeId === 4) {
      this.router.navigateByUrl('/admin/dashboard')
    }
  }
  companyNameClick() {
    let tokenGet = this.cookieService.get('token')
    if (tokenGet && tokenGet != null) {
      let decoded = jwt_decode(tokenGet, 'llp')
      if (decoded.userTypeId === 1) {
        this.router.navigateByUrl('/provider/dashboard')
      } else if (decoded.userTypeId === 2) {
        if (decoded.enrollment_flg) {
          this.router.navigateByUrl('/patient/dashboard')
        } else {
          this.router.navigateByUrl('/patient/enrollment')
        }
        
      } else if (decoded.userTypeId === 4) {
        this.router.navigateByUrl('/admin/dashboard')
      }
      else if (decoded.userTypeId === 3) {
        this.router.navigateByUrl('/support/dashboard')
      }
    } else {
      this.router.navigateByUrl('/signin')
    }
  }
  signInWithGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider()
    return this.socialSignIn(provider);
  }
  private socialSignIn(provider) {
    return this.afAuth.signInWithPopup(provider)
      .then(async result => {
        const user = result.user;
        var name = user.displayName.split(" ");
        var data = {
          uid: user.uid,
          first_name: name[0],
          last_name: name[1],
          fullname: user.displayName,
          email: user.email,
          phone: user.phoneNumber,
          photoURL: user.photoURL
        }
        this.updateUserData(data)
      })
      .catch(error => {
        console.log(error)
      });
  }
  updateUserData(data){
    this.commanService.socialLogin(data)
    .subscribe((resmsg: any) => {
      if(resmsg.success){
        if(resmsg.requiredProfile){
          var obj = {
            first_name:resmsg.data.first_name,
            last_name:resmsg.data.last_name,
            email:resmsg.data.email,
          }
          // Encrypt obj
          var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(obj), 'secret_key_curamei_123').toString();
          sessionStorage.setItem("userData", ciphertext)
          this.router.navigate(['/complete-profile']);
        }else{
          this.handleLoginResponse(resmsg)
        }
      }else{
        this.errorStatus = true
        this.errorMessage = resmsg.message
      }
    })
  }
  forgotPassword(){
    this.router.navigate(['forgot-password'])
  }
  changeValue(value) {
    this.checked = !value;
  }


  checkUserLoginedFlag(){
    var user = JSON.parse(localStorage.getItem('user'));
       if(user && user.remeberMe){
        this.commanService.userLogined().subscribe(
          (resmsg: any) => {
            
            if(resmsg.success){
              this.checked=true;
              this.handleLoginResponse(resmsg);
            }else{
              this.loadingFlag=false;
            }
          },(err) => {
            console.log("===============err===========");
            console.log(err);
            this.loadingFlag=false;
          });
       }else{
        this.loadingFlag=false;
       }
   }

   openAppleSignIn(){
    SignInWithApple.Authorize().then(async res => {
      console.log("====================Get data from Apple signin step1 start  ================");
      console.log(res.response);
       console.log("====================Get data from Apple signin step1 end================");
      if (res.response && res.response.identityToken) {
        console.log("====================Get data from Apple signin step2 start  ================");
        console.log(res.response);
       console.log("====================Get data from Apple signin step2 end================");
        var data = {
          identityToken:res.response.identityToken,
          authorizationCode: res.response.authorizationCode,
          uid: res.response.user,
          first_name:res.response.familyName,
          last_name: res.response.givenName,
          fullname: res.response.familyName + " " + res.response.givenName,
          email: res.response.givenName,
          phone:'',
          photoURL:''
        }
        this.updateUserData(data);
         
      } else {
        this._snackBar.open(res.response, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    }).catch(response => {
      this._snackBar.open(response, this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
    });
   }


}
