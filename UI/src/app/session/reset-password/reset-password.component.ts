import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { CommmanService } from '../../_services/commman.service'
import { CustomValidators } from 'ng2-validation'
import { MatSnackBar } from '@angular/material/snack-bar'

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  hide = true
  serverErrorMessages: any
  public resetForm: FormGroup
  public forgotForm: FormGroup

  key: any;

  rightFlag: boolean = false;
  wrongFlag: boolean = false;

  action = null


  constructor (
    private formBuilder: FormBuilder,
    private commanService: CommmanService,
    private router: Router,
    private _snackBar: MatSnackBar,

  ) {
  }

  ngOnInit(): void {
    const ext = this.router.url.split('key=')[1];
    this.key = ext;
    this.validateResetPassword(ext)
    let pass = new FormControl('', Validators.required)
    let cpass = new FormControl('', CustomValidators.equalTo(pass))
    
    this.resetForm = this.formBuilder.group({
      password: pass,
      repassword: cpass,
    })

  }
  validateResetPassword(key){
    this.commanService.validateResetPasswordKey(key).subscribe((result: any) => {
      if(result.success){
        this.rightFlag = true;
      }else{
        this.wrongFlag = true;
      }
    },(err: any) => {

    })
  }
  onSubmit() {
    if(!this.resetForm.invalid){
      this.commanService.resetPassword(this.resetForm.value, this.key).subscribe((result: any) => {
        if(result.success){
          this.router.navigate(['password-changed'])
        }else{
          this._snackBar.open(result.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      })
    }
  }


}
