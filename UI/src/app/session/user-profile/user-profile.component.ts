import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as jwtDecode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../environments/environment';
import { CommmanService } from '../../_services/commman.service';
import { Router } from '@angular/router';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {UserProfileMobile } from '../../session/user-profile-mobile/user-profile-mobile.component';

// Date of birth should not be for person younger than specified age
export function minimumAge(minAge: number): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    var today = new Date();
    var birthDate = new Date(control.value);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return (age < 18) ? { minimumAge: {value: control.value} } : null;
  };
}

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit {
  userobj: any;
  userName: any;
  userEmail: any;
  userData: [] = [];
  userProfileForm: FormGroup;
  gender = [{ name: 'Male' }, { name: 'Female' }, { name: 'Others' }];
  usaStateList: Object;
  user_fn: any;
  action: any = '';
  updateBtn = 'Update';
  previewFlag = false;

  fileData: File = null;
  previewUrl: any = "https://via.placeholder.com/100x120";
  uploadedFilePath: string = null;
  imagePath: any;
  basePath: any;
  usr_id: any;
  userTypeId:any;
  systemId:boolean=false;
  imageUploadBtn='Upload';
  item:any;
  providerEmailShow:boolean=false;
  errorMessageServer='';
  constructor (
    private commanService: CommmanService,
    private cookieService: CookieService,
    private _snackBar: MatSnackBar,
    private http: HttpClient,
    private router: Router,
    private _bottomSheet: MatBottomSheet,
  ) {
    var decoded = jwtDecode(this.cookieService.get('token'));
    this.usr_id = decoded.id;
    this.userTypeId = decoded.userTypeId;
    this.basePath = environment.apiBaseUrl;
    this.userProfileForm = new FormGroup({
      first_name: new FormControl('',Validators.required),
      last_name: new FormControl('',Validators.required),
      dob: new FormControl('',[Validators.required, minimumAge(18)]),
      username: new FormControl('',Validators.required),
      id: new FormControl('',Validators.required),
      imageUrl: new FormControl(''),
      email: new FormControl('',[Validators.required,Validators.email]),
      pemail: new FormControl(''),
      phone_number: new FormControl('',Validators.required),
      address1: new FormControl('',[Validators.required]),
      address2: new FormControl(''),
      city: new FormControl('', [Validators.required]),
      state_id: new FormControl('',[Validators.required]),
      zipcode: new FormControl('',[Validators.required,  Validators.maxLength(5),
        Validators.minLength(5),])
    });
  }

  ngOnInit(): void {
    this.getUserData();

    this.commanService
      .getUsaStateList()
      .subscribe((data) => (this.usaStateList = data));
  }

  addUser() {
    this.updateBtn = 'Loading...';
    // this.userProfileForm.controls.imageUrl = this.imagePath;
    console.log(this.userProfileForm.value)
    this.commanService.updateUserProfile(this.userProfileForm.value).subscribe(
      (res: any) => {
        console.log(res.success);
        this.updateBtn = 'Update';
        if (res.success) {
          this._snackBar.open(res.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            // horizontalPosition: 'end', //'start' | 'center' | 'end' | 'left' | 'right'
            panelClass: ['green-snackbar'],
          });
        }
      },
      (err) => {
        console.log('Profile Not Added');
      }
    );
  }



  getUserData() {
    this.commanService.getUserProfileList(this.userTypeId).subscribe(
      (res: any) => {
        console.log(res.data);
        if (res.success) {
            if(this.userTypeId==3){
              this.providerEmailShow=true;
            }
            res.data.state_id = res.data.state;
          this.userProfileForm.patchValue(res.data);
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  fileProgress(fileInput: any) {
    this.previewFlag = true;
    this.fileData = <File>fileInput.target.files[0];
    this.preview();

  }

  preview() {
    // Show preview
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    }
  }

  upload() {
    const formData: any = new FormData();
    const file = this.fileData;
    if (file == null) {
      this._snackBar.open('Please select photo', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        // horizontalPosition: 'end', //'start' | 'center' | 'end' | 'left' | 'right'
        panelClass: ['red-snackbar'],
      });
    } else {
      formData.append("upload", file);
    }
    this.imageUploadBtn = 'Uploading...';
    this.commanService.uploadProfileImage(formData).subscribe((result: any) => {
      if (result.success) {
        this.imageUploadBtn='Upload';
        // this.router.navigate(['/patient/patient-profile']);
        this._snackBar.open('File Uploaded successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          // horizontalPosition: 'end', //'start' | 'center' | 'end' | 'left' | 'right'
          panelClass: ['green-snackbar'],
        });
        
      }
      else {
        this.imageUploadBtn='Upload';
        console.log('File Not Uploaded ')
      }
    })
  }




// mobile functionality code here


previewImageFromMobile(imageUrl){
  this.previewFlag = true;
  this.previewUrl =imageUrl;

}



DataURIToBlob(dataURI: string) {
  const splitDataURI = dataURI.split(',')
  const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
  const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

  const ia = new Uint8Array(byteString.length)
  for (let i = 0; i < byteString.length; i++)
      ia[i] = byteString.charCodeAt(i)

  return new Blob([ia], { type: mimeString })
}



async uploadImageFromMobile() {
 const file = await this.DataURIToBlob(this.previewUrl)
  const formData: any = new FormData();
  if (file == null) {
    this._snackBar.open('Please select photo', this.action, {
      duration: 3000,
      verticalPosition: 'top', // 'top' | 'bottom'
      // horizontalPosition: 'end', //'start' | 'center' | 'end' | 'left' | 'right'
      panelClass: ['red-snackbar'],
    });
  } else {
      formData.append("upload", file);
      this.imageUploadBtn = 'Uploading...';
    this.commanService.uploadProfileImage(formData).subscribe((result: any) => {
      if (result.success) {
        this.imageUploadBtn='Upload';
        // this.router.navigate(['/patient/patient-profile']);
        this._snackBar.open('File Uploaded successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          // horizontalPosition: 'end', //'start' | 'center' | 'end' | 'left' | 'right'
          panelClass: ['green-snackbar'],
        });
        
      }
      else {
        this.imageUploadBtn='Upload';
        console.log('File Not Uploaded ');  
        this.errorMessageServer = result.message;
         
      }
    })
  }
  


}

// async uploadImageFromMobile1() {
//   fetch(this.previewUrl).then(res => res.blob()).then(blob => {
//     const file = new File([blob], "filename.jpeg");
//   const formData: any = new FormData();
//   alert(file);
//   if (file == null) {
//     this._snackBar.open('Please select photo', this.action, {
//       duration: 3000,
//       verticalPosition: 'top', 
//       panelClass: ['red-snackbar'],
//     });
//   } else {
//       formData.append("upload", file);
//       this.imageUploadBtn = 'Uploading...';
//     this.commanService.uploadProfileImage(formData).subscribe((result: any) => {
//       if (result.success) {
//         this.imageUploadBtn='Upload';
//         this._snackBar.open('File Uploaded successfully', this.action, {
//           duration: 3000,
//           verticalPosition: 'top', 
//           panelClass: ['green-snackbar'],
//         });
        
//       }
//       else {
//         this.imageUploadBtn='Upload';
//         console.log('File Not Uploaded ');  
//         this.errorMessageServer = result.message;
         
//       }
//     })
//   }
  
// })

// }

openBottomSheet(objData): void {
  const bottomSheetRef =this._bottomSheet.open(UserProfileMobile,{
    data: { dataValue:objData }
  });


  bottomSheetRef.afterDismissed().subscribe((result:any) => {
       if(result.type=='image')
      this.previewImageFromMobile(result.userImageRead);
  });

}




}
