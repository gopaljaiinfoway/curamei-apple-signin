import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileMobile } from './user-profile-mobile.component';

describe('SharePatientResourceMobile', () => {
  let component: UserProfileMobile;
  let fixture: ComponentFixture<UserProfileMobile>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileMobile ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileMobile);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
