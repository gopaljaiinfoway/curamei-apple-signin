import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderIdproofMobile } from './provider-idproof-mobile.component';

describe('ProviderIdproofMobile', () => {
  let component: ProviderIdproofMobile;
  let fixture: ComponentFixture<ProviderIdproofMobile>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderIdproofMobile ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderIdproofMobile);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
