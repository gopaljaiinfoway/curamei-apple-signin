
import { Component, OnInit , Inject,  ChangeDetectorRef,ChangeDetectionStrategy} from '@angular/core'
import {MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Chooser, ChooserResult } from '@ionic-native/chooser/ngx';
@Component({
  selector: 'app-provider-idproof-mobile',
  templateUrl: './provider-idproof-mobile.component.html',
  styleUrls: ['./provider-idproof-mobile.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  
})

export class ProviderIdproofMobile implements OnInit {
 dataObj:any;
 uploadFileName:any;
 userImage;
 fileObj:any;
 npiNoGet:any;
 imageShowFlag:boolean=false;
 fileShowFlag:boolean=false;
 saveButtonShowFlag:boolean=false;
 
 ext:any;
 cameraOptions: CameraOptions = {
  quality: 50,
  targetWidth: 800,
  targetHeight: 600,
  destinationType: this.camera.DestinationType.DATA_URL,
  encodingType: this.camera.EncodingType.JPEG,
  mediaType: this.camera.MediaType.PICTURE,
  allowEdit: false
 }

 gelleryOptions: CameraOptions = {
  quality: 50,
targetWidth: 800,
targetHeight: 600,
  sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  destinationType: this.camera.DestinationType.DATA_URL,
  allowEdit: false
  }




  constructor(public dialogRef: MatBottomSheetRef<ProviderIdproofMobile> , @Inject(MAT_BOTTOM_SHEET_DATA) public data:any,
  private camera : Camera,private changeDetectorRef: ChangeDetectorRef,
  private chooser: Chooser) {

   }
  ngOnInit(): void {
    this.npiNoGet = this.data.npiNoGet;
  }



takePhoto() {
  this.camera.getPicture(this.cameraOptions).then((imgData) => {
this.onArticlePictureCreated(imgData);
    }, (err) => {
    console.log(err);
    })
   }


   openGallery() {
    this.camera.getPicture(this.gelleryOptions).then((imgData) => {
    this.onArticlePictureCreated(imgData);
  
     }, (err) => {
     console.log(err);
     })
    }





      openFiles(){
      //this.chooser.getFile("image/png,image/jpeg,application/pdf")
      this.chooser.getFile()
      .then(async(file:ChooserResult) => {
       this.fileObj =file.dataURI; 
       await this.fileNameGenerate(this.fileObj);
       this.saveButtonShowFlag =true;
       this.userImage = this.fileObj;
       this.changeDetectorRef.detectChanges();
      })
   .catch((error: any) => console.error(error));
 
 
     }



     async onArticlePictureCreated(imageData: string){
     
      let base64Img = 'data:image/jpeg;base64,' + imageData;
      await this.fileNameGenerate(base64Img);
      this.saveButtonShowFlag =true;
      this.userImage = base64Img;
      this.changeDetectorRef.detectChanges();
    }



     async fileNameGenerate(uploadType){
      let fileNameRand = await this.getRandomString(15);
      this.ext = 'jpeg';
      this.ext =uploadType.match(/[^:/]\w+(?=;|,)/)[0];
      this.ext = this.ext.toLowerCase();

      if(this.ext == 'png' || this.ext == 'jpg' || this.ext == 'jpeg'){
        this.imageShowFlag=true;
        this.fileShowFlag = false;
      }
      else{
        this.imageShowFlag=false;
        this.fileShowFlag = true;
      }

      this.uploadFileName = `${fileNameRand}.${this.ext}`;
     
    }


  getRandomString(length) {
      var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var result = '';
      for ( var i = 0; i < length; i++ ) {
          result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
      }
      return result;
  }
  


    savePhoto(){
      this.dialogRef.dismiss({type:'image',userImageRead:this.userImage,uploadFileName:this.uploadFileName,ext:this.ext});
    }

}
