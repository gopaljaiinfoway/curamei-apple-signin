import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { CommmanService } from '../../_services/commman.service'
import { MatSnackBar } from '@angular/material/snack-bar'

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

 
  public forgotForm: FormGroup
  buttonText='Submit';
  serverErrorMessages='';
  notValid=false;
  constructor (
    private formBuilder: FormBuilder,
    private commanService: CommmanService,
    private router: Router,
    private _snackBar: MatSnackBar,

  ) {

  }
  ngOnInit(): void {
    this.forgotForm = this.formBuilder.group({
      username: [null, Validators.required],
    })

  }
  forgotPasswordClicked(){
    if(!this.forgotForm.invalid){
      this.buttonText='Loading...';
      this.serverErrorMessages='';
      this.notValid=false;
      this.commanService.forgotPassword(this.forgotForm.value).subscribe((result: any) => {
        this.buttonText = 'Submit';
          if(result.success)
            this.router.navigate(['account-confirmed'])
           else{
            this.notValid=true;
            this.serverErrorMessages = result.message;
           }      
      },
      (err) => {
        this.buttonText = 'Submit'
        this.notValid=true;
        this.serverErrorMessages = err.error.message;
      }
      
      )
    }
  }
}
