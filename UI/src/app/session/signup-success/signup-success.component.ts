import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

@Component({
  selector: 'app-signup-success',
  templateUrl: './signup-success.component.html',
  styleUrls: ['./signup-success.component.css']
})
export class SignupSuccessComponent implements OnInit {
  buttonText="Sign In";

  signupFlag: boolean = false;
  forgotPasswordFlag: boolean = false;
  passwordChangedFlag: boolean = false;

  constructor(private router: Router){}
  ngOnInit(): void {
    if(this.router.url == '/signup-success'){
      this.signupFlag = true;
    }else if(this.router.url == '/account-confirmed'){
      this.forgotPasswordFlag = true;
    }else if(this.router.url == '/password-changed'){
      this.passwordChangedFlag = true;
    }
  }

}
