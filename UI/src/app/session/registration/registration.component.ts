import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators, ValidatorFn } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import jwt_decode from 'jwt-decode'
import { CustomValidators } from 'ng2-validation'
import { CookieService } from 'ngx-cookie-service'
import { CommmanService } from '../../_services/commman.service';
import { MatSnackBar } from '@angular/material/snack-bar'
import * as CryptoJS from 'crypto-js';
import { JsonPipe } from '@angular/common';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import { ProviderIdproofMobile} from '../../session/provider-idproof-mobile/provider-idproof-mobile.component';

// Date of birth should not be for person younger than specified age
export function minimumAge(minAge: number): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    var today = new Date();
    var birthDate = new Date(control.value);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return (age < 18) ? { minimumAge: {value: control.value} } : null;
  };
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {

  @ViewChild('fileUpload')
  fileUpload: ElementRef
  
  hide = true
  hideTwo = true
  buttonText = 'Sign Up'
  eEmail = false
  pEmailFlag = false
  eContact = false
  eNpi = false
  result: any
  results: any
  npidata: any
  npidetails: any
  firstname: any
  lastname: any
  birth: any
  npinumber: any
  public signUpForm: FormGroup
  serverErrorMessages: any
  taxocode: any
  taxostate: any
  taxolicense: any
  showForm = false
  supportForm = false
  errorNpi: boolean = false
  selectedType = ''
  usaStateList: Object
  showStateName: boolean = false
  displayProviderForm: boolean = false
  displayPatientForm: boolean = false
  showSignUpType: boolean = true
  phonenumPattern = '^((\\+1-?)|0)?[0-9]{10}$'
  hideModal: boolean = false
  openModalSubmit: any = true
  hideErrorMsg = false
  npi: any
  apiDataError: string
  showApiError: boolean = false
  displaySupportForm: boolean = false
  eProviderEmail: boolean = false
  showSupportField: boolean = false
  errorMessageProviderEmail = 'Invalid provider email'
  npiBox: boolean
  npirecords: any
  taxorecord: any
  filterTaxoRecord: any
  npiInputCheck: boolean = true
  formStepOne: boolean
  formStepTwo: boolean
  formStepThree: boolean
  termservicevalue: boolean = false;
  trueValue: boolean = true;
  falseValue: boolean = false;
  cvalue: any;
  checked: boolean = false;
  action = null

  throughSocialLoginFlag: boolean = false;
  socialEmail: any;
  isReadOnly: boolean = false;
  uploadedFile: any;
  uploadedFileName: any;
  constructor (
    private formBuilder: FormBuilder,
    private commanService: CommmanService,
    private cookieService: CookieService,
    private router: Router,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private _snackBar: MatSnackBar,
    private _bottomSheet: MatBottomSheet,
  ) {
    let pass = new FormControl('', Validators.required)
    let cpass = new FormControl('', CustomValidators.equalTo(pass))
    this.signUpForm = new FormGroup({
      sType: new FormControl('', Validators.required),
      fname: new FormControl('', Validators.required),
      lname: new FormControl(''),
      dob: new FormControl('', minimumAge(18)),
      npi: new FormControl('', Validators.maxLength(10)),
      emailid: new FormControl('', [Validators.required, CustomValidators.email]),
      phonenumber: new FormControl('', [
        Validators.required,
      ]),
      // phoneone: new FormControl('', [
      //   Validators.required,
      //   Validators.maxLength(3),
      //   Validators.minLength(3),
      //   CustomValidators.number,
      // ]),
      // phonetwo: new FormControl('', [
      //   Validators.required,
      //   Validators.maxLength(3),
      //   Validators.minLength(3),
      //   CustomValidators.number,
      // ]),
      // phonethree: new FormControl('', [
      //   Validators.required,
      //   Validators.maxLength(4),
      //   CustomValidators.number,
      // ]),
      addressone: new FormControl(''),
      addresstwo: new FormControl(''),
      city: new FormControl(''),
      faxone: new FormControl('', [
        Validators.maxLength(3),
        Validators.minLength(3),
        CustomValidators.number,
      ]),
      faxtwo: new FormControl('', [
        Validators.maxLength(3),
        Validators.minLength(3),
        CustomValidators.number,
      ]),
      faxthree: new FormControl('', [
        Validators.maxLength(4),
        Validators.minLength(4),
        CustomValidators.number,
      ]),
      stateid: new FormControl(''),
      zipcode: new FormControl('', [
        Validators.maxLength(5),
        Validators.minLength(5),
        CustomValidators.number,
      ]),
      pemail: new FormControl('', [CustomValidators.email]),
      termsofservices: new FormControl('', [Validators.required]),
      idproof: new FormControl(''),
      pass: pass,
      cpass: cpass,

    })
  }

  ngOnInit(): void {
    console.log(this.signUpForm.controls.idproof.valid)
    this.commanService.getUsaStateList().subscribe((data) => (this.usaStateList = data))
    if(this.router.url == '/complete-profile'){
      this.throughSocialLoginFlag = true;
      var ciphertext = sessionStorage.getItem('userData');
      // Decrypt
      var userData  = JSON.parse(CryptoJS.AES.decrypt(ciphertext, 'secret_key_curamei_123').toString(CryptoJS.enc.Utf8));     
      if(userData && userData.first_name){
        this.firstname = userData.first_name;
        this.signUpForm.get('fname').setValue(userData.first_name);
      }
      if(userData && userData.last_name){
        this.lastname = userData.last_name;
        this.signUpForm.get('lname').setValue(userData.last_name);
      }
      if(userData && userData.email){
        this.selectedType = '2';
        this.signUpForm.get('emailid').setValue(userData.email)
        this.isReadOnly = true;
      }
    }
  }

  check() {
    this.termservicevalue = true;
  }

  changeValue(value) {
    this.checked = !value;
    console.log(this.checked)
  }

  onSubmit() {
    this.buttonText = 'Loading....'
    console.log(this.signUpForm.value)
    if (!this.signUpForm.invalid) {
      if(this.selectedType === '1'){
        // Upload ID-proof on drive
        const fd = new FormData();
        fd.append('ID_proof', this.uploadedFile, this.uploadedFile.name);
        fd.append('npi_no', JSON.stringify(this.signUpForm.value.npi));
        this.commanService.uploadIDProof(fd).subscribe((res: any) => {
          console.log(res)
          if(res.success){
            this.signUpForm.value.idproof= res.savedFileName;
            this.saveUser();
          }else{
            this._snackBar.open(res.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['red-snackbar'],
            });
          }
        })
      }else{
        this.saveUser();
      }    
    }else{
    }
  }
  saveUser(){
    this.commanService.userSignUp(this.signUpForm.value).subscribe(
      (res: any) => {
        if (res.success) {
          this.router.navigateByUrl('signup-success')
          this.buttonText = 'Sign Up'
        }else{
          this._snackBar.open(res.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      },
      (err) => {
        this.buttonText = 'Sign Up'
        this.serverErrorMessages = err.error.message
      }
    ) 
  }
  checkEmail(emailValue) {
    if (emailValue !== undefined) {
      if (emailValue.length > 0) {
        this.commanService.emailCheckExists(emailValue).subscribe((result: any) => {
          console.log(result)
          if (result.success == true) {
            this.eEmail = true
          } else {
            this.eEmail = false
          }
        })
      }
    }
  }

  checkContact(contactValue) {
    if (contactValue !== undefined) {
      if (contactValue.length > 0) {
        this.commanService.ContactCheckExists(contactValue).subscribe((result: any) => {
          console.log(result)
          if (result.success == true) {
            this.eContact = true
          } else {
            this.eContact = false
          }
        })
      }
    }
  }

  checkProviderEmail(providerEmailValue) {
    console.log('focusout')
    if (providerEmailValue !== undefined) {
      if (providerEmailValue.length > 0) {
        this.commanService.userExists(providerEmailValue).subscribe((result: any) => {
          console.log(result)
          if (result.success == true) {
            this.eProviderEmail = false
          } else {
            this.eProviderEmail = true;
          }
        })
      }
    }
  }

  checkNpi(npi) {
    if (npi !== undefined) {
      if (npi.length > 0) {
        this.commanService.NpiCheckExists(npi).subscribe((result: any) => {
          console.log(result)
          if (result.success == true) {
            this.eNpi = true
            this.showApiError = true
            this.apiDataError = result.message
          } else {
            // this.eNpi = false;

            this.commanService.getnpi(npi).subscribe((data) => {
              this.npidata = data
              this.eNpi = false;
              this.npiBox = true
              this.npiInputCheck = false;
              const objects = Object.keys(data).map((key) => data[key])
              this.npirecords = objects[1]
              console.log(this.npirecords.length)
              if (this.npirecords.length == 0) {
                console.log(this.npirecords)
                this.npiInputCheck = true;
                this.npiBox = false;
                this.showApiError = true
                this.apiDataError = 'Invalid NPI'
              }
              else {
                console.log('not empty')
                this.npirecords.forEach((record) => {
                  if (record.enumeration_type === 'NPI-1') {
                    console.log('******************************')
                    console.log(record)
                    this.npinumber = record.number
                    this.firstname = record.basic.first_name
                    this.lastname = record.basic.last_name
                    this.taxorecord = record.taxonomies
                    this.taxorecord.forEach((record) => {
                      if (record.primary) {
                        console.log('---------------------------------')
                        this.filterTaxoRecord = record
                        console.log(this.filterTaxoRecord)
                        console.log('---------------------------------')
                      }
                    })
                    console.log('******************************')
                  } else if (record.enumeration_type === 'NPI-2') {
                    console.log('Not a valid NPI no.')
                    this.npiInputCheck = true;
                    this.npiBox = false;
                    this.showApiError = true
                    this.apiDataError = 'Invalid NPI'
                  }
                })
              }
            })
          }
        })
      }
    }
  }

  onChange(event) {
    this.selectedType = event.target.value
    console.log(this.selectedType)
  }


  fillInfo() { }

  ngAfterViewChecked() {
    this.cdr.detectChanges()
  }



  hideForm() {
    this.showForm = false
    console.log('clicked' + this.showForm)
  }

  // displayForm() {
  //   this.showForm = true
  //   this.displayProviderForm = false
  // }

  // selectSignUpForm() {
  //   if (this.selectedType === '1') {
  //     this.displayProviderForm = true
  //     this.showSignUpType = false
  //   } else if (this.selectedType === '2') {
  //     this.displayPatientForm = true
  //     this.showSignUpType = false
  //   } else {
  //     this.displaySupportForm = true
  //     this.showSignUpType = false
  //   }
  // }







  hoverState() {
    this.showStateName = true
  }



  checkError() {
    this.eNpi = false
    this.showApiError = false
    console.log('focus in ' + this.hideErrorMsg)
  }

  gobacktoNpiBox() {
    this.npiInputCheck = true;
    this.npiBox = false;
  }

  companyNameClick() {
    var tokenGet = this.cookieService.get('token')
    if (tokenGet && tokenGet != null) {
      let decoded = jwt_decode(tokenGet, 'llp')
      if (decoded.userTypeId === 1) {
        this.router.navigateByUrl('/provider/dashboard')
      } else if (decoded.userTypeId === 2) {
        if (decoded.enrollment_flg) {
          this.router.navigateByUrl('/patient/dashboard')
        } else {
          this.router.navigateByUrl('/patient/enrollment')
        }
      } else if (decoded.userTypeId === 4) {
        this.router.navigateByUrl('/admin/dashboard')
      }
    } else {
      this.router.navigateByUrl('/signin')
    }
  }

  displayForm() {
    this.npiBox = false
    this.showForm = true

    this.formStepOne = true
    this.formStepTwo = false
    // this.displayProviderForm = false
  }

  gotoFormStepTwo() {
    this.formStepOne = false
    this.formStepTwo = true
  }

  goBackStepNpiForm() {
    this.npiInputCheck = true
    this.npiBox = false
    this.showForm = false
    this.formStepOne = false
  }

  goBacktopStepOne() {
    this.formStepOne = true
    this.formStepTwo = false
  }

  goBacktopStepTwo() {
    this.formStepTwo = true
    this.formStepThree = false
  }

  gotoFormStepThree() {
    this.formStepTwo = false
    this.formStepThree = true
  } 
  onClick(event) {
    if (this.fileUpload)
      this.fileUpload.nativeElement.click()
  }


  // onFileSelected(file) {
  //   this.uploadedFile = file.target.files[0];
  //   this.uploadedFileName = this.uploadedFile.name;
  // }


 onFileSelected(file) {
    var mimeType = file.target.files[0].type;
    const match = ['image/png','image/jpg','image/jpeg','application/pdf'];
    if (match.indexOf(mimeType) === -1) {
      this._snackBar.open("Invalid file type! Only pdf,png,jpg,jpeg supported.", this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
      this.signUpForm.get('idproof').setValue('');
       this.signUpForm.get('idproof').setValidators([Validators.required]);  
    }else{
         this.uploadedFile = file.target.files[0];
        this.uploadedFileName = this.uploadedFile.name;
   }
  }




  selectSignUpForm() {
    this.signUpForm.get('idproof').setErrors(null);
    this.signUpForm.get('idproof').clearValidators();
    if (this.selectedType === '1') {
      this.signUpForm.get('idproof').setValidators([Validators.required]);
      this.displayProviderForm = true
      this.showSignUpType = false
    } else if (this.selectedType === '2') {
      this.displayPatientForm = true
      this.showSignUpType = false
      this.formStepOne = true
    } else {
      this.displaySupportForm = true
      this.showSignUpType = false
      this.formStepOne = true
    }
  }

  selectUserForm() {
    if (this.selectedType === '1') {
      this.displayProviderForm = true
      this.showSignUpType = false
    } else if (this.selectedType === '2') {
      this.displayPatientForm = true
      this.showSignUpType = false
    } else if (this.selectedType === '3') {
      this.displaySupportForm = true
      this.showSignUpType = false
    } else {
      this.displayProviderForm = false
      this.displaySupportForm = false
      this.displaySupportForm = false
      this.showSignUpType = true
    }
  }





// mobile code



async onFileSelectedByMobile(data) {
  if(data.ext == 'png' || data.ext == 'jpg' || data.ext == 'jpeg' || data.ext == 'pdf'){
    this.uploadedFile = await this.DataURIToBlob(data.userImageRead);
    this.uploadedFileName = data.uploadFileName;
     this.signUpForm.value.idproof= data.uploadFileName;
   this.signUpForm.controls["idproof"].clearValidators();
    this.signUpForm.controls["idproof"].updateValueAndValidity();
   // this.signUpForm.value.idproof= data.uploadFileName;
  }else{
    this._snackBar.open("Invalid file type! Only pdf,png,jpg,jpeg supported.", this.action, {
      duration: 3000,
      verticalPosition: 'top', // 'top' | 'bottom'
      panelClass: ['red-snackbar'],
    });
    this.signUpForm.get('idproof').setValue('');
     this.signUpForm.get('idproof').setValidators([Validators.required]); 
  }
 
}




 DataURIToBlob(dataURI: string) {
    const splitDataURI = dataURI.split(',')
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

    const ia = new Uint8Array(byteString.length)
    for (let i = 0; i < byteString.length; i++)
        ia[i] = byteString.charCodeAt(i)

    return new Blob([ia], { type: mimeString })
  }


  openBottomSheet(objData): void {
   let npiNoGet = (this.signUpForm.value.npi);
    const bottomSheetRef =this._bottomSheet.open(ProviderIdproofMobile,{
      data: { dataValue:objData,npiNoGet:npiNoGet }
    });


    bottomSheetRef.afterDismissed().subscribe((result:any) => {
           this.onFileSelectedByMobile(result);
        
    });
  
  }




  ngOnDestroy(){
    if(sessionStorage.getItem('userData')){
      sessionStorage.removeItem('userData')
    }
 
  }
}
