import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularMaterialModule } from 'src/app/material-module';
import { CommmanService } from 'src/app/_services/commman.service';
import { RegistrationComponent } from './registration.component';


fdescribe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegistrationComponent],
      imports: [ReactiveFormsModule, FormsModule, RouterTestingModule, HttpClientModule, AngularMaterialModule],
      providers: [CommmanService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('[Component-Check] - should create Register Component', () => {
    expect(component).toBeTruthy();
  });

  it('[Stype-Check] - should check sType is valid', () => {
    let stype = component.signUpForm.controls['sType'];
    expect(stype.valid).toBeFalsy();
    expect(stype.pristine).toBeTruthy();
    expect(stype.errors['required']).toBeTruthy();
    stype.setValue(true);
  })

  it('[Select User - Check]', () => {

  })
});
