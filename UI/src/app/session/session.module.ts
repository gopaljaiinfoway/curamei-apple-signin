import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { LoginComponent } from './login/login.component';
import { PatientAccountSetupComponent } from './patient-account-setup/patient-account-setup.component';
import { SessionRoutingModule } from './session-routing.module';
import { TermsOfServicesComponent } from './terms-of-services/terms-of-services.component';
import { SocialUserProfileComponent } from './social-user-profile/social-user-profile.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

@NgModule({
  declarations: [LoginComponent, TermsOfServicesComponent, PatientAccountSetupComponent, SocialUserProfileComponent, ResetPasswordComponent, ForgotPasswordComponent],
  imports: [
    CommonModule,
    SessionRoutingModule,
    BsModalService,
    ModalModule.forRoot(),
    RouterModule,
  ],
})
export class SessionModule { }
