import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class globalProviderSelect {
    
    private dataObs$ = new BehaviorSubject(null);
    private refreshAlert$ = new BehaviorSubject(false);
    private loggedFlag$ = new BehaviorSubject(false);

    setId(value: number) {
        this.dataObs$.next(value);
    }

    getId() {
        return this.dataObs$;
    }

    refresh() {
        this.refreshAlert$.next(true);
    }

    getRefresh() {
        return this.refreshAlert$;
    }

    resetRefresh() {
        this.refreshAlert$.next(false);
    }


    setLoggedFlag(value:boolean){
        this.loggedFlag$.next(value);   
     }
     getLoggedFlag(){
       return this.loggedFlag$;   
     }

}