import { Injectable } from '@angular/core';
import { CommmanService } from './commman.service';
import { CookieService } from 'ngx-cookie-service'
import { Router, NavigationEnd } from '@angular/router'; 
@Injectable({
  providedIn: 'root'
})
export class UserLoginedService {
  userData: any
  constructor(private commanService: CommmanService,
    private cookieService: CookieService,public router: Router) { 
     // this.checkUserLoginedFlag();
    }

    handleLoginResponse(resmsg){
      var expire = new Date()
      var time = Date.now() + 3600 * 1000 * 84 // current time + 7 day ///
      expire.setTime(time)
  
      if(resmsg.data.userTypeId === 4){
        this.cookieService.set('admintoken', resmsg.data.token, expire);
      }else{
        this.cookieService.set('token', resmsg.data.token, expire);
        this.userData = {
          fullName: resmsg.data.fullName,
          last_login: resmsg.data.last_login,
          oneup_user_id: resmsg.data.oneup_user_id,
          remeberMe:true
        }
        localStorage.setItem('user', JSON.stringify(this.userData));
      }
      if (resmsg.data.userTypeId === 1) {
        this.router.navigateByUrl('/provider/dashboard')
      } else if (resmsg.data.userTypeId === 2) {
        this.router.navigateByUrl('/patient/dashboard')
      } else if (resmsg.data.userTypeId === 3) {
        this.router.navigateByUrl('/support/dashboard')
      } 
      else if (resmsg.data.userTypeId === 4) {
        this.router.navigateByUrl('/admin/dashboard')
      }
    }
  
    checkUserLoginedFlag(){
      var user = JSON.parse(localStorage.getItem('user'));
         if(user && user.remeberMe){
          this.commanService.userLogined().subscribe(
            (resmsg: any) => {
              if(resmsg.success)
              this.handleLoginResponse(resmsg);
              else
              this.router.navigateByUrl('/signin')
            },(err) => {
              this.router.navigateByUrl('/signin')
            });
         }else{
          this.router.navigateByUrl('/signin')
         }
     }
}
