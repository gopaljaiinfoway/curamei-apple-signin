import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { map } from 'rxjs/operators';
import 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { UserAuthenticationCheckService } from './user-authentication-check.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  headers: any;
  basePath: any;
  headersWithMultipart: any;
  constructor(
    private http: HttpClient,
    private userGetHeader: UserAuthenticationCheckService,
    private cookieService: CookieService
  ) {
    this.basePath = environment.apiBaseUrl;
    this.headers = this.userGetHeader.getAdminHeader();
    this.headersWithMultipart = this.userGetHeader.getHeaderWithMultipart();
  }
  // Get all users
  getAllUsers(){
    return this.http
      .request(
        'get',
        Location.joinWithSlash(`${this.basePath}`, `all-users`),
        {
          observe: 'response',
          headers: this.headers,
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
  // Apply MFA
  applyMFA(id: any, status: boolean){
    return this.http
      .request(
        'post',
        Location.joinWithSlash(`${this.basePath}`, `apply-mfa/${encodeURIComponent(id)}`),
        {
          observe: 'response',
          headers: this.headers,
          body: {mfa_status: status}
        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }

// Show Provider IdProof
  
showproviderIdProof(filename :any){
    return this.http
      .request(
        'get',
        Location.joinWithSlash(`${this.basePath}`, `show-idProof/${encodeURIComponent(filename)}`),
        {
          observe: 'response',
          headers: this.headers,
          responseType: 'blob',

        }
      )
      .pipe(
        map((response) => {
          switch (response.status) {
            case 200: {
              return response.body;
            }
          }
        })
      );
  }
}
