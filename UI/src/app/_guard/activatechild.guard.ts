import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {CommmanService} from '../_services/commman.service';

@Injectable({
  providedIn: 'root'
})
export class ActivatechildGuard implements CanActivateChild  {
  constructor(private userLog:CommmanService, private router:Router){}

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
       if (!this.userLog.isLoggedInUser()) {
        this.router.navigateByUrl('/signin');
        this.userLog.deleteToken();
        return false;
      }
    return true;
    }
}
