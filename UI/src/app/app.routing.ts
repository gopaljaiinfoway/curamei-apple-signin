import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { FitbitDashboardComponent } from './dashboard/fitbit-dashboard/fitbit-dashboard.component';
import { DataSourcesComponent } from './patient/data-sources/data-sources.component';
import { EncountersComponent } from './patient/encounters/encounters.component';
import { EnrollmentComponent } from './patient/enrollment/enrollment.component';
import { Enrollment1Component } from './patient/enrollment1/enrollment1.component';
import { Enrollment2Component } from './patient/enrollment2/enrollment2.component';
import { Enrollment3Component } from './patient/enrollment3/enrollment3.component';
import { Enrollment4Component } from './patient/enrollment4/enrollment4.component';
import { ManualMedicalRecordsComponent } from './patient/manual-medical-records/manual-medical-records.component';
import { MedicalRecordsComponent } from './patient/medical-records/medical-records.component';
import { PatientProviderComponent } from './patient/patient-provider/patient-provider.component';
import { PdashboardComponent } from './patient/pdashboard/pdashboard.component';
import { PractitionersComponent } from './patient/practitioners/practitioners.component';
import { ProviderConnectComponent } from './patient/provider-connect/provider-connect.component';
import { ProviderListComponent } from './patient/provider-list/provider-list.component';
import { RequestHealthSystemComponent } from './patient/request-health-system/request-health-system.component';
import { SelectDataSourceComponent } from './patient/select-data-source/select-data-source.component';
import { SharePatientResourceComponent } from './patient/share-patient-resource/share-patient-resource.component'

import { ProviderDashboardComponent } from './provider/provider-dashboard/provider-dashboard.component';
import { LoginComponent } from './session/login/login.component';
import { RegistrationComponent } from './session/registration/registration.component';
import { SignupSuccessComponent } from './session/signup-success/signup-success.component';
import { signupVerifyComponent } from './session/signup-verify/signup-verify.component';
import { TermsOfServicesComponent } from './session/terms-of-services/terms-of-services.component';
import { ActivatechildGuard } from './_guard/activatechild.guard';
import { AdminLayoutComponent } from './_layout/admin-layout/admin-layout.component';
import { PatientLayoutComponent } from './_layout/patient-layout/patient-layout.component';
import { ProviderLayoutComponent } from './_layout/provider-layout/provider-layout.component';
import { PatientMedicalViewComponent } from './provider/patient-medical-view/patient-medical-view.component';
import { ManageApplicationPermissionComponent } from './provider/manage-application-permission/manage-application-permission.component';
import { ProviderFitbitDashboardComponent } from './provider/provider-fitbit-dashboard/provider-fitbit-dashboard.component';
import { SupportLayoutComponent } from './_layout/support-layout/support-layout.component';
import { SupportDashboardComponent } from './support-dashboard/support-dashboard.component';
import {UserProfileComponent } from './session/user-profile/user-profile.component';
import { ProviderProfileComponent } from './provider/provider-profile/provider-profile.component';
import { ManagePatientAccessComponent } from './provider/manage-patient-access/manage-patient-access.component';
import { NarrativeComponent } from './narrative/narrative.component';
import { DigitalHealthComponent } from './patient/digital-health/digital-health.component'
import { SocialUserProfileComponent } from './session/social-user-profile/social-user-profile.component';
import { ResetPasswordComponent } from './session/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './session/forgot-password/forgot-password.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { UsersComponent } from './admin/users/users.component';
import { AdminAuthGuard } from './_guard/activate.guard';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { HomeComponent } from './home/home.component';
export const AppRoutes: Routes = [
  {
    path: 'admin-login',
    component: AdminLoginComponent,
  },
  {
    path: 'admin',
    canActivate: [AdminAuthGuard],
    component: AdminLayoutComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: AdminDashboardComponent },
      { path: 'users', component: UsersComponent}
    ],
  },
  {
    path: 'support',
    canActivateChild: [ActivatechildGuard],
    component: SupportLayoutComponent,
    children: [
      // { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: SupportDashboardComponent },
      { path: 'support-profile', component: UserProfileComponent },
    ],
  },

  {
    path: 'provider',
    canActivateChild: [ActivatechildGuard],
    component: ProviderLayoutComponent,
    children: [
      // { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: ProviderDashboardComponent },
      { path: 'patient-medical-records', component: PatientMedicalViewComponent },
      { path: 'manage-application-permission', component: ManageApplicationPermissionComponent },
      { path: 'patient-fitbit-dashboard', component: ProviderFitbitDashboardComponent },
      { path: 'fitbit-dashboard', component: FitbitDashboardComponent },
      { path: 'provider-list', component: ProviderListComponent },
      { path: 'profile', component: ProviderProfileComponent },
      { path: 'manage-patient-access', component: ManagePatientAccessComponent },
      { path: 'narrative', component: NarrativeComponent},
      { path: 'medical-records', component: MedicalRecordsComponent }, 
    ],
  },

  {
    path: 'patient',
    canActivateChild: [ActivatechildGuard],
    component: PatientLayoutComponent,
    children: [
      // { path: '', redirectTo: '/dashboard', pathMatch: 'full' },

      { path: 'dashboard', component: PdashboardComponent },
      { path: 'enrollment', component: EnrollmentComponent },
      { path: 'enrollment1', component: Enrollment1Component },
      { path: 'enrollment2', component: Enrollment2Component },
      { path: 'enrollment3', component: Enrollment3Component },
      { path: 'enrollment4', component: Enrollment4Component },
      { path: 'provider-connect', component: ProviderConnectComponent },
      { path: 'datasource', component: DataSourcesComponent },
      { path: 'datasource/success', component: DataSourcesComponent },
      
      { path: 'patient-provider', component: PatientProviderComponent },
      { path: 'provider-list', component: ProviderListComponent },
      { path: 'medical-records', component: MedicalRecordsComponent },
      { path: 'practitioners', component: PractitionersComponent },
      { path: 'encounters', component: EncountersComponent },
      { path: 'fitbit-dashboard', component: FitbitDashboardComponent },
      { path: 'request-health-system', component: RequestHealthSystemComponent },
      { path: 'select-data-source', component: SelectDataSourceComponent },
      { path: 'manual-medical-records', component: ManualMedicalRecordsComponent },
      { path: 'share-patient-resource', component: SharePatientResourceComponent },
      { path: 'access-controls', component: SharePatientResourceComponent },
      { path: 'patient-profile', component: UserProfileComponent },
      { path: 'narrative', component: NarrativeComponent},
      { path: 'digital-health', component: DigitalHealthComponent },
    ],
  },

  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'signin', component: LoginComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'reset-password', component: ResetPasswordComponent},
  { path: 'signup', component: RegistrationComponent },
  { path: 'signup-success', component: SignupSuccessComponent },
  { path: 'account-confirmed', component: SignupSuccessComponent },
  { path: 'password-changed', component: SignupSuccessComponent},
  { path: 'signup-verify', component: signupVerifyComponent },
  { path: 'terms', component: TermsOfServicesComponent },
  { path: 'complete-profile', component: RegistrationComponent},

  // { path: '**', component: LoginComponent },
];
