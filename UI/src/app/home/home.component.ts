import { Component, OnInit } from '@angular/core';
import { UserLoginedService} from '../_services/user-logined.service';
import { globalProviderSelect } from 'src/app/_services/globalProviderSelect.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public userLoginedService:UserLoginedService,private _globalProviderSelect: globalProviderSelect) {
    this._globalProviderSelect.setLoggedFlag(true);
    this.userLoginedService.checkUserLoginedFlag();
   }

  ngOnInit(): void {

  }

}
