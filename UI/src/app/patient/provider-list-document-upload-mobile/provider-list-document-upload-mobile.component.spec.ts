import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderListDocumentUploadMobile } from './provider-list-document-upload-mobile.component';

describe('ProviderListDocumentUploadMobile', () => {
  let component: ProviderListDocumentUploadMobile;
  let fixture: ComponentFixture<ProviderListDocumentUploadMobile>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderListDocumentUploadMobile ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderListDocumentUploadMobile);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
