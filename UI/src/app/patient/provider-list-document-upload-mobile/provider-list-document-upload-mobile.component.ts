
import { Component, OnInit, Inject, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Chooser, ChooserResult } from '@ionic-native/chooser/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';

@Component({
  selector: 'app-provider-list-document-upload-mobile',
  templateUrl: './provider-list-document-upload-mobile.component.html',
  styleUrls: ['./provider-list-document-upload-mobile.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,

})

export class ProviderListDocumentUploadMobile implements OnInit {
  dataObj: any;
  uploadFileName: any;
  npiNoGet: any;
  saveButtonShowFlag: boolean = false;
  fileObj:any;
  uploadFileObj=[];
  options: any;
  ext: any;
  cameraOptions: CameraOptions = {
    quality: 50,
    targetWidth: 800,
    targetHeight: 600,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    allowEdit: false
  }

  constructor(public dialogRef: MatBottomSheetRef<ProviderListDocumentUploadMobile>, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    private camera: Camera, private changeDetectorRef: ChangeDetectorRef,
    private chooser: Chooser,
    private imagePicker: ImagePicker) {

  }
  ngOnInit(): void {
    this.npiNoGet = this.data.npiNoGet;
  }



  openFiles(){
    //this.chooser.getFile("image/png,image/jpeg,application/pdf")
    this.chooser.getFile()
    .then(async(file:ChooserResult) => {
     this.fileObj =file.dataURI; 
     this.saveButtonShowFlag = true;
     await this.fileNameGenerate(this.fileObj);
    })
 .catch((error: any) => console.error(error));

   }



  takeCameraPhoto() {
    this.camera.getPicture(this.cameraOptions).then(async (imageData) => {
      await this.bae64CreateImage(imageData);
    }, (err) => {
      console.log(err);
    });
  }



  getGalleryImages() {
    this.options = {
      width: 200,
      quality: 25,
      outputType: 1,
      // maximumImagesCount: 5,
    };
    this.imagePicker.getPictures(this.options).then(async (results) => {
      for (var i = 0; i < results.length; i++) {
        await this.bae64CreateImage(results[i]);
      }
    }, (err) => {
      alert(err);
    });
  }

  async bae64CreateImage(imageData: string) {
    let base64Img = 'data:image/jpeg;base64,' + imageData;
    this.saveButtonShowFlag = true;
    await this.fileNameGenerate(base64Img);

  }

  async fileNameGenerate(uploadType) {
    let fileNameRand = await this.getRandomString(15);
    fileNameRand = "pat-doc-" + fileNameRand;
    this.ext = uploadType.match(/[^:/]\w+(?=;|,)/)[0];
    this.ext = this.ext.toLowerCase();
    this.uploadFileName = `${fileNameRand}.${this.ext}`;
    if (this.ext == 'png' || this.ext == 'jpg' || this.ext == 'jpeg') {
      this.uploadFileObj.push({
        type:'image',
        uploadType:uploadType,
        fileName:this.uploadFileName,
        ext:this.ext
      });
    }
    else {
      this.uploadFileObj.push({
        type:'file',
        uploadType:uploadType,
        fileName:this.uploadFileName,
        ext:this.ext
      });
    }
    this.uploadFileObj.reverse();
    this.changeDetectorRef.detectChanges();


  }


  getRandomString(length) {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var result = '';
    for (var i = 0; i < length; i++) {
      result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
  }

  deletePhoto(index){
    this.uploadFileObj.splice(index, 1);
    if(this.uploadFileObj.length == 0)
    this.saveButtonShowFlag = false;
 }

  savePhoto() {
    this.dialogRef.dismiss({uploadFileObj: this.uploadFileObj});
  }





}
