import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Enrollment2Component } from './enrollment2.component';

describe('Enrollment2Component', () => {
  let component: Enrollment2Component;
  let fixture: ComponentFixture<Enrollment2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Enrollment2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Enrollment2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
