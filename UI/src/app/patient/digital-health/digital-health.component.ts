import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog,MatDialogRef} from '@angular/material/dialog';
import { CommmanService } from 'src/app/_services/commman.service';
import { DigitalHealthDetailsComponent } from '../digital-health-details/digital-health-details.component';
import { MatSnackBar } from '@angular/material/snack-bar'
@Component({
  selector: 'app-digital-health',
  templateUrl: './digital-health.component.html',
  styleUrls: ['./digital-health.component.css']
})
export class DigitalHealthComponent implements OnInit {
 fitbitConfigurationFlag : boolean=false;
 noProduct:string='Loading...';
 fitbit__url = null;
 action:null;
  constructor(private commanService: CommmanService,
    private router: Router, public dialog: MatDialog, private _snackBar: MatSnackBar,) { }

  ngOnInit(): void {
    this.checkUserConfigured();
  }


  checkUserConfigured() {
    this.commanService.checkFitbitConfig().subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.noProduct='';
          this.fitbitConfigurationFlag=true;
          localStorage.setItem('fitbit_refresh_token', resmsg.data);
        }else{
          this.fitbitConfigurationFlag=false;
           this.fitbit__url=resmsg.url
          this.noProduct='No Digital Health Products Connected.';
          localStorage.setItem('fitbit_refresh_token', null);
        }
        
      },
      (err) => {
        this.noProduct='No Product Add.';
        console.log(err);
      }
    );
  }

  removeFitbitConfig(){
    if (confirm("Are you sure you want to remove FitBit Digital Health ? ")) {
    this.commanService.removeFitbitConfigByUser().subscribe((resmsg: any) => {
        if (resmsg.success == true) {
         this.checkUserConfigured();
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-green'],
          });
        }else{
          this._snackBar.open(resmsg.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
          this.fitbitConfigurationFlag=true;
        }
        
      },
      (err) => {
        console.log(err);
      }
    );
    } 
  }

 goToFitbitDashboard(){
  this.router.navigateByUrl('/patient/fitbit-dashboard');
 }
 openDialog() {
  const dialogRef = this.dialog.open(DigitalHealthDetailsComponent, {
    minHeight: 'calc(60vh - 70px)',
    height : 'auto',
    width: '700px',
    maxWidth: '96vw',
    
    data: {Flag : this.fitbitConfigurationFlag,url:this.fitbit__url}
  }

  );
  dialogRef.afterClosed().subscribe(result => {
  console.log('OK');
  });
}

}
