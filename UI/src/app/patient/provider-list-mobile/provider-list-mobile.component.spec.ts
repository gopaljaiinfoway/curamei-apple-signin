import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderListMobileComponent } from './provider-list-mobile.component';

describe('ProviderListComponent', () => {
  let component: ProviderListMobileComponent;
  let fixture: ComponentFixture<ProviderListMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderListMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderListMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
