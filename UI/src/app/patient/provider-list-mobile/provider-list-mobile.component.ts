
import { Component, OnInit , Inject} from '@angular/core'
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
@Component({
  selector: 'app-provider-list-mobile',
  templateUrl: './provider-list-mobile.component.html',
  styleUrls: ['./provider-list-mobile.component.css'],
})
 
export class ProviderListMobileComponent implements OnInit {
 dataObj:any;
 throughProviderFlag: boolean = false;
 buttonTypeName='edit';
 titleName="Edit";
 displayFileName:any;
  constructor(public dialogRef: MatBottomSheetRef<ProviderListMobileComponent> , @Inject(MAT_BOTTOM_SHEET_DATA) public data:any) { }
  ngOnInit(): void {
   this.dataObj = this.data.objData;
   this.displayFileName=this.dataObj.file_displayname;
   this.throughProviderFlag = this.data.throughProviderFlag;
    if(this.dataObj.file_displayname.length > 20){
       let subStringData = this.dataObj.file_displayname.substr(0,20);
      var ext = this.dataObj.file_displayname.substr(this.dataObj.file_displayname.lastIndexOf(".")+1);
       this.displayFileName = subStringData + '...' + ext;
    }  
  }


  closedPopupBottom(type,upiid, fileName = null){
    this.dialogRef.dismiss({type:type,upiid:upiid,fileName:fileName});
  }

  closePopUp(){
    this.dialogRef.dismiss(null);
  }

}
