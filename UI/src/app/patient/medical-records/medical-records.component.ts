import { Component, OnInit, Input, ɵConsole } from '@angular/core';
import { CommmanService } from 'src/app/_services/commman.service';
import { globalProviderSelect } from 'src/app/_services/globalProviderSelect.service'
import { records } from './medial-record';
import {formatDate} from '@angular/common';
import { Router } from '@angular/router';
import { MedicalRecordsDetailsComponent} from './medical-records-details/medical-records-details.component';
import { MedicalRecordsDetalisMobile} from './medical-records-detalis-mobile/medical-records-detalis-mobile.component';
import {MatDialog,MatDialogRef} from '@angular/material/dialog';
import { NgxSpinnerService } from "ngx-spinner";
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
@Component({
  selector: 'app-medical-records',
  templateUrl: './medical-records.component.html',
  styleUrls: ['./medical-records.component.css']
})
export class MedicalRecordsComponent implements OnInit {

  @Input() user_id: any = null;
  @Input() user_type: any;
  users: any = [];

  throughProviderFlag: boolean = false;
  view: boolean = true;
  noRecordFond = 'Loading...';
  panelOpenState: Boolean;
  medRecord: any;
  medicalRecord: any;
  progessValue:any;
  finalMedRecord: any;
  resourceRecord: any;
  resourceRecordType: any;
  resourceCode: any;
  allresourceCode: any[];
  arr: any[];
  enableLoading:boolean=false;
  dataStatusDispaly='Please wait...';

  selectedPatientId: any;

  constructor(
    private commanService: CommmanService,
    private router: Router, public dialog: MatDialog, private spinner: NgxSpinnerService,
    private _globalProviderSelect: globalProviderSelect, private _bottomSheet: MatBottomSheet,
  ) {
    this.medicalRecord = [
      {
        key: 1,
        name: "Conditions",
        resorceType: "Condition",
        records: []
      },
      {
        key: 2,
        name: "Medications",
        resorceType: "MedicationStatement",
        records: []
      },
      {
        key: 3,
        name: "Allergies",
        resorceType: "AllergyIntolerance",
        records: []
      },
      {
        key: 4,
        name: "Immunizations",
        resorceType: "Immunization",
        records: []
      },
      {
        key: 6,
        name: "Observations",
        resorceType: "Observation",
        records: []
       
      }, 
      {
        key: 4,
        name: "Family History",
        resorceType: "FamilyMemberHistory",
        records: []
      }, 
      // {
      //   key: 7,
      //   name: "Diagonostic Reports",
      //   resorceType: "DiagnosticReport",
      //   records: []
      // }, 
      {
        key: 8,
        name: "Procedures",
        resorceType: "Procedure",
        records: []
      }, {
        key: 9,
        name: "Prescriptions",
        resorceType: "MedicationOrder",
        records: []
      }, {
        key: 10,
        name: "Care Plans",
        resorceType: "CarePlan",
        records: []
      }, 
      {
        key: 11,
        name: "Goals",
        resorceType: "Goal",
        records: []
      },
      {
        key: 12,
        name: "Encounter",
        resorceType: "Encounter",
        records: []
      }
    ];

  }

  ngOnInit(): void {
    this.spinner.show();
    if(this.router.url == '/patient/medical-records'){
      this.view = true;
      this.throughProviderFlag = false;
      this.getResourcesData()
    }else if(this.router.url == '/provider/medical-records'){
      this.getAllPatient('Medical_record');
      this.view = false;
      this.throughProviderFlag = true;
      this._globalProviderSelect.getId().subscribe(n => {
        this.selectedPatientId = n;
        this.selectedUser(this.selectedPatientId);
      });
      this.selectedUser(this._globalProviderSelect.getId());
    }
  }
  getResourcesData(){
    this.contitionsGCPHealthcareResource('Condition');
    this.allergiesGCPHealthcareResource('AllergyIntolerance');
    this.immunizationsGCPHealthcareResource('Immunization');
    this.observationsGCPHealthcareResource('Observation');
    this.medicationStatementGCPHealthcareResource('MedicationStatement');
    this.familyMemberHistoryGCPHealthcareResource('FamilyMemberHistory');
    this.procedureGCPHealthcareResource('Procedure');
    this.medicationOrderGCPHealthcareResource('MedicationOrder');
    this.goalGCPHealthcareResource('Goal');
    this.carePlanGCPHealthcareResource('CarePlan');
   // this.diagnosticReportGCPHealthcareResource('DiagnosticReport');
    this.encounterGCPHealthcareResource('Encounter');
  }
  getResourcesDataByPatient(patient_id){
    this.fetchGCPHealthcareResource('Condition', patient_id);
    this.fetchGCPHealthcareResource('AllergyIntolerance', patient_id);
    this.fetchGCPHealthcareResource('Immunization', patient_id);
    this.fetchGCPHealthcareResource('Observation', patient_id);
    this.fetchGCPHealthcareResource('MedicationStatement', patient_id);
    this.fetchGCPHealthcareResource('FamilyMemberHistory', patient_id);
    this.fetchGCPHealthcareResource('Procedure', patient_id);
    this.fetchGCPHealthcareResource('MedicationOrder', patient_id);
    this.fetchGCPHealthcareResource('Goal', patient_id);
    this.fetchGCPHealthcareResource('CarePlan', patient_id);
    this.fetchGCPHealthcareResource('DiagnosticReport', patient_id);
    this.fetchGCPHealthcareResource('Encounter', patient_id);
  }
  fetchGCPHealthcareResource(resourType, patient_id) {
    this.commanService.fetchGCPHealthcareResource(resourType, 'none', patient_id)
      .subscribe((result: any) => {
        // console.log('============================================')
        // console.log(result)
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly= `Resource  data is  not available.`;
          }
       }else{
         this.dataStatusDispaly= `${result.message}`;
       }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }
  // All Patient
  getAllPatient(type){
    this.commanService.getAllPatient(type)
    .subscribe((result: any) => {
      if(result.success){
          this.noRecordFond = 'No patient user selected.';
         this.users = result.users;
      }
    })
  }
  selectedUser(id){
    if(id != undefined){
      this.user_id = id;
      this.view = true;
      this.getResourcesDataByPatient(this.user_id);
    }else{
      this.view = false;
      this.user_id = null;
    }
  }

  openDialog(resourceData) {
    var editFlag = true;
    if(this.user_id == null){
      editFlag = true;
    }else{
      editFlag = false;
    }
  const dialogRef = this.dialog.open(MedicalRecordsDetailsComponent, {
    minHeight: 'calc(80vh - 90px)',
    height : 'auto',
    width: '600px',
    data: {data: resourceData, editFlag: editFlag},
  }

  );
  dialogRef.afterClosed().subscribe(result => {
    this.GCPHealthcareResourceByType(result);
  });
}



 




openDialogMobileView(resourceData): void {
  var editFlag = true;
  if(this.user_id == null){
    editFlag = true;
  }else{
    editFlag = false;
  }
  const bottomSheetRef =this._bottomSheet.open(MedicalRecordsDetalisMobile,{
    disableClose: true,
   panelClass: 'custom-width-bottomSheet',
    data:{data: resourceData,editFlag: editFlag}
  });


  bottomSheetRef.afterDismissed().subscribe((result:any) => {
    this.GCPHealthcareResourceByType(result);
  });

}


















  // =======================GOPAL Pull OneUp health resources & push it into GCP healthcare===========================
  getOneUpResourceByType(resourType) {
    if(this.user_id == null){
      this.enableLoading=true;
      this.commanService.getOneUpHealthResource(resourType)
        .subscribe((result: any) => {
          this.enableLoading=false;
          this.GCPHealthcareResourceByType(resourType);
          if (result.success) {
            this.GCPHealthcareResourceByType(resourType);
          }else{
            this.dataStatusDispaly=`Resource data is  not available.`;
          }
        }, (error: any) => {
          console.log(error)
        })
    }else{
      // get gcphealthData by patient
      this.enableLoading=true;
      this.commanService.fetchGCPHealthcareResource(resourType,'none',this.user_id)
      .subscribe((result: any) => {
        this.enableLoading=false;
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly= `Resource data is  not available.`;
          }
       }else{
         this.dataStatusDispaly= `${result.message}`;
       }
      }, (error: any) => {
        this.enableLoading=false;
        this.dataStatusDispaly=error;
      })
    }
    
  }

  GCPHealthcareResourceByType(resourType){
    this.dataStatusDispaly='Please wait...';
       if(resourType=='Condition')
       this.contitionsGCPHealthcareResource(resourType);
       else if(resourType=='AllergyIntolerance')
       this.allergiesGCPHealthcareResource(resourType);
       else if(resourType=='Immunization')
       this.immunizationsGCPHealthcareResource(resourType);
       else if(resourType=='Observation')
       this.observationsGCPHealthcareResource(resourType);
       else if(resourType=='MedicationStatement')
       this.medicationStatementGCPHealthcareResource(resourType);
       else if(resourType=='Procedure')
       this.procedureGCPHealthcareResource(resourType);
       else if(resourType=='MedicationOrder')
       this.medicationOrderGCPHealthcareResource(resourType);
       else if(resourType=='FamilyMemberHistory')
       this.familyMemberHistoryGCPHealthcareResource(resourType);
       else if(resourType=='Goal')
       this.goalGCPHealthcareResource(resourType);
       else if(resourType=='CarePlan')
       this.carePlanGCPHealthcareResource('CarePlan');
       //else if(resourType=='DiagnosticReport')
       //this.diagnosticReportGCPHealthcareResource('DiagnosticReport');
       else if(resourType=='Encounter')
       this.encounterGCPHealthcareResource('Encounter');
       
       
       else
       console.log('-------------------No type match-----------------');
  }


  
  contitionsGCPHealthcareResource(resourType) {
    this.commanService.getContitionsGCPHealthcareResource(resourType)
      .subscribe((result: any) => {
        if (result.success) {
           if(result.data.length > 0){
               this.medicalRecord.forEach(element => {
              if(element.resorceType==resourType){
               element.records = (result.data); 
              }
             });
           }else{
               this.dataStatusDispaly='Resource data is  not available.'
           }
        }else{
          this.dataStatusDispaly= `${result.message}`;
        }

      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }


  allergiesGCPHealthcareResource(resourType) {
    this.commanService.getAllergiesGCPHealthcareResource(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly='Resource resource data is  not available.'
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }


  immunizationsGCPHealthcareResource(resourType) {
    this.commanService.getImmunizationsGCPHealthcareResource(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly='Resource resource data is  not available.'
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }


  
  observationsGCPHealthcareResource(resourType) {
    this.commanService.getObservationsGCPHealthcareResource(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly='Resource resource data is  not available.'
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }

  


  medicationStatementGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`Resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }



  procedureGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`Resource  data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }



  medicationOrderGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`Resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }



  goalGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`Resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }



  familyMemberHistoryGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {

        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`Resource resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }

  carePlanGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        console.log("==================CALL carePlanGCPHealthcareResource-------------------"+resourType);
        console.log(result);
        console.log("==================CALL carePlanGCPHealthcareResource-------------------");
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`Resource resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }


  // diagnosticReportGCPHealthcareResource(resourType) {
  //   this.commanService.getGCPHealthcareResourceByResourceType(resourType)
  //     .subscribe((result: any) => {
  //       console.log("==================CALL diagnosticReportGCPHealthcareResource-------------------"+resourType);
  //       console.log(result);
  //       console.log("==================CALL diagnosticReportGCPHealthcareResource-------------------");
  //       if (result.success) {
  //         if(result.data.length > 0){
  //             this.medicalRecord.forEach(element => {
  //            if(element.resorceType==resourType){
  //             element.records = (result.data); 
  //            }
  //           });
  //         }else{
  //             this.dataStatusDispaly=`${resourType} resource data is  not available.`
  //         }
  //      }else{
  //       this.dataStatusDispaly=`${result.message}`;
  //     }
  //     }, (error: any) => {
  //       this.dataStatusDispaly=error;
  //     })
  // }

  
  encounterGCPHealthcareResource(resourType) {
    this.commanService.getGCPHealthcareResourceByResourceType(resourType)
      .subscribe((result: any) => {
        console.log("==================CALL encounterGCPHealthcareResource-------------------"+resourType);
        console.log(result);
        console.log("==================CALL encounterGCPHealthcareResource-------------------");
        if (result.success) {
          if(result.data.length > 0){
              this.medicalRecord.forEach(element => {
             if(element.resorceType==resourType){
              element.records = (result.data); 
             }
            });
          }else{
              this.dataStatusDispaly=`Resource data is  not available.`
          }
       }else{
        this.dataStatusDispaly=`${result.message}`;
      }
      }, (error: any) => {
        this.dataStatusDispaly=error;
      })
  }

  // Get GCP healthcare resource by ID & resourceType
  getHealthcareResourcesByID(user_type, user_id, resource_type){
    this.commanService.getHealthData(user_type, user_id, resource_type)
    .subscribe((result: any) => {
      console.log('====getHealthcareResourcesByID====');
      console.log(result)
      //this.filterHealthcareData(result, resource_type)
    }, (error: any) => {
      console.log(error)
    })
  }
  // Get GCP healthcare resources ================ REMOVE BELOW ALL  AFTER ALL COMPLETED WORK ===========================
  getGCPHealthcareResources(resourType) {
    for (let i = 0; i < 20; i++) {
      this.progessValue = i;
    }
    this.commanService.getGCPHealthResource(resourType)
      .subscribe((result: any) => {
       // this.filterHealthcareData(result, resourType);
        

      }, (error: any) => {
        console.log(error)
      })
  }

}
