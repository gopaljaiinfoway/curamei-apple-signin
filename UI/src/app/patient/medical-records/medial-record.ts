export var records =
{
  "resourceType": "Bundle",
  "type": "searchset",
  "total": 84,
  "entry": [
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e5036e9d131bf04809e1abb60bb27899c599bc",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "valueString": "The following orders were created for panel order CBC and differential.\r\nProcedure                               Abnormality         Status                   \r\n---------                               -----------         ------                   \r\nCBC auto differential[973580]           Abnormal            Final result             \r\n\r\n\r\nPlease view results for these tests on the individual orders.",
        "identifier": [
          {
            "value": "T.kP0ynFiOMogm8bwZdB6xk2FJJzmPZMiRLzWHpCpPxQB",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:03.230Z",
          "versionId": "9000000000000"
        },
        "status": "final",
        "effectiveDateTime": "2015-10-03T20:21:00.000Z",
        "code": {
          "text": "Narrative"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "laboratory",
              "display": "Laboratory"
            }
          ],
          "text": "Laboratory"
        },
        "id": "f8fedcd9e6e5036e9d131bf04809e1abb60bb27899c599bc"
      }
    },
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e504829903a55284cc88babb16d6f3d78b50da",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "identifier": [
          {
            "value": "Te4K6zjiXtD2Z7Fdc.AXXOK4fRc9ik5zXUAuWq9v65uAB",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:03.038Z",
          "versionId": "9000000000000"
        },
        "status": "preliminary",
        "effectiveDateTime": "2015-10-03T20:20:00.000Z",
        "comments": "This is a corrected result. Previous result was 44 on 10/3/2015 at 1522 CDT",
        "referenceRange": [
          {
            "low": {
              "unit": "%",
              "code": "%",
              "system": "http://unitsofmeasure.org",
              "value": 41
            },
            "high": {
              "unit": "%",
              "code": "%",
              "system": "http://unitsofmeasure.org",
              "value": 53
            },
            "text": "41 - 53 %"
          }
        ],
        "issued": "2015-10-03T20:20:00Z",
        "code": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "4544-3",
              "display": "Hematocrit [Volume Fraction] of Blood by Automated count"
            }
          ],
          "text": "HEMATOCRIT (%) BY AUTOMATED COUNT"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "laboratory",
              "display": "Laboratory"
            }
          ],
          "text": "Laboratory"
        },
        "valueQuantity": {
          "unit": "%",
          "code": "%",
          "system": "http://unitsofmeasure.org",
          "value": 41
        },
        "id": "f8fedcd9e6e504829903a55284cc88babb16d6f3d78b50da"
      }
    },
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e5064e1161dd2d34440579204387cb880e7205",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "identifier": [
          {
            "value": "Tl4xxjAMDmAdEfs3nmEjOkrvrspqwsUY16P..71sL7JIB",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:03.337Z",
          "versionId": "9000000000000"
        },
        "status": "final",
        "effectiveDateTime": "2016-02-06T18:00:00.000Z",
        "referenceRange": [
          {
            "low": {
              "unit": "mmol/L",
              "code": "mmol/L",
              "system": "http://unitsofmeasure.org",
              "value": 99
            },
            "high": {
              "unit": "mmol/L",
              "code": "mmol/L",
              "system": "http://unitsofmeasure.org",
              "value": 108
            },
            "text": "99 - 108 mmol/L"
          }
        ],
        "code": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "2075-0",
              "display": "Chloride [Moles/volume] in Serum or Plasma"
            }
          ],
          "text": "CHLORIDE [MMOL/L] IN SER/PLAS"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "laboratory",
              "display": "Laboratory"
            }
          ],
          "text": "Laboratory"
        },
        "valueQuantity": {
          "unit": "mmol/L",
          "code": "mmol/L",
          "system": "http://unitsofmeasure.org",
          "value": 101
        },
        "id": "f8fedcd9e6e5064e1161dd2d34440579204387cb880e7205"
      }
    },
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e507b84e6b62a69a32852cc0745af6627d0881",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "identifier": [
          {
            "value": "Te4K6zjiXtD2Z7Fdc.AXXOLSx4mk1Lwsci2vRVLZSJHwB",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:02.772Z",
          "versionId": "9000000000000"
        },
        "status": "preliminary",
        "effectiveDateTime": "2015-10-03T20:20:00.000Z",
        "referenceRange": [
          {
            "low": {
              "unit": "pg",
              "code": "pg",
              "system": "http://unitsofmeasure.org",
              "value": 26
            },
            "high": {
              "unit": "pg",
              "code": "pg",
              "system": "http://unitsofmeasure.org",
              "value": 34
            },
            "text": "26.0 - 34.0 pg"
          }
        ],
        "issued": "2015-10-03T20:20:00Z",
        "code": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "785-6",
              "display": "Erythrocyte mean corpuscular hemoglobin [Entitic mass] by Automated count"
            }
          ],
          "text": "ERYTHROCYTE MEAN CORPUSCULAR HEMOGLOBIN [PG] BY AUTOMATED COUNT"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "laboratory",
              "display": "Laboratory"
            }
          ],
          "text": "Laboratory"
        },
        "id": "f8fedcd9e6e507b84e6b62a69a32852cc0745af6627d0881"
      }
    },
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e51103f635585454879a24d189e9b3f1baaf92",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "identifier": [
          {
            "value": "Tnf7t0.SP6znu2Dc1kPsrog5qrfqH33vJQOxAByVedwC-xFrBhsufKRxbYH1hARD8B",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:03.357Z",
          "versionId": "9000000000000"
        },
        "status": "final",
        "effectiveDateTime": "2015-08-25T22:46:00.000Z",
        "code": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "8716-3",
              "display": "Vital Signs grouping"
            },
            {
              "system": "http://loinc.org",
              "code": "9279-1",
              "display": "Respiratory rate"
            }
          ],
          "text": "Resp"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "vital-signs",
              "display": "Vital Signs"
            }
          ],
          "text": "Vital Signs"
        },
        "performer": [
          {
            "reference": "Practitioner/99c389c2674d",
            "display": "MOORE, NICK"
          }
        ],
        "valueQuantity": {
          "unit": "/min",
          "code": "/min",
          "system": "http://unitsofmeasure.org",
          "value": 20
        },
        "id": "f8fedcd9e6e51103f635585454879a24d189e9b3f1baaf92"
      }
    },
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e5150462fb6548007e4ee5723ec977183d668e",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "identifier": [
          {
            "value": "TDsM-v6omSaRsvT.-NvxIwNhErI9xU8QLC5C9jKK.3bIB",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:03.219Z",
          "versionId": "9000000000000"
        },
        "status": "final",
        "effectiveDateTime": "2015-10-03T20:21:00.000Z",
        "referenceRange": [
          {
            "low": {
              "unit": "%",
              "code": "%",
              "system": "http://unitsofmeasure.org",
              "value": 3
            },
            "high": {
              "unit": "%",
              "code": "%",
              "system": "http://unitsofmeasure.org",
              "value": 10
            },
            "text": "3 - 10 %"
          }
        ],
        "issued": "2015-10-03T20:21:00Z",
        "code": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "5905-5",
              "display": "Monocytes/100 leukocytes in Blood by Automated count"
            }
          ],
          "text": "MONOCYTES/100 LEUKOCYTES IN BLOOD BY AUTOMATED COUNT"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "laboratory",
              "display": "Laboratory"
            }
          ],
          "text": "Laboratory"
        },
        "id": "f8fedcd9e6e5150462fb6548007e4ee5723ec977183d668e"
      }
    },
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e5164ecdcb7487bfe3e86630a5d097ff3555cd",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "identifier": [
          {
            "value": "TdrL2iCYfK7IMmu0DpUbcQUMwUeJ5d4aTHz61XZoflSMB",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:02.776Z",
          "versionId": "9000000000000"
        },
        "status": "final",
        "effectiveDateTime": "2015-11-08T16:18:00.000Z",
        "code": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "6298-4",
              "display": "Potassium [Moles/volume] in Blood"
            }
          ],
          "text": "POTASSIUM BLOOD"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "laboratory",
              "display": "Laboratory"
            }
          ],
          "text": "Laboratory"
        },
        "valueQuantity": {
          "value": 3.1
        },
        "id": "f8fedcd9e6e5164ecdcb7487bfe3e86630a5d097ff3555cd"
      }
    },
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e51a52576b97ed214321599008c78080ac2631",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "identifier": [
          {
            "value": "Tnf7t0.SP6znu2Dc1kPsrog5qrfqH33vJQOxAByVedwC2C0rv4mz1Yn.XbBMaol8RB",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:03.280Z",
          "versionId": "9000000000000"
        },
        "status": "final",
        "effectiveDateTime": "2015-08-25T22:46:00.000Z",
        "code": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "8302-2",
              "display": "Body height"
            },
            {
              "system": "http://loinc.org",
              "code": "8716-3",
              "display": "Vital Signs grouping"
            }
          ],
          "text": "Height"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "vital-signs",
              "display": "Vital Signs"
            }
          ],
          "text": "Vital Signs"
        },
        "performer": [
          {
            "reference": "Practitioner/99c389c2674d",
            "display": "MOORE, NICK"
          }
        ],
        "valueQuantity": {
          "unit": "cm",
          "code": "cm",
          "system": "http://unitsofmeasure.org",
          "value": 184
        },
        "id": "f8fedcd9e6e51a52576b97ed214321599008c78080ac2631"
      }
    },
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e51f87eec6fce84519f33036cc32796ca5a8f6",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "identifier": [
          {
            "value": "Tl4xxjAMDmAdEfs3nmEjOkiWW1NSaAcoag28gMuON5.QB",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:00.101Z",
          "versionId": "9000000000000"
        },
        "status": "final",
        "effectiveDateTime": "2016-02-06T18:00:00.000Z",
        "referenceRange": [
          {
            "low": {
              "unit": "mg/dL",
              "code": "mg/dL",
              "system": "http://unitsofmeasure.org",
              "value": 4
            },
            "high": {
              "unit": "mg/dL",
              "code": "mg/dL",
              "system": "http://unitsofmeasure.org",
              "value": 21
            },
            "text": "4 - 21 mg/dL"
          }
        ],
        "code": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "3094-0",
              "display": "Urea nitrogen [Mass/volume] in Serum or Plasma"
            }
          ],
          "text": "UREA NITROGEN [MG/DL] IN SER/PLAS"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "laboratory",
              "display": "Laboratory"
            }
          ],
          "text": "Laboratory"
        },
        "valueQuantity": {
          "unit": "mg/dL",
          "code": "mg/dL",
          "system": "http://unitsofmeasure.org",
          "value": 14
        },
        "id": "f8fedcd9e6e51f87eec6fce84519f33036cc32796ca5a8f6"
      }
    },
    {
      "fullUrl": "https://api.1up.health/fhir/dstu2/Observation/f8fedcd9e6e52289ee9aae759fafb584481c82d7262ee0a2",
      "search": {
        "mode": "match"
      },
      "resource": {
        "subject": {
          "reference": "Patient/f8fedcd9e6e5"
        },
        "identifier": [
          {
            "value": "Tl4xxjAMDmAdEfs3nmEjOkkzFPOncRpATzFoBT-Qx1hAB",
            "system": "https:open-ic.epic.com"
          }
        ],
        "meta": {
          "lastUpdated": "2020-07-08T07:13:00.737Z",
          "versionId": "9000000000000"
        },
        "status": "final",
        "effectiveDateTime": "2016-02-06T18:00:00.000Z",
        "referenceRange": [
          {
            "low": {
              "unit": "mg/dL",
              "code": "mg/dL",
              "system": "http://unitsofmeasure.org",
              "value": 0.6
            },
            "high": {
              "unit": "mg/dL",
              "code": "mg/dL",
              "system": "http://unitsofmeasure.org",
              "value": 1.3
            },
            "text": "0.6 - 1.3 mg/dL"
          }
        ],
        "code": {
          "coding": [
            {
              "system": "http://loinc.org",
              "code": "2160-0",
              "display": "Creatinine [Mass/volume] in Serum or Plasma"
            }
          ],
          "text": "CREATININE [MG/DL] IN SER/PLAS"
        },
        "resourceType": "Observation",
        "category": {
          "coding": [
            {
              "system": "http://hl7.org/fhir/observation-category",
              "code": "laboratory",
              "display": "Laboratory"
            }
          ],
          "text": "Laboratory"
        },
        "valueQuantity": {
          "unit": "mg/dL",
          "code": "mg/dL",
          "system": "http://unitsofmeasure.org",
          "value": 0.8
        },
        "id": "f8fedcd9e6e52289ee9aae759fafb584481c82d7262ee0a2"
      }
    }
  ],
}