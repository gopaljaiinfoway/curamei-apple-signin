import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalRecordsDetalisMobile } from './medical-records-detalis-mobile.component';

describe('SharePatientResourceMobile', () => {
  let component: MedicalRecordsDetalisMobile;
  let fixture: ComponentFixture<MedicalRecordsDetalisMobile>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalRecordsDetalisMobile ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalRecordsDetalisMobile);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
