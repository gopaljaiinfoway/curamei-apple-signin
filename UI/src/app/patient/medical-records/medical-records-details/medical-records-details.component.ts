import { Component, OnInit, Inject, ViewChild, ɵConsole } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { CommmanService } from 'src/app/_services/commman.service';
import { MatSnackBar } from '@angular/material/snack-bar'
@Component({
  selector: 'app-medical-records-details',
  templateUrl: './medical-records-details.component.html',
  styleUrls: ['./medical-records-details.component.css']
})
export class MedicalRecordsDetailsComponent implements OnInit {
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  resourceData: any;
  resourceDataForSave: any;
  conditionModel: any;
  allergyIntoleranceModel: any;
  immunizationModel: any;
  observationModel: any;
  medicationStatementModel: any;
  procedureModel: any;
  medicationOrderModel: any;
  familyMemberHistoryModel: any;
  goalModel: any;
  carePlanModel : any;
  encounterModel : any;
  editEnableFlag:boolean=false;
  doEditFlag:boolean=true;

  action = null;
  UpdateBtn = 'Update'
  condition_status = [{ id: 1, name: 'active' }, { id: 2, name: 'recurrence' }, { id: 3, name: 'relapse' }, { id: 4, name: 'inactive' }, { id: 4, name: 'remission' }, { id: 4, name: 'resolved' }];
  condition_verification = [{ id: 1, name: 'unconfirmed' }, { id: 2, name: 'provisional' }, { id: 3, name: 'differential' }, { id: 4, name: 'confirmed' }];
  allergy_criticality = [{ key: 'Low', name: 'CRITL' }, { key: 'High', name: 'CRITH' }, { key: 'Unknown', name: 'CRITU' }];
  allergy_type = [{ key: 'allergy', name: 'allergy' }, { key: 'intolerance', name: 'intolerance' }];
  medicalStatement_status = [{ id: 1, name: 'active' }, { id: 2, name: 'completed' }, { id: 3, name: 'intended' }];
  familyMemberHistory_gender = [{ id: 1, name: 'male' }, { id: 2, name: 'female' }, { id: 3, name: 'other' }];
  vaccinationProtocolCode:any;
   vaccinationProtocolObj = [];
   carePlanActivityObj = [];
   customSeriesFlag:boolean=false;
   carePlaneActivityFlag:boolean=false;
   
   allergyIntoleranceTool = {
    substance: "Substance responsible for allergy/intolerance",
    category: "Type of susbstance",
    clinicalStatus: "Current status of medication consumption",
    type: "Whether condition is allergy or intolerance",
    criticality: "Severity of the reaction",
    note: "Additional notes",
    recordedDate: "Date the allergy or intolerance was recorded"
  };

  observationTool = {
    code: "Observation made or recorded",
    category: "Type of the observation",
    clinicalStatus: "Current status of the observation",
    bodySite: "Relevant observed body part",
    method: "Method by which observation was made",
    encounter: "Encounter with practitioner when this observation was made",
    effectiveDateTime: "When the observation value was recorded",
    value: "Value describing observation outcome or finding",
    dataAbsentReason: "Reason for no entered value",
    referenceRange: "Range of values to interpret observation value",
    comments: "Additional comments"
  };

  immunizationTool = {
    vaccineCode: "Identification of the administered vaccine",
    clinicalStatus: "Currnet status of immunization",
    date: "Date that the vaccine was administered",
    route: "How vaccine entered the body",
    site: "Body part where the vaccine was administered",
    targetDisease: "Disease(s) immunized against"
  };

  medicationStatementTool = {
    medication: "Medication being taken",
    dateAsserted: "Date that this record of medication was created",
    clinicalStatus: "Current status of this record",
    reasonForUse: "Reason for using the medication",
    timing: "Timing of medication consumption",
    dosageDescription: "Text description of dosage",
    asNeeded: "Whether medication is taken as needed",
    method: "Method of dosage consumption",
    quantity: "Quantity in single medication dosage",
    note: "Additional notes"
  };
  
  conditionTool = {
    code: "Name of the condition",
    category: "Type of condition",
    clinicalStatus: "Current status of the condition",
    verificationStatus: "Supporting information on the status of the condition",
    stageSummary: "Description of the current stage of the condition",
    abatement: "Date when condition was resolved"
  };

  familyMemberHistoryTool = {
    code: "Condition suffered by the family member",
    relationship: "Relationship of the person with patient",
    gender: "Gender of the family member",
    date: "Date that this history was recorded",
    status: "Current clinical status of this record of family history",
    conditionOutcome: "Outcomes of the condition(s) faced by the family member"
  };

  procedureTool = {
    code: "Identification of the procedure",
    category: "Classification of the procedure",
    status: "Current status of the procedure",
    outcome: "Outcome of the procedure",
    complication: "Complication(s) that arose during the procedure",
    performed: "Whether the procedure was not performed"
  };

  medicationOrderTool = {
    medication: "Medication prescribed",
    dateWritten: "Date that the prescription was written",
    prescriber: "Practitioner who made the prescription",
    status: "Current status of the prescription",
    dosageInstruction: "Text description of the dosage",
    dosageMethod: "Technique for consuming medication",
    dosageQuantity: "Amount of medication per dosage",
    dispenseQuantity: "Amount of medication supplied on pickup",
    dispenseDuration: "Expected number of days' worth of supply in pickup",
    note: "Additional notes"
  };

  goalTool = {
    description: "Desired outcome of the goal",
    category: "Type of goal",
    priority: "Priority of the goal",
    startDate: "When the goal started",
    status: "Current status of the goal",
    statusReason: "Reason for the current status"
  };

  encounterTool = {
    type: "Type of encounter",
    class: "Classification of the type of encounter",
    status: "Current status of the encounter",
    start: "When the encounter began",
    end: "When the encounter ended",
    priority: "Priority og the encounter",
    reason: "Reason for the encounter",
    partOf: "Other encounter(s) which this was a part of"
  };

  carePlanTool = {
    category: "Type of care plan",
    status: "Current status of the care plan",
    period: "Time period that the care plan covers",
    description: "Text description of the care plan",
    activityCategory: "Categorization of the primary activity",
    activityCode: "Identification of the primary activity",
    activityStatus: "Current status of the primary activity",
    activityDescription: "Text description of the primary activity"
  };

  constructor(
    private commanService: CommmanService, private _snackBar: MatSnackBar, public dialogRef: MatDialogRef<MedicalRecordsDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
     console.log("=====================popup get date ====================");
     console.log(this.data);
     console.log("=====================popup get date ====================");
    //console.log(this.data.route.text);
    this.resourceData = this.data.data;
    this.doEditFlag = this.data.editFlag;

    if (this.resourceData.resourceType == 'Condition') {
      this.conditionModel = {
        verificationStatus: (this.resourceData.verificationStatus) ? this.resourceData.verificationStatus : '',
        summary: (this.resourceData.stage) ? this.resourceData.stage.summary.coding[0].display : '',
        abatementDateTime: (this.resourceData.abatementDateTime) ? this.resourceData.abatementDateTime : '',
      }
    }
    if (this.resourceData.resourceType == 'AllergyIntolerance') {
    //  console.log("-----------------------" + this.resourceData.note.text);
      this.allergyIntoleranceModel = {
        type: (this.resourceData.type) ? this.resourceData.type : '',
        criticality: (this.resourceData.criticality) ? this.resourceData.criticality : '',
        note: (this.resourceData.note && this.resourceData.note.text) ? this.resourceData.note.text : '',
      }
    }
    if (this.resourceData.resourceType == 'Immunization') {
      this.immunizationModel = {
        route: (this.resourceData.route && this.resourceData.route.text) ? this.resourceData.route.text : '',
        site: (this.resourceData.site && this.resourceData.site.text) ? this.resourceData.site.text : '',
       vaccinationProtocolCode: '',
       vaccinationProtocolSeries: '',
       vaccinationProtocolCustomSeries:''
      }
         
            if(this.resourceData.vaccinationProtocol && this.resourceData.vaccinationProtocol.length){
               var vaccinationProtocol = this.resourceData.vaccinationProtocol;
                for(let i=0; i < vaccinationProtocol.length;i++){
                  var obj = {
                  series: vaccinationProtocol[i].series,
                  code: vaccinationProtocol[i].targetDisease[0].coding[0].code,
                  }
                  this.vaccinationProtocolObj.push(obj);
                }
          
            }

          
    }

     if(this.resourceData.resourceType == 'Observation'){
       this.observationModel = {
        bodySite : (this.resourceData.bodySite && this.resourceData.bodySite.text && this.resourceData.bodySite.text != 'default')? this.resourceData.bodySite.text : '',
        method : (this.resourceData.method && this.resourceData.method.text && this.resourceData.method.text != 'default')? this.resourceData.method.text : '',
        valueQuantity : (this.resourceData.valueQuantity && this.resourceData.valueQuantity.value)? this.resourceData.valueQuantity.value : 0,
        dataAbsentReason : (this.resourceData.dataAbsentReason && this.resourceData.dataAbsentReason.text && this.resourceData.dataAbsentReason.text != 'default')? this.resourceData.dataAbsentReason.text : '',
        comments : (this.resourceData.comments  && this.resourceData.comments != 'default')? this.resourceData.comments : '',
       referenceRangeLow : (this.resourceData.referenceRange  && this.resourceData.referenceRange[0].low.value)? this.resourceData.referenceRange[0].low.value : 0,
       referenceRangeHigh : (this.resourceData.referenceRange  && this.resourceData.referenceRange[0].high.value)? this.resourceData.referenceRange[0].high.value : 0,
      }
     }

     if(this.resourceData.resourceType == 'MedicationStatement'){
      this.medicationStatementModel = {
        dosageText : (this.resourceData.dosage && this.resourceData.dosage[0].text && this.resourceData.dosage[0].text != 'default')? this.resourceData.dosage[0].text : '',
        dosageAsNeededBoolean : (this.resourceData.dosage && this.resourceData.dosage[0].asNeededBoolean)? this.resourceData.dosage[0].asNeededBoolean : false,
        dosageTiming : (this.resourceData.dosage && this.resourceData.dosage[0].timing && this.resourceData.dosage[0].timing.repeat)? this.resourceData.dosage[0].timing.repeat.boundsPeriod.start : '',
        dosageMethod : (this.resourceData.dosage && this.resourceData.dosage[0].method && this.resourceData.dosage[0].method.text && this.resourceData.dosage[0].method.text !='default')? this.resourceData.dosage[0].method.text : '',
       dosageQuantityQuantity : (this.resourceData.dosage  && this.resourceData.dosage[0].quantityQuantity && this.resourceData.dosage[0].quantityQuantity.value)? this.resourceData.dosage[0].quantityQuantity.value : 0,
       note : (this.resourceData.note  && this.resourceData.note !='default')? this.resourceData.note : '',
     }
    }

    if(this.resourceData.resourceType == 'Procedure'){
      this.procedureModel = {
        notPerformed : (this.resourceData.notPerformed)? this.resourceData.notPerformed : false,
        outcome : (this.resourceData.outcome && this.resourceData.outcome.text && this.resourceData.outcome.text !='default')? this.resourceData.outcome.text : '',
        complication : (this.resourceData.complication && this.resourceData.complication[0].text && this.resourceData.complication[0].text !='default')? this.resourceData.complication[0].text : '',
     }
    }

    if(this.resourceData.resourceType == 'MedicationOrder'){
      this.medicationOrderModel = {
        prescriber : (this.resourceData.prescriber && this.resourceData.prescriber.display && this.resourceData.prescriber.display !='default')? this.resourceData.prescriber.display : '',
        dosageInstructionText : (this.resourceData.dosageInstruction && this.resourceData.dosageInstruction[0].text && this.resourceData.dosageInstruction[0].text != 'default')? this.resourceData.dosageInstruction[0].text : '',
        dosageInstructionMethod : (this.resourceData.dosageInstruction && this.resourceData.dosageInstruction[0].method && this.resourceData.dosageInstruction[0].method.text && this.resourceData.dosageInstruction[0].method.text !='default')? this.resourceData.dosageInstruction[0].method.text : '',
        dosageInstructionDoseQuantity : (this.resourceData.dosageInstruction  && this.resourceData.dosageInstruction[0].doseQuantity && this.resourceData.dosageInstruction[0].doseQuantity.value)? this.resourceData.dosageInstruction[0].doseQuantity.value : 0,
        dispenseRequestExpectedSupplyDuration : (this.resourceData.dispenseRequest && this.resourceData.dispenseRequest.expectedSupplyDuration && this.resourceData.dispenseRequest.expectedSupplyDuration.value)? this.resourceData.dispenseRequest.expectedSupplyDuration.value : 0,
        dispenseRequestQuantity : (this.resourceData.dispenseRequest && this.resourceData.dispenseRequest.quantity && this.resourceData.dispenseRequest.quantity.value)? this.resourceData.dispenseRequest.quantity.value : 0,
        note : (this.resourceData.note  && this.resourceData.note !='default')? this.resourceData.note : '',
     }
    }
 
    if(this.resourceData.resourceType == 'FamilyMemberHistory'){
      this.familyMemberHistoryModel = {
        gender : (this.resourceData.gender)? this.resourceData.gender : '',
        outcome : (this.resourceData.condition && this.resourceData.condition[0].outcome && this.resourceData.condition[0].outcome.coding[0].display && this.resourceData.condition[0].outcome.coding[0].display !='default')? this.resourceData.condition[0].outcome.coding[0].display : '',
     }
    }

    if(this.resourceData.resourceType == 'Goal'){
      this.goalModel = {
        StartDate : (this.resourceData.startDate)? this.resourceData.startDate : '',
        priority : (this.resourceData.priority && this.resourceData.priority.text && this.resourceData.priority.text !='default')? this.resourceData.priority.text: '',
        statusReason : (this.resourceData.statusReason && this.resourceData.statusReason.text && this.resourceData.statusReason.text !='default')? this.resourceData.statusReason.text: '',
      }
    }

    if(this.resourceData.resourceType == 'CarePlan'){
      this.carePlanModel = {
        period : (this.resourceData.period && this.resourceData.period.end)? this.resourceData.period.end : '',
        description : (this.resourceData.description && this.resourceData.description !='default')? this.resourceData.description: '',
        carePlaneCategory:'',
        carePlaneCode:'',
        carePlaneStatus:'',
        carePlaneDescription:''
      }
      if(this.resourceData.activity && this.resourceData.activity.length){
        var carePlanActivity = this.resourceData.activity;
         for(let i=0; i < carePlanActivity.length;i++){
           var objCarePlan = {
            category: carePlanActivity[i].detail.category.text,
            code: carePlanActivity[i].detail.code.text,
            status: carePlanActivity[i].detail.status,
            description: carePlanActivity[i].detail.description,
           //goal: carePlanActivity[i].targetDisease[0].coding[0].code,
           }
           this.carePlanActivityObj.push(objCarePlan);
         }
   
     }
    }

    if(this.resourceData.resourceType == 'Encounter'){
        this.encounterModel = {
          priority : (this.resourceData.priority && this.resourceData.priority.text && this.resourceData.priority.text !='default')? this.resourceData.priority.text : '',
          reason : (this.resourceData.reason && this.resourceData.reason[0].text && this.resourceData.reason[0].text !='default')? this.resourceData.reason[0].text : '',
          partOf : (this.resourceData.partOf && this.resourceData.partOf.display && this.resourceData.partOf.display !='default')? this.resourceData.partOf.display : '',
          
        }
    }

  }


  editEnable(){
    this.editEnableFlag = (this.editEnableFlag) ? false :true;
  }

  createSeries(){
   this.customSeriesFlag = true;
   this.immunizationModel.vaccinationProtocolCode = '';
  }

  selectSeries(series){
    this.carePlaneActivityFlag=true;
          let myKeysObj = this.vaccinationProtocolObj.filter(key => key.series == series);
          this.immunizationModel.vaccinationProtocolCode = myKeysObj[0].code;
  }



  selectCarePlanActivities(category){
        if(category !=''){
          this.carePlaneActivityFlag=true;
          let myKeysObj = this.carePlanActivityObj.filter(key => key.category == category);
          this.carePlanModel.carePlaneCode = myKeysObj[0].code;
          this.carePlanModel.carePlaneStatus = myKeysObj[0].status;
          this.carePlanModel.carePlaneDescription = myKeysObj[0].description;
        }else{
          this.carePlaneActivityFlag=false;
        }
  }




  updateGcpHeathData(resourceType) {
    if (resourceType == 'Condition') {
      this.resourceData.verificationStatus = this.conditionModel.verificationStatus;
      this.resourceData.abatementDateTime = this.conditionModel.abatementDateTime;
      this.resourceData.stage = { "summary": { "coding": [{ "display": this.conditionModel.summary }], } };
    } else if (resourceType == 'AllergyIntolerance') {
      this.resourceData.type = this.allergyIntoleranceModel.type;
      this.resourceData.criticality = this.allergyIntoleranceModel.criticality;
      this.resourceData.note =
      {
        text: this.allergyIntoleranceModel.note
      }

    } else if (resourceType == 'Immunization') {
      if(this.resourceData.route)
      this.resourceData.route.text= this.immunizationModel.route;
    else
      this.resourceData.route =  { "text": this.immunizationModel.route }

      if(this.resourceData.site)
      this.resourceData.site.text= this.immunizationModel.site;
    else
      this.resourceData.site =  { "text": this.immunizationModel.site }
 

      //  if(this.immunizationModel.vaccinationProtocolSeries=='Create Series'){

      //      if( this.resourceData.vaccinationProtocol && this.resourceData.vaccinationProtocol.length > 0 ){

      //       var obj = {
      //         "series": this.immunizationModel.vaccinationProtocolCustomSeries,
      //       "targetDisease": [
      //         {
      //           "coding": [
      //             {
      //               "system": "http://snomed.info/sct",
      //               "code": this.immunizationModel.vaccinationProtocolCode
      //             }
      //           ]
      //         }
      //       ],
      //       "doseSequence": 1,
      //       "doseStatus" : 
      //         {
      //               "text":"default"
      //         }
            
      //     }

      //       this.resourceData.vaccinationProtocol.push(obj);
      //      }else{
      //       var objArr = [];
      //       objArr.push(obj);
      //     this.resourceData.vaccinationProtocol = objArr;
      //      }
      //  }else{
      //   if(this.resourceData.vaccinationProtocol.length){
      //     var vaccinationProtocol = this.resourceData.vaccinationProtocol;
      //      for(let i=0; i < vaccinationProtocol.length;i++){
      //                if(vaccinationProtocol[i].series==this.immunizationModel.vaccinationProtocolSeries){
      //                 this.resourceData.vaccinationProtocol[i].targetDisease[0].coding[0].code =this.immunizationModel.vaccinationProtocolCode;   
      //                }
      //      }
     
      //  } 
      //  }


    }
    else if (resourceType == 'Observation') {
      if(this.resourceData.bodySite)
        this.resourceData.bodySite.text= this.observationModel.bodySite;
      else
        this.resourceData.bodySite =  { "text": this.observationModel.bodySite }

        if(this.resourceData.method)
        this.resourceData.method.text= this.observationModel.method;
      else
        this.resourceData.method =  { "text": this.observationModel.method }

        if(this.resourceData.dataAbsentReason)
        this.resourceData.dataAbsentReason.text= this.observationModel.dataAbsentReason;
      else
        this.resourceData.dataAbsentReason =  { "text": this.observationModel.dataAbsentReason }  

        this.resourceData.comments= this.observationModel.comments;

      if(this.resourceData.valueQuantity && this.resourceData.valueQuantity.value){
        this.resourceData.valueQuantity.value = parseFloat(this.observationModel.valueQuantity);
      }else{
        this.resourceData.valueQuantity = {
          "unit": "mg/dL",
          "code": "mg/dL",
          "system": "http://unitsofmeasure.org",
          "value": parseFloat(this.observationModel.valueQuantity)}
      }

      if( this.resourceData.referenceRange && this.resourceData.referenceRange[0].low && this.resourceData.referenceRange[0].high){
        this.resourceData.referenceRange[0].low.value = parseFloat(this.observationModel.referenceRangeLow);
        this.resourceData.referenceRange[0].high.value = parseFloat(this.observationModel.referenceRangeHigh);
      }else{
        this.resourceData.referenceRange = [
          {
            "low": {
              "value":parseFloat(this.observationModel.referenceRangeLow),
              "unit": "g/dl",
              "system": "http://unitsofmeasure.org",
              "code": "g/dL"
            },
            "high": {
              "value": parseFloat(this.observationModel.referenceRangeHigh),
              "unit": "g/dl",
              "system": "http://unitsofmeasure.org",
              "code": "g/dL"
            }
          }
        ];
      }

    }
    else if (resourceType == 'MedicationStatement') {
      if(this.resourceData.dosage && this.resourceData.dosage[0].text)
        this.resourceData.dosage[0].text= this.medicationStatementModel.dosageText;
      if(this.resourceData.dosage && this.resourceData.dosage[0].asNeededBoolean)
        this.resourceData.dosage[0].asNeededBoolean= this.medicationStatementModel.dosageAsNeededBoolean;
      if(this.resourceData.dosage && this.resourceData.dosage[0].timing && this.resourceData.dosage[0].timing.repeat)
      this.resourceData.dosage[0].timing.repeat.boundsPeriod.start= this.medicationStatementModel.dosageTiming;
      if(this.resourceData.dosage && this.resourceData.dosage[0].method && this.resourceData.dosage[0].method.text)
      this.resourceData.dosage[0].method.text= this.medicationStatementModel.dosageMethod;
      if(this.resourceData.dosage  && this.resourceData.dosage[0].quantityQuantity && this.resourceData.dosage[0].quantityQuantity.value)
      this.resourceData.dosage[0].quantityQuantity.value= parseFloat(this.medicationStatementModel.dosageQuantityQuantity);

      this.resourceData.note= this.medicationStatementModel.note;
    }
    else if(resourceType == 'Procedure'){
      this.resourceData.notPerformed = this.procedureModel.notPerformed;
      if(this.resourceData.outcome && this.resourceData.outcome.text)
      this.resourceData.outcome.text= this.procedureModel.outcome;
    else
      this.resourceData.outcome =  { "text": this.procedureModel.outcome}

      if(this.resourceData.complication && this.resourceData.complication[0].text)
      this.resourceData.complication[0].text= this.procedureModel.complication;
    else
      this.resourceData.complication =  [{ "text": this.procedureModel.complication}]
    }

    else if(resourceType == 'MedicationOrder'){
      if(this.resourceData.prescriber && this.resourceData.prescriber.display)
      this.resourceData.prescriber.display = this.medicationOrderModel.prescriber;
      if(this.resourceData.dosageInstruction && this.resourceData.dosageInstruction[0].text)
      this.resourceData.dosageInstruction[0].text = this.medicationOrderModel.dosageInstructionText;
      if(this.resourceData.dosageInstruction && this.resourceData.dosageInstruction[0].method)
      this.resourceData.dosageInstruction[0].method.text = this.medicationOrderModel.dosageInstructionMethod;
      if(this.resourceData.dosageInstruction  && this.resourceData.dosageInstruction[0].doseQuantity)
      this.resourceData.dosageInstruction[0].doseQuantity.value = parseFloat(this.medicationOrderModel.dosageInstructionDoseQuantity);
      if(this.resourceData.dispenseRequest)
      this.resourceData.dispenseRequest.expectedSupplyDuration.value = parseInt(this.medicationOrderModel.dispenseRequestExpectedSupplyDuration);
      if(this.resourceData.dispenseRequest)
      this.resourceData.dispenseRequest.quantity.value = parseFloat(this.medicationOrderModel.dispenseRequestQuantity);
      this.resourceData.note = this.medicationOrderModel.note;
    }
    else if(resourceType == 'FamilyMemberHistory'){
      this.resourceData.gender = this.familyMemberHistoryModel.gender;
      if(this.resourceData.condition && this.resourceData.condition[0].outcome)
      this.resourceData.condition[0].outcome.coding[0].display = this.familyMemberHistoryModel.outcome;
    }
    else if(resourceType == 'Goal'){
      this.resourceData.StartDate = this.goalModel.StartDate;
      if(this.resourceData.priority)
      this.resourceData.priority.text = this.goalModel.priority;
      if(this.resourceData.statusReason)
      this.resourceData.statusReason.text = this.goalModel.statusReason;
    }
    else if(resourceType == 'CarePlan'){
      this.resourceData.description = this.carePlanModel.description;

      if(this.resourceData.period && this.resourceData.period.end)
      this.resourceData.period.end = this.carePlanModel.period;
      else
      this.resourceData.period = {"end":this.carePlanModel.period};

       if(this.carePlanModel.carePlaneCategory !=''){
               if(this.resourceData.activity && this.resourceData.activity.length){
                var carePlanActivity = this.resourceData.activity;
                  for(let i=0; i < carePlanActivity.length;i++){
                     if(carePlanActivity[i].detail.category.text==this.carePlanModel.carePlaneCategory){
     
                       carePlanActivity[i].detail.category.text =this.carePlanModel.carePlaneCategory;  
                       carePlanActivity[i].detail.code.text =this.carePlanModel.carePlaneCode;
  
                        if(carePlanActivity && carePlanActivity[i].detail.status)
                        carePlanActivity[i].detail.status =this.carePlanModel.carePlaneStatus;
                        else
                       carePlanActivity[i].detail = {"status":this.carePlanModel.carePlaneStatus};
  
                        if(carePlanActivity && carePlanActivity[i].detail.description)
                        carePlanActivity[i].detail.description =this.carePlanModel.carePlaneStatus;
                        else
                        carePlanActivity[i].detail = {"description":this.carePlanModel.carePlaneStatus};
                      
                            break;
     
                     }
               }
     
           } 
       }
        
    }

    else if(resourceType == 'Encounter'){
      if(this.resourceData.priority)
      this.resourceData.priority.text= this.encounterModel.priority;
     else
      this.resourceData.priority =  { "text": this.encounterModel.priority };

      if(this.resourceData.reason)
      this.resourceData.reason[0].text= this.encounterModel.reason;
     else
      this.resourceData.reason =  [{ "text": this.encounterModel.reason }];

      if(this.resourceData.partOf)
      this.resourceData.partOf.display= this.encounterModel.partOf;
     else
      this.resourceData.partOf =  { "display": this.encounterModel.partOf };
        
    }

    this.resourceDataForSave = this.resourceData;
       delete this.resourceDataForSave['custom_type'];
      console.log("--------------------------final resource data for updation-------------");
      console.log(this.resourceDataForSave)
      console.log("--------------------------final resource data for updation-------------");
    this.UpdateBtn = 'Updating...';
    this.commanService.updateGcpHeathData(this.resourceDataForSave, resourceType)
      .subscribe((result: any) => {
        this.UpdateBtn = 'Update';
        if (result.success) {
          this.dialogRef.close(resourceType);
          this._snackBar.open(result.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['green-snackbar'],
          });
        } else {
          this._snackBar.open(result.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }
      }, (error: any) => {
        this._snackBar.open(error.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      })
  }

  onClosedClick(): void {
    this.editEnableFlag = false;
    this.carePlaneActivityFlag=false;
    this.dialogRef.close({ data: 'gopal' });
  }

}
