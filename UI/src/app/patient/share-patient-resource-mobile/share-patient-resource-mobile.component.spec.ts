import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharePatientResourceMobile } from './share-patient-resource-mobile.component';

describe('SharePatientResourceMobile', () => {
  let component: SharePatientResourceMobile;
  let fixture: ComponentFixture<SharePatientResourceMobile>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharePatientResourceMobile ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharePatientResourceMobile);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
