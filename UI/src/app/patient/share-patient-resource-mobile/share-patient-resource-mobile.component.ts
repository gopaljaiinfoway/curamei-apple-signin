
import { Component, OnInit , Inject} from '@angular/core'
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
@Component({
  selector: 'app-share-patient-resource-mobile',
  templateUrl: './share-patient-resource-mobile.component.html',
  styleUrls: ['./share-patient-resource-mobile.component.css'],
})

export class SharePatientResourceMobile implements OnInit {
 dataObj:any;
 displayFileName:any;
  constructor(public dialogRef: MatBottomSheetRef<SharePatientResourceMobile> , @Inject(MAT_BOTTOM_SHEET_DATA) public data:any) { }
  ngOnInit(): void {
   this.dataObj = this.data.objData;

   console.log("--- ----- --------------------------");
   console.log(this.dataObj);
   console.log("--- ----- --------------------------");
   this.displayFileName=this.dataObj.fullname;
//    if(this.dataObj.fullname.length > 30){
//     let subStringData = this.dataObj.fullname.substr(0,30);
//    var ext = this.dataObj.fullname.substr(this.dataObj.fullname.lastIndexOf(".")+1);
//     this.displayFileName = subStringData + '...' + ext;
//  } 

  }


  closedPopupBottom(type,patProvId){
    this.dialogRef.dismiss({type:type,patProvId:patProvId});
  }

  closePopUp(){
    this.dialogRef.dismiss(null);
  }

}
