import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { CommmanService } from 'src/app/_services/commman.service';
import { MatSnackBar } from '@angular/material/snack-bar'
declare var $: any;
@Component({
  selector: 'app-data-sources',
  templateUrl: './data-sources.component.html',
  styleUrls: ['./data-sources.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DataSourcesComponent implements OnInit {
  //selectedIndex = 1;
  addDataSource: boolean
  selectDataSource: boolean
  patientPortal: boolean
  uploadFile: boolean
  openRequestHealthPortal: boolean = false
  organisationData: any
  records: [] = []
  returnedArray = []
  finalRecord: number
  p: number = 1
  panelOpenState = false
  oneUpUserId: any;
  userobj: any;
  providerList: any;
  potalConnectionList: any;
  particularOrgInfo: any;
  oneupSuccessStatus:boolean=true;
  action = null;
  returnUrl:any;
  potalConnectionLoadingFlag:boolean=true;
  constructor (private commanService: CommmanService, private router: Router,private route: ActivatedRoute,
    private _snackBar: MatSnackBar,) {
    this.userobj = JSON.parse(localStorage.getItem('user'));
    this.oneUpUserId = this.userobj.oneup_user_id;
    this.oneupSuccessStatus = this.route.snapshot.queryParams["success"];
  }


  ngOnInit(): void {

     this.classTabActive();

     if(this.oneupSuccessStatus){
      this.patientOneupUserIdSave();
     }else{
      // localStorage.removeItem('providerSystemHealthId');
      // this._snackBar.open('Error Occur! Please try again', this.action, {
      //   duration: 3000,
      //   verticalPosition: 'top', // 'top' | 'bottom'
      //   panelClass: ['red-snackbar'],
      // });
      this.returnUrl = this.route.snapshot.queryParams['returnUrl']
      this.listDataBundle();
     }
   
  }



  listDataBundle(){
    this.addDataSource = true
    this.selectDataSource = false
    this.getHealthOrganisationDefault();
    this.PatientProviderwithFileList();
    this.checkAccessToken();
    this.PatientPotalConnectionList();
  }



  PatientProviderwithFileList() {
    this.commanService.getPatientProviderwithFileList().subscribe((res: any) => {
      if (res.success && res.data.length > 0) {
        this.providerList = res.data;
      } else {
        this.providerList = [];
      }
    })
  }




  PatientPotalConnectionList() {
    this.commanService.getPatientPotalConnectionList().subscribe((res: any) => {
      if (res.success && res.data.length > 0) {
        this.potalConnectionList = res.data;
        console.log('**********potalConnectionList********************');
        console.log(this.potalConnectionList)
        console.log('***********potalConnectionList*********************')
      } else {
        this.potalConnectionList = [];
      }
      this.potalConnectionLoadingFlag=false;
    })
  }




  addDataSourceBtn() {
    this.router.navigateByUrl('/patient/select-data-source')
  }

  backToAddSource() {
    this.router.navigateByUrl('/patient/datasource')
  }

  selectPatientPortal() {
    var eleOne = document.getElementById('patientPortal')
    var eleTwo = document.getElementById('fileUpload')
    eleOne.classList.toggle('active-box')
    eleTwo.classList.remove('active-box')
    this.patientPortal = true
    this.uploadFile = false
    console.log(this.patientPortal)
  }

  selectFileUpload() {
    var eleTwo = document.getElementById('fileUpload')
    var eleOne = document.getElementById('patientPortal')
    eleTwo.classList.toggle('active-box')
    eleOne.classList.remove('active-box')
    this.uploadFile = true
    this.patientPortal = false
    console.log('UF' + this.uploadFile)
  }

  clickContinue() {

    if (this.patientPortal) {
      this.openRequestHealthPortal = true
      this.addDataSource = false
      this.selectDataSource = false
      this.router.navigateByUrl('/patient/provider-connect');

    } else if (this.uploadFile) {
      // this.openRequestHealthPortal = true;
      this.router.navigateByUrl('/patient/request-health-system')
      this.addDataSource = false
      this.selectDataSource = false
    } else {
      this.uploadFile = false
      this.patientPortal = false
      'do noting'
    }
  }

  patientProvider() {
    this.router.navigateByUrl('/patient/patient-provider');
  }


  getProviderData(value: number) {
    this.commanService.getProvidertHealthSystem(value).subscribe((data: any) => {
      this.particularOrgInfo = data;
      this.router.navigate(['/patient/patient-provider'], { state: { data: this.particularOrgInfo.data[0] } })

    })
  }


  checkAccessToken() {
    this.commanService.checkAccessTokenExists(this.oneUpUserId).subscribe((resObj: any) => {
      if (resObj.success) {
        this.userobj.oneup_user_id = resObj.data.oneup_user_id;
        this.userobj.refresh_token = resObj.data.refresh_token;
        this.userobj.access_token = resObj.data.access_token;
        localStorage.setItem('user', JSON.stringify(this.userobj));
      }
    })
  }


  // getHealthOrganisation(orgname) {
  //   this.commanService.getHealthSystemName(orgname).subscribe((data) => {
  //     this.organisationData = data
  //     // console.log(data)
  //     const objects = Object.keys(data).map((key) => data[key])
  //     this.records = objects[1]
  //     this.finalRecord = this.records.length

  //     this.records.forEach((record) => {
  //       console.log(record)
  //     })
  //   })
  // }

  getHealthOrganisationDefault() {
    this.commanService.getHealthSystemNameDefault().subscribe((data) => {
      this.organisationData = data
      // console.log(data)
      const objects = Object.keys(data).map((key) => data[key])
      this.records = objects[1]
      this.finalRecord = this.records.length

      this.records.forEach((record) => {
        console.log(record)
      })
    })
  }


  patientOneupUserIdSave() {
    this.potalConnectionList = [];
    this.potalConnectionLoadingFlag=true;
    let systemHealthId = localStorage.getItem('providerSystemHealthId');
    let systemHealthName = localStorage.getItem('providerSystemHealthName');
    let providerSystemHealthAddress = localStorage.getItem('providerSystemHealthAddress');
    this._snackBar.open('Your data has been imported to Curamei', this.action, {
      duration: 3000,
      verticalPosition: 'top', // 'top' | 'bottom'
      panelClass: ['green-snackbar'],
    });
   
      this.commanService.getpatientOneupUserIdSave(systemHealthId,systemHealthName,providerSystemHealthAddress).subscribe((result: any) => {
  
        if (result.success == true) {
          localStorage.removeItem('providerSystemHealthId');
          localStorage.removeItem('providerSystemHealthName');
          localStorage.removeItem('providerSystemHealthAddress');
          //this.backToAddSource();
          if(!localStorage.getItem('providerSystemHealthAddress')){
            window.location.href='/patient/datasource';
          }
         
        } 
       
 
      
      })
    
  }

  classTabActive(){
   // $(".firstTab").addClass(".mat-tab-list .mat-tab-labels .mat-tab-label-active");
    $("#firstTab").click(function(){
      alert("ok");
    });
  }


}
