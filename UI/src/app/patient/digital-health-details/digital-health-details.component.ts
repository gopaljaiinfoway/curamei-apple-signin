import { Component, OnInit, Inject, ViewChild, ɵConsole } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommmanService } from 'src/app/_services/commman.service';
import { MatSnackBar } from '@angular/material/snack-bar';
//import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-digital-health-details',
  templateUrl: './digital-health-details.component.html',
  styleUrls: ['./digital-health-details.component.css']
})
export class DigitalHealthDetailsComponent implements OnInit {
 fitbit__url:any;
  bitBitFlag:boolean;
  constructor( private _snackBar: MatSnackBar, public dialogRef: MatDialogRef<DigitalHealthDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    //  this.fitbit_client_id = environment.fitbit_client_id;
    //  this.fitbit_callback_url = environment.fitbit_callback_url;
     }

  ngOnInit(): void {
    console.log(this.data);
      this.bitBitFlag = !(this.data.Flag);
      this.fitbit__url = this.data.url;
  }

  redictDigitalHealth(name) {
    if (confirm("Are you sure you want to redirect to Digital Health ? ")) {
      console.log(this.fitbit__url);
      window.location.href = this.fitbit__url;
      //window.location.href = `https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=${this.fitbit_client_id}&redirect_uri=${this.fitbit_callback_url}&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight`;

    }
  }

  onClosedClick(): void {
    this.dialogRef.close({ data: 'gopal' });
  }

}
