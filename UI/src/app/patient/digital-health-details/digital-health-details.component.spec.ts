import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitalHealthDetailsComponent } from './digital-health-details.component';

describe('DigitalHealthDetailsComponent', () => {
  let component: DigitalHealthDetailsComponent;
  let fixture: ComponentFixture<DigitalHealthDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DigitalHealthDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitalHealthDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
