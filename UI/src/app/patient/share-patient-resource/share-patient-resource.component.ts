import { Component, OnInit } from '@angular/core';
import { CommmanService } from 'src/app/_services/commman.service';
import {formatDate} from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import jwt_decode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
import {SharePatientResourceMobile } from '../share-patient-resource-mobile/share-patient-resource-mobile.component';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
@Component({
  selector: 'app-share-patient-resource',
  templateUrl: './share-patient-resource.component.html',
  styleUrls: ['./share-patient-resource.component.css']
})
export class SharePatientResourceComponent implements OnInit {
  noRecordFond:any;
  providerList:any;
  ResouceData:any;
  buttonText="Grant Access";
  buttonTextPermission="Permission";
  action=null;
  noRecordFondResourceSharing:any;
  public patientResourceShareForm: FormGroup;
  dataSharingFlag:boolean=false;
  roleData = [];
  IconName='add';
  loading:any;
  displayedColumns: string[] = ['provider_name', 'resource_name', 'action']
  dataSource: MatTableDataSource<any>;
  dataArrObj:[];
  roleId:any;
  addingProvider = false;
 // dataSourceAppPer: MatTableDataSource<any>;
  constructor(
    private commanService: CommmanService,  private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar, private cookieService: CookieService,
    private _bottomSheet: MatBottomSheet,
  ) {
    let tokenGet = this.cookieService.get('token')
    if (tokenGet && tokenGet != null) {
      let decoded = jwt_decode(tokenGet, 'llp');
      this.roleId = decoded.role_id;
    }

    this.patientResourceShareForm = this.formBuilder.group({
      providerName: [null, Validators.required],
      ResourceName: [null, Validators.required],
    })

  }

  ngOnInit(): void {
   this.patientProviderList();
   this.patientResourceList();
   this.patientResourceSharing();
  }
  ToggleForm(){
     if(this.dataSharingFlag){
      this.dataSharingFlag=false;
      this.IconName='add';
     }else{
      this.dataSharingFlag=true;
      this.IconName='menu';
     }
  }

  toggleForm() {
    this.addingProvider = !this.addingProvider;
  }

  onSubmit() {
   this.buttonText = 'Sending...'
   console.log(this.patientResourceShareForm.value)
    this.commanService.patientResourceSharingAdd(this.patientResourceShareForm.value).subscribe(
      (resmsg: any) => {
             if(resmsg.success){
              this.patientResourceSharing();
              this.buttonText = 'Grant Access'
              this.patientResourceShareForm.reset();
              this.addingProvider = false;
              this._snackBar.open(resmsg.message, this.action, {
                duration: 3000,
                verticalPosition: 'top', // 'top' | 'bottom'
                panelClass: ['green-snackbar'],
              });
             }else {
              this.addingProvider = false;
              this._snackBar.open(resmsg.message, this.action, {
                duration: 3000,
                verticalPosition: 'top', // 'top' | 'bottom'
                panelClass: ['red-snackbar'],
              });
              this.buttonText = 'Grant Access';
            }
      },
      (err) => {
        this.addingProvider = false;
        this.buttonText = 'Grant Access';
      }
    )
  }

  patientProviderList() {
    this.commanService.getPatientProviderList().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
           this.providerList = res.data;
        } else {
          this.providerList = [];
          this.noRecordFond = 'No access controls set';
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }
  patientResourceList() {
    this.commanService.getSharingResourceList().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
           this.ResouceData = res.data;
        } else {
          this.ResouceData = [];
          this.noRecordFond = 'No access controls set';
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  patientResourceSharing() {
    this.noRecordFondResourceSharing="Loading...";
    this.commanService.getpatientResourceSharing().subscribe(
      (res: any) => {
        console.log('============================================================')
        if (res.success && res.data.length > 0) {
          console.log(res.data)
          var dataList = res.data;
          console.log(dataList)
          for(var i=0; i<dataList.length; i++){
            console.log(dataList[i].fullname)
          }
          this.dataSource = new MatTableDataSource(res.data);
          this.dataArrObj=res.data;
          // console.log(this.dataSource)
        } else {
          this.dataSource = new MatTableDataSource([]);
          this.dataArrObj=[];
          this.noRecordFondResourceSharing = 'No access controls set';
        }
      },
      (err) => {
        this.dataArrObj=[];
        this.noRecordFondResourceSharing = 'No access controls set';
        console.log(err);
      }
    );
  }

  deletepatientResourceSharing(patProvId) {
    if (confirm("Do you want to permanently delete this resource sharing ?")) {
      this.commanService.deletepatientResourceSharing(patProvId).subscribe((res: any) => {
        if (res.success) {
          this.patientResourceSharing();
          this._snackBar.open(res.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }

      },
        err => {
          console.log(err.message);
        })
    }
  }


  openBottomSheetForRevoke(objData): void {
    const bottomSheetRef =this._bottomSheet.open(SharePatientResourceMobile,{
      data:{objData:objData}
    });


    bottomSheetRef.afterDismissed().subscribe((result:any) => {
         if(result.type=='revoke')
          this.deletepatientResourceSharing(result.patProvId);
    });
  
  }

}
