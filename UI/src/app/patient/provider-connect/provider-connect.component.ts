import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommmanService } from 'src/app/_services/commman.service';
import { environment } from '../../../environments/environment';
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'app-provider-connect',
  templateUrl: './provider-connect.component.html',
  styleUrls: ['./provider-connect.component.css']
})
export class ProviderConnectComponent implements OnInit {
  organisationData: any
  records = [];
  returnedArray = []
  finalRecord: number
  p: number = 1
  panelOpenState = false
  accessTokenId: any;
  client_id: any;
  userobj: any;
  filterName: any;
  dataFlg: boolean = false;
  errorMessage: any;
  dataFoundMessage = 'spinner';
  constructor (private commanService: CommmanService, private router: Router, private _snackBar: MatSnackBar) {
    this.client_id = environment.client_id;
    this.userobj = JSON.parse(localStorage.getItem('user'));
    this.accessTokenId = this.userobj.access_token;
    this.filterName = 'mi';
  }

  ngOnInit(): void {
    this.openSnackbar();
    this.SystemHealthList();
  }

  goToSelectDataSource() {
    this.router.navigateByUrl('/patient/request-health-system')
  }

  openSnackbar() {
    this._snackBar.open("Some patient portal connections may be temporarily unavailable.","", {
      duration: 2000,
      panelClass: ['mat-toolbar', 'mat-warn', 'warning-snackbar-text'],
      verticalPosition: 'top'
    });
  }

  SystemHealthList(){
    this.records=[];
    this.dataFoundMessage="spinner";
    this.commanService.getAllSystemHealthList(this.accessTokenId,this.filterName).subscribe(
      (res: any) => {  
          if(res.success && res.data.length > 0){
            this.records =  res.data;
          }else{
            this.records=[];
            this.errorMessage = res.message;
            this.dataFoundMessage='Data is not available.';
          }
      },
      (err) => {
        console.log("not get data");
        this.dataFoundMessage='Data is not available.';
      }
    )
  }

  redictOneUPWeb(clinicalId, name, city, state) {
    if (confirm("Are you sure you want to connect to this health portal?")) {
      let address = city + "," + state;
      localStorage.setItem('providerSystemHealthId',clinicalId);
      localStorage.setItem('providerSystemHealthName',name);
      localStorage.setItem('providerSystemHealthAddress',address);
      window.location.href = `https://api.1up.health/connect/system/clinical/${clinicalId}?client_id=${this.client_id}&access_token=${this.accessTokenId}`;
    }
  }
 
  searchFilter(data) {
    console.log("data " + encodeURIComponent(this.filterName))
    if (data == '') {
      this.filterName = 'cer';
    } else if (this.filterName != data){
      this.filterName=data;
    }
    this.records=[];
    this.SystemHealthList();
  }
}
