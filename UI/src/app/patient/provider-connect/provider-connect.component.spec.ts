import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderConnectComponent } from './provider-connect.component';

describe('ProviderConnectComponent', () => {
  let component: ProviderConnectComponent;
  let fixture: ComponentFixture<ProviderConnectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderConnectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderConnectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
