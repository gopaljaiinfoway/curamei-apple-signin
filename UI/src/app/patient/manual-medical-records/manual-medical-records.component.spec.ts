import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualMedicalRecordsComponent } from './manual-medical-records.component';

describe('ManualMedicalRecordsComponent', () => {
  let component: ManualMedicalRecordsComponent;
  let fixture: ComponentFixture<ManualMedicalRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualMedicalRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualMedicalRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
