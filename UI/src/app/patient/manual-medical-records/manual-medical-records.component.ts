import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CommmanService } from 'src/app/_services/commman.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';
import { MatSnackBar } from '@angular/material/snack-bar';
import {ManualMedicalRecordsMobile } from '../manual-medical-records-mobile/manual-medical-records-mobile.component';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
@Component({
  selector: 'app-manual-medical-records',
  templateUrl: './manual-medical-records.component.html',
  styleUrls: ['./manual-medical-records.component.css']
})
export class ManualMedicalRecordsComponent implements OnInit {
  
  addFlag: Boolean = false;
  ifManualPatientExist = false;
  panelOpenState: Boolean;
  selectedResourceType: any = {display: 'Record Type'};
  type: any;
  pType = new FormControl('');
  selectedType = '';
  encounters = [{id: 123, name: 'encounter 1'},{id: 1234, name: 'encounter 2'}];
  immnuization_Disease = [{id: 123, name: 'AAA'},{id:1234, name:'BBB'}];
  genders = [{ id: 1, name: 'male' }, { id: 2, name: 'female' }, { id: 3, name: 'other' }];
  martial_status = [{ id: 1, name: 'single' }, { id: 1, name: 'married' }, { id: 1, name: 'other' }];
  allergy_category = [{ id: 1, name: 'food' }, { id: 2, name: 'medication' }, { id: 3, name: 'environment' }, { id: 4, name: 'biologic' }];
  allergy_status = [{ id: 1, name: 'active' }, { id: 2, name: 'inactive' }, { id: 3, name: 'resolved' }];
  allergy_criticality = [{ id: 1, name: 'CRITL', desc: 'Low criticality'},{ id: 2, name: 'CRITH', desc: 'High criticality'},{ id: 3, name: 'CRITU', desc: 'Unknown criticality'}];
  observation_status = [{ id: 1, name: 'registered' }, { id: 2, name: 'preliminary' }, { id: 3, name: 'final' }, { id: 4, name: 'amended' }];
  immnuization_status = [{ id: 1, name: 'completed' }, { id: 2, name: 'entered-in-error' }, { id: 3, name: 'not-done' }];
  immnuization_wasNotGiven = [{ id: 1, name: true }, { id: 2, name: false }];
  immnuization_reported = [{ id: 1, name: true }, { id: 2, name: false }];
  medication_status = [{ id: 1, name: 'active' }, { id: 2, name: 'completed' }, { id: 3, name: 'entered-in-error' }, { id: 3, name: 'intended' }, { id: 3, name: 'stopped' }, { id: 3, name: 'on-hold' }];
  medication_timing = [{ id: 1, name: '1:00' }, { id: 2, name: '2:00' }, { id: 3, name: '3:00' }, { id: 4, name: '4:00' }, { id: 5, name: '5:00' }, { id: 6, name: '6:00' }, { id: 7, name: '7:00' }, { id: 8, name: '8:00' }, { id: 9, name: '9:00' }, { id: 10, name: '10:00' }, { id: 11, name: '11:00' }, { id: 12, name: '12:00' }];
  medication_meridian = [{ id: 1, name: 'AM' }, { id: 2, name: 'PM' }];
  medication_needed = [{ id: 1, name: true }, { id: 2, name: false }];
  condition_category = [{ id: 1, name: "problem-list-item" }, { id: 2, name: "encounter-diagnosis" }]; // problem-list-item | encounter-diagnosis
  condition_status = [{ id: 1, name: 'active' }, { id: 2, name: 'recurrence' }, { id: 3, name: 'relapse' }, { id: 4, name: 'inactive' }, { id: 4, name: 'remission' }, { id: 4, name: 'resolved' }];
  condition_verification = [{ id: 1, name: 'unconfirmed' }, { id: 2, name: 'provisional' }, { id: 3, name: 'differential' }, { id: 4, name: 'confirmed' }, { id: 4, name: 'refuted' }, { id: 4, name: 'entered-in-error' }];
  familyMemberHistory_gender = [{ id: 1, name: 'male' }, { id: 2, name: 'female' }, { id: 3, name: 'other' }];
  familyMemberHistory_status = [{ id: 1, name: 'partial' }, { id: 2, name: 'entered-in-error' }, { id: 3, name: 'completed' },{ id: 3, name: 'health-unknown' }];
  procedure_status = [{ id: 1, name: 'in-progress' }, { id: 2, name: 'entered-in-error' }, { id: 3, name: 'aborted' },{ id: 3, name: 'completed' }];
  procedure_notPerformed = [{ id: 1, name: true }, { id: 2, name: false }];
  goal_status = [{ id: 1, name: 'roposed' }, { id: 2, name: 'planned' }, { id: 3, name: 'accepted' },{ id: 4, name: 'rejected' },{ id: 5, name: 'in-progress' },
  { id: 6, name: 'achieved' },{ id: 7, name: 'sustaining' },{ id: 8, name: 'on-hold' },{ id: 9, name: 'cancelled' }];
  encounter_status = [{ id: 1, name: 'arrived' }, { id: 2, name: 'planned' }, { id: 3, name: 'in-progress' },{ id: 4, name: 'onleave' },{ id: 5, name: 'finished' },{ id: 6, name: 'cancelled' }];
  encounter_class = [{ id: 1, name: 'inpatient' }, { id: 2, name: 'outpatient' }, { id: 3, name: 'ambulatory' },{ id: 4, name: 'emergency' }];
  careplan_status = [{ id: 1, name: 'proposed' }, { id: 2, name: 'draft' }, { id: 3, name: 'active' },{ id: 4, name: 'completed'},{ id: 5, name: 'cancelled'}];
  careplan_activity_status = [{ id: 1, name: 'not-started' }, { id: 2, name: 'scheduled' }, { id: 3, name: 'in-progress' },{ id: 4, name: ' on-hold'},{ id: 5, name: 'completed'},{ id: 5, name: 'cancelled'}];
  action:null;
  newPatientForm: FormGroup;
  allergyForm: FormGroup;
  observationForm: FormGroup;
  immunizationsForm: FormGroup;
  medicationForm: FormGroup;
  conditionForm: FormGroup;
  containers = [];
  MedicationStatementContainers = [];
  AllergyIntoleranceContainers = [];
  ImmunizationContainers = [];
  allergyIntoleranceForm: FormControl;
  immunizationForm: FormControl;


  medicationStatementForm: FormGroup;
  familyMemberHistoryForm:FormGroup;
  ProcedureForm:FormGroup;
  medicationOrderForm:FormGroup;
  goalForm:FormGroup;
  encounterForm:FormGroup;
  carePlanForm:FormGroup;

  

  patientResourceType: string = "Patient";
  allergyResourceType: string = "AllergyIntolerance";
  observationResourceType: string = "Observation";
  immunizationsResourceType: string = "Immunization";
  medicationResourceType: string = "MedicationStatement";
  condtionResourceType: string = "Condition";
  familyMemberHistoryResourceType: string = "FamilyMemberHistory";
  procedureResourceType: string = "Procedure";
  medicationOrdeResourceType: string = "MedicationOrder";
   goalResourceType: string = "Goal";
   encounterResourceType: string = "Encounter";
   carePlanResourceType: string = "CarePlan";

   allResourceNameObj: any;
   allResourceFlagFormObj: any; 
  patientStatus: Boolean = true;
  clinicalStatus: Boolean;
  abatement: any;
  allergySubstance: any;
  allergyCategory: any;
  allergyStatus: any;
  allergyType: any;
  observation_bodySite: any;
  observationEncounter: any;
  observation_absent_reason: any;
  observation_reference_range: any;
  observation_comments: any;
  immunizationRoute: any;
  immunizationSite: any;
  immunizationDisease: any;
  observation_method: any;
  allergyNote: any;
  allergyCriticality: any;
  obsevationCode: any;
  observationCategory: any;
  observationStatus: any;
  observationDateTime: any;
  observationQuantity: any;
  immunizationVaccineCode: any;
  immunizationStatus: any;
  immunizationWasNotGiven: any;
  immunizationDate: any;
  immunizationReported: any;
  medicationsMedication: any;
  medicationsDosage: any;
  medicationsStatus: any;
  medicationsDateAsserted: any;
  medicationsReasonForUse: any;
  medicationsDosageTiming: any;
  medicationsDosageTimingMeridian: any;
  medicationsDaosageNeeded: any;
  medicationsDosageMethod: any;
  medicationsDosageQuantity: any;
  medicationsDosageNote: any;
  medicationsDosageUnit: any;
  condtionCode: any;
  condtionCategory: any;
  condtionStatus: any;
  condtionVerificationStatus: any;
  condtionStageSummary: any;
  patientName: any;
  patientGender: any;
  patientBirthDate: any;
  patientMartialStatus: any;
  patientAddress: any;
  progessValue: any = 0;

  allergyIntoleranceTool = {
    substance: "Substance responsible for allergy/intolerance",
    category: "Type of susbstance",
    clinicalStatus: "Current status of medication consumption",
    type: "Whether condition is allergy or intolerance",
    criticality: "Severity of the reaction",
    note: "Additional notes",
    recordedDate: "Date the allergy or intolerance was recorded"
  };

  observationTool = {
    code: "Observation made or recorded",
    category: "Type of the observation",
    clinicalStatus: "Current status of the observation",
    bodySite: "Relevant observed body part",
    method: "Method by which observation was made",
    encounter: "Encounter with practitioner when this observation was made",
    effectiveDateTime: "When the observation value was recorded",
    value: "Value describing observation outcome or finding",
    dataAbsentReason: "Reason for no entered value",
    referenceRange: "Range of values to interpret observation value",
    comments: "Additional comments"
  };

  immunizationTool = {
    vaccineCode: "Identification of the administered vaccine",
    clinicalStatus: "Currnet status of immunization",
    date: "Date that the vaccine was administered",
    route: "How vaccine entered the body",
    site: "Body part where the vaccine was administered",
    targetDisease: "Disease(s) immunized against"
  };

  medicationStatementTool = {
    medication: "Medication being taken",
    dateAsserted: "Date that this record of medication was created",
    clinicalStatus: "Current status of this record",
    reasonForUse: "Reason for using the medication",
    timing: "Timing of medication consumption",
    dosageDescription: "Text description of dosage",
    asNeeded: "Whether medication is taken as needed",
    method: "Method of dosage consumption",
    quantity: "Quantity in single medication dosage",
    note: "Additional notes"
  };
  
  conditionTool = {
    code: "Name of the condition",
    category: "Type of condition",
    clinicalStatus: "Current status of the condition",
    verificationStatus: "Supporting information on the status of the condition",
    stageSummary: "Description of the current stage of the condition",
    abatement: "Date when condition was resolved"
  };

  familyMemberHistoryTool = {
    code: "Condition suffered by the family member",
    relationship: "Relationship of the person with patient",
    gender: "Gender of the family member",
    date: "Date that this history was recorded",
    status: "Current clinical status of this record of family history",
    conditionOutcome: "Outcomes of the condition(s) faced by the family member"
  };

  procedureTool = {
    code: "Identification of the procedure",
    category: "Classification of the procedure",
    status: "Current status of the procedure",
    outcome: "Outcome of the procedure",
    complication: "Complication(s) that arose during the procedure",
    performed: "Whether the procedure was performed"
  };

  medicationOrderTool = {
    medication: "Medication prescribed",
    dateWritten: "Date that the prescription was written",
    prescriber: "Practitioner who made the prescription",
    status: "Current status of the prescription",
    dosageInstruction: "Text description of the dosage",
    dosageMethod: "Technique for consuming medication",
    dosageQuantity: "Amount of medication per dosage",
    dispenseQuantity: "Amount of medication supplied on pickup",
    dispenseDuration: "Expected number of days' worth of supply in pickup",
    note: "Additional notes"
  };

  goalTool = {
    description: "Desired outcome of the goal",
    category: "Type of goal",
    priority: "Priority of the goal",
    startDate: "When the goal started",
    status: "Current status of the goal",
    statusReason: "Reason for the current status"
  };

  encounterTool = {
    type: "Type of encounter",
    class: "Classification of the type of encounter",
    status: "Current status of the encounter",
    start: "When the encounter began",
    end: "When the encounter ended",
    priority: "Priority og the encounter",
    reason: "Reason for the encounter",
    partOf: "Other encounter(s) which this was a part of"
  };

  carePlanTool = {
    category: "Type of care plan",
    status: "Current status of the care plan",
    period: "Time period that the care plan covers",
    description: "Text description of the care plan",
    activityCategory: "Categorization of the primary activity",
    activityCode: "Identification of the primary activity",
    activityStatus: "Current status of the primary activity",
    activityDescription: "Text description of the primary activity"
  };

  constructor (
    private formBuilder: FormBuilder,
    private commanService: CommmanService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private notifier: NotifierService,
    private _snackBar: MatSnackBar,
    private _bottomSheet: MatBottomSheet,
    ) {
    this.newPatientForm = this.formBuilder.group({
      name: '',
      // contact: '',
      gender: '',
      birthDate: '',
      maritalStatus: '',
      address: '',
      resourceType: '',
      active: ''
    });
    this.conditionForm = this.formBuilder.group({
      code: '',
      category: '',
      status: '',
      verificationStatus: '',
      stageSummary: '',
      resourceType: '',
      abatement: ''
    });
    this.allergyForm = this.formBuilder.group({
      substance: '',
      category: '',
      status: '',
      criticality: '',
      type: '',
      note: '',
      resourceType: '',
      recordedDate:''
    });
    this.observationForm = this.formBuilder.group({
      code: '',
      category: '',
      status: '',
      body_site: '',
      method: '',
      encounter: '',
      absent_reason:'',
      reference_range:'',
      comment:'',
      effectiveDateTime: '',
      quantity: '',
      resourceType: '',
    });
    this.immunizationsForm = this.formBuilder.group({
      vaccineCode: '',
      date: '',
      status: '',
      // wasNotGiven: '',
      route: '',
      site: '',
      disease: '',
      resourceType: '',
    });
    this.medicationForm = this.formBuilder.group({
      medication: '',
      dosage: '',
      status: '',
      dateAsserted: '',
      reasonForUse: '',
      boundsPeriod:'',
      daosageNeeded: '',
      dosageMethod: '',
      dosageQuantity: '',
      dosageNote: '',
      dosageUnit: '',
      resourceType: '',
    });
    // this.conditionForm = this.formBuilder.group({
    //   code: '',
    //   category: '',
    //   fields: this.formBuilder.array([])
    //   // clinical_status: new FormControl(''),
    //   // verification_status: new FormControl(''),
    //   // stage_summary: new FormControl(''),
    // });
    this.medicationStatementForm = new FormGroup({
      medication: new FormControl(''),
      dosage_text: new FormControl(''),
      clinical_status: new FormControl(''),
      date_asserted: new FormControl(''),
      reason: new FormControl(''),
      dosage_timing: new FormControl(''),
      dosage_as_needed: new FormControl(''),
      dosage_method: new FormControl(''),
      dosage_quantity: new FormControl(''),
      note: new FormControl(''),
    });
    this.allergyIntoleranceForm = new FormControl({
      substance: new FormControl(''),
      category: new FormControl(''),
      clinical_status: new FormControl(''),
      type: new FormControl(''),
      critically: new FormControl(''),
      note: new FormControl(''),
    });
    this.immunizationForm = new FormControl({
      vaccine_code: new FormControl(''),
      explanation_reason: new FormControl(''),
      clinical_status: new FormControl(''),
      date_administered: new FormControl(''),
      target_diseases: new FormControl('')
    });


    this.familyMemberHistoryForm = this.formBuilder.group({
      condition: '',
      relationship: '',
      date: '',
      status:'',
      gender: '',
      condition_outcome: '',
      resourceType: '',
    });


    this.ProcedureForm = this.formBuilder.group({
      code: '',
      category: '',
      status: '',
      notPerformed:'',
      outcome:'',
      complication: '',
      resourceType:''
    });


    this.medicationOrderForm = this.formBuilder.group({
      medicationReference: '',
      dateWritten:'',
      status:'',
      prescriber:'',
      dosageInstructionText:'',
      dosageInstructionMethod:'',
      dosageInstructionDoseQuantity:'',
      dosageInstructionDoseQuantityUnit:'',
      dispenseRequestExpectedSupplyDuration:'',
      dispenseRequestExpectedSupplyUnit:'',
      dispenseRequestQuantity:'',
      dispenseRequestUnit:'',
      dosageNote:'',
      resourceType:''
    });
    
    
    this.goalForm = this.formBuilder.group({
      category: '',
      status: '',
      description:'',
      startDate:'',
      priority: '',
      statusReason:'',
      resourceType:''
    });

    this.encounterForm = this.formBuilder.group({
      class:'',
      type: '',
     periodStartDate: '', 
      periodEndDate:'',
      status:'',
      priority: '',
      partOf:'',
      reason:'',
      resourceType:''
    });

    this.carePlanForm = this.formBuilder.group({
      resourceType:'',
      category:'',
      status:'',
      period:'',
      description:'',
      activityCategory:'',
      activityCode:'',
      activityStatus:'',
      activityDescription:'',
    });

    
  }

  fields(): FormArray {
    return this.conditionForm.get('fields') as FormArray;
  }

  newField(): FormGroup {
    return this.formBuilder.group({
      clinical_status: '',
      verification_status: '',
      stage_summary: '',
    });
  }

  addField() {
    this.fields().push(this.newField());
  }

  removeField(i: number) {
    this.fields().removeAt(i);
  }

  ngOnInit(): void {

    this.allResourceNameObj = [
      {
        id: 1,
        name : 'Patient',
        display: 'Personal',
        desc: 'General personal and identifying information'
      },
      {
        id: 2,
        name: 'AllergyIntolerance',
        display: 'Allergy and Intolerance',
        desc: 'Risk of harmful or undesirable physiological responses to certain substances'
      },
      {
        id: 3,
        name: 'Observation',
        display: 'Observation',
        desc: 'Measurements and simple statements about the patient'
      },
      {
        id: 4,
        name: 'Immunization',
        display: 'Immunization',
        desc: 'Information relevant to the event of vaccination being administered'
      },
      {
        id: 5,
        name: 'MedicationStatement',
        display: 'Medication Statement',
        desc: 'Record of medication that is being taken now, in the past, or the future'
      },
      {
        id: 6,
        name: 'Condition',
        display: 'Condition',
        desc: 'Details on conditions, problems, or diagnoses'
      },
      {
        id: 7,
        name: 'FamilyMemberHistory',
        display: 'Family Member History',
        desc: 'Significant health events and conditions faced by relatives'
      },
      {
        id: 8,
        name: 'Procedure',
        display: 'Procedure',
        desc: 'Action performed on a patient'
      },
      {
        id: 9,
        name: 'MedicationOrder',
        display: 'Prescription',
        desc: 'Prescription for supply and consumption instructions of a medication'
      },
      {
        id: 10,
        name: 'Goal',
        display: 'Goal',
        desc: 'Objectives related to well-being and treatment'
      },
      {
        id: 11,
        name: 'Encounter',
        display: 'Encounter',
        desc: 'Interaction with healthcare providers for health services and assessments'
      },
      {
        id: 12,
        name: 'CarePlan',
        display: 'Care Plan',
        desc: 'Intended plan for delivery of care through completion of specified activities'
      }
  ]

  this.allResourceFlagFormObj = {
    Patient : false,
    AllergyIntolerance : false,
    Observation : false,
    Immunization:false,
    MedicationStatement:false,
    Condition:false,
    FamilyMemberHistory : false,
    Procedure : false,
    MedicationOrder : false,
    Goal : false,
    Encounter : false,  
    CarePlan : false, 
  }
    

 
    if(this.router.url == '/patient/manual-medical-records'){
      this.addFlag = true;
    }else{
      this.addFlag = false;
    }
    this.checkPatientIfExist('Manual');
    // this.getGCPHealthcareResources(this.allergyResourceType);
  };
  checkPatientIfExist(type){
    this.commanService.checkAnyPatientIfExist(type)
    .subscribe((result: any) => {
      if(result.success){
        this.ifManualPatientExist = true;
      }else{
        this.ifManualPatientExist = false;
      }
    })
  }

  /**
   * Create New patient
   */

  createNewPatient() {
    this.newPatientForm.controls.active.setValue(this.patientStatus);
    this.newPatientForm.controls.resourceType.setValue(this.patientResourceType);
    let formValue = this.newPatientForm.value;
    this.commanService.createNewResource(formValue).subscribe((data) => {
      this.newPatientForm.reset();
      this.allResourceFlagFormObj['Patient']=false;
      this.checkPatientIfExist('Manual');
      this._snackBar.open('Patient Added Successfully', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['green-snackbar'],
      });
    },(res: any) => {
     // this.spinner.hide()
    })
  }


  /**
   * Create Allergy Resource
   */

  addAllergyRecord() {
    this.allergyForm.controls.resourceType.setValue(this.allergyResourceType);
    let formValue = this.allergyForm.value;
    this.commanService.createNewResource(formValue).subscribe((data :any) => {
       if(data.success){
        this.allergyForm.reset();
        this._snackBar.open('Allergy Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('Allergy Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }
    },(res: any) => {
      this.spinner.hide()
    })
  }

  /**
   * Create Observation Resource
   */

  addObservationRecord() {
    this.observationForm.controls.resourceType.setValue(this.observationResourceType);
    let formValue = this.observationForm.value;
    this.commanService.createNewResource(formValue).subscribe((data: any) => {
      if(data.success){
        this.observationForm.reset();
        this._snackBar.open('Observation Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('Observation Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }

    },(res: any) => {
      this.spinner.hide()
    })
  }

  /**
   * Create Immunization Resource
   */

  addImmunizationRecord() {
    this.immunizationsForm.controls.resourceType.setValue(this.immunizationsResourceType);
    let formValue = this.immunizationsForm.value;
    this.commanService.createNewResource(formValue).subscribe((data:any) => {
      if(data.success){
        this.immunizationsForm.reset();
        this._snackBar.open('Immunization Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('Immunization Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }
    },(res: any) => {
      this.spinner.hide()
    })
  }


  /**
   * Create Medication Resource
   */
  addMedicationRecord() {
    this.medicationForm.controls.resourceType.setValue(this.medicationResourceType);
    let formValue = this.medicationForm.value;
    this.commanService.createNewResource(formValue).subscribe((data:any) => {
      if(data.success){
        this.medicationForm.reset();
        this._snackBar.open('Medication Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('Medication Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }

    },(res: any) => {
      this.spinner.hide()
    })
  }

  /**
   * Create Condition Resource
   */

  addConditionRecord() {
    this.conditionForm.controls.resourceType.setValue(this.condtionResourceType);
    let formValue = this.conditionForm.value;
    this.spinner.show();
    this.commanService.createNewResource(formValue)
    .subscribe((result:any) => {
      this.spinner.hide()
      if(result.success){
        this.conditionForm.reset();
        this._snackBar.open('Condition Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
      }else{
        this._snackBar.open('Condition Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
      }
    },(res: any) => {
      this.spinner.hide()
    })
  }




  addfamilyMemberHistoryRecord() {
    this.familyMemberHistoryForm.controls.resourceType.setValue(this.familyMemberHistoryResourceType);
    let formValue = this.familyMemberHistoryForm.value;
    this.commanService.createNewResource(formValue).subscribe((data:any) => {

      if(data.success){
        this.familyMemberHistoryForm.reset();
        this._snackBar.open('FamilyMemberHistory Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('FamilyMemberHistory Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }


    },(res: any) => {
     // this.spinner.hide()
    })
  }


  addProcedureRecord() {
    this.ProcedureForm.controls.resourceType.setValue(this.procedureResourceType);
    let formValue = this.ProcedureForm.value;
    this.commanService.createNewResource(formValue).subscribe((data:any) => {
      if(data.success){
        this.ProcedureForm.reset();
        this._snackBar.open('Procedure Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('Procedure Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }


    },(res: any) => {
    //  this.spinner.hide()
    })
  }


  
  addMedicationOrderRecord() {
    this.medicationOrderForm.controls.resourceType.setValue(this.medicationOrdeResourceType);
    let formValue = this.medicationOrderForm.value;
    this.commanService.createNewResource(formValue).subscribe((data:any) => {

      if(data.success){
        this.medicationOrderForm.reset();
        this._snackBar.open('MedicationOrder Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('MedicationOrder Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }
      
    },(res: any) => {
    //  this.spinner.hide()
    })
  }


  addGoalRecord() {
    this.goalForm.controls.resourceType.setValue(this.goalResourceType);
    let formValue = this.goalForm.value;
    this.commanService.createNewResource(formValue).subscribe((data:any) => {
     
      if(data.success){
        this.goalForm.reset();
        this._snackBar.open('Goal Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('Goal Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }



    },(res: any) => {
     // this.spinner.hide()
    })
  }
  
  
  addEncounterRecord() {
    this.encounterForm.controls.resourceType.setValue(this.encounterResourceType);
    let formValue = this.encounterForm.value;
    this.commanService.createNewResource(formValue).subscribe((data:any) => {
      
      if(data.success){
        this.encounterForm.reset();
        this._snackBar.open('Encouter Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('Encouter Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }


      
    },(res: any) => {
     // this.spinner.hide()
    })
  }

  addCarePlanRecord() {
    this.carePlanForm.controls.resourceType.setValue(this.carePlanResourceType);
    let formValue = this.carePlanForm.value;
    this.commanService.createNewResource(formValue).subscribe((data:any) => {
      if(data.success){
        this.carePlanForm.reset();
        this._snackBar.open('CarePlan Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
       }else{
        this._snackBar.open('CarePlan Not Added Successfully', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
       }


    },(res: any) => {
      //this.spinner.hide()
    })
  }

  getResourceNameAndShowForm(resource){
    var resourceName = resource.name;
    if(this.ifManualPatientExist){
     if(resourceName && resourceName != ''){
      this.allResourceNameObj.forEach(element => {
        if(element.name == resourceName){
          this.allResourceFlagFormObj[element.name]=true;
          }else
         this.allResourceFlagFormObj[element.name]=false;
       });
     }else{
      this._snackBar.open('Please select resource name', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });   
     }
    }else{
      this.allResourceFlagFormObj['Patient']=true;
      this._snackBar.open('Please add  patient record data first', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });   
    }
    
  }





  openDialogMobileView(resource): void {

    const bottomSheetRef =this._bottomSheet.open(ManualMedicalRecordsMobile,{
      disableClose: true,
      panelClass: 'custom-width-mr-bottomSheet',
      data : {
        resourceObj: resource, 
        ifManualPatientExist: this.ifManualPatientExist
      }
    });
  
  
    bottomSheetRef.afterDismissed().subscribe((result:any) => {
     // this.GCPHealthcareResourceByType(result);
    });
  
  }

  
  /**
   * Get Resource Data
  */

  // getGCPHealthcareResources(resourType) {
  //   for (let i = 0; i < 20; i++) {
  //     this.progessValue = i;
  //   }
    
  //   this.commanService.getManualGCPHealthResource(resourType,'Manual')
  //     .subscribe((result: any) => {
  //       for (let i = 0; i < 90; i++) {
  //         this.progessValue = i;
  //       }
  //       this.progessValue = 100;
  //       if (resourType == 'Patient') {
  //         let recordData = result.data;
  //         this.patientName = recordData.name[0].family;
  //         this.patientGender = recordData.gender;
  //         this.patientBirthDate = recordData.birthDate;
  //         this.patientMartialStatus = recordData.maritalStatus.coding[0].code;
  //         this.patientAddress = recordData.address[0].text;

  //       }

  //       if (resourType == 'Observation') {
  //         let recordData = result.data;
  //         console.log(recordData)
  //         // this.obsevationCode = recordData.code.coding[0].code;
  //         this.observationCategory = recordData.category.coding[0].code;
  //         this.observationStatus = recordData.fieldRecords[1].value;
  //         this.observationDateTime = recordData.fieldRecords[2].value;
  //         this.observationQuantity = recordData.fieldRecords[0].value;
  //       }

  //       if (resourType == 'AllergyIntolerance') {
  //         console.log(result.data);
  //         let recordData = result.data;
  //         this.allergySubstance = recordData.substance.text;
  //         this.allergyCategory = recordData.category;
  //         this.allergyStatus = recordData.status;
  //         this.allergyCriticality = recordData.criticality;
  //         this.allergyResourceType = recordData.resourceType;
  //       }

  //       if (resourType == 'Immunization') {
  //         let recordData = result.data;
  //         this.immunizationVaccineCode = recordData.vaccineCode.id;
  //         this.immunizationStatus = recordData.status;
  //         this.immunizationWasNotGiven = JSON.stringify(recordData.wasNotGiven);
  //         this.immunizationDate = recordData.expirationDate;
  //         this.immunizationReported = JSON.stringify(recordData.reported);
  //       }

  //       if (resourType == 'MedicationStatement') {
  //         let recordData = result.data;
  //         this.medicationsMedication = recordData.medicationCodeableConcept.coding[0].code;
  //         this.medicationsDosage = recordData.medicationCodeableConcept.text;
  //         this.medicationsStatus = recordData.status;
  //         this.medicationsDateAsserted = recordData.dateAsserted;
  //         this.medicationsReasonForUse = recordData.reasonForUseCodeableConcept.coding[0].display;
  //         this.medicationsDosageTiming = recordData.dosage[0].timing.code.coding[0].display;
  //         this.medicationsDosageTimingMeridian = recordData.dosage[0].timing.code.coding[0].code;
  //         this.medicationsDaosageNeeded = JSON.stringify(recordData.dosage[0].asNeededBoolean);
  //         this.medicationsDosageMethod = recordData.dosage[0].method.text;
  //         this.medicationsDosageQuantity = recordData.dosage[0].quantityQuantity.value;
  //         this.medicationsDosageNote = recordData.note;
  //         this.medicationsDosageUnit = recordData.dosage[0].quantityQuantity.unit;
  //       }

  //       if (resourType == 'Condition') {
  //         let recordData = result.data;
  //         this.condtionCode = recordData.code.coding[0].code;
  //         this.condtionCategory = recordData.category.coding[0].code;
  //         this.condtionStatus = recordData.clinicalStatus;
  //         this.condtionVerificationStatus = recordData.verificationStatus;
  //         this.condtionStageSummary = recordData.stage.summary.coding[0].display;
  //       }

  //     }, (error: any) => {
  //       console.log(error);
  //     });

  // }



}
