import { state, style, trigger } from '@angular/animations'
import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core'
import { FormBuilder, FormControl, FormGroup } from '@angular/forms'
import { MatDialog, MatDialogConfig } from '@angular/material/dialog'
import { MatSnackBar } from '@angular/material/snack-bar'
import { MatTableDataSource } from '@angular/material/table'
import { Router } from '@angular/router'
import { NotifierService } from 'angular-notifier'
import { BsModalService } from 'ngx-bootstrap/modal'
import { CommmanService } from '../../_services/commman.service'
import { globalProviderSelect } from '../../_services/globalProviderSelect.service'
import { EditProviderDialogComponent } from '../edit-provider-dialog/edit-provider-dialog.component';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {ProviderListMobileComponent } from '../provider-list-mobile/provider-list-mobile.component';
import {ProviderListDocumentUploadMobile } from '../provider-list-document-upload-mobile/provider-list-document-upload-mobile.component';
import { Plugins,Capacitor, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';

const { Filesystem } = Plugins;

@Component({
  selector: 'app-provider-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
    ]),
  ],
})


export class ProviderListComponent implements OnInit {
  users: any = [];
  throughProviderFlag: boolean = false;
  displayedColumns: string[] = [
    'npino',
    'name',
    'state_name',
    'postal_code',
    'addressone',
    // 'addresstwo',
  //  'city',
    'taxonomy',
    'action',
    'details',
  ];
  mobileDisplayedColumns: string[] = [
    'description',
    'action',
  ]
  buttonTypeName='edit';
  titleName="Edit";
  UploadText = '';
  filesToUpload: Array<File> = [];
  action = null
  addFilesForm: FormGroup
  buttonUserText = 'Add Provider'
  expandedElement: any
  providerList: [];
  dataSource: MatTableDataSource<any>;
  noRecordFond = 'Loading...';
  upId: any;
  filleList: any = [];
  filleList1: any = [];
  panelOpenState = false;
  deleteBtn = "Remove";
  archiveBtn = "Archive";
  orgData: any
  orgDataRecord: any
  orgDataRecordTaxo: any;
  particularProviderRecord: object;
  editRowId:any;
  downloadingWait:boolean=false;
  @ViewChild('myInput') myInputVariable: ElementRef;
  filterValue: string;
  addDataFileUploadButtonFlg:boolean=true;

  selectedPatientId: any;
  multipleFilesUpload=[];

  editFileNameFlag:boolean=true;
  dissmissBottomSeetPopup:boolean=false;
  
  bottomSheetRef:any;

  constructor (private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private modalService: BsModalService,
    private commanService: CommmanService,
    public notifierService: NotifierService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private _bottomSheet: MatBottomSheet,
    private _globalProviderSelect: globalProviderSelect) {
    this.addFilesForm = new FormGroup({
      fileName: new FormControl('')
    });
  }



  ngOnInit(): void {

    if(this.router.url == '/provider/provider-list'){
       // this.getAllPatient('Fitbit');
       this.getAllPatient('Manual_upload');
      this.throughProviderFlag = true;
      this.displayedColumns = [
        'npino',
        'name',
        'state_name',
        'postal_code',
        'addressone',
      //  'city',
        'taxonomy',
        'details',
      ];
      this._globalProviderSelect.getId().subscribe(n => {
        this.selectedPatientId = n;
       
        this.selectedUser(this.selectedPatientId);
      });
      //this.selectedUser(this._globalProviderSelect.getId());
    }
    if(this.router.url == '/patient/provider-list'){
      this.throughProviderFlag = false;
      this.patientProviderList();
    }
  }

  getAllPatient(type){
    this.commanService.getAllPatient(type)
    .subscribe((result: any) => {
      if(result.success){
        this.noRecordFond = 'No patient user selected.';
        this.users = result.users;
      }
    })
  }

  selectedUser(user_id){
    if(user_id == undefined){
      this.dataSource = new MatTableDataSource([]);
      this.noRecordFond = 'No patient user selected.';
    }else{
      this.commanService.getPatientProviderListByUser(user_id).subscribe(
        (res: any) => {
          this.providerData(res);
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }

  checkButton() {
    console.log('check-button')
  }

  patientProviderList() {
    this.commanService.getPatientProviderList().subscribe(
      (res: any) => {
        this.providerData(res);
      },
      (err) => {
        console.log(err);
      }
    );
  }
  providerData(res){
    if (res.success && res.data.length > 0) {
      for (let i = 0; i < res.data.length; i++) {
        res.data[i].zipcode = res.data[i].zipcode.substring(0, 5);
      }

      console.log('*******************providerData***************************')
      console.log(res.data)
      console.log('*****************providerData*****************************')
      this.dataSource = new MatTableDataSource(res.data);
    } else {
      this.dataSource = new MatTableDataSource([]);
      this.noRecordFond = 'No added providers to manually upload data';
    }
  }
  getParticularProviderDetails(id) {
    this.commanService.getProvderFromList(id).subscribe((res: any) => {
      this.particularProviderRecord = res.data[0];

      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = this.particularProviderRecord;
      dialogConfig.width = "40%";
      dialogConfig.panelClass = "mob-dialog";
      const dialogRef = this.dialog.open(EditProviderDialogComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (result.status) {
          console.log('filter ' + this.filterValue)
          if (this.filterValue === undefined) {
            this.patientProviderList();
          } else if (this.filterValue !== undefined) {

            this.patientProviderList();
          } else {
            this.patientProviderList();
          }
        }
      });

    })
  }

  patientProviderUploadList() {
    this.commanService.getPatientProviderUploadList(this.upId).subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
         // this.filleList = res.data;
         var fileListData= [];
           fileListData =  res.data;
           var subStringData;
           var ext;
             for(var i=0; i< fileListData.length; i++ ) {
                 subStringData = fileListData[i].file_displayname; 
                      if(fileListData[i].file_displayname.length >25){
                         subStringData = fileListData[i].file_displayname.substr(0,25);
                         ext= fileListData[i].file_displayname.substr(fileListData[i].file_displayname.lastIndexOf(".")+1);
                        subStringData = subStringData + '...'+ ext;
                        fileListData[i].file_displaynameMob = subStringData;
                      }else{
                        fileListData[i].file_displaynameMob = fileListData[i].file_displayname;;
                      }
                         
                 };
                 this.filleList = fileListData;

                  console.log("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                  console.log(this.filleList);

        } else {
          this.filleList = [];
          this.noRecordFond = 'No added providers to manually upload data';
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getproviderId(Id) {
    this.upId = Id;
    this.patientProviderUploadList();
  }

  fileUploadEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }

  cancelUpload() {
    this.filesToUpload = [];
    this.addDataFileUploadButtonFlg = true;
    console.log(this.addDataFileUploadButtonFlg);
  }

  upload() {
    const formData: any = new FormData();
    const files: Array<File> = this.filesToUpload;
    if (files.length <= 0) {
      this._snackBar.open('Please first choose files', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
    } else {
      this.UploadText = "Uploading files...";
      formData.append('upId', this.upId);
      for (let i = 0; i < files.length; i++) {
        formData.append("uploads[]", files[i], files[i]['name']);
      }

      this.commanService.uploadFilesPatient(formData)
        .subscribe((result: any) => {
          this.UploadText = "";
          if (result.success) {
            this.filesToUpload = [];
            this.myInputVariable.nativeElement.value = '';
            this.addFilesForm.reset();
            this._snackBar.open(result.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['green-snackbar'],
            });
            this.patientProviderUploadList();
          } else {
            this._snackBar.open(result.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['red-snackbar'],
            });
          }
        }, err => {
          console.log(err.message);
        })
    }
  }


  ParticularDelete(upiId) {
    if (confirm("Do you want to permanently delete this provider?")) {
      //  this.deleteBtn = "Wait...";
      this.commanService.deletePatientProvider(upiId).subscribe((res: any) => {
        if (res.success) {
          this.patientProviderList();
          this.deleteBtn = "Remove";
          this._snackBar.open(res.message, this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        }

      },
        err => {
          console.log(err.message);
          this.deleteBtn = "Remove";
        })
    }
  }

  deletePatientProviderFile(upiId) {
    if (confirm("Do you want to permanently delete this file ?")) {
      //  this.deleteBtn = "Wait...";
      this.commanService.deletePatientProviderFile(upiId).subscribe((res: any) => {
        this.patientProviderUploadList();
        this.deleteBtn = "Remove";
      },
        err => {
          console.log(err.message);
          this.deleteBtn = "Remove";
        })
    }
  }


  archivePatientProviderFile(upiId) {
    if (confirm("Do you want to permanently archive this file ?")) {
      this.archiveBtn = "Wait...";
      this.commanService.archivePatientProviderFile(upiId).subscribe((res: any) => {
        this.patientProviderUploadList();
        this.archiveBtn = "Archive";
      },
        err => {
          console.log(err.message);
          this.archiveBtn = "Archive";
        })
    }
  }

  applyFilter(event: Event) {
    this.filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = this.filterValue.trim().toLowerCase();
  }

  // applyFilter(data) {
  //   if (data == '' || data == undefined) {
  //     this.patientProviderList();
  //   } else {

  //     this.commanService.filterProviderList(data).subscribe(
  //       (res: any) => {
  //         if (res.success && res.data.length > 0) {
  //           console.log(res.data);
  //           this.dataSource = new MatTableDataSource(res.data);
  //         } else {
  //           this.dataSource = new MatTableDataSource([]);
  //         }
  //       },
  //       (err) => {
  //         console.log(err);
  //       }
  //     );
  //   }
  // }

  downloadDocument(upiId,fileName) {
    this.downloadingWait = true;
      this.commanService.downloadDocument(upiId,this.selectedPatientId).subscribe((res: any) => {
        this.downloadingWait = false;
      const blob = new Blob([res]);
        let url = URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
      },
        err => {
          console.log(err.message);
        })
    
  }


  downloadDocumentInAndroid(upiId,fileName) {
    this.downloadingWait = true;
      this.commanService.downloadDocument(upiId,this.selectedPatientId).subscribe(async(res: any) => {
        this.downloadingWait = false;
      const blob = new Blob([res]);
        var base64data;
       var reader = new FileReader();
       reader.readAsDataURL(blob); 
       reader.onloadend = async function() {
            base64data = reader.result;   
           Filesystem.writeFile({
            path: fileName,
            data: base64data,
            directory: FilesystemDirectory.Documents,
           // encoding: FilesystemEncoding.UTF8
          });
          
       }
       this._snackBar.open('File has downloaded', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['green-snackbar'],
      });

      },
        err => {
          this._snackBar.open('File has not downloaded', this.action, {
            duration: 3000,
            verticalPosition: 'top', // 'top' | 'bottom'
            panelClass: ['red-snackbar'],
          });
        })
    
  }







  editFileName(id) {
    this.buttonTypeName='save';
    this.titleName='Save';
    this.editRowId = id;
    this.editFileNameFlag=false;
  }
 
  updateFileNameRow(upiid,value){
    this.buttonTypeName='edit';
    this.titleName='Edit';
    this.editRowId=null;
    if(this.dissmissBottomSeetPopup)
    this.bottomSheetRef.dismiss();
    this.commanService.updateDocumentFileName({name:value,upiid:upiid}).subscribe((res: any) => {
      this.editFileNameFlag=true;
      if(res.success){
        this.patientProviderUploadList();
        this._snackBar.open(res.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['green-snackbar'],
        });
      }else{
        this._snackBar.open(res.message, this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
    },
      err => {
        console.log(err.message);
      })
  }

  addDataFlagButton(){
    this.addDataFileUploadButtonFlg=false;
  }


  openBottomSheet(objData): void {
    if(this.editFileNameFlag){
    this.bottomSheetRef =this._bottomSheet.open(ProviderListMobileComponent,{
      data:{objData:objData,throughProviderFlag:this.throughProviderFlag}
    });

    this.bottomSheetRef.afterDismissed().subscribe((result:any) => {
          if(result.type=='delete')
            this.archivePatientProviderFile(result.upiid);
            if(result.type=='rename') {
              this.dissmissBottomSeetPopup=true;
              this.editFileName(result.upiid);
              }
           
            if(result.type=='download')
            this.downloadDocumentInAndroid(result.upiid,result.fileName);
    });
  }
  
  }


  DataURIToBlob(dataURI: string) {
    const splitDataURI = dataURI.split(',')
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

    const ia = new Uint8Array(byteString.length)
    for (let i = 0; i < byteString.length; i++)
        ia[i] = byteString.charCodeAt(i)

    return new Blob([ia], { type: mimeString })
  }


  openBottomSheetUploadDocument(objData): void {
    this.addDataFileUploadButtonFlg=false;
    const bottomSheetRef =this._bottomSheet.open(ProviderListDocumentUploadMobile,{
     panelClass: 'custom-width-mr-bottomSheet-manual-upload',
     data: { dataValue:objData }
    });




    bottomSheetRef.afterDismissed().subscribe((result:any) => {
      this.multipleFilesUpload = result.uploadFileObj;  
    });
  
  }



  async uploadDocumentByMobile() {
    var arrayObj = this.multipleFilesUpload;
    const formData: any = new FormData();

       if(!this.upId){
        this._snackBar.open('Please first select provider', this.action, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
        this.multipleFilesUpload = [];
        
        return false;
       }

    if (arrayObj.length <= 0) {
      this._snackBar.open('Please first choose files', this.action, {
        duration: 3000,
        verticalPosition: 'top', // 'top' | 'bottom'
        panelClass: ['red-snackbar'],
      });
      this.addDataFileUploadButtonFlg = true;
    } else {
      this.UploadText = "Uploading files...";
      formData.append('upId', this.upId);
      for (let i = 0; i < arrayObj.length; i++) {
        let fileUrl = await this.DataURIToBlob(arrayObj[i].uploadType);
        formData.append("uploads[]", fileUrl, arrayObj[i].fileName);
      }

      this.commanService.uploadFilesPatient(formData)
        .subscribe((result: any) => {
          this.UploadText = "";
          this.addDataFileUploadButtonFlg = true;
          if (result.success) {
            this.multipleFilesUpload = [];
            this._snackBar.open(result.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['green-snackbar'],
            });
            this.patientProviderUploadList();
          } else {
            this._snackBar.open(result.message, this.action, {
              duration: 3000,
              verticalPosition: 'top', // 'top' | 'bottom'
              panelClass: ['red-snackbar'],
            });
          }
        }, err => {
          console.log(err.message);
        })
    }
  }


  cancelUploadByMoble() {
    this.multipleFilesUpload = [];
    this.addDataFileUploadButtonFlg = true;
  }





}
