import { state, style, trigger } from '@angular/animations'
import { Component, OnInit } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { MatTableDataSource } from '@angular/material/table';
import { CommmanService } from 'src/app/_services/commman.service'
declare var $: any

@Component({
  selector: 'app-pdashboard',
  templateUrl: './pdashboard.component.html',
  styleUrls: ['./pdashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      // transition(
      //   'expanded <=> collapsed',
      //   animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      // ),
    ]),
  ],
})
export class PdashboardComponent implements OnInit {
  constructor(private commanService: CommmanService) {}
 
  displayedColumns: string[] = ['type', 'name', 'email',  'created', 'details']
  dataSource: MatTableDataSource<any>;
  panelOpenState = false
  expandedElement: any;
  public icon = 'add';
  emailFilter:'';
  noFitbitConfigurarionSet:boolean=false;
  dataFilterLoading:any;
   patientMedicalRecordsData=[];
   patientManualUploadRecordsData = [];
   patientProviderAccessControlsData = [];
   patientDigitalHeathConfigurationData = [];
   patientMedicalRecordMsg='Loading...';
   ManualUploadRecordsMsg='Loading...';
   patientProviderAccessControlsMsg='Loading...';
   patientDigitalHeathConfigurationMsg = 'Loading...';
   todayStep = 0;
   todayHours = 0;
   todayMinutes = 0;
   tabFlag = true;
  public toggleIcon() {
    if (this.icon === 'add') {
      this.icon = 'remove'
    } else {
      this.icon = 'add'
    }
  }

  ngOnInit(): void {
    this.sidebarCollapse();
    this.adminDashboardList();
    this.patientMedicalRecords();
    this.patientMenualUploads();
    this.patientProviderAccessControls();
    this.patientDigitalHeathConfiguration();
    this.checkUserConfigured();
  }
  sidebarCollapse() {
    $('#sidebar').toggleClass('active')
    $('#body').toggleClass('active')
  }

  adminDashboardList(){
    this.dataFilterLoading='Loading...';
    this.commanService.adminDashboardList().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
          this.dataSource = new MatTableDataSource(res.data);
        } else {
          this.dataSource = new MatTableDataSource([]);
          this.dataFilterLoading='Record is not available.';
        }
      },
      (err) => {
        this.dataFilterLoading='Record is not available.';
      }
    );
  }



  patientMedicalRecords(){
    this.patientMedicalRecordMsg='Loading...';
    this.commanService.getPatientMedicalRecords().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
            this.patientMedicalRecordsData = res.data;
        } else {
          this.patientMedicalRecordMsg='You have not connected to any patient portals.';
          this.patientMedicalRecordsData = [];
        }
      },
      (err) => {
        this.patientMedicalRecordsData = [];
        this.patientMedicalRecordMsg='This data is currently unavailable.';
      }
    );
  }


  patientMenualUploads(){
    this.ManualUploadRecordsMsg='Loading...';
    this.commanService.getPatientMenualCountUploads().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
            this.patientManualUploadRecordsData = res.data;
        } else {
          this.ManualUploadRecordsMsg='You have not added any providers to upload files under.';
          this.patientManualUploadRecordsData = [];
        }
      },
      (err) => {
        this.patientManualUploadRecordsData = [];
        this.ManualUploadRecordsMsg='This data is currently unavailable.';
      }
    );
  }

  patientProviderAccessControls(){
    this.patientProviderAccessControlsMsg='Loading...';
    this.commanService.getpatientProviderAccessControls().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
            // format data into list of objects with access + MM/DD/YYYY pairs
            var raw_data = [];
            res.data.forEach((data, i) => {
              var access_data = data.string_agg.split(',');
              var dates = data.created_on.split(',');
              var resAccess = [];
              access_data.forEach((acc) => {
                resAccess.push({
                  access: acc,
                  created_on: new Date(dates[i]).toLocaleDateString('en-US')
                });
              })
              raw_data.push({
                fullname: data.fullname,
                accesses: resAccess
              });              
            })
            this.patientProviderAccessControlsData = raw_data;
        } else {
          this.patientProviderAccessControlsMsg='You have not shared your health data with any providers.';
          this.patientProviderAccessControlsData = [];
        }
      },
      (err) => {
        this.patientProviderAccessControlsData = [];
        this.patientProviderAccessControlsMsg='This data is currently unavailable.';
      }
    );
  }



  



  patientDigitalHeathConfiguration(){
    this.patientDigitalHeathConfigurationMsg='Loading...';
    this.commanService.getPatientDigitalHeathConfiguration().subscribe(
      (res: any) => {
        if (res.success && res.data.length > 0) {
            this.patientDigitalHeathConfigurationData = res.data;
        } else {
          this.patientDigitalHeathConfigurationMsg='You have not connected any digital health products.';
          this.patientDigitalHeathConfigurationData = [];
        }
      },
      (err) => {
        this.patientDigitalHeathConfigurationData = [];
        this.patientDigitalHeathConfigurationMsg='This data is currently unavailable.';
      }
    );
  }


  checkUserConfigured(){
    this.commanService.checkFitbitConfig()
    .subscribe(
      (resmsg: any) => {
        if(resmsg.success){
          this.noFitbitConfigurarionSet = true;
          localStorage.setItem('fitbit_refresh_token', resmsg.data);
          this.getTodayActivities(resmsg.data, this.tabFlag);
          this.getlastTwoWeekSleep(resmsg.data, this.tabFlag);
        }else{
          this.noFitbitConfigurarionSet = false;
        }
      }, (err) => {
        console.log(err)
      })
  }

  
  getTodayActivities(access_token, tabFlag) {
    this.commanService.getTodayActivities(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {

          this.todayStep = Math.round(resmsg.activityObj.steps);


        } else {
          this.todayStep =0;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }



  getlastTwoWeekSleep(access_token, tabFlag) {
    this.commanService.getlastTwoWeekSleep(access_token, tabFlag).subscribe(
      (resmsg: any) => {
        if (resmsg.success == true) {
          this.todayHours = resmsg.sleepObj.todayHours;
          this.todayMinutes = resmsg.sleepObj.todayMinutes;

        } else {
          this.todayHours = 0;
          this.todayMinutes = 0;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  applyFilter() {
    if (this.emailFilter == '' || this.emailFilter == undefined) {
     this.adminDashboardList();
    } else {

      this.dataFilterLoading='Loading...';
      this.commanService.adminUserGetInfoByEmail(this.emailFilter).subscribe(
        (res: any) => {
          if (res.success && res.data.length > 0) {
            this.dataSource = new MatTableDataSource(res.data);
          } else {
            this.dataSource = new MatTableDataSource([]);
            this.dataFilterLoading='Record is not available.';
          }
        },
        (err) => {
          console.log(err);
          this.dataFilterLoading='Record is not available.';
        }
      );
    }
  }

}

