import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Enrollment1Component } from './enrollment1.component';

describe('Enrollment1Component', () => {
  let component: Enrollment1Component;
  let fixture: ComponentFixture<Enrollment1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Enrollment1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Enrollment1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
