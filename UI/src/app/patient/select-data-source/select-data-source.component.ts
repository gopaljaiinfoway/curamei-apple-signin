import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-select-data-source',
  templateUrl: './select-data-source.component.html',
  styleUrls: ['./select-data-source.component.css']
})
export class SelectDataSourceComponent implements OnInit {
  patientPortal: boolean;
  uploadFile: boolean;

  constructor (private router: Router) { }

  ngOnInit(): void {
  }

  selectPatientPortal() {
    var eleOne = document.getElementById('patientPortal')
    var eleTwo = document.getElementById('fileUpload')
    eleOne.classList.toggle('active-box')
    eleTwo.classList.remove('active-box')
    this.patientPortal = true
    this.uploadFile = false
    console.log(this.patientPortal)
  }

  selectFileUpload() {
    var eleTwo = document.getElementById('fileUpload')
    var eleOne = document.getElementById('patientPortal')
    eleTwo.classList.toggle('active-box')
    eleOne.classList.remove('active-box')
    this.uploadFile = true
    this.patientPortal = false
    console.log('UF' + this.uploadFile)
  }

  clickContinue() {

    if (this.patientPortal) {

      this.router.navigateByUrl('/patient/provider-connect');

    } else if (this.uploadFile) {
      // this.openRequestHealthPortal = true;
      this.router.navigateByUrl('/patient/request-health-system')

    } else {
      this.uploadFile = false
      this.patientPortal = false
      'do noting'
    }
  }

  backToAddSource() {
    this.router.navigateByUrl('/patient/datasource')
  }

}
