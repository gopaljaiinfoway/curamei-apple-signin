import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualMedicalRecordsMobile } from './manual-medical-records-mobile.component';

describe('SharePatientResourceMobile', () => {
  let component: ManualMedicalRecordsMobile;
  let fixture: ComponentFixture<ManualMedicalRecordsMobile>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualMedicalRecordsMobile ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualMedicalRecordsMobile);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
