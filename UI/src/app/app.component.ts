import { Component } from '@angular/core';
import { ConnectionService } from 'ng-connection-service';
import { SocketioService } from './socketIO.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, NavigationEnd } from '@angular/router'; 
import { UserLoginedService} from './_services/user-logined.service';
declare var $: any;


import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';


//declare var ga: Function;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  title = 'UI';
  isConnected=true;
  status:string;
  constructor(public connectionService: ConnectionService, private socketService: SocketioService, private _snackBar: MatSnackBar,public router: Router,
    private permissions: AndroidPermissions) {
    status='Checking.......';
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this._snackBar.open('Internet access achieved', null, {
          duration: 3000,
          verticalPosition: 'top', // 'top' | 'bottom'
          panelClass: ['red-snackbar'],
        });
      }
      else {
       this.status='No internet access';
      }
    })


    // this.router.events.subscribe(event => {

    //   if (event instanceof NavigationEnd) {
    //     ga('set', 'page', event.urlAfterRedirects);
    //     ga('send', 'pageview');
    //   }
    // });
    this.checkPermissionAllAndroid();
   

  }
  
  ngOnInit() {
    this.socketService.setupSocketConnection();

    

  }


  checkPermissionAllAndroid(){


    this.permissions.checkPermission(this.permissions.PERMISSION.INTERNET).
    then((result) => {
       if(!result.hasPermission){
         this.permissions.requestPermission(this.permissions.PERMISSION.INTERNET);
       }
    }, (err) => {
     this.permissions.requestPermission(this.permissions.PERMISSION.INTERNET);
    });


     this.permissions.checkPermission(this.permissions.PERMISSION.CAMERA).
     then((result) => {
        if(!result.hasPermission){
          this.permissions.requestPermission(this.permissions.PERMISSION.CAMERA);
        }
     }, (err) => {
      this.permissions.requestPermission(this.permissions.PERMISSION.CAMERA);
     });


     this.permissions.checkPermission(this.permissions.PERMISSION.READ_EXTERNAL_STORAGE).
     then((result) => {
        if(!result.hasPermission){
          this.permissions.requestPermission(this.permissions.PERMISSION.READ_EXTERNAL_STORAGE);
        }
     }, (err) => {
      this.permissions.requestPermission(this.permissions.PERMISSION.READ_EXTERNAL_STORAGE);
     })




     this.permissions.checkPermission(this.permissions.PERMISSION.WRITE_EXTERNAL_STORAGE).
     then((result) => {
        if(!result.hasPermission){
          this.permissions.requestPermission(this.permissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
        }
     }, (err) => {
      this.permissions.requestPermission(this.permissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
     })




  }


  
  
}

