import { environment } from 'src/environments/environment';
import * as io from 'socket.io-client';
import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
 
@Injectable()
export class SocketioService {
    observable: Observable<string>;
    socket;
    flag = false;
    constructor() {  
      this.socket = io(environment.SOCKET_ENDPOINT, {
        query: {
          token: 'cde-curamei'
        }
      });
     }
    setupSocketConnection() {
      this.socket = io(environment.SOCKET_ENDPOINT, {
        query: {
          token: 'cde-curamei'
        }
      });
    }
    getData(): Observable<string> {
      return this.observable = new Observable((observer) => 
        this.socket.on('notification', (data) => observer.next(data))
      );
    }
    getDropdownMenuUserList(): Observable<string> {
      return this.observable = new Observable((observer) => 
        this.socket.on('globalProviderMenu', (data) => observer.next(data))
      );
    }
    getlatestProfilePic(): Observable<string> {
      return this.observable = new Observable((observer) => 
        this.socket.on('profilePicture', (data) => observer.next(data))
      );
    }
  }